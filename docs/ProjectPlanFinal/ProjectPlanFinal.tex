\documentclass[11pt,letterpaper]{article}
\usepackage[margin=1in]{geometry}
\usepackage{setspace}
\usepackage{fancyhdr}
\usepackage{mathtools}
\usepackage{amsthm,amssymb}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{cite}
\usepackage{wrapfig}
\usepackage{float}
\usepackage{subfig}
\usepackage{sidecap}
\usepackage{listings}
\usepackage{cancel}
\usepackage[usenames,dvipsnames,svgnames,table]{xcolor}
\usepackage{fancyhdr}

\floatstyle{plaintop}
\restylefloat{table}

\lstdefinestyle{customc}{
  belowcaptionskip=1\baselineskip,
  breaklines=true,
  frame=L,
  xleftmargin=\parindent,
  language=C,
  showstringspaces=false,
  basicstyle=\footnotesize\ttfamily,
  keywordstyle=\bfseries\color{green!60!black},
  commentstyle=\itshape\color{purple!60!black},
  identifierstyle=\color{blue},
  stringstyle=\color{red}
}

\lstset{
escapechar=@, style=customc
}

\pagestyle{fancy}
\fancyhf{}
\lhead{MicroCART Project Plan}
\rhead{Page \thepage}

\begin{document} %Refer and describe all figures in text
%\lstset{language=C}
\newcommand{\call}[1]{\textsf{\bf #1}}
\newcommand{\mytilde}{\raise.17ex\hbox{$\scriptstyle\mathtt{\sim}$}}

\begin{titlepage}
\vspace*{3in}
\begin{center}
{\Huge \bf MicroCART Project Plan} \\
{\LARGE Group May15-28} \\
\vspace{0.3in}
{\large Members: Paul Gerver, Tyler Kurtz, Joe Boldrey, \\ Adam Campbell, Matt Vitale, Ravi Nagaraju, Jacob Rigdon} \\
\vspace{0.3in}
{\large Client: Dr. Phillip Jones III} \\
\vspace{0.3in}
{\large Advisers: Dr. Phillip Jones III, Dr. Nicola Elia}
\end{center}
\vfill
\end{titlepage}

\tableofcontents

\listoffigures
\listoftables

\newpage

\section{Problem Statement}
The current Micro-Controller Aerial Research Team (MicroCART) platform in the Distributed Sensing and Decision Making Laboratory, while sufficient for demoing, does not meet the client's expectations and wants. The old system requires a revamp to a new platform that removes several bottlenecks including a restricting radio controller (RC) mixer from the physical quadcopter. The client also feels the current system does not have adequate data analysis tools to enable the system to become autonomous, so the team will be focused on getting the new system running with better analysis tools.

\section{Concept Sketch}
The end goal of the project is to create a new platform and removes the dependency of the IR cameras and RC mixer from the system. By removing these constraints, the quad can operate solely off its on-board sensor readings. Additionally, the new system will have extensive data analytics tools that compare the quad's sensors and the fine-grain position data of the IR cameras. The simplified diagram shown in Figure \ref{fig:concept} below provides an example of how the communication flows across the entire system when it is complete. 

\begin{figure}[H]
\begin{center}
	\includegraphics[width=0.6\textwidth]{concept.png}
	\caption{\small High-level concept diagram of the new system}
	\label{fig:concept}
\end{center}
\end{figure}


\section{Deliverables}
Based off the client's end goals, the team has formulated steps and checkpoint deliverables required to complete the base project. Specifically, milestones have been created for each semester to help guide the team to success. To accompany each task, the team will provide detailed documentation that summaries the motivation, description, and how the specified task fits into the overall project. Additionally, tasks that complete a function  need to  have a detailed procedure explaining what steps users need to take to accomplish its function. For tasks that provide results, descriptions of the outcomes and outputs will be produced, and an FAQ guide will be written to help users troubleshoot problems that may arise. 
\\\\
The team will relinquish all source code for the quad and base station, its data analytics, and documentation through its shared Google Drive and GitLab repository hosted by Iowa State.

\subsection{First Semester}
\begin{itemize}
\setlength\itemsep{0em}
\item Fully programmed Zybo quad board 
	\begin{itemize}
	\setlength\itemsep{0em}
	\item Reading gyro and accelerometer data from 3-axis sensor board 
	\item Communicate with board through Bluetooth
	\item Controlling the quad’s motors through RC receiver and Bluetooth
	\item Software-implemented RC Mixer 
	\item PID controllers for stable roll and pitch
	\end{itemize}
\item Mounted Zybo board and sensors on new chassis
\item Power and voltage regulator circuits for system's batteries
\item Implement a system to record high-speed camera data
\item Manually fly quad with stable PID controllers for roll, pitch, and yaw
\item Flyable demonstration of new system using old system’s demo program
\item Collected system data is parsed, analyzed, and visualized post-flight
\end{itemize}

\subsection{Second Semester}
\begin{itemize}
\setlength\itemsep{0em}
\item Working additional peripherals on quad
	\begin{itemize}
	\setlength\itemsep{0em}
	\item GPS
	\item Barometer
	\item Camera
	\end{itemize}
\item Fine-tuned PID controllers and better filtered sensor data
\item Demonstrate flight playbacks and full autonomous flights
\item Demonstrate quad flight removed from camera environment
\item Fly quadcopter outside of camera system
\item Visualize flight data in real-time
\end{itemize}


\section{Specifications}

\subsection{Hardware}
The new system will be using a DJI FlameWheel F450 with DJI ESCs and DJI brushless motors to provide thrust for the quad. For the quad to fly properly and safely, a programmable processing board will handle computations required to operate the quadcopter. Specifically, the team will be using a Diligent ZyBo board to program the interconnections between sensors and peripherals like a Bluetooth module or SparkFun MPU9150. An example configuration of a flying functional system will appear similar to the diagram seen in Figure \ref{fig:hardware}.

\begin{figure}[H]
\begin{center}
	\includegraphics[width=0.4\textwidth]{hardware.jpg}
	\caption{\small Zybo Board Hardware Block Diagram}
	\label{fig:hardware}
\end{center}
\end{figure}
\noindent The diagram shows the RC receiver sending commands to the board, but this will be done either through the receiver or through Bluetooth. However, the RC receiver will have the highest priority.

\subsection{Software}
The Zybo board will have a system level program in charge of collecting sensor data, sending data, receiving flight commands, and mixing flight values to operate the four motors. The program will be loaded on a microSD card so when the board is powered on, the program will automatically run once the reconfigurable FPGA has been programmed.
\\\\ 
For the base station, a main MicroCART program will need to be run in order to start a quad flight. To control the new system, a software graphical user interface (GUI) will be used in the program to communicate with the powered on quad through an RC trainer or Bluetooth module. The GUI will allow users to send commands in different ways like through a command line interface or 2D drawing and adjust settings as needed. 

\subsection{Controls}
The control mixer for the system mixes the incoming throttle, roll, pitch, and yaw PWMs from the RC receiver or Bluetooth values and translates the individual signal for a flight component to a signal that all four motors on the quad react to. This software is run by the Zybo board since it handles all communication for the quad. For example, if a users wants to move the quad horizontally to the right, the RC receiver will change the roll to rotate slightly clockwise which means the two motors on the left side of the quad need to increase power and the right ones need to decrease power. 

\subsection{Operating Environment}
It should be noted that the operating environment for the quad will be within the Distributed Sensing and Decision Making lab in Coover 3050 which does not contain natural weather conditions. In other words, nearly all testing and flight runs will be done in a controlled, ideal environment. 


\section{System Requirements}
\subsection{Functional Requirements}
\begin{itemize}
\setlength\itemsep{0em}
\item Zybo board collects several readings from main sensor peripheral
\item Zybo board communicates data through Bluetooth and Wi-Fi to base station
\item Zybo board can receive throttle, roll, pitch, and yaw values from different sources and generate PWM signals to each motor
\item Zybo board can boot hardware configuration and software from microSD card
\item Base station communicates RC data to quad
\item PID controllers regulate throttle, roll, pitch, and yaw values 
\item Base station receives IR camera data
\item Data analytic tools plot data after run
\item Zybo board is mounted on new quad chassis
\item Main sensor board is centered on quad chassis
\item Power management circuits regulates voltage, detect reverse polarity, and doesn't allow for over draining
\item Quad and Zybo board are powered by batteries at an appropriate voltage
\end{itemize}

\subsection{Non-Functional Requirements}
\begin{itemize}
\setlength\itemsep{0em}
\item Quad system has minimal data bottleneck
\item PID controller is optimal and makes quad flight easy
\item Easy board and sensor accessibility when mounted on chassis
\item Easily maintainable if new parts are required
\item Board and sensor protection and stability during flight runs
\item Easily expandable with additional peripherals and board configuration
\item Easily usable and demoable for operators and curious people 
\item Quad doesn't immediately fall and crash when battery is low
\item System is reliable and doesn't change parameters after every run
\item System can be deployed with proper program and RC controller
\end{itemize}

\subsection{Project Requirements}
The client has provided several requirements and criteria that the new system will need to have. Additionally, the client has set forward what tools and some items that the team is required to use. Although there is not many design choices, much of the implementation and additional circuits allow for design choices.
\subsubsection{Sensors and Peripherals}
The on-board sensors for the quad need to provide consistent accelerometer, magnetometer, and gyroscope readings for the base station to safely conduct the quads movements. Additionally, the sensor needs to be placed near the quad's center of gravity in order to obtain useful data. The board could be placed else where, but would not embody the core of the quad system.
\\\\
The sensor board can only be accessed through an I2C protocol, so the Zybo board's on-board I2C controller will be programmed to interface with the device. However, the board's data can be read in several ways: through a burst read method or through a hardware FIFO. The team has decided to collect data through the burst read method due to latency.
\\\\
The Bluetooth BT2PMOD module was selected by the client for communication based off its easy compatibility with PMOD connectors and availability.

\subsubsection{Physical Quadcopter}
The quad chassis needs to be sturdy and provide a platform for a computing board, power supplies, and sensors. The chassis needs to be able to protect the items it is carrying while not being too closely bound that can cause disruptive vibrations on the systems. Additionally, the mounted processing board and its peripherals should not clear the highest point of the quad in case the quad flips over. 
\\\\
There are several considerations for the placement of parts and by what materials. The team has decided to mount the Zybo and sensor board using plastic that is not too sturdy as to send undesired vibrations and not to large that add weight and make the board stick above the quad. Additionally, a clear plastic cover has been mounted above the Zybo to protect it from damage in case the quad flips upside down.

\subsubsection{ZyBo Board}
The main processing component on board the quad system, the Zybo board, needs to perform an ample amount of functions including operating each of the four quad motors, mixing the throttle, pitch, roll, and yaw signals received from the RC receiver or Bluetooth module, and sending sensor data back to the base station appropriately. 
\\\\
The Zybo board is a specific requirement of the client, so no design choices were made. Similar boards do exist, but the Zybo board is fully capable for the project.

\subsubsection{Data Analytics}
The new system requires data analytics that pull from multiple sources and compares the different measured points to gauge the accuracy of the on-board sensors. In other words, the OptiTrack camera system will be the basis for the quadcopter's location and will used as the control dataset since it provides precise measurements at a fast pace. The data analytic tools will need to be robust enough to effectively communicate to users what errors or outliers occur in readings received from the Zybo board. The tools should also be automated to let users not have to worry about locating the data file and running certain scripts to visualize the data. A data flow diagram for the data collection process can be seen in Figure \ref{fig:analytics}.
\begin{figure}[H]
\begin{center}
	\includegraphics[width=1.0\textwidth]{data_analytics.png}
	\caption{\small Data Collection Flow Diagram}
	\label{fig:analytics}
\end{center}
\end{figure}
\noindent MATLAB has been selected as the data analysis tool for processing the system data because it can handle large matrices of data and has some scripting features that are useful for parsing log files.

\subsubsection{PID Controller}
A PID, proportional-integral-derivative, controller is required for the quad to maintain altitude and provide quick, smooth, and accurate movement for the quad in a three-dimensional space. The controller will be implemented in software to provide the system with feedback to correct for present, past, and future error that the system will run into. Several PID controllers will exist for each channel of flight: throttle, roll, pitch, and yaw. Because the different amounts of error need to be tuned appropriately to obtain a good controller, a lot of the teams time will be spent on tuning the PID controller along with the quad's sensors to obtain an optimal controller. 
\\\\
Similar to the Zybo board, PID controllers are design choices selected by the client. If characteristics, can be acquired, then there are other methods that can replace the PID controllers of the system.


\subsubsection{Power Management}
For a functional system, several problems regarding power management need to be addressed. First, the system needs to accurately relay current battery level information back to the user so the system can be controlled appropriately based on its current status. Second, the system should independently handle cases when there is not enough power and should drop altitude at a safe rate rather than stop mid flight and crash. Third, the system needs to regulate voltage levels of the batteries as they can be permanently damaged if the drained too much. 
\\\\
To power the quad and combat the many issues that can arise from the battery supplies, two power management boards are being created: one to power the Zybo and one for monitoring the motor's battery. 



\subsubsection{IR Camera System}
While one of the client's goal is to move outside of the refined environment of the lab, the quadcopter still needs to operate with the high-speed IR cameras and use the data collected in conjunction with the data received from the quad's sensors. The differences between the two data sets must be analyzed and tested in order for the quad to be decoupled from the camera system.
\\\\
The client has specified that the camera system will not be needed for the final system, but it will heavily be utilized for testing and analysis of on-board sensors. 


\subsection{Proposed Solution Assessment}
The design choices have been made for this project, yet alternative solutions do exist in case of unforeseen obstacles. If an instance like that were to occur, the team would consult the client with the pros and cons of the current and alternative solutions. 

\subsubsection{Sensors and Peripherals}
The current SparkFun MPU9150 sensor board responsible for gyro and accelerometer data was selected by the client because it is relatively cheap and can be accessed using an on-board controller. However, because of a cheaper chip, the gyroscope is not as fine-grain as a more sensitive, and expensive, sensor and is prone to a higher drift over time than a nicer one. Regardless, the sensor works well with the accelerometer and the drift can be accounted for in software.
\\\\
As for the process of collecting data from the board, the team decided to read the data through a burst read method. This method is good because multiple read commands can be sent to the device rather than a write and read command that is required to read a single register--which is the case for the FIFO method. However, the FIFO method allows for all data to be flushed into the FIFO at a single timestamp whereas the burst read method may have non time-aligned data. Nevertheless, after some latency testing, the team found that the burst reads took \mytilde650us compared to the 1.4ms through FIFO.

\subsubsection{ZyBo Board}
The ZyBo board is similar to other Diligent boards, but is fits well with the new FlameWheel quad chassis. Additionally, instead of a micro-controller, the ZyBo board has an FPGA that allows for custom logic blocks to be used in a system allowing for portions of software code to be hardware accelerated or have certain tasks complete handled entirely in hardware that is not possible on a micro-controller. On the other hand, the Zybo is larger both in size and weight than many micro-controllers that could easily fly the quadcopter. In short, the Zybo has the best of both worlds and is very versatile allowing for greater capabilities, even though it is larger.
\subsubsection{Data Analytics}
There are many data analytic tools, but each analytic tool is often crafted for a single purpose. On the other hand, MATLAB is a great collection of functions that allow for scripting, signal processing, modeling with Simulink, and other things. With MATLAB, the team can create their own plotting and log parsing scripts easily. MATLAB is not without its shortcomings, however. The program is fairly large in size and takes awhile to start since it relies on components like Java. 

\subsubsection{PID Controller}
One alternative for PID controllers involves a characterized model, that could essentially indicate good PID coefficient, that can be mapped to a state-space. The problem with the state-space method is that the characterization and mathematical modeling of all components of the system can take time to obtain and calculate especially for complex systems. For the team's purposes, PID controllers are sufficient for now. 



\subsection{Validation and Acceptance Test}
Each requirement provided by the client has an associated task that the team has planned and scheduled for. To validate the requirements, the team will conduct the following acceptance tests for each required function.

\begin{itemize}
\setlength\itemsep{0em}
\item Sensor board - Does data appear in the Putty/COM terminal values when read?
\item Bluetooth - Can 'Hello World' be sent to the PC paired with the module when the program is run on the board?
\item RC receiver/Bluetooth TRPY values - Do the motors increase rotations when throttle is increased?
\item microSD boot - Does the blue 'Done' light turn on when the board is powered on?
\item Base station - Do the quad motors change when controlled by the RC trainer?
\item PID - Do the mixer values change accordingly to the quads orientation when the quad is not perpendicular to the ground or rotated?
\item Base station - Does camera data appear when the quad IR sensors are on and visible? 
\item Data analytics - Do plots and MATLAB appear when a run has ended?
\item Quad Chassis - Does the Zybo and peripherals fall off if the quad is shook and turned upside down?
\item Main sensor board - Is the sensor board visually located on the center of the quad?
\item Power management circuits - Does the Zybo power and motors power on when batteries are plugged in? Do things power on if batteries are plugged in reverse? Does battery work if plugged in over night?
\end{itemize}



\section{User Interface and System Descriptions}
\subsection{Hardware}
The new quadcopter system, when complete, will consist of the programmable Zybo board mounted on the quad-chassis, 4 ESCs - each connected to a propeller, a gyroscope/accelerometer/magnetometer sensor peripheral, and Bluetooth/Wi-Fi communication modules. A microSD card will be inserted into the board, containing a proper boot binary file, in order for the board to program its FPGA and correctly utilize and communicate with external peripherals. The board provides power to all these components on the quad, and therefore needs to be plugged into batteries to be able to be flown. The board and its sensors will not need to be tampered with in order to fly, unless modifications to a component need to be made. 
\newline\newline
Additionally, a base station, either a desktop computer or other device, will need to be running the main MicroCART program in order to communicate, compute, and analyze flight data as needed.

\subsection{Software}
The platform's main program will have a GUI (seen in Figure \ref{fig:GUI}) for configuring automatic or manual controls as well as information of the quad system's positional data: pitch, roll, yaw, altitude, latitude, and longitude. The GUI will also provide users with a command line interpreter for the user to enter control commands, a grid for users to draw paths for the quad to move through, and an option for the system to playback a flight. 

\begin{figure}[H]
\begin{center}
	\includegraphics[width=0.5\textwidth]{GUIpicture.png}
	\caption{\small A sample layout of the GUI}
	\label{fig:GUI}
\end{center}
\end{figure}



\subsection{Control \& Automation}
The MicroCART platform will allow for two modes of control: manual and automatic. Manual control allows a user to use the RC trainer to maneuver the quad. Users who opt to manually fly the system should have previous experience flying RC quadcopters and know proper procedure to stop the system nicely. It should be noted that the manual mode will be only available when the RC receiver is on the quad as the team plans to phase out this hardware bottleneck. In Automatic mode, the base station computer will send commands through either through the RC trainer, Bluetooth, or Wi-Fi. 


\subsection{Test Plan}
Testing the individual components and pieces of the system will be conducted as each task is accomplished. For initial hardware and signal debugging, an oscilloscope will be used to identify issues in the system. For example, proper I2C communication can be observed when probing the sensor board when attached to the Zybo board. 
\\\\
For software, an agile approach will be taken by creating a small working prototype and building more functionality upon the prototype. At this stage, functional and unit testing will be conducted to ensure defined functions operate as specified and handle edge cases as needed. Once a software component is complete, it can be integrated into the system level software that will need to perform system verification to check that the integrated software functions within the system software. Multiple problems are expected to arise at the integration stage so it is important for developers to update the source control repository, so the team can roll back changes if needed.
\\\\  
In regards to controls, heavy testing and manual tuning is being done for the flight dynamics PID controllers. Testing for a single axis is done by constraining two axes by means of a turn-table or horizontal pole. By oscillating the quad back and forth, the appropriate amounts of present, past, and future error factors can be identified as the quad should stabilize and move back to a stead state if displaced. Again, this stabilization is keen on having good factors as to not overshoot or compensate too quickly.


\section{Work Breakdown Structure}
To maximize parallel development and workloads, project members have been assigned technical positions. Each member will be responsible for his technical position and will be expected to help the team with areas that fall under their title. By specific request of the client, team members should have at least a little understanding of each component that others are working on, so work on that section can be done if the student is absent. To accommodate other team members and future project members, extensive documentation should be written for each task and include an overview of why the task is relevant, a step-by-step walkthrough to perform the task, an FAQ, and any troubleshooting tips that users may run across.
\\\\
The team has decided the following roles for each member based on previous experience and interest: \\
Joe Boldrey: {\emph{Physical System Design and Controls Lead; Communication Officer}} \\
Adam Campbell: {\emph{GUI and Platform Software Lead; Webmaster}} \\
Paul Gerver: {\emph{Data Collection and Analysis Lead; Key Concept Holder}} \\
Tyler Kurtz: {\emph{Mixing and Motor Lead; Key Concept Holder }}\\
Ravi Nagaraju: {\emph{Power, Controls Management Lead; Webmaster}} \\
Jacob Rigdon: {\emph{Quad Communications Lead; Communication Officer}} \\
Matt Vitale: {\emph{Sensor and Peripheral Lead; Team Lead}} 


\subsection{Project Schedule}
The team and client have created a list of tasks that need to be completed for a successful project and to keep deadlines. The projects schedule Gantt charts can be seen in Figures \ref{fig:timeline1} and \ref{fig:timeline2} on page \pageref{fig:timeline2}.
\begin{figure}[H]
	\includegraphics[width=\textwidth]{timeline1.png}
	\caption{Timeline for the first semester}
	\label{fig:timeline1}
\end{figure}
\begin{figure}[H]
	\includegraphics[width=\textwidth]{timeline2.png}
	\caption{Timeline for the first and second semester}
	\label{fig:timeline2}
\end{figure}
\subsection{Physical Dangers \& Equipment Safety}
The quad systems, both old and new, spin plastic propellers extremely fast, and safety should be regarded for all people and the systems themselves. Users operating the quads should be aware of all personnel around them and ensure the quad controller is disarmed when done flying. Similarly, everyone in the operating environment should be cautious when near an armed and operating quadcopter. 
\\\\
For the physical systems, team members should make sure the equipment on the quad cannot be easily damaged from a short fall or flip. This mentality is important when designing the placement of the different boards and the peripherals on the new chassis. Additionally, care will be taken when powering the quad, the Zybo board, and any of its peripherals as to not cause electric shorts, excessive draining, or anything else that could cause a fire.

\subsection{Risk to Project Timeline}
There always exist risks to the timeline if equipment is broken and part ordering takes time. To avoid such risks, caution should be taken when handling equipment and when designing and testing components for the system electronically and physically. Likewise, it's important to know where any existing parts were sourced, so their replacement can be expedited if needed.
\\\\
Budget constraints can also slow down the progress of the project or cause it to fail completely. The foundation of proper financial management is appointing a secretary and keeping meticulous records. Planning expenditures for the entire project timeline will avoid any unforeseen shortfalls. 
\\\\
Additionally, team members can cause a risk to the project timeline if they do not communicate with other team members, clients, and advisers or seek help when getting blocked by obstacles. To help avoid these situations, the team lead--but certainly not limited to others--shall be responsible to assist team members who are struggling in their work.

\subsection{Resource Requirements}
While this project requires many resources, the client will provide nearly all the components except for the custom circuits needed for power management. The full table of resources and their associated cost can be seen in Table \ref{table:resources}.
\begin{table}[H]
\caption{Table of resources requirements}
\begin{center}
\begin{tabular}{| p{8cm} | c | c |}
\hline
{\bf Resource} & {\bf How will team obtain it?} & {\bf Estimated Cost} 			\\ \hline
DJI FlameWheel F450 & Provided by Client & \$32 				\\ \hline
ZYBO Zynq™-7000 Development Board & Provided by Client & \$125 	\\ \hline
SparkFun MPU-9150 & Provided by Client & \$34.95 				\\ \hline
Set of Batteries (Zippy 2100, Hyperion) & Provided by Client & N/A \\ \hline
4GB Micro SD Card & Provided by Client & \$3					\\ \hline
USB to MicroSD converter & Provided by Client & \$13			\\ \hline
Micro USB cable & Provided by Client & \$1						\\ \hline
IR Mirror Balls & Provided by Client & N/A						\\ \hline
OmniTrack IR Cameras & Provided by Client & \$599					\\ \hline
Nexys™2 Spartan-3E FPGA Board & Provided by Client & \$140		\\ \hline
SpekTrum DX6i RC Controller & Provided by Client & \$130		\\ \hline
InterLink Elite Controller & Provided by Client & \$170			\\ \hline
Electrical Speed Controllers (ESCs) & Provided by Client & \$80	\\ \hline
DJI Motors & Provided by Client & \$80							\\ \hline
Simulink Simulation Software & Provided by Client & N/A	\\ \hline
MATLAB Software & Provided by Client & N/A						\\ \hline
Power/Voltage Regulator Circuit & Order parts & 57.33 \\ \hline
Various Small Hardware Items (connectors, screws, electrical tape, etc.) & Provided by Client & TBD \\ \hline
Workspace/Testing Area & Provided by Client & N/A				\\ \hline
Various Hand Tools (screwdrivers, pliers, soldering iron, etc.) & Provided by Client & N/A\\ \hline
\end{tabular}
\end{center}

\label{table:resources}
\end{table}

\subsection{Market/Literature Survey}
While many ready-to-fly quadcopters exist commercially today, the MicroCART project is not intended for market in the near future. Rather, this project establishes a better platform for unmanned aerial vehicle applications and research as the system will be versatile and provide ample amounts of data for analysis. For example, this platform can be used to improve CprE 488: Embedded System Design and further research for the Distributed Sensing and Decision Making Lab at Iowa State. Regarding other universities performing similar kinds of research, the University of Pennsylvania has been deeply involved with quadcopter maneuvers around obstacles and distributed flying and Stanford University has been working cognitive distributed systems including, but not limited to quads.



\section{Conclusion}
The MicroCART team is focused on improving the current quadcopter system in the Distributed Sensing and Decision Making Lab. The new project includes replacing the current quadcopter with a stronger frame and a development board that is reconfigurable. The new system will also decouple a hardware RC mixer and the lab's IR cameras to allow the platform to be taken outside its confined environment.


\end{document}