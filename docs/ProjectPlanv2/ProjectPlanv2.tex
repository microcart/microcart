\documentclass[11pt,letterpaper]{article}
\usepackage[margin=1in]{geometry}
\usepackage{setspace}
\usepackage{fancyhdr}
\usepackage{mathtools}
\usepackage{amsthm,amssymb}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{cite}
\usepackage{wrapfig}
\usepackage{float}
\usepackage{subfig}
\usepackage{sidecap}
\usepackage{listings}
\usepackage{cancel}
\usepackage[usenames,dvipsnames,svgnames,table]{xcolor}

\floatstyle{plaintop}
\restylefloat{table}

\lstdefinestyle{customc}{
  belowcaptionskip=1\baselineskip,
  breaklines=true,
  frame=L,
  xleftmargin=\parindent,
  language=C,
  showstringspaces=false,
  basicstyle=\footnotesize\ttfamily,
  keywordstyle=\bfseries\color{green!60!black},
  commentstyle=\itshape\color{purple!60!black},
  identifierstyle=\color{blue},
  stringstyle=\color{red}
}

\lstset{
escapechar=@, style=customc
}

\begin{document} %Refer and describe all figures in text
%\lstset{language=C}
\newcommand{\call}[1]{\textsf{\bf #1}}
\newcommand{\mytilde}{\raise.17ex\hbox{$\scriptstyle\mathtt{\sim}$}}

\begin{titlepage}
\vspace*{3in}
\begin{center}
{\Huge \bf MicroCART Project Plan} \\
{\LARGE Group May15-28} \\
\vspace{0.3in}
{\large Members: Paul Gerver, Tyler Kurtz, Joe Benedict, \\ Adam Campbell, Matt Vitale, Ravi Nagaraju, Jacob Rigdon} \\
\vspace{0.3in}
{\large Client: Dr. Phillip Jones III} \\
\vspace{0.3in}
{\large Advisers: Dr. Phillip Jones III, Dr. Nicola Elia}
\end{center}
\vfill
\end{titlepage}

\tableofcontents
\listoffigures

\section{Problem Statement}
The current Micro-Controller Aerial Research Team (MicroCART) platform in the Distributed Sensing and Decision Making Laboratory, while sufficient for demoing, does not meet the client's expectations and wants. The old system requires a revamp to a new platform that removes several bottlenecks including a bulky radio controller (RC) mixer from the physical quadcopter. The client also feels the current system does not have adequate data analysis tools to enable the system to become autonomous, so the team will be focused on getting the new system running with better analysis tools.

\section{Concept Sketch}
The end goal of the project is to remove the cameras from the system so the quad will need to provide adequate data from its sensors to help locate and control the quad. The simplified diagram below provides an example of how the communication flows across the entire system with the tracking cameras included only to demonstrate how testing will be conducted. 

\begin{figure}[H]
\begin{center}
	\includegraphics[width=0.6\textwidth]{concept.png}
	\caption{\small High-Level Concept Sketch}
	\label{fig:concept}
\end{center}
\end{figure}

\subsection{Hardware Block Diagram}
For the quad to fly properly and safely, a programmable processing board will handle computations required to operate the quadcopter. Specifically, the team will be using a Diligent ZyBo board to program the interconnections between sensors and peripherals. An example configuration of a flying functional system will appear similar to the diagram seen in Figure \ref{fig:hardware}.

\begin{figure}[H]
\begin{center}
	\includegraphics[width=0.4\textwidth]{hardware.jpg}
	\caption{\small Zybo Board Hardware Block Diagram}
	\label{fig:hardware}
\end{center}
\end{figure}


\subsection{Software \& Controls}
To control the new system, a software graphical user interface (GUI) will be used to communicate with the quad through a radio controller. An example of the how the GUI will look can be seen in Figure \ref{fig:GUI}.

\begin{figure}[H]
\begin{center}
	\includegraphics[width=0.5\textwidth]{GUIpicture.png}
	\caption{\small A sample layout of the GUI}
	\label{fig:GUI}
\end{center}
\end{figure}

\subsection{Operating Environment}
It should be noted that the operating environment for the quad will be within the Distributed Sensing and Decision Making lab in Coover 3050 which does not contain natural weather conditions. In other words, nearly all testing and flight runs will be done in a controlled, ideal environment.

\section{Specifications}
The client has provided several specifications and criteria that the new system will need to have. 
\subsection{Sensors and Peripherals}
The on-board sensors for the quad need to provide consistent accelerometer, magnetometer, and gyroscope readings for the base station to safely conduct the quads movements. Additionally, the sensor needs to be placed near the quad's center of gravity in order to obtain useful data.
\subsection{Physical Quadcopter}
The quad chassis needs to be sturdy and provide a platform for a computing board, power supplies, and sensors. The chassis needs to be able to protect the items it is carrying while not being too closely bound that can cause disruptive vibrations on the systems. Additionally, the mounted processing board and its peripherals should not clear the highest point of the quad in case the quad flips over.

\subsection{ZyBo Board}
The main processing component on board the quad system, the Zybo board, needs to perform an ample amount of functions including operating each of the four quad motors, mixing the throttle, pitch, roll, and yaw signals received from the RC receiver, and sending sensor data back to the base station appropriately. 

\subsection{Data Analytics}
The new system requires data analytics that pull from multiple sources and compares the different measured points to gauge the accuracy of the on-board sensors. In other words, the OptiTrack camera system will be the basis for the quadcopter's location and will used as the control dataset since it provides precise measurements at a fast pace. The data analytic tools will need to be robust enough to effectively communicate to users what errors or outliers occur in readings received from the Zybo board. The tools should also be automated to let users not have to worry about locating the data file and running certain scripts to visualize the data. A data flow diagram for the data collection process can be seen in Figure \ref{fig:analytics}.
\begin{figure}[H]
\begin{center}
	\includegraphics[width=0.8\textwidth]{data_analytics.png}
	\caption{\small Data Collection Flow Diagram}
	\label{fig:analytics}
\end{center}
\end{figure}


\subsection{PID Controller}
A PID, proportional-integral-derivative, controller is required for the quad to maintain altitude and provide quick, smooth, and accurate movement for the quad in a three-dimensional space. The controller will be implemented in software to provide the system with feedback to correct for present, past, and future error that the system will run into. Several PID controllers will exist for each channel of flight: throttle, roll, pitch, and yaw. Because the different amounts of error need to be tuned appropriately to obtain a good controller, a lot of the teams time will be spent on tuning the PID controller along with the quad's sensors to obtain an optimal controller. 

\subsection{Power Management}
For a functional system, several problems regarding power management need to be addressed. First, the system needs to accurately relay current battery level information back to the user so the system can be controlled appropriately based on its current status. Second, the system should independently handle cases when there is not enough power and should drop altitude at a safe rate rather than stop mid flight and crash. Third, the system needs to regulate voltage levels of the batteries as they can be permanently damaged if the drained too much. 

\subsection{IR Camera System}
While one of the client's goal is to move outside of the refined environment of the lab, the quadcopter still needs to operate with the high-speed IR cameras and use the data collected in conjunction with the data received from the quad's sensors. The differences between the two data sets must be analyzed and tested in order for the quad to be decoupled from the camera system. 

\section{User Interface Description}
\subsection{Hardware}
The new quadcopter system, when complete, will consist of the programmable Zybo board mounted on the quad-chassis, 4 ESCs - each connected to a propeller, a gyroscope/accelerometer/magnetometer sensor peripheral, and Bluetooth/Wi-Fi communication modules. A microSD card will be inserted into the board, containing a proper boot binary file, in order for the board to program its FPGA and correctly utilize and communicate with external peripherals. The board provides power to all these components on the quad, and therefore needs to be plugged into a power source in the form of batteries to be able to be flown. The board and its sensors will not need to be tampered with in order to fly, unless modifications to a component need to be made. 
\newline\newline
Additionally, a base station, either a desktop computer or other device, will need to be running the main MicroCART program in order to communicate, compute, and analyze flight data as needed.

\subsection{Software}
The platform's main program will have a GUI for configuring automatic or manual controls as well as information of the quad system's positional data: pitch, roll, yaw, altitude, latitude, and longitude. The GUI will also provide users with a command line interpreter for the user to enter control commands, a grid for users to draw paths for the quad to move through, and an option for the system to playback a flight. 

\subsection{Functional Requirements}
\begin{itemize}
\setlength\itemsep{0em}
\item Zybo board collects several readings from main sensor peripheral
\item Zybo board communicates data through Bluetooth and Wi-Fi to base station
\item Zybo board can receive RC values from different sources and generate PWM signals to each motor
\item Zybo board can boot hardware configuration and software from microSD card
\item Base station communicates RC data to quad
\item PID controller regulates RC values
\item Base station receives IR camera data
\item Data analytic tools plot data after run.
\item Zybo board is mounted on new quad chassis
\item Main sensor board is centered on quad chassis
\item Quad and Zybo board are powered by a battery
\item Battery regulates voltage, detect reverse polarity, and doesn't allow for overdrain
\end{itemize}

\subsection{Non-Functional Requirements}
\begin{itemize}
\setlength\itemsep{0em}
\item Quad system has minimal data bottleneck
\item PID controller is optimal and makes quad flight easy
\item Easy board and sensor accessibility when mounted on chassis
\item Easily maintainable if new parts are required
\item Board and sensor protection and stability during flight runs
\item Easily expandable with additional peripherals and board configuration
\item Easily usable and demoable for operators and curious people 
\item Quad doesn't immediately fall and crash when battery is low
\item System is reliable and doesn't change parameters after every run
\item System can be deployed with proper program and RC controller
\end{itemize}


\subsection{Control \& Automation}
The MicroCART platform will allow for two modes of RC: manual and automatic. Manual control allows a user to use the RC device to maneuver the quad. Users who opt to manually fly the system should have previous experience flying RC quadcopters and know proper procedure to stop the system nicely. It should be noted that the manual mode will be only available when the RC receiver is on the quad as the team plans to phase out this hardware bottleneck. In Automatic mode, the base station computer will send commands through either through the RC trainer, Bluetooth, or Wi-Fi. 

\section{Deliverables}
Based off the client'sz end goals, the team has formulated steps and checkpoint deliverables required to complete the minimum requirements. Specifically, milestones have been created for each semester to help guide the team to success. To accompany each task, the team will provide detailed documentation that summaries the motivation, description, and how the specified task fits into the overall project. Additionally, tasks that complete a function  need to  have a detailed procedure explaining what steps users need to take to accomplish its function. For tasks that provide results, descriptions of the outcomes and outputs will be produced, and an FAQ guide will be written to help users troubleshoot problems that may arise.
\subsection{First Semester}
\begin{itemize}
\setlength\itemsep{0em}
\item Fully programmed Zybo quad board 
	\begin{itemize}
	\setlength\itemsep{0em}
	\item Working “Hello World” program demonstrating basic LED and button usage
	\item Reading data from 3-axis sensor board 
	\item Controlling the quad’s motors through RC receiver
	\item Software-implemented RC Mixer 
	\item Communicates positional data to base station through Bluetooth
	\end{itemize}
\item Mounted Zybo board and sensors on new chassis
\item Working power and voltage regulators for system’s batteries
\item Implement a system to record high-speed camera data
\item Implement a system to record on-board data for comparison to camera data
	\begin{itemize}
	\setlength\itemsep{0em}
	\item On-board data will initially be captured via on-board SD card
	\end{itemize}
\item Flyable demonstration of new system using old system’s demo program
\item Collected system data is parsed, analyzed, and visualized post-flight
\end{itemize}

\subsection{Second Semester}
\begin{itemize}
\setlength\itemsep{0em}
\item Demonstrate additional peripherals on quad
	\begin{itemize}
	\setlength\itemsep{0em}
	\item GPS
	\item Camera
	\end{itemize}

\item Demonstrate flight playbacks and full autonomous flights
\item Demonstrate quad flight removed from  camera environment
\item Show integration with sister project
	\begin{itemize}
	\setlength\itemsep{0em}
	\item Tethering machines together and having them travel synchronously
	\item Collecting data from both machines and integrating them
	\item Running test flights and plotting the data
	\item Enabling the quadcopter to fly outdoors
	\end{itemize}
\item Visualize flight data in real-time
\end{itemize}


\section{Work Breakdown Structure}
To maximize parallel development and workloads, project members have been assigned technical positions that they are responsible for and will help the team with regard toward the area. By specific request of the client, team members should have at least a little understanding of each component that others are working on, so work on that section can be done if the student is absent. The team has decided the following roles for each member based on previous experience and interest: 
\\\\
Joe Benedict: Physical System Design and Controls Lead; Communication Officer \\
Adam Campbell: GUI and Platform Software Lead; Webmaster \\
Paul Gerver: Data Collection and Analysis Lead; Key Concept Holder \\
Tyler Kurtz: Mixing and Motor Lead; Key Concept Holder \\
Ravi Nagaraju: Power, Controls Management Lead; Webmaster \\
Jacob Rigdon: Quad Communications Lead; Communication Officer \\
Matt Vitale: Sensor and Peripheral Lead; Team Lead \\

\section{Resource Requirements}
\begin{table}[H]
\caption{ Table of resources requirements}
\begin{center}
\begin{tabular}{| p{8cm} | c | c |}
\hline
{\bf Resource} & {\bf How will team obtain it?} & {\bf Estimated Cost} 			\\ \hline
DJI FlameWheel F450 & Provided by Client & \$32 				\\ \hline
ZYBO Zynq™-7000 Development Board & Provided by Client & \$125 	\\ \hline
SparkFun MPU-9150 & Provided by Client & \$34.95 				\\ \hline
Set of Batteries (Zippy 2100, Hyperion) & Provided by Client & N/A \\ \hline
4GB Micro SD Card & Provided by Client & \$3					\\ \hline
USB to MicroSD converter & Provided by Client & \$13			\\ \hline
Micro USB cable & Provided by Client & \$1						\\ \hline
IR Mirror Balls & Provided by Client & N/A						\\ \hline
OmniTrack IR Cameras & Provided by Client & \$599					\\ \hline
Nexys™2 Spartan-3E FPGA Board & Provided by Client & \$140		\\ \hline
SpekTrum DX6i RC Controller & Provided by Client & \$130		\\ \hline
InterLink Elite Controller & Provided by Client & \$170			\\ \hline
Electrical Speed Controllers (ESCs) & Provided by Client & \$80	\\ \hline
DJI Motors & Provided by Client & \$80							\\ \hline
Simulink Simulation Software & Provided by Client & N/A	\\ \hline
MATLAB Software & Provided by Client & N/A						\\ \hline
Power/Voltage Regulator Circuit & Order parts & TBD \\ \hline
Various Small Hardware Items (connectors, screws, electrical tape, etc.) & Provided by Client & TBD \\ \hline
Workspace/Testing Area & Provided by Client & N/A				\\ \hline
Various Hand Tools (screwdrivers, pliers, soldering iron, etc.) & Provided by Client & N/A\\ \hline
\end{tabular}
\end{center}

\label{table:resources}
\end{table}


\section{Project Schedule}
\begin{figure}[H]
	\includegraphics[width=\textwidth]{timeline1.png}
	\label{fig:timeline1}
\end{figure}
\begin{figure}[H]
	\includegraphics[width=\textwidth]{timeline2.png}
	\label{fig:timeline2}
\end{figure}
\section{Risks}
\subsection{Physical Dangers \& Equipment Safety}
The quad systems, both old and new, spin plastic propellers extremely fast, and safety should be regarded for all people and the systems themselves. Users operating the quads should be aware of all personnel around them and ensure the quad controller is disarmed when done flying. Similarly, everyone in the operating environment should be cautious when near an armed quadcopter. 
\newline\newline
For the physical systems, team members should make sure the equipment on the quad cannot be easily damaged from a short fall or flip. This mentality is important to keep in mind when designing the placement of the board and the peripherals on the new chassis. Additionally, care will be taken when powering the quad, the Zybo board, and any of its peripherals as to not cause electric shorts, excessive draining, or anything else that could cause a fire.
\subsection{Risk to Project Timeline}
There always exist a risk to the timeline if equipment is broken and part ordering takes time. To avoid such risks, caution should be taken when handling equipment and when designing and testing components for the system electronically and physically. Likewise, it's important to know where any existing parts were sourced, so their replacement can be expedited if needed.
Budget constraints can slow down the progress of the project or cause it to fail completely. The foundation of proper financial management is appointing a secretary and keeping meticulous records. Planning expenditures for the entire project timeline will avoid any unforeseen shortfalls. 
\newline\newline
Additionally, team members can cause a risk to the project timeline if they do not communicate with other team members, clients, and advisers or seek help when getting blocked by obstacles. To help avoid these situations, the team lead--but certainly not limited to others--shall be responsible to assist team members who are struggling in their work.
\section{Market/Literature Survey}
While many ready-to-fly quadcopters exist commercially today, the MicroCART project is not intended for market in the near future. Rather, this project establishes a better platform for unmanned aerial vehicle applications and research as the system will be versatile and provide ample amounts of data for analysis. For example, this platform can be used to improve CprE 488: Embedded System Design and further research for the Distributed Sensing and Decision Making Lab at Iowa State. Regarding other universities performing similar kinds of research, the University of Pennsylvania has been deeply involved with quadcopter maneuvers around obstacles and distributed flying and Stanford University has been working cognitive distributed systems including, but not limited to quads.

\end{document}