\documentclass[11pt,letterpaper]{article}
\usepackage[margin=1in]{geometry}
\usepackage{setspace}
\usepackage{fancyhdr}
\usepackage{mathtools}
\usepackage{amsthm,amssymb}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{cite}
\usepackage{wrapfig}
\usepackage{float}
\usepackage{subfig}
\usepackage{sidecap}
\usepackage{listings}
\usepackage{cancel}
\usepackage{multirow}
\usepackage[usenames,dvipsnames,svgnames,table]{xcolor}

\lstdefinestyle{customc}{
  belowcaptionskip=1\baselineskip,
  breaklines=true,
  frame=L,
  xleftmargin=\parindent,
  language=C,
  showstringspaces=false,
  basicstyle=\footnotesize\ttfamily,
  keywordstyle=\bfseries\color{green!60!black},
  commentstyle=\itshape\color{purple!60!black},
  identifierstyle=\color{blue},
  stringstyle=\color{red}
}

\lstset{
escapechar=@, style=customc
}

\begin{document} %Refer and describe all figures in text
%\lstset{language=C}
\newcommand{\call}[1]{\textsf{\bf #1}}
\newcommand{\mytilde}{\raise.17ex\hbox{$\scriptstyle\mathtt{\sim}$}}

\begin{titlepage}
\vspace*{3in}
\begin{center}
{\Huge \bf MicroCART Design Document} \\
{\LARGE Group May15-28} \\
\vspace{0.3in}
{\large Paul Gerver, Tyler Kurtz, Joe Benedict, \\ Adam Campbell, Matt Vitale, Ravi Nagaraju, Jacob Rigdon} \\
\vspace{0.3in}
{\large 10/28/2014}
\end{center}
\vfill
\end{titlepage}

\tableofcontents
\listoffigures
\listoftables

\newpage

\section{Definition of Terms}
$ I^2C $ - A serial bus protocol invented by Philips.\\
RC Trainer - Device that controls thrust, roll, pitch, and yaw through positional pulse modulation. Another term for RC controller.

\section{Abbreviations}
ADC: Analog-to-digital converter \\
BT: Bluetooth \\
CAD: Computer-aided design \\
DSP: Digital Signal Processing \\
ESCs: Electronic Speed Controllers \\
FPGA: Field Programmable Gate Array \\
Gyro: Gyroscope \\
I2C: Inter-Integrated Circuit \\
IR: Infra-red \\
MicroCART: Micro-Controller Aerial Research Team \\
m.o.i: moment of inertia \\
PID: Proportional-Integral-Derivative \\
PMOD: Peripheral Module \\
PPM: Positional pulse modulation\\
PWM: Pulse width modulation \\
quad: quadcopter \\
RAM: Random Access Memory \\
RC: Radio Controller \\
XPS: Xilinx Platform Studio \\
XSDK: Xilinx Software Development Kit \\

\newpage

\section{Executive Summary}
The goal of this project is to design, integrate, and implement a new quad system to replace the current system for the Distributed Sensing and Decision Making Laboratory at Iowa State University. The current system requires an upgrade to remove unnecessary and slow bottlenecks, like an RC mixer, and needs better data analysis tools for comparing measurements from a variety of sensors. These components are all necessary as the system must be able to handle new workloads and environments. 

\section{System Design}
\subsection{System Description}
\subsubsection{Overview}

\begin{figure}[H]
\begin{center}
	\includegraphics[width=0.7\textwidth]{concept.png}
	\caption{\small System concept block diagram}
	\label{fig:concept}
\end{center}
\end{figure}
\vspace{-0.5cm}
This project is an expansion of multiple years of work within the Distributed Sensing and Decision Making lab geared toward preparing a new platform for research. The new system that will be in place allows for multiple projects to be developed at the same time and each can be unique since the system will be versatile with swappable peripherals and reconfigurable hardware. In other words, the new system can be used for application-specific or research projects depending on the client's interests. \\

\subsubsection{Quad System}
The quad system is the mobile unit in the group that performs most of the actuations in the system. Specifically, the quad contains four motors that allow it to move in three dimensional space and receives control signals that is sent by the base station by means of an RC receiver. The hardware for the quad consists of the physical frame, motors, ESCs, reconfigurable processing board, and multiple sensors and peripherals like a Bluetooth module, gyroscope, and accelerometer. \\

\subsubsection{Base Station}
The base station is the main processing unit that handles the controls of the system. The software on the desktop includes the system program for running flights through a manual RC trainer or automatically, and it has data analytic tools for interpreting flight data. Since the base station is responsible for controls, PID controllers are incorporated into the main program to handle the error within the feedback control loop to safely control the quad to a desired altitude, longitude, and latitude. For hardware, the base station consists of a standard desktop running Red Hat 6 Linux with a Nexsys2 Spartan-3E FPGA board used as the interconnect between the computer and the RC trainer to send commands through PPM waves to the quad. \\

\subsubsection{OptiTrack Cameras}
The 12 high-speed IR cameras located in the Distributed Sensing and Decision Making Lab are used for tracking the location of the quad within its range. The camera system relays positional information to the base station that will incorporate its measurements into its controls. The team is working on removing the requirement to have the cameras in order to fly the quad; however, the team is utilizing them to test and compare with on-board sensors for accuracy. \\

\subsection{System Requirements}
\subsubsection{Functional}
\subsubsection*{Software}
{\bf Quad System} \\
The software for the new quad system needs to be able to collect several readings from its main sensor peripheral through I2C protocol, communicate data through Bluetooth to the base station, receive PPMs through an RC receiver, and appropriately mix incoming position signals to pulse wave modulations for each of the four motors on the quad.  
\newline\newline
Additionally, the processing board does contain volatile memory and a reconfigurable FPGA, so the board needs to be configured without having to plug it into a computer. \\\\
{\bf Base Station} \\
The base station is the heart of the system so it needs to be able to communicate or receive data from all devices in the system including the OptiTrack camera system, the RC Trainer, and the Zybo board located on the quad itself. Some tools already exist to communicate with the RC controller and the cameras, but project members will still learn about these components in order to incorporate them into the design of the new system. 
\\\\
The base stations also requires data analytic tools to interpret the diverse range of data being received and transmitted. Again, the data collected will be utilized ins PID controller as well as post-flight analysis for testing measurements against the camera and the quad's on-board sensors. The PID controller in software needs to be tuned properly to the new system's quad to properly fly it.  
\subsubsection*{Hardware}
{\bf Board and Peripheral Mounting} \\
For the quad to operate, the Zybo board, among its peripherals, need to be mounted and placed somewhere on the new quad chassis so the Zybo board can operate the motors. Additionally, the main sensor board that measures gyro and accelerometer data needs to be placed at the center of gravity on the quad to receive useful data. In regards to how the board will be powered, an external, mobile, and replaceable power supply is placed on the carrying the platform to power the board, ESCs, and motors so the quad can operate in a wide environment and not be restricted. \\\\
{\bf Power Supply} \\
There are a few requirements for the power supply batteries, both to the quad motors and the Zybo board, including a need for proper voltage regulation, detecting reverse polarity, and not allowing for overdrain so the batteries do not get damaged.\\

\subsubsection{Non-Functional}
\subsubsection*{Software}
{\bf Quad System} \\
There are many timing constraints that the quad should meet and the data bottleneck should be minimal when communicating data back to the base station so the quad can have a smooth flight. The system should also be reliable and shouldn't need to be reset or configured after each flight, or behave strangely when launching the main program. Overall, users shouldn't have to touch the software aspects of the quad when starting the basic flying program. \\\\
{\bf Base Station}\\
The software for collecting data from the IR cameras and sending the correct instructions through the Nexsys2 board should not be of any concern to users wanting to fly the quad. These components should also be reliable and need to be tampered with unless absolutely necessary. The data analytic tools also need to be automated so users do not need to dig through log files and look for scripts to run in order to view results from a flight. Lastly, the PID controllers on the system need to be optimal to make flying the quad easy and safe for users. \\


\subsubsection*{Hardware}
{\bf Board and Peripheral Mounting} \\
The most important aspect of the new system is that it does not injure any of its components when testing. For this reason, the placement of the Zybo board and its peripherals on the quad chassis is critical to not damage equipment which can cost time and money to replace. The mounted board should not be the tallest point on the quad since it is likely the quad will tip over upside at some point, and it should be protected and stable during test flights. Additionally, accessibility should be considered when mounting the board and peripherals so they are fairly easy to access and remove if needed. 
\\\\
{\bf Power Supply}\\
The power supply batteries used in the system need to be monitored and handled properly when their power is too low to operate the quad. For maintainability of the quad, the quad needs to safely land rather than stop altogether when in a flight, and the batteries need to not be damaged in the process.

\subsection{System Analysis}
\subsubsection{Modeling and Simulation}
Characterization of the new quad system will conducted to obtain key constants that can be used to model the system in Simulink or MATLAB to identify proper control parameters. For the PID controls for the quad, several simulated and actual tests are being conducted to identify appropriate P, I, and D constants for each of the four channels of control: thrust, roll, pitch, and yaw. To make modeling easy at first, these parameters are obtained through extensive testing using equipment that constrain the quad to only one axis of rotation at a time. 
\\\\
The main characterizations that will be collected for the quad are detailed in Table \ref{table:characs}. 

\begin{table}[H]
\begin{center}
\begin{tabular}{|l|l|}
\hline
{\bf Symbol} & {\bf Description} \\ \hline
$J_{reg}$ & rotor+motor m.o.i around motor axis of rotation\\
$K_T$ & propeller thrust constant \\
$K_d$ & rotor drag constant \\ \hline
$R_m$ & motor winding resistance \\
$K_Q$ & motor torque constant \\
$i_f$ & motor internal friction current\\
$K_v$ & motor back-emf constant \\ \hline
$m_q$ & vehicle mass \\
$m_b$ & battery mass \\
$\alpha$ & angular acceleration of disk+rig+quad\\
$\Theta$ & angular displacement \\
$\tau$ & applied torque \\
\hline
\end{tabular}
\end{center}
\caption{Quad characterization variables}
\label{table:characs}
\end{table}

\subsection{Operating Environment}
The new system is being developed and tested within the Distributed Sensing and Decision Making Lab. The lab has different electrical equipment for signal debugging, physical component characterization, charging batteries, computers for programming the Zybo board and data analysis, and the OptiTrack IR Camera system for position measurements. This operating environment is ideal for quad testing and flying as there are no natural forces like wind that disrupt conditions.


\section{Detailed Design}
\subsection{Hardware Specification}
\subsubsection{DJI FlameWheel F450}
The new quad system utilizes a four arm DJI FlameWheel to house and carry the proper components in order to fly the system. The chassis has two platforms: one that sits on the intersection of the four arms, and one below that acts as a base. Both of the platforms are conductive and they share a single pair of wire leads for powering the ESCs and motors tightly constrained to the chassis' arms. The exact specifications and recommendations for the FlameWheel can be seen in Table \ref{table:flamewheel}.
\\\\
\begin{table}[h]
\begin{center}
\begin{tabular}{|l|l|}
\hline
{\bf Spec} & {\bf Description} \\ \hline
Frame Weight & 282g \\
Diagonal Wheelbase & 450mm \\
Takeoff Weight & 800g $\sim$ 1200g \\
Recommended Propeller & 10 x 3.8in; 8 x 4.5in \\
Recommended Batter & 3S $\sim$ 4S LiPo \\
Recommended Motor & 2212 $\sim$ 2216 (Stator Size) \\
Recommended ESC & 15A \~ 25A \\
\hline
\end{tabular}
\end{center}
\caption{Specifications of the DJI FlameWheel F450}
\label{table:flamewheel}
\end{table}


\subsubsection{DJI Motors}
The motors are the key actuators in the system that eventually move the quad with the help of propellers. The DJI Motors are stepper motors that are controlled by magnets inside the motor that generating precise magnetic fields to make the motor rotate rapidly. With four total motors on the quad, proper mixing of signals is required for the motors to change their number of resolutions per minute so the quad can turn or rotate correctly. The specifications for the DJI motors can be seen in Table \ref{table:motors}.  
\begin{table}[H]
\begin{center}
\begin{tabular}{|l|l|}
\hline
{\bf Spec} & {\bf Description} \\ \hline
Stator Size & 22 x 12mm \\
Brushless Motor KV & 920rpm/V \\
\hline
\end{tabular}
\end{center}
\caption{Specifications of the DJI Brushless Motors}
\label{table:motors}
\end{table}


\subsubsection{ESCs}
The ESCs act as the middle-man between the Zybo board and the motors. The ESCs are responsible for translating the pulse wave modulation sent by the Zybo board into how much power to give the motors in order to spin. The ESCs are attached to the quad frame by zip ties and are powered by the conductive platforms on the frame. The ESCs the team are using are specified in Table \ref{table:ESC}

\begin{table}[h]
\begin{center}
\begin{tabular}{|l|l|}
\hline
{\bf Spec} & {\bf Description} \\ \hline
Electric Current & 30A OPTO\\
Compatible Signal Frequency & 30Hz - 450Hz \\
Battery & 3S $\sim$ 4S LiPo\\
\hline
\end{tabular}
\end{center}
\caption{The specifications for the DJI ESCs}
\label{table:ESC}
\end{table}


\subsubsection{Diligent ZYBO Zynq-7000 Development Board}
The Zybo board is the core processing unit on the quad system responsible for mixing controls from the base station and handling gyro and accelerometer data for its sensors. Additionally, the board is responsible for communicating data appropriately to the base station through the use of Bluetooth or WiFi. A full list of features the Zybo board offers can be seen in Table \ref{table:Zybo_features}.
\begin{table}[H]
\begin{center}
\begin{tabular}{|l| p{8cm} |}
\hline
{\bf Component} & {\bf Description} \\ \hline
Processor & 650MHz dual-core Cortex-A9 Processr \\ \hline
Memory & DDR3 Memory Controller with 8 DMA channels\\ \hline
High-bandwith peripheral controllers & 1G Ethernet, USB 2.0, SDIO \\ \hline
{Low-bandwith peripheral controllers} & SPI, UART, I2c \\ \hline
\multirow{4}{*}{FPGA} & 28K logic cells \\
& 240KB Block RAM \\
& 80 DSP slices \\
& On-chip dual channel, 12-bit ADC\\ \hline
\multirow{4}{*}{Misc.} & MicroSD slot \\
& 6 pushbuttons, 4 slide switches, 5 LEDs \\
& 6 PMOD connectors \\
& On-board JTAG programming and UART to USB converter\\
\hline
\end{tabular}
\end{center}
\caption{Features for the Diligent ZyBo Board}
\label{table:Zybo_features}
\end{table}
\noindent The Zybo board is used on the new system because it has 6 PMOD connectors (see Figures \ref{fig:pmoddiagram} and \ref{fig:pmodpinout}) and an FPGA that can programmed to be utilize different peripherals including the 9 Degrees of Freedom sensor board, Bluetooth module, and the outputs signals for controlling the motors. Another reason the board was selected is due to its many internal controllers like UART and I2C so developers can use the FPGA for hardware acceleration or even on-board PID controlling. 
\begin{figure}[H]
\begin{center}
	\includegraphics[width=0.4\textwidth]{pmodpindiagram.png}
	\caption{\small PMOD Diagram}
	\label{fig:pmoddiagram}
\end{center}
\end{figure}
\begin{figure}[H]
\begin{center}
	\includegraphics[width=0.7\textwidth]{pmodpinout.png}
	\caption{\small ZyBo Pin out Table}
	\label{fig:pmodpinout}
\end{center}
\end{figure}
\noindent For programming the Zybo board, users can develop a bitstream file through XPS to be flashed onto the FPGA that can configure the processing system and programming logic portions of chip on the board. The actual process of programming is done either through UART JTAG if connected to a computer in the XSDK or through a microSD card if set up properly.

\subsubsection{SpekTrum DX6i RC Controller}
The RC trainer is the main device for communicating the thrust, roll, pitch, and yaw levels to the quad system. The RC trainer is connected to the base station through the Nexsys2 FPGA and sends 50 PPMs a second. The RC receiver located on the quad receives the 6 channel PPM and splits the channels to individual PWMs for each flight dynamic. The specifications for the SpekTrum RC trainer can be seen inf Table \ref{table:rc}. 

\begin{table}[H]
\begin{center}
\begin{tabular}{|l|l|}
\hline
{\bf Spec} & {\bf Description} \\ \hline
Modulation & DSM2 \\
Band & 2.4GHz \\
Receiver & AR6200 \\
Programming Features & Helicopter \& Airplane \\
Model Memory & 10 \\
Modes & Mode 2 \\
Transmitter (Tx) Battery Type & AA NiMH 1500mAh batteries \\
Charger &  4-cell 150mAh wall charger \\
\hline
\end{tabular}
\end{center}
\caption{Specification of the SpekTrum DX6i RC Controller}
\label{table:rc}
\end{table}

\subsubsection{Voltage Regulator}
The voltage regulator is key to powering the Zybo board while on the quad. The regulator is also responsible for identifying the battery level, detecting reverse polarity, and stopping power from being drawn from the batter after certain level. The specifications for the voltage regulator can be seen in Table \ref{table:volt-reg}, the circuit diagram and voltage and power graphs can be seen in Figure \ref{fig:volt-reg-circuit} and \ref{fig:volt-reg-graphs}, and lastly the bill of materials can be seen in Figure \ref{fig:volt-reg-parts}.

\begin{table}[H]
\begin{center}
\begin{tabular}{|l|l|}
\hline
{\bf Spec} & {\bf Description} \\ \hline
Desired Voltage Output & 5 V \\ 
Desired Current Output & 2.5A \\
Expected input battery voltage range & 6.5V - 8.4V \\
\hline
\end{tabular}
\end{center}
\caption{Specifications for the voltage regulator}
\label{table:volt-reg}
\end{table}
	
	
	\begin{figure}[H]
	\begin{center}
		\includegraphics[width=\textwidth]{voltage_regulator.png}
		\caption{The circuit design for the voltage regulator}
		\label{fig:volt-reg-circuit}
	\end{center}
	\end{figure}
	
	\begin{figure}[H]
	\begin{center}
		\includegraphics[width=0.8\textwidth]{voltage_regulator_graphs.png}
		\caption{Voltage and power graphs for voltage regulator}
		\label{fig:volt-reg-graphs}
	\end{center}
	\end{figure}
	
	\begin{figure}[H]
	\begin{center}
		\includegraphics[width=0.7\textwidth]{voltage_regulator_materials.png}
		\caption{Materials needed for voltage regulator}
		\label{fig:volt-reg-parts}
	\end{center}
	\end{figure}

\subsubsection{SparkFun MPU9150: 9 Degrees of Freedom}
The 9 Degrees of Freedom board contains several useful sensors including a gyroscope, accelerometer, temperature sensor, and a magnetometer. The specifications for the board can be seen in Table \ref{table:mpu9150}.
	\begin{table}[H]
	\begin{center}
	\begin{tabular}{| l| l |}
	\hline
	{\bf Spec} & {\bf Description} \\ \hline
	VDD & 2.375 - 3.465V \\ \hline
	\multirow{3}{*}{VLogic} & Min = 1.71V\\
	& Max = VDD\\
	& 1.8 $\pm$ 5\% OR VDD\\ \hline
	Absolute Maximum Ratings: VDD & -0.5 to 6V \\
	\hline
	\end{tabular}
	\end{center}
	\caption{Specifications for the SparkFun MPU9150}
	\label{table:mpu9150}
	\end{table}
\noindent The board uses a I2C bus protocol to communicate data which the Zybo board will be configured and program to accommodate it.
The following figure indicates the pin outs of the board.
	\begin{figure}[H]
	\begin{center}
		\includegraphics[width=0.7\textwidth]{9150pinout.png}
		\caption{\small SparkFun MPU9150 Pin Out and Signal Description}
		\label{fig:9150pinout}
	\end{center}
	\end{figure}

\subsubsection{Diligent PMOD-BT2}
For non-control communication, a Bluetooth device will be used on the Zybo board to send data to the base station for analysis or for PID controller integration. The team will be utilizing a Diligent PMOD-BT2 device that can easily be plugged into the Zybo board and the pin out for the module can be seen in Figure \ref{fig:bluetooth}.
\begin{figure}[H]
\begin{center}
	\includegraphics[width=0.3\textwidth]{btpmod.png}
	\caption{\small Diligent PMOD-BT2 Diagram}
	\label{fig:bluetooth}
\end{center}
\end{figure}
\subsubsection{OptiTrack IR Cameras}
The OptiTrack cameras in the lab are set to capture reflected IR waves from the quad with the proper pieces of equipment. In order to track the quad's location, the quad frame carries a set of reflective IR balls that the cameras can track. Again, the data produced from the cameras are sent to an intermediate desktop through USB 2.0 where it is then sent to the base station through WiFi. More specifications for the camera system can be seen in Table \ref{cameras}.

\begin{table}[H]

\begin{center}
\begin{tabular}{|l| l |}
\hline
{\bf Spec} & {\bf Description} \\ \hline
Resolution & 0.3MP (640 x 480) \\
Frame Rate & 100 FPS \\
Horizontal FOV & $46^o$ \\
Filter Switcher & Optional \\
Interface & USB 2.0 \\
No. of LEDs & 26 \\
Latency & 10ms \\
\hline
\end{tabular}
\end{center}
\caption{Specifications for the OptiTrack Cameras}
\label{cameras}
\end{table}


\subsection{Software Specification}
\subsubsection{Zybo Board Program: Sensor Board}
For each peripheral added to the Zybo board, appropriate software needs to accommodate for the new module. By developing through the XSDK, project members can initialize, configure, and operate devices through C code and pre-defined function calls, if users are using Xilinx provided files.
\\\\
One example use case for programming the Zybo board is the software required to speak with the SparkFun MPU9150 sensor board. The sensor peripheral uses I2C to communicate, so the board's software needs to accommodate for the configured hardware's internal I2C controller. A process flow for this specific example can be seen in Figure \ref{fig:sensor_board}; however, the initial setups seen in the diagram are similar across different peripherals with the only difference being the type of controller or I/O.


\begin{figure}[H]
\begin{center}
	\includegraphics[width=0.5\textwidth]{i2c.png}
	\caption{\small Program flow for reading data from the SparkFun MPU9150}
	\label{fig:sensor_board}
\end{center}
\end{figure}
\vspace{-0.5cm}

\subsubsection{Control Mixer}
The control mixer for the system mixes the incoming PWMs from the RC receiver and translates the individual signal for a flight component to a signal that all four motors on the quad react to. This software is run by the Zybo board since it handles all communication for the quad. For example, if a users wants to move the quad horizontally to the right, the RC receiver will change the roll to rotate slightly clockwise which means the two motors on the left side of the quad need to increase power and the right ones need to decrease power. 

\subsubsection{Data Analytics}
The data analytics for the system are done using MATLAB to parse data logs for both the entire system and individual component logs and produces visual plots of the data with respect to time. The data analytics tools are also automated, and the automated flow of the data can be seen in Figure \ref{fig:data_analytics}.
\begin{figure}[H]
\begin{center}
	\includegraphics[width=\textwidth]{data_analytics.png}
	\caption{\small Data flow for automated data analytics}
	\label{fig:data_analytics}
\end{center}
\end{figure}
\vspace{-0.5cm}


\subsubsection{PID Controller}
The PID controller is the basis for a steady flight for the quad, because it factors in the present, past, and future error of the system and uses it to correct itself rather than overshoot its ideal destination. Based off a desired location, the controller will take a weighted sum of the different errors and provide feedback to the current system. For the quad, four different PID controllers, one for each flight dynamic, are being created. The psuedo-code for a generic PID controller can be seen below:


\begin{lstlisting}
while(true){
	error = desired value - measured value; 
	D = (error -– previous)/dt; 
	I += error*dt; 
	Output = Kp*error + Ki*I + Kd*D; 
	previous = error; 
	sleep(dt);
}
\end{lstlisting}

\subsection{User Interface Specification}
\subsubsection{CLI/GUI}
The GUI system is used to interact with the quad using the OptiTrack camera system, the base station FPGA board, and the manual controller. The GUI starts interacting with these by first taking in camera system data. It then does a quick Kalman filter and PID calculation with the information received. Upon start of the GUI, the code implements a home system. It takes the x, y, and
z coordinates that the quad is currently at and saves it as the home location. From this location, the GUI directs the quad in the direction it needs to go in, adjusting the pitch, roll, and yaw to keep it in that position until it receives the
command to start moving. The full layout of the GUI can be seen in Figure \ref{fig:gui}.

	\begin{figure}[H]
	\begin{center}
		\includegraphics[width=0.5\textwidth]{GUIpicture.png}
		\caption{\small GUI diagram}
		\label{fig:gui}
	\end{center}
	\end{figure}
	\vspace{-0.5cm}
\noindent {\bf Side Bar} \\
Upon initially starting up the GUI, it connects to the base station FPGA board. The image on the left shows the sidebar of the GUI that is static throughout
each view. The bottom of the sidebar contains the x, y, and z coordinates that the quad is at in relation to the home position. The pitch and roll graphics in the center show the orientation the quad is currently in. The compass shows the quad's current heading in relation to the camera system.
\\\\
The buttons toward the top of the sidebar show the quad ­takeoff and landing functionality as well as the ability to start or stop the communication link. When clicked, the START button begins communications with the camera system and initializes the PPMs sent over to the quad. The STOP button stops the system from reading camera data and stops the transfer of data
to the quad.
\\\\
The Take Off and Land button are only functional after the START button has been pushed. The Take Off button is an auto take­off feature for the quad to get in the air on its own. It works within the code by slowly raising the throttle PPM signal sent through the base station FPGA board. The GUI also uses camera
system data to keep the quad stable as it is raising off the ground. The
auto-takeoff feature is currently set to raise the quadcopter until it gets 0.5 meters off the ground.
\\\\
The Land button brings the quad onto the ground from the
air. The GUI uses the quad’s current position to keep it
balanced while slowly reducing throttle. Once the quad
gets to about 0.1 meters off the ground, the GUI shuts off
all the motors at once so that the quad doesn’t bounce
upon hitting the ground.
\\\\
{\bf Settings}\\
The settings view in the GUI is pictured in Figure 3. This
area is divided into the four following categories:
Behavior, Modules, Controller Selection, and Grid View.
The Behavior section is used to determine what system it
will be running on. It currently only has two behaviors,
PID 2012 and Test. The only system actually used to fly
the quad is the PID 2012. The other option is
the GUI’s visual style, but there is only one option at the
moment.
\\\\
The Modules and Controller Selection determine which controller will be used to fly the quad. The default option is OptiTrackVRPN, and that is currently the only option capable of running the flight controls. Communication can be set to the trainer. The Manual control selection determines whether the quad is fully automatic, or controlled manually when manual control is selected. The Controller Selection allows you to change which specific controls will be either automatic or manual.
\\\\
{\bf Grid}\\
The Grid’s basic functionality is to have the user draw a flight path for the quad. The desired flight path is drawn in the white box using. Once the path is drawn, the user can click the send button to start the quad new flight path. When the user clicks the clear button, the drawn flight path is erased, allowing a new flight path to be created. The load button is used to load
an existing flight path, and the save button is used to save the path to reuse later.\\\\
{\bf Console}\\
The console is used as a textbased controller for the quad. The current supported commands are the following:
\begin{itemize}
\setlength\itemsep{0em}
\item alt -­ used to increase or decrease altitude
\item lon -­ used to increase or decrease longitude
\item lat -­ used to increase or decrease latitude
\end{itemize}
The console receives and outputs data given from the settings section in real-time displays that data. The user types in the command to give toward the bottom of the interface and clicks the send button to initiate the command.
The quad measures all changes given from the console in meters.
\\\\
Sensors\\
The Sensors area of the GUI is non-interactive. It shows
where the quad is going on the left side, and where the
quad is currently at on the left. The user can see the
longitude, latitude, and altitude the quad is currently at and he
or she can this the information for testing purposes. This
information can also be used to make sure the
quad is reading information correctly, ensuring the quad does not leave its designated area.
\\\\
{\bf CLI} \\
The CLI (command line interface) will be a menu driven interface that is simpler and faster than the GUI. The supported commands will allow for incrementing and decrementing the altitude, latitude, and longitude for movement, as well as incrementing and decrementing throttle, pitch, roll, yaw for testing.


\subsection{Test Specification}
Testing the individual components and pieces of the system will be conducted as each task is accomplished. For initial hardware and signal debugging, an oscilloscope will be used to identify issues in the system. For example, proper I2C communication can be observed when probing the sensor board when attached to the Zybo board. For software, an agile approach will be taken by creating a small working prototype and building more functionality upon the prototype. As more functionality is introduced and problems arise, a developer can roll back the source control to a working state.
\\\\
As the system becomes more elaborate, the following order of logically questions will be asked while stepping through the hardware and software flow of the system:
\begin{itemize}
\setlength\itemsep{0em}
\item Is the quad's power supply connected?
\item Is the Zybo board being programmed? Is the "Done" LED on?
\item Is the main program on the base station started?
\item Are the proper PID constants used?
\item Is the RC Trainer plugged in, set to the proper mode, and armed?
\item Is the camera system software on and set for the quad?
\end{itemize}    
In regards to controls, heavy testing and tuning is being done for the flight dynamics PID controllers. Testing for a single axis is done by constraining two axes by means of a turn-table or horizontal pole. By oscillating the quad back and forth, the appropriate amounts of present, past, and future error factors can be identified as the quad should stabilize and move back to a stead state if displaced. Again, this stabilization is keen on having good factors as to not overshoot or compensate too quickly.

\subsection{CAD Drawings}
\subsubsection{Mechanical CAD}
The quad frame requires several mounts for the Zybo board, peripherals, and testing equipment. Physical support pieces were created using CAD software for each case and were, more specifically, a mount reflective IR balls, a base platform for securing the quad to a testing platform, and support beams for mounting the Zybo board to the chassis; all of these pieces can be seen in Figures \ref{fig:mount_ir}, \ref{fig:mount_test}, and \ref{fig:mount_adapter}. 

	\begin{figure}[H]
	\begin{center}
		\includegraphics[width=\textwidth]{mount_ir.png}
		\caption{\small CAD diagram for chassis' IR mount}
		\label{fig:mount_ir}
	\end{center}
	\end{figure}
	\vspace{-0.5cm}
	
	\begin{figure}[H]
	\begin{center}
		\includegraphics[width=\textwidth]{mount_test.png}
		\caption{\small CAD diagram for chassis' test platform}
		\label{fig:mount_test}
	\end{center}
	\end{figure}
	\vspace{-0.5cm}

	\begin{figure}[H]
	\begin{center}
		\includegraphics[width=\textwidth]{mount_adapter.png}
		\caption{\small CAD diagram for Zybo board mounting adapter}
		\label{fig:mount_adapter}
	\end{center}
	\end{figure}
	\vspace{-0.5cm}
\subsection{Implementation Issues}
The main issue the teams is focused on is the safety of the quad system when the batteries' power levels are too low. The team needs to ensure that all components function properly and safely which may slow or hinder progress of the building the system. Additionally, this project consists of many different components that need to be integrated well with one another, and problems can arise from different pieces and subsystems.


\section{Deliverables}
The team will deliver the assembled and working system to the client at the end of their second term. The final system will consist of the quad system with a mounted board, sensors, and receivers as well as the mixing, configuration, and communication software. Additionally, the team will relinquish the source code for base station, its data analytics, and documentation. 


\section{Closing Material}
\subsection{Client}
Distributed Sensing and Decision Making Laboratory, Iowa State University\\
Contact: Dr. Philip Jones III

\subsection{Team Info}
{\bf Physical Hardware, Power Management, and Controls} \\
\indent Joe Benedict (Electrical Engineering) - joeb@iastate.edu \\
\indent Ravi Nagaraju (Electrical Engineering) - nagaraju@iastate.edu \\\\
{\bf Peripheral } \\
\indent Tyler Kurtz (Electrical Engineering) - tkurtz@iastate.edu\\
\indent Jacob Rigdon (Computer Engineering) - jrigdon@iastate.edu\\
\indent Matt Vitale (Computer Engineering) - mvitale@iastate.edu\\\\
{\bf Software and Data Analytics} \\
\indent Adam Campbell (Software Engineering) - awjc@iastate.edu \\
\indent Paul Gerver (Computer Engineering) - pfgerver@iastate.edu\\\\
{\bf Advisor} \\
\indent Dr. Philip Jones III (Assistant Professor, Dept. of ECpE)

\end{document}