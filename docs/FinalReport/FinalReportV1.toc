\contentsline {section}{\numberline {1}Definition of Terms}{4}{section.1}
\contentsline {section}{\numberline {2}Abbreviations}{4}{section.2}
\contentsline {section}{\numberline {3}Executive Summary}{5}{section.3}
\contentsline {section}{\numberline {4}System Design}{5}{section.4}
\contentsline {subsection}{\numberline {4.1}High-Level System Description}{5}{subsection.4.1}
\contentsline {subsubsection}{\numberline {4.1.1}Quadcopter Vehicle}{5}{subsubsection.4.1.1}
\contentsline {subsubsection}{\numberline {4.1.2}Ground Station}{6}{subsubsection.4.1.2}
\contentsline {subsubsection}{\numberline {4.1.3}RC Transmitter}{6}{subsubsection.4.1.3}
\contentsline {subsubsection}{\numberline {4.1.4}Camera Environment}{6}{subsubsection.4.1.4}
\contentsline {subsection}{\numberline {4.2}Functional Decomposition}{6}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}System Requirements}{7}{subsection.4.3}
\contentsline {subsubsection}{\numberline {4.3.1}Functional}{7}{subsubsection.4.3.1}
\contentsline {subsubsection}{\numberline {4.3.2}Non-Functional}{8}{subsubsection.4.3.2}
\contentsline {subsection}{\numberline {4.4}System Analysis}{9}{subsection.4.4}
\contentsline {subsection}{\numberline {4.5}Operating Environment}{9}{subsection.4.5}
\contentsline {section}{\numberline {5}Detailed Design}{10}{section.5}
\contentsline {subsection}{\numberline {5.1}Hardware Specifications}{10}{subsection.5.1}
\contentsline {subsubsection}{\numberline {5.1.1}DJI FlameWheel F450}{10}{subsubsection.5.1.1}
\contentsline {subsubsection}{\numberline {5.1.2}ESCs}{11}{subsubsection.5.1.2}
\contentsline {subsubsection}{\numberline {5.1.3}DJI Brushless Motors}{12}{subsubsection.5.1.3}
\contentsline {subsubsection}{\numberline {5.1.4}Propeller Blades}{12}{subsubsection.5.1.4}
\contentsline {subsubsection}{\numberline {5.1.5}Diligent ZYBO Zynq-7000 Development Board}{13}{subsubsection.5.1.5}
\contentsline {subsubsection}{\numberline {5.1.6}Diligent PMOD-BT2 - Bluetooth Interface}{14}{subsubsection.5.1.6}
\contentsline {subsubsection}{\numberline {5.1.7}SparkFun MPU9150: 9 Degrees of Freedom}{15}{subsubsection.5.1.7}
\contentsline {subsubsection}{\numberline {5.1.8}SpekTrum DX6i RC Controller}{15}{subsubsection.5.1.8}
\contentsline {subsubsection}{\numberline {5.1.9}Voltage Regulator and Power Management}{16}{subsubsection.5.1.9}
\contentsline {subsubsection}{\numberline {5.1.10}OptiTrack IR Cameras}{19}{subsubsection.5.1.10}
\contentsline {subsection}{\numberline {5.2}CAD Drawings}{20}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}Zybo Board Software and Logic Specification}{22}{subsection.5.3}
\contentsline {subsubsection}{\numberline {5.3.1}Sensor Board}{22}{subsubsection.5.3.1}
\contentsline {subsubsection}{\numberline {5.3.2}Sensor Filtering}{23}{subsubsection.5.3.2}
\contentsline {subsubsection}{\numberline {5.3.3}PID Controller}{24}{subsubsection.5.3.3}
\contentsline {subsubsection}{\numberline {5.3.4}Bluetooth}{25}{subsubsection.5.3.4}
\contentsline {subsubsection}{\numberline {5.3.5}RC Receiver Recorder}{26}{subsubsection.5.3.5}
\contentsline {subsubsection}{\numberline {5.3.6}Control Mixer}{26}{subsubsection.5.3.6}
\contentsline {subsubsection}{\numberline {5.3.7}Motor Output}{27}{subsubsection.5.3.7}
\contentsline {subsection}{\numberline {5.4}Ground Station Software Specification}{28}{subsection.5.4}
\contentsline {subsubsection}{\numberline {5.4.1}Quadcopter Logging}{29}{subsubsection.5.4.1}
\contentsline {subsubsection}{\numberline {5.4.2}Command Terminal}{30}{subsubsection.5.4.2}
\contentsline {subsubsection}{\numberline {5.4.3}Camera System Data Pass-through}{30}{subsubsection.5.4.3}
\contentsline {subsubsection}{\numberline {5.4.4}MATLAB GUI}{31}{subsubsection.5.4.4}
\contentsline {subsection}{\numberline {5.5}Test Specification}{32}{subsection.5.5}
\contentsline {subsubsection}{\numberline {5.5.1}PID Tuning}{32}{subsubsection.5.5.1}
\contentsline {subsubsection}{\numberline {5.5.2}Software Testing}{34}{subsubsection.5.5.2}
\contentsline {subsubsection}{\numberline {5.5.3}Test results}{34}{subsubsection.5.5.3}
\contentsline {subsection}{\numberline {5.6}Implementation Details}{35}{subsection.5.6}
\contentsline {subsubsection}{\numberline {5.6.1}Hardware \& Software Safety}{36}{subsubsection.5.6.1}
\contentsline {subsubsection}{\numberline {5.6.2}Issues}{37}{subsubsection.5.6.2}
\contentsline {section}{\numberline {6}Deliverables}{38}{section.6}
\contentsline {section}{\numberline {7}Conclusion}{39}{section.7}
\contentsline {section}{\numberline {8}Closing Material}{39}{section.8}
\contentsline {subsection}{\numberline {8.1}Client}{39}{subsection.8.1}
\contentsline {subsection}{\numberline {8.2}Team Info}{39}{subsection.8.2}
\contentsline {section}{\numberline {A}Operations Manual}{40}{appendix.A}
\contentsline {section}{\numberline {B}Alternative Designs}{42}{appendix.B}
\contentsline {section}{\numberline {C}Considerations, Lessons Learned}{44}{appendix.C}
\contentsline {section}{\numberline {D}Code}{45}{appendix.D}
