function [headings, A] = parse_log(filename)
%PARSE_LOG: Parses log file with # commmented headers, plots data
%Inputs: filename of log
%Output: Matrix A of data
%TODO: Fix file parsing

%Check if file exists
if (~exist(filename,'file'))
    error(strcat(filename, ' does not exist'));
end

%Open file
FileID = fopen(filename, 'r');

string = fgetl(FileID);

%Test first line, if not formatted correctly, reject
if(size(regexp(string, '^#')) == 0)
    error(strcat(filename, ' is not properly formatted, and does not contain "#" headers'));
end

%Loop through header lines
while( regexp(string, '^#') == 1 )
    
    %Print out string and move on
    disp(string)
    old = string;
    string = fgetl(FileID);
    
end

%Obtain data headers
headings = strsplit(old);
headings{1} = strrep(headings{1},'#', '');

%Get all data into cells; convert cells to a single matrix
A = textscan(FileID, '%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f');
A = cell2mat(A);

figureCount = 0;
%Plot data points
for i = 1:size(headings,2)
    
    
    %create a new subplot if 2x2 is filled or doesn't exist
   if(mod(i,9) == 1)
       figTitle = sprintf('System Log Results %d', figureCount);
       figureCount = figureCount + 1;
       figure('name', figTitle);
   end
   
   subplot(3,3,mod(i-1,9)+1);
   plot(A(:,1), A(:,i));
   title(headings{i});
   xlabel(headings{1});
   ylabel(headings{i});
   
end

end %function
