#!/bin/sh -e
#
#Runs the MicroCART program and then opens matlab afterwards


#export QTDIR=$QTDIR:/usr/lib64/qt-3.3
#export QTINC=$QTINC:/usr/local/Trolltech/Qt-4.8.1/demos/shared:/usr/local/Trolltech/Qt-4.8.1/include/Qt:/usr/local/include
#export QT_IM_MODULE=xim
#export QTLIB=$QTLIB:/usr/local/Trolltech/Qt-4.8.1/lib:/opt/qtcreator-2.4.1/lib:/usr/local/lib64
#export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$QTLIB


pwd=$PWD
MICROCART_BASE="/local/ucart/Desktop/ground_station_copy"


MATLAB_FUNCTION_LOC="~/Documents/MicroCART_14_15/microcart/tasks/matlab_data_collection/scripts"

cd $MICROCART_BASE
#Start MicroCart Program
./MicroCART
cd $PWD


#TODAY=$(date +"%Y-%m-%d_%a")
TODAY=$(ls -rt $MICROCART_BASE/Logs | tail -1)
echo $TODAY


#Get latest file
SYSTEM_LOG="$(ls -rt $MICROCART_BASE/Logs/$TODAY | tail -1)"

echo syslog: $SYSTEM_LOG
echo "Opening MATLAB now..."

matlab -r "cd $MATLAB_FUNCTION_LOC; parse_log('$MICROCART_BASE/Logs/$TODAY/$SYSTEM_LOG'); cd $pwd" -nosplash 
