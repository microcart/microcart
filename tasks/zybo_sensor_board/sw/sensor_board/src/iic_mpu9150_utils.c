/**
 * IIC_MPU9150_UTILS.c
 *
 * Utility functions for using I2C on a Diligent Zybo board and
 * focused on the SparkFun MPU9150
 *
 * For function descriptions please see iic_mpu9150_utils.h
 *
 * Author: 	Paul Gerver (pfgerver@gmail.com)
 * Created: 01/20/2015
 */

#include <stdio.h>
#include <sleep.h>

#include "iic_mpu9150_utils.h"
#include "xbasic_types.h"
#include "xiicps.h"

XIicPs_Config* i2c_config;
XIicPs I2C0;

int initI2C0(){

	//Make sure CPU_1x clk is enabled for I2C controller
	Xuint16* aper_ctrl = (Xuint16*) IO_CLK_CONTROL_REG_ADDR;

	if(*aper_ctrl & 0x00040000){
		xil_printf("CPU_1x is set to I2C0\r\n");
	}

	else{
		xil_printf("CPU_1x is not set to I2C0..Setting now\r\n");
		*aper_ctrl |= 0x00040000;
	}


	// Look up
	i2c_config = XIicPs_LookupConfig(XPAR_PS7_I2C_0_DEVICE_ID);

	XStatus status = XIicPs_CfgInitialize(&I2C0, i2c_config, i2c_config->BaseAddress);

	// Check if initialization was successful
	if(status != XST_SUCCESS){
		printf("ERROR: Initializing I2C0\r\n");
		return -1;
	}

	// Reset the controller and set the clock to 400kHz
	XIicPs_Reset(&I2C0);
	XIicPs_SetSClk(&I2C0, 400000);


	return 0;
}

void startMPU9150(){

	// Device Reset & Wake up
	iic0Write(0x6B, 0x80);
	usleep(5000);

	// Set clock reference to Z Gyro
	iic0Write(0x6B, 0x03);

	// Configure Digital Low/High Pass filters (if needed)
	iic0Write(0x1A,0x04);

	// Configure Gyro to 2000dps, Accel. to +/-8G
	iic0Write(0x1B, 0x18);
	iic0Write(0x1C, 0x10);

	// Setup Mag
	// Enable I2C bypass for AUX I2C (Magnetometer)
	iic0Write(0x37, 0x02);


	// Initial Reads
	Xuint16 sensor_data[ACCEL_GYRO_READ_SIZE];
	int i;
	for(i=0; i < 20; ++i){
		iic0Read(sensor_data, ACCEL_GYRO_BASE_ADDR, (int) ACCEL_GYRO_READ_SIZE);
		usleep(1000);
	}
}

void stopMPU9150(){

	//Put MPU to sleep
	iic0Write(0x6B, 0b01000000);
}

void iic0Write(u8 register_addr, u8 data){

	u16 device_addr = MPU9150_DEVICE_ADDR;
	u8 buf[] = {register_addr, data};

	// Check if within register range
	if(register_addr < 0 || register_addr > 0x75){
		printf("ERROR: Cannot register address, 0x%x, out of bounds\r\n", register_addr);
		return;
	}

	if(register_addr <= 0x12){
		device_addr = MPU9150_COMPASS_ADDR;
	}



	XIicPs_MasterSendPolled(&I2C0, buf, 2, device_addr);

}

void iic0Read(u8* recv_buffer, u8 register_addr, int size){

	u16 device_addr = MPU9150_DEVICE_ADDR;
	u8 buf[] = {register_addr};

	// Check if within register range
	if(register_addr < 0 || register_addr > 0x75){
		printf("ERROR: Cannot register address, 0x%x, out of bounds\r\n", register_addr);
	}

	// Set device address to the if 0x00 <= register address <= 0x12
	if(register_addr <= 0x12){
		device_addr = MPU9150_COMPASS_ADDR;
	}


	XIicPs_MasterSendPolled(&I2C0, buf, 1, device_addr);
	XIicPs_MasterRecvPolled(&I2C0, recv_buffer,size,device_addr);
}




// Old code
/*
void iic0_write(Xuint16 reg_addr, Xuint8 data){

    Xuint16* iic_status =           IIC0_STATUS_REG_ADDR;
	Xuint16* iic_addr =             IIC0_SLAVE_ADDR_REG;
	Xuint16* iic_data =             IIC0_DATA_REG_ADDR;
	Xuint8* iic_transfer_size =     IIC0_TRANFER_SIZE_REG_ADDR;
	Xuint16* iic_intr_status =      IIC0_INTR_STATUS_REG_ADDR;


	iic0_config_ctrl_to_transmit();


	//Put data into fifo
	*iic_data = reg_addr;
	*iic_data = data;


	//Write slave addr (starts transaction)

	if(reg_addr < 0x12){
		*iic_addr = MPU9150_COMPASS_ADDR;
	}
	else
		*iic_addr = MPU9150_DEVICE_ADDR;

	int i=0;

	for(i=0; i < 2; ++i){
		while((*iic_status & 0x0040) != 0);
	}


	//Poll Interrupt to see when transfer complete
	while((*iic_intr_status & 0x0001) == 0);

	for(i=0; i < 2; ++i){
		while((*iic_status & 0x0040) != 0){
			if(*iic_intr_status & WRITE_INTR_MASK){
				//Interrupt occured, exit
				printf("ERROR: Interrupt occurred in iic0_write\n\r");
				return;
			}
		}
	}


	//Poll Interrupt to see when transfer complete
	while((*iic_intr_status & 0x0001) == 0){
		if(*iic_intr_status & WRITE_INTR_MASK){
			//Interrupt occured, exit
			printf("ERROR: Interrupt occurred in iic0_write\n\r");
			return;
		}


	}

}


Xuint8 iic0_read(Xuint16 reg_addr){

    Xuint16* iic_status =           IIC0_STATUS_REG_ADDR;
	Xuint16* iic_addr =             IIC0_SLAVE_ADDR_REG;
	Xuint16* iic_data =             IIC0_DATA_REG_ADDR;
	Xuint8* iic_transfer_size =     IIC0_TRANFER_SIZE_REG_ADDR;
	Xuint16* iic_intr_status =      IIC0_INTR_STATUS_REG_ADDR;


	iic0_config_ctrl_to_transmit();
	iic0_clear_intr_status();

	//Set number of bytes in transfer size reg
	*iic_transfer_size = 1;

	*iic_data = reg_addr;

	if(reg_addr < 0x12){
		*iic_addr = MPU9150_COMPASS_ADDR;
	}
	else
		*iic_addr = MPU9150_DEVICE_ADDR;

	//Poll TXDV in status register to see when FIFO is clear
	while((*iic_status & 0x0040) == 0);

	//Poll Interrupt to see when transfer complete
	while((*iic_intr_status & 0x0001) == 0);

	//Poll TXDV in status register to see when FIFO is clear
	while((*iic_status & 0x0040) == 0){

		if(*iic_intr_status & WRITE_INTR_MASK){
			printf("ERROR0: Interrupt occurred in iic0_read\n\r");
			//Interrupt occured, exit
			return NULL;
		}
	}

	//Poll Interrupt to see when transfer complete
	while((*iic_intr_status & 0x0001) == 0){
		//Double check that there wasn't an error
		if(*iic_intr_status & WRITE_INTR_MASK){
			printf("ERROR1: Interrupt occurred in iic0_read\n\r");
			return NULL;
		}
	}



	//Now get that data
	iic0_config_ctrl_to_receive();

	*iic_transfer_size = 2;

	//Start Receiving transaction
	if(reg_addr < 0x12){
		*iic_addr = MPU9150_COMPASS_ADDR;
	}
	else
		*iic_addr = MPU9150_DEVICE_ADDR;

	//Poll RXDV in status register
	while((*iic_status & 0x0020) == 0);

	//Now get that data
	iic0_config_ctrl_to_receive();

	*iic_transfer_size = 2;

	//Start Receiving transaction
	*iic_addr = MPU9150_DEVICE_ADDR;

	//Poll RXDV in status register
	while((*iic_status & 0x0020) == 0){

		if(*iic_intr_status & READ_INTR_MASK){
			printf("ERROR2: Interrupt occurred in iic0_read\n\r");
			//Interrupt occured, exit
			return NULL;
		}
	}


	//return data received
	return *iic_data;
}

int iic0_read_bytes(Xuint8* dst, Xuint16 reg_addr, int bytes){

    Xuint16* iic_status =           IIC0_STATUS_REG_ADDR;
	Xuint16* iic_addr =             IIC0_SLAVE_ADDR_REG;
	Xuint16* iic_data =             IIC0_DATA_REG_ADDR;
	Xuint8* iic_transfer_size =     IIC0_TRANFER_SIZE_REG_ADDR;
	Xuint16* iic_intr_status =      IIC0_INTR_STATUS_REG_ADDR;


	iic0_config_ctrl_to_transmit();

	//Set number of bytes in transfer size reg
	*iic_transfer_size = 1;

	*iic_data = reg_addr;


	if(reg_addr < 0x12){
		*iic_addr = MPU9150_COMPASS_ADDR;
	}
	else
		*iic_addr = MPU9150_DEVICE_ADDR;


	// Debug
	int status   = *iic_intr_status;
	int arb_lost = status & 0x200;
	int rx_unf   = status & 0x80;
	int tx_ovf   = status & 0x40;
	int rx_ovf   = status & 0x20;
	int slv_rdy  = status & 0x10;
	int time_out = status & 0x08;
	int not_ack  = status & 0x04;
	int more_dat = status & 0x02;
	int complete = status & 0x01;
	char debug[100];

	int countDebug = 0;

	//Poll TXDV in status register to see when FIFO is clear
	while((*iic_status & 0x0040) == 0) {
		if(*iic_intr_status & WRITE_INTR_MASK){
			sprintf(debug, "Error 0:\t\t Type: %x\r\n", *iic_intr_status & WRITE_INTR_MASK);
			//printf("Error 0:\t\t Type: %x\r\n", *iic_intr_status & WRITE_INTR_MASK);
			//Interrupt occurred, exit
			printf(debug);
			return -1;
		}

		if(countDebug++ > 1000000) {
			printf("count exceeded iic0_read_bytes 0\r\n");
			return -1;
		}
	}


	//Poll Interrupt to see when transfer complete
	while((*iic_intr_status & 0x0001) == 0){
		if(*iic_intr_status & WRITE_INTR_MASK){
			sprintf(debug, "Error 1:\t\t Type: %x\r\n", *iic_intr_status & WRITE_INTR_MASK);
			//printf("Error 1:\t\t Type: %x\r\n", *iic_intr_status & WRITE_INTR_MASK);
			//Interrupt occurred, exit
			printf(debug);
			//uart0_sendStr("ERROR1: Interrupt occurred in iic0_read_bytes\r\n");
			return -1;
		}
		if(countDebug++ > 100000) {
			printf("count exceeded iic0_read_bytes 1\r\n");
			return -1;
		}
	}



	//Now get that data
	iic0_config_ctrl_to_receive();

	*iic_transfer_size = bytes;

	//Start Receiving transaction
	*iic_addr = MPU9150_DEVICE_ADDR;

	int count =0;

	//Poll RXDV in status register
	while(*iic_transfer_size > 0){
		while((*iic_status & 0x0020) == 0){
			if(*iic_intr_status & READ_INTR_MASK){
				sprintf(debug, "Error 2:\t\t Type: %x\r\n", *iic_intr_status & READ_INTR_MASK);
				//printf("Error 2:\t\t Type: %x\r\n", *iic_intr_status & READ_INTR_MASK);
				//Interrupt occurred, exit
				printf(debug);
				//uart0_sendStr("ERROR2: Interrupt occurred in iic0_read_bytes\r\n");
				return -1;
			}
			if(countDebug++ > 100000) {
				printf("count exceeded\r\n read bytes 2");
				//printf("count exceeded read bytes 2");
				return -1;
			}
		}

		dst[count] = *iic_data;
		count++;
	}
/*
	//Poll TXDV in status register to see when FIFO is clear
	while((*iic_status & 0x0040) == 0);

	//Poll Interrupt to see when transfer complete
	while((*iic_intr_status & 0x0001) == 0);



	//Now get that data
	iic0_config_ctrl_to_receive();

	*iic_transfer_size = bytes;

	//Start Receiving transaction
	if(reg_addr < 0x12){
		*iic_addr = MPU9150_COMPASS_ADDR;
	}
	else
		*iic_addr = MPU9150_DEVICE_ADDR;

	int count =0;
	//Poll RXDV in status register
	while(*iic_transfer_size > 0){
		while((*iic_status & 0x0020) == 0);

		dst[count] = *iic_data;
		count++;
	}

	return 0;
}


void init_iic0(){
	// initialize hardware controller on Zybo
	iic0_hw_init();

	// configure controller to transmit data first
	iic0_config_ctrl_to_transmit();
}

void iic0_hw_init(){

	Xuint32* slcr_iic_reset = IIC_SYSTEM_CONTROLLER_RESET_REG_ADDR;

	//Assert and de-assert reset on I2C controller
	*slcr_iic_reset |= 0x00000010;
	*slcr_iic_reset &= ~(0x00000010);

	xil_printf("I2C0 Controller Reset\r\n");


	//Make sure CPU_1x clk is enabled for I2C controller
	Xuint16* aper_ctrl = (Xuint16*) IO_CLK_CONTROL_REG_ADDR;

	if(*aper_ctrl & 0x00040000){
		xil_printf("CPU_1x is set to I2C0\r\n");
	}

	else{
		xil_printf("CPU_1x is not set to I2C0..Setting now\r\n");
		*aper_ctrl |= 0x00040000;
	}
}


void iic0_clear_intr_status(){

	//Clear interrupt statuses
	Xuint16* iic_intr_status = IIC0_INTR_STATUS_REG_ADDR;

	Xuint16 read = *iic_intr_status;
	*iic_intr_status = read;

}


void iic0_config_ctrl_to_transmit(){
    Xuint16* iic_ctrl = IIC0_CONTROL_REG_ADDR;
    Xuint8* iic_timeout = IIC0_TIMEOUT_REG_ADDR;

    //Set CLK divisors in Control Register
   //Here, we're setting divsor a = 0, b=60
   *iic_ctrl = 0x0C00;

   //Set CLR_FIFO, ACKEN, Normal Addressing, MS, and WR
   *iic_ctrl |= 0x004E;

   *iic_timeout = 0xFF;

   iic0_clear_intr_status();
}

void iic0_config_ctrl_to_receive(){
    Xuint16* iic_ctrl = IIC0_CONTROL_REG_ADDR;
    Xuint8* iic_timeout =  IIC0_TIMEOUT_REG_ADDR;

    //Set CLK divisors in Control Register
   //Here, we're setting divsor a = 0, b=60
   *iic_ctrl = 0x0C00;

   //Set CLR_FIFO, ACKEN, Normal Addressing, MS, and WR
   *iic_ctrl |= 0x004F;

   *iic_timeout = 0xFF;

   iic0_clear_intr_status();
}
*/
