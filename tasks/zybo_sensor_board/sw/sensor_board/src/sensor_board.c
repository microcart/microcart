/**
 * Sensor_board.c
 *
 * Program to test reading Gyroscope and Accelerometer data from
 * a Sparkfun MPU9150 device through a Diligent ZyBo Board
 *
 * Zybo Board configuration:
 * 		Here is a list of steps to configure the Zybo Board for this test
 * 		1) Create a project in the Xilinx Platform Studio targeted for the Zybo Board
 * 		2) In system view, click the green I/O Peripheral Box
 * 		3) Enable I2C0 and under IO set it to MIO 10..11
 * 		4) In the MIO Configuration side, enable "Show I/O Standard Options"
 * 		5) Set the I/O Type to LVCMOS1.8 for MIO10 and 11
 * 		6) VERY IMPORTANT: "enable" pullup for 10 and 11
 * 		7) Synthesize the project and you're good to go
 *
 * 		Another example can be seen in Section 20.5.1 in the Zybo TRM
 * 		PLEASE READ CHAPTER 20 in the Zybo TRM to understand the hw and sw aspects on I2C on the Zybo Board
 *
 *
 * MPU9150 configuration:
 *
 * 		To the Zybo Board PMOD JF (MIO10 and 11):
 * 			Wire SDA to JF3
 *			SCL to JF2
 *			GND to JF5 or JF11
 *			VCC to JF6 or JF12
 *
 *
 * 	//MPU9150 Transaction (Single-byte read)
	// S SA+W 	 RA	  S SA+R	  	 	NACK P
	//		 ACK   ACK 	      ACK Data
	//S - start
	//SA - I2C Slave Address (0b110100x)
	//RA - Address to read on slave
	//P - Stop
 *
 *  Please refer to the MPU9150 Datasheet for write transactions
 */
#include "xparameters.h"
#include "xiicps.h"
#include "xil_printf.h"
#include "xbasic_types.h"
#include "xtime_l.h"
#include "iic_mpu9150_utils.h"
#include <math.h>
#include <stdio.h>


XIicPs_Config* i2c_config;
XIicPs I2C0;

double magX_correction = -1, magY_correction, magZ_correction;

//Please see the MPU9150 Register Map
//Magnetometer Status and data are put into registers after gyro
#define DATA_BASE_ADDR 0x3B
#define DATA_READ_SIZE 21


// Gyro is configured for +/-2000dps
// Sensitivity gain is based off MPU9150 datasheet (pg. 11)
#define GYRO_SENS 16.4
#define GYROX_BIAS 1.22
#define GYROY_BIAS 1.22
#define GYROZ_BIAS -1.22


//Gyro, accelerometer, and magnetometer data structure
//Used for reading an instance of the sensor data
typedef struct {

	double accel_xAngle; //In degrees
	double accel_yAngle;

	double ax; //In Gs
	double ay;
	double az;

	double gyro_xVel; // In degrees per second
	double gyro_yVel;
	double gyro_zVel;

	double mag_x; //Magnetic north: ~50 uT
	double mag_y;
	double mag_z;

}gam_t;


void ReadGAM(gam_t* gam);

void CalcMagSensitivity();
void ReadMag(gam_t* gam);
void ReadGyroAccel(gam_t* gam);



int main()
{

	/*Variable declarations*/

	int i;
	double compX, compY, loop_period = 0; //Complementary Angles (Pitch and Roll angles)
	gam_t gam = {};

    //Xilinx timing
    XTime loopB, loopA,before, after;


	printf("\nMAIN: MPU9150 Sensor Board Example: Start\n\r");


    init_platform();


    initI2C0();
    printf("MAIN: IIC0 configurations complete\r\n");
    startMPU9150();
    printf("MAIN: MPU9150 Started\r\n");


    //Time a burst read of all data
    //XTime_GetTime(&before);
    //iic0Read(accel_gyro, ACCEL_GYRO_BASE_ADDR, ACCEL_GYRO_READ_SIZE);
    //XTime_GetTime(&after);
    //printf("MAIN: Time to get accel,gyro, and mag data: %0.3E seconds\r\n", (after-before) / (double) COUNTS_PER_SECOND);


    compX = compY = 0;


    XTime_GetTime(&loopB);


    double heading; // in degrees

    //MAG: http://www51.honeywell.com/aero/common/documents/myaerospacecatalog-documents/Defense_Brochures-documents/Magnetic__Literature_Application_notes-documents/AN203_Compass_Heading_Using_Magnetometers.pdf
    /*while(1){

    	ReadMag(&gam);


    	if(gam.mag_y < 5 && gam.mag_y > -5 && gam.mag_x < 0){
    		heading = 180.0;
    	}
    	else if(gam.mag_y < 5 && gam.mag_y > -5 && gam.mag_x > 0){
    		heading = 0.0;
    	}
    	else if(gam.mag_y > 0){
    		heading = 90 - atan2(gam.mag_x, gam.mag_y) * 180.0/3.14159265;
    	}
    	else if (gam.mag_y < 0){
    		heading = 270 - atan2(gam.mag_x, gam.mag_y) * 180.0/3.14159265;
    	}

	    while (heading < 0) heading += 360;
	    while (heading > 360) heading -= 360;


	    printf("Heading: %8.3lf M: %8.3lf %8.3lf %8.3lf\r\n", heading, gam.mag_x, gam.mag_y, gam.mag_z);
    }*/

    //Loop through multiple reads and grab GAM data
	for(i=0; i < 40000; ++i){
		XTime_GetTime(&before);


		ReadGyroAccel(&gam);

		if(i % 100 == 0)
			ReadMag(&gam);




		compX = 0.98*(compX + gam.gyro_xVel * loop_period) + 0.02*gam.accel_xAngle;
		compY = 0.98*(compY + gam.gyro_yVel * loop_period) + 0.02*gam.accel_yAngle;


		//Filter? http://www.i2cdevlib.com/forums/topic/148-unusable-raw-magnetometer-data/
	  	double heading = atan2(gam.mag_x, gam.mag_y) * 180.0/3.14159265 + 180;
	    while (heading < 0) heading += 360;
	    while (heading > 360) heading -= 360;


	    //printf("Heading: %8.3lf\r\n", heading);
		//printf("G: %8.3lf %8.3lf %8.3lf\tA: %8.3lf %8.3lf\tM: %8.3lf %8.3lf %8.3lf\r\n", gam.gyro_xVel, gam.gyro_yVel, gam.gyro_zVel, gam.accel_xAngle, gam.accel_yAngle, gam.mag_x, gam.mag_y, gam.mag_z);
		//printf("Pitch Angle: %8.3lf\tRoll Angle: %8.3lf\tMag X: %8.3lf Y: %8.3lf Z: %8.3lf\r\n", compX, compY, gam.mag_x, gam.mag_y, gam.mag_z);
		printf("Pitch Angle: %8.3lf\tRoll Angle: %8.3lf\r\n", compX, compY);


		XTime_GetTime(&after);
		loop_period = (after-before) / (double) (COUNTS_PER_SECOND);

	}
	XTime_GetTime(&loopA);

	double time = (loopB-loopA) / (double) COUNTS_PER_SECOND;
	printf("Iterations: %5d TotalTime: %0.3E AvgLoopTime: %0.3E\r\n", i, time, time/i);

	stopMPU9150();

    return 0;
}




void ReadGAM(gam_t* gam){
	ReadGyroAccel(gam);
	ReadMag(gam);
}

void ReadGyroAccel(gam_t* gam){

	u8 sensor_data[ACCEL_GYRO_READ_SIZE];

	Xint16 accel_x, accel_y, accel_z;
	Xint16 gyro_x, gyro_y, gyro_z;

	iic0Read(sensor_data, ACCEL_GYRO_BASE_ADDR, ACCEL_GYRO_READ_SIZE);
	usleep(1000);

	//Calculate accelerometer data
	gam->ax = accel_x = (sensor_data[ACC_X_H] << 8 | sensor_data[ACC_X_L]);
	gam->ay = accel_y = (sensor_data[ACC_Y_H] << 8 | sensor_data[ACC_Y_L]);
	gam->az = accel_z = (sensor_data[ACC_Z_H] << 8 | sensor_data[ACC_Z_L]);


	//Get X and Y angles
	gam->accel_yAngle = -atan(accel_x / sqrt(accel_y*accel_y + accel_z*accel_z)) * RAD_TO_DEG;
	gam->accel_xAngle = atan(accel_y / sqrt(accel_x*accel_x + accel_z*accel_z)) * RAD_TO_DEG;


	//Format gyro data correctly
	gyro_x = (sensor_data[GYR_X_H] << 8) | sensor_data[GYR_X_L]; //* G_GAIN;
	gyro_y = (sensor_data[GYR_Y_H] << 8) | sensor_data[GYR_Y_L];// * G_GAIN;
	gyro_z = (sensor_data[GYR_Z_H] << 8) | sensor_data[GYR_Z_L];// * G_GAIN;

	//Get the number of degrees per second
	gam->gyro_xVel = (gyro_x / GYRO_SENS) + GYROX_BIAS;
	gam->gyro_yVel = (gyro_y / GYRO_SENS) + GYROY_BIAS;
	gam->gyro_zVel = (gyro_z / GYRO_SENS) + GYROZ_BIAS;

}

void CalcMagSensitivity(){

	u8 buf[3];
	u8 ASAX, ASAY, ASAZ;

	// Quickly read from the factory ROM to get correction coefficents
	iic0Write(0x0A, 0x0F);
	usleep(10000);

	// Read raw adjustment values
	iic0Read(buf, 0x10,3);
	ASAX = buf[0];
	ASAY = buf[1];
	ASAZ = buf[2];

	// Set the correction coefficients
	magX_correction = (ASAX-128)*0.5/128 + 1;
	magY_correction = (ASAY-128)*0.5/128 + 1;
	magZ_correction = (ASAZ-128)*0.5/128 + 1;
}


void ReadMag(gam_t* gam){

	u8 mag_data[6];
	Xint16 raw_magX, raw_magY, raw_magZ;

	// Grab calibrations if not done already
	if(magX_correction == -1){
		CalcMagSensitivity();
	}

	// Set Mag to single read mode
	iic0Write(0x0A, 0x01);
	usleep(10000);
	mag_data[0] = 0;

	// Keep checking if data is ready before reading new mag data
	while(mag_data[0] == 0x00){
		iic0Read(mag_data, 0x02, 1);
	}

	// Get mag data
	iic0Read(mag_data, 0x03, 6);

	raw_magX = (mag_data[1] << 8) | mag_data[0];
	raw_magY = (mag_data[3] << 8) | mag_data[2];
	raw_magZ = (mag_data[5] << 8) | mag_data[4];

	// Set magnetometer data to output
	gam->mag_x = raw_magX * magX_correction;
	gam->mag_y = raw_magY * magY_correction;
	gam->mag_z = raw_magZ * magZ_correction;

}
