################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/iic_mpu9150_utils.c \
../src/platform.c \
../src/sensor_board.c 

LD_SRCS += \
../src/lscript.ld 

OBJS += \
./src/iic_mpu9150_utils.o \
./src/platform.o \
./src/sensor_board.o 

C_DEPS += \
./src/iic_mpu9150_utils.d \
./src/platform.d \
./src/sensor_board.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: ARM gcc compiler'
	arm-xilinx-eabi-gcc -Wall -O0 -g3 -c -fmessage-length=0 -I../../sensor_board_bsp/ps7_cortexa9_0/include -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


