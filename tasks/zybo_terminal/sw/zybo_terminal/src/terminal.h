/*
 * zybo_terminal.h
 *
 *  Created on: Sep 24, 2014
 *      Author: Adam
 */

#ifndef ZYBO_TERMINAL_H_
#define ZYBO_TERMINAL_H_

#include "stringBuilder.h"

#define NUM_TERMINAL_LINES_TO_CLEAR 8

void receiveCommand(stringBuilder_t* sb);

void clearTerminal();

int runTerminal(int (*)(char*));

#endif /* ZYBO_TERMINAL_H_ */
