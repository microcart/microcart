/*
 * stringBuilder_test.c
 *
 *  Created on: Sep 24, 2014
 *      Author: Adam
 */

#include <stdio.h>
#include <stdlib.h>
#include "stringBuilder.h"

#include "platform.h"


#if 0

int main(int argc, char *argv[]) {
	init_platform();
printf("HI\r\n");
	stringBuilder_t* sb = stringBuilder_create();
	{
		printf("1SB: %s\r\n", sb->buf); // should print "Why, hello there!"
		sb->addChar(sb, 'i');
		printf("2SB: %s\r\n", sb->buf); // should print "Why, hello there!"
		sb->addStr(sb, " hllo");
		printf("3SB: %s\r\n", sb->buf); // should print "Why, hello there!"
		sb->addCharAt(sb, 'h', 0);
		printf("4SB: %s\r\n", sb->buf); // should print "Why, hello there!"
		sb->addCharAt(sb, 'e', 4);
		printf("5SB: %s\r\n", sb->buf); // should print "Why, hello there!"
		sb->addStr(sb, " there");
		printf("6SB: %s\r\n", sb->buf); // should print "Why, hello there!"
		sb->addCharAt(sb, '!', sb->length);
		printf("7SB: %s\r\n", sb->buf); // should print "Why, hello there!"
		sb->addCharAt(sb, ',', 2);
		printf("8SB: %s\r\n", sb->buf); // should print "Why, hello there!"
		sb->removeCharAt(sb, 0);
		printf("9SB: %s\r\n", sb->buf); // should print "Why, hello there!"
		sb->removeCharAt(sb, 0);
		printf("10SB: %s\r\n", sb->buf); // should print "Why, hello there!"
		sb->addStrAt(sb, "Why", 0);
		printf("11SB: %s\r\n", sb->buf); // should print "Why, hello there!"

		printf("ALL DONE\r\n");
	}
	stringBuilder_free(sb);
	return 0;
}

#endif
