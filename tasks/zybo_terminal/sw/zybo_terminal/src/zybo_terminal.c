/*
 * run_zybo_terminal.c
 *
 *  Created on: Oct 21, 2014
 *      Author: Adam
 */

#include "terminal.h"
#include "commandproc.h"
#include "platform.h"

int main(int argc, char *argv[]) {
	init_platform();

	runTerminal(processCommand);

	return 0;
}

