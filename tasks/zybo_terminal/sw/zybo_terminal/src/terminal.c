/*
 * zybo_terminal.c: simple terminal application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include <stdio.h>
#include "stringBuilder.h"
#include "terminal.h"

void print(char *str);

int alive = 1;

stringBuilder_t* sbPrev;


void clearTerminal() {
	int i;
	for(i=0; i < NUM_TERMINAL_LINES_TO_CLEAR; i++) {
		printf("\r\n");
	}
}

int runTerminal(int (*processCommand)(char*)) {
	// Turn off buffering
	setvbuf(stdin, NULL, _IONBF, 0);
	setvbuf(stdout, NULL, _IONBF, 0);

	clearTerminal();

	printf("===== ZYBOTERM (TM) =====\r\n");
	printf("\r\nWelcome!\r\n");

	sbPrev = stringBuilder_create();
	stringBuilder_t* sb = stringBuilder_create();
	{
		do {
			receiveCommand(sb);
			if (alive) {
				int ret = processCommand(sb->buf);
				if (ret) {
					alive = 0;
				}

				sb->clear(sb);
			}
		} while (alive);
	}
	stringBuilder_free(sb);
	stringBuilder_free(sbPrev);

	return 0;
}

void receiveCommand(stringBuilder_t* sb) {
	printf("\r\n$> ");

	while (1) {
		char c;

		int numRead = scanf("%c", &c);
		if (numRead != 1) {
			printf("Error reading..");
			return;
		}

		// hacky way of detecting up arrow ("^[A"), will cause problems with ESC
		if (c == 27) {
			c = getc(stdin);
			c = getc(stdin);
			c = getc(stdin);
			c = getc(stdin);

			if (c == 65)  {
				c = getc(stdin);

				printf("%s", sbPrev->buf);
				sb->clear(sb);
				sb->addStr(sb, sbPrev->buf);
			}
		} else {
			printf("%c", c);
		}
#if DEBUG
		printf("CHAR READ: %d, %d\r\n", numRead, c);
#endif

		// Backspace
		if (c == '\b' || c == 127) {
			if (sb->length > 0) {
				sb->removeCharAt(sb, sb->length - 1);
			}
		}

		// Stop reading at newline
		else if (c == '\n' || c == '\r') {
#if DEBUG
			printf("\r\n%s read\r\n", sb->buf);
#endif
			// Save it if string not empty
			if (sb->buf[0]) {
				sbPrev->clear(sbPrev);
				sbPrev->addStr(sbPrev, sb->buf);
			}
			return;
		}

		// Else add as normal
		else if (c) {
			sb->addChar(sb, c);
		}
	}
}
