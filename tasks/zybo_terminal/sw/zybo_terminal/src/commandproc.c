/*
 * commandproc.c
 *
 *  Created on: Sep 24, 2014
 *      Author: Adam
 */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "commandproc.h"
#include "platform.h"
#include "xparameters.h"
#include "phrases.h"
#include "sleep.h"
#include "terminal.h"

int processCommand(char* cmd) {
	int length = strlen(cmd);
	if (length >= 0) {
		if (strncmp(cmd, "hello", 5) == 0) {
			printf("\r\nHello there!");
		} else if (strncmp(cmd, "who are you", 11) == 0) {
			printf("\r\nI am the muffin man");
		} else if (strcmp(cmd, "led") == 0 || strncmp(cmd, "led ", 4) == 0) {
			int count;
			int numRead = sscanf(cmd + 3, "%d", &count);
			if (numRead != 1) {
				count = 64;
				printf("\r\n[Using default count of %d]", count);
			}

			printf("\r\nFlashing LEDs...\r\n");
			int num = 1;
			while (count--) {
				*((int*) XPAR_LEDS_BASEADDR) = num & 0x0F;
				num++;
				usleep(1e5);
			}
			printf("Done!");
		} else if (strncmp(cmd, "btn", 3) == 0) {
			int btns = *((int*) XPAR_BTNS_BASEADDR);
			printf("\r\nButtons = %x", btns);
		} else if (strncmp(cmd, "talk", 4) == 0) {
			int num = rand() % NUM_PHRASES;
			printf("\r\n%s", phrases[num]);
		} else if (strncmp(cmd, "ledbtn", 6) == 0){
			int btns = *((int*) XPAR_BTNS_BASEADDR);
			*((int*) XPAR_LEDS_BASEADDR) = btns;
		} else if (strncmp(cmd, "quit", 4) == 0 ||
				   strncmp(cmd, "exit", 4) == 0) {

			printf("\r\n\r\nGoodbye!\r\n");
			return 1;
		}
	}

	return 0;
}
