/*
 * commandproc.h
 *
 *  Created on: Sep 24, 2014
 *      Author: Adam
 */

#ifndef COMMANDPROC_H_
#define COMMANDPROC_H_

int processCommand(char*);

#endif /* COMMANDPROC_H_ */
