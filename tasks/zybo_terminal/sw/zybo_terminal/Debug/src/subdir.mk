################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/commandproc.c \
../src/platform.c \
../src/stringBuilder.c \
../src/stringBuilder_test.c \
../src/terminal.c \
../src/zybo_terminal.c 

LD_SRCS += \
../src/lscript.ld 

OBJS += \
./src/commandproc.o \
./src/platform.o \
./src/stringBuilder.o \
./src/stringBuilder_test.o \
./src/terminal.o \
./src/zybo_terminal.o 

C_DEPS += \
./src/commandproc.d \
./src/platform.d \
./src/stringBuilder.d \
./src/stringBuilder_test.d \
./src/terminal.d \
./src/zybo_terminal.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: ARM gcc compiler'
	arm-xilinx-eabi-gcc -Wall -O0 -g3 -c -fmessage-length=0 -I../../zybo_terminal_bsp/ps7_cortexa9_0/include -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


