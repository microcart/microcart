##############################################################################
## Filename:          /local/ucart/microcart/tasks/Quad_PID/system/drivers/pwm_signal_out_wkillswitch_v1_00_a/data/pwm_signal_out_wkillswitch_v2_1_0.tcl
## Description:       Microprocess Driver Command (tcl)
## Date:              Fri Apr 17 15:01:50 2015 (by Create and Import Peripheral Wizard)
##############################################################################

#uses "xillib.tcl"

proc generate {drv_handle} {
  xdefine_include_file $drv_handle "xparameters.h" "pwm_signal_out_wkillswitch" "NUM_INSTANCES" "DEVICE_ID" "C_BASEADDR" "C_HIGHADDR" 
}
