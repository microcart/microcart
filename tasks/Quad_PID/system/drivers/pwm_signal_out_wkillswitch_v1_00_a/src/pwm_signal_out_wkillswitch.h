/*****************************************************************************
* Filename:          /local/ucart/microcart/tasks/Quad_PID/system/drivers/pwm_signal_out_wkillswitch_v1_00_a/src/pwm_signal_out_wkillswitch.h
* Version:           1.00.a
* Description:       pwm_signal_out_wkillswitch Driver Header File
* Date:              Fri Apr 17 15:01:50 2015 (by Create and Import Peripheral Wizard)
*****************************************************************************/

#ifndef PWM_SIGNAL_OUT_WKILLSWITCH_H
#define PWM_SIGNAL_OUT_WKILLSWITCH_H

/***************************** Include Files *******************************/

#include "xbasic_types.h"
#include "xstatus.h"
#include "xil_io.h"

/************************** Constant Definitions ***************************/


/**
 * User Logic Slave Space Offsets
 * -- SLV_REG0 : user logic slave module register 0
 * -- SLV_REG1 : user logic slave module register 1
 */
#define PWM_SIGNAL_OUT_WKILLSWITCH_USER_SLV_SPACE_OFFSET (0x00000000)
#define PWM_SIGNAL_OUT_WKILLSWITCH_SLV_REG0_OFFSET (PWM_SIGNAL_OUT_WKILLSWITCH_USER_SLV_SPACE_OFFSET + 0x00000000)
#define PWM_SIGNAL_OUT_WKILLSWITCH_SLV_REG1_OFFSET (PWM_SIGNAL_OUT_WKILLSWITCH_USER_SLV_SPACE_OFFSET + 0x00000004)

/**
 * Software Reset Space Register Offsets
 * -- RST : software reset register
 */
#define PWM_SIGNAL_OUT_WKILLSWITCH_SOFT_RST_SPACE_OFFSET (0x00000100)
#define PWM_SIGNAL_OUT_WKILLSWITCH_RST_REG_OFFSET (PWM_SIGNAL_OUT_WKILLSWITCH_SOFT_RST_SPACE_OFFSET + 0x00000000)

/**
 * Software Reset Masks
 * -- SOFT_RESET : software reset
 */
#define SOFT_RESET (0x0000000A)

/**************************** Type Definitions *****************************/


/***************** Macros (Inline Functions) Definitions *******************/

/**
 *
 * Write a value to a PWM_SIGNAL_OUT_WKILLSWITCH register. A 32 bit write is performed.
 * If the component is implemented in a smaller width, only the least
 * significant data is written.
 *
 * @param   BaseAddress is the base address of the PWM_SIGNAL_OUT_WKILLSWITCH device.
 * @param   RegOffset is the register offset from the base to write to.
 * @param   Data is the data written to the register.
 *
 * @return  None.
 *
 * @note
 * C-style signature:
 * 	void PWM_SIGNAL_OUT_WKILLSWITCH_mWriteReg(Xuint32 BaseAddress, unsigned RegOffset, Xuint32 Data)
 *
 */
#define PWM_SIGNAL_OUT_WKILLSWITCH_mWriteReg(BaseAddress, RegOffset, Data) \
 	Xil_Out32((BaseAddress) + (RegOffset), (Xuint32)(Data))

/**
 *
 * Read a value from a PWM_SIGNAL_OUT_WKILLSWITCH register. A 32 bit read is performed.
 * If the component is implemented in a smaller width, only the least
 * significant data is read from the register. The most significant data
 * will be read as 0.
 *
 * @param   BaseAddress is the base address of the PWM_SIGNAL_OUT_WKILLSWITCH device.
 * @param   RegOffset is the register offset from the base to write to.
 *
 * @return  Data is the data from the register.
 *
 * @note
 * C-style signature:
 * 	Xuint32 PWM_SIGNAL_OUT_WKILLSWITCH_mReadReg(Xuint32 BaseAddress, unsigned RegOffset)
 *
 */
#define PWM_SIGNAL_OUT_WKILLSWITCH_mReadReg(BaseAddress, RegOffset) \
 	Xil_In32((BaseAddress) + (RegOffset))


/**
 *
 * Write/Read 32 bit value to/from PWM_SIGNAL_OUT_WKILLSWITCH user logic slave registers.
 *
 * @param   BaseAddress is the base address of the PWM_SIGNAL_OUT_WKILLSWITCH device.
 * @param   RegOffset is the offset from the slave register to write to or read from.
 * @param   Value is the data written to the register.
 *
 * @return  Data is the data from the user logic slave register.
 *
 * @note
 * C-style signature:
 * 	void PWM_SIGNAL_OUT_WKILLSWITCH_mWriteSlaveRegn(Xuint32 BaseAddress, unsigned RegOffset, Xuint32 Value)
 * 	Xuint32 PWM_SIGNAL_OUT_WKILLSWITCH_mReadSlaveRegn(Xuint32 BaseAddress, unsigned RegOffset)
 *
 */
#define PWM_SIGNAL_OUT_WKILLSWITCH_mWriteSlaveReg0(BaseAddress, RegOffset, Value) \
 	Xil_Out32((BaseAddress) + (PWM_SIGNAL_OUT_WKILLSWITCH_SLV_REG0_OFFSET) + (RegOffset), (Xuint32)(Value))
#define PWM_SIGNAL_OUT_WKILLSWITCH_mWriteSlaveReg1(BaseAddress, RegOffset, Value) \
 	Xil_Out32((BaseAddress) + (PWM_SIGNAL_OUT_WKILLSWITCH_SLV_REG1_OFFSET) + (RegOffset), (Xuint32)(Value))

#define PWM_SIGNAL_OUT_WKILLSWITCH_mReadSlaveReg0(BaseAddress, RegOffset) \
 	Xil_In32((BaseAddress) + (PWM_SIGNAL_OUT_WKILLSWITCH_SLV_REG0_OFFSET) + (RegOffset))
#define PWM_SIGNAL_OUT_WKILLSWITCH_mReadSlaveReg1(BaseAddress, RegOffset) \
 	Xil_In32((BaseAddress) + (PWM_SIGNAL_OUT_WKILLSWITCH_SLV_REG1_OFFSET) + (RegOffset))

/**
 *
 * Reset PWM_SIGNAL_OUT_WKILLSWITCH via software.
 *
 * @param   BaseAddress is the base address of the PWM_SIGNAL_OUT_WKILLSWITCH device.
 *
 * @return  None.
 *
 * @note
 * C-style signature:
 * 	void PWM_SIGNAL_OUT_WKILLSWITCH_mReset(Xuint32 BaseAddress)
 *
 */
#define PWM_SIGNAL_OUT_WKILLSWITCH_mReset(BaseAddress) \
 	Xil_Out32((BaseAddress)+(PWM_SIGNAL_OUT_WKILLSWITCH_RST_REG_OFFSET), SOFT_RESET)

/************************** Function Prototypes ****************************/


/**
 *
 * Run a self-test on the driver/device. Note this may be a destructive test if
 * resets of the device are performed.
 *
 * If the hardware system is not built correctly, this function may never
 * return to the caller.
 *
 * @param   baseaddr_p is the base address of the PWM_SIGNAL_OUT_WKILLSWITCH instance to be worked on.
 *
 * @return
 *
 *    - XST_SUCCESS   if all self-test code passed
 *    - XST_FAILURE   if any self-test code failed
 *
 * @note    Caching must be turned off for this function to work.
 * @note    Self test may fail if data memory and device are not on the same bus.
 *
 */
XStatus PWM_SIGNAL_OUT_WKILLSWITCH_SelfTest(void * baseaddr_p);
/**
*  Defines the number of registers available for read and write*/
#define TEST_AXI_LITE_USER_NUM_REG 2


#endif /** PWM_SIGNAL_OUT_WKILLSWITCH_H */
