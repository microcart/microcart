##############################################################################
## Filename:          /local/ucart/microcart/tasks/Quad_PID/system/drivers/pwm_recorder_withkillswitch_v1_00_a/data/pwm_recorder_withkillswitch_v2_1_0.tcl
## Description:       Microprocess Driver Command (tcl)
## Date:              Tue Apr  7 14:05:50 2015 (by Create and Import Peripheral Wizard)
##############################################################################

#uses "xillib.tcl"

proc generate {drv_handle} {
  xdefine_include_file $drv_handle "xparameters.h" "pwm_recorder_withkillswitch" "NUM_INSTANCES" "DEVICE_ID" "C_BASEADDR" "C_HIGHADDR" 
}
