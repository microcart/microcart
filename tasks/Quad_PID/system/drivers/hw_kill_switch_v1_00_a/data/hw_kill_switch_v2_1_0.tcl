##############################################################################
## Filename:          /local/ucart/microcart/tasks/Quad_PID/system/drivers/hw_kill_switch_v1_00_a/data/hw_kill_switch_v2_1_0.tcl
## Description:       Microprocess Driver Command (tcl)
## Date:              Tue Apr  7 13:59:46 2015 (by Create and Import Peripheral Wizard)
##############################################################################

#uses "xillib.tcl"

proc generate {drv_handle} {
  xdefine_include_file $drv_handle "xparameters.h" "hw_kill_switch" "NUM_INSTANCES" "DEVICE_ID" "C_BASEADDR" "C_HIGHADDR" 
}
