/*
 * PID.h
 *
 *  Created on: Nov 10, 2014
 *      Author: imciner2
 */

#ifndef PID_H_
#define PID_H_


typedef struct PID_t {
	float sensor;			// Current value of the system
	float setpoint;			// Desired value of the system
	float Kp;				// Proportional constant
	float Ki;				// Integral constant
	float Kd;				// Derivative constant
	float prev_error;		// Previous error
	float acc_error;		// Accumulated error
	float pid_correction;	// Correction factor computed by the PID
} PID_t;

typedef struct PID_values{
	float P;
	float I;
	float D;
	float err;
} PID_values;

// Computes control error and correction
PID_values pid_computation(PID_t *pid);

#endif /* PID_H_ */
