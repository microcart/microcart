#ifndef _QUADPOSITION_H
#define _QUADPOSITION_H

//Camera system info
typedef struct {
	float northPos;
	float eastPos;
	float altPos;

	float yaw;
	float roll;
	float pitch;
} quadPosition_t;

#endif /* _QUADPOSITION_H */
