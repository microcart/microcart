/*
 * controllers.h
 *
 *  Created on: Oct 11, 2014
 *      Author: Tyler
 */
#ifndef _CONTROLLERS_H
#define _CONTROLLERS_H

#include "util.h"
#include "gam.h"
#include "quadposition.h"

/**
 *
 *  USING PLUS CONFIGURATION
 *              0           R            CW               E
 *            1   2       W   R      CCW    CCW        N     S
 *              3           W            CW               W
 *
 *
 *  USING X CONFIGURATION
 *
 *
 *         0   2           R   R        CW    CCW
 *         1   3           W   W        CCW   CW
 */
#define X_CONFIG

/**
 * Pin hook ups
 *
 * PWM Recorder port mapping
 * 3.3 V || GND || PWM_REC_3 || PWM_REC_2 || PWM_REC_1 || PWM_REC_0
 *
 * Rx PINS
 * GEAR     -> JD7
 * THROTTLE -> JE1
 * AILE     -> JE2
 * ELEV     -> JE3
 * RUDD     -> JE4
 * GND      -> JE5
 *
 * JE PMOD  TOP PINS
 * Unused   || GND  || YAW  || PITCH || ROLL || THROTTLE
 *
 * BOTTOM PINS
 *
 *  Unused  || GND  || PWM3 || PWM2  || PWM1 || PWM0
 */

/**
 * Gear settings
 * 1 - F mode = 171135
 * 0 - Gear   = 118363
 * Kill if gear is around 118363
 */


/*
 * Aero channel declaration
 */

#define THROTTLE 0
#define ROLL     1
#define PITCH    2
#define YAW      3
#define AUX 	 4

#define GEAR_KILL     118363 // The kill point for the program
#define BASE          150000

/**
 * Signals from the Rx mins and maxes and ranges
 */
#define THROTTLE_MAX  192238
#define THROTTLE_MIN  113200
#define THROTTLE_RANGE THROTTLE_MAX - THROTTLE_MIN

#define ROLL_MAX      182000
#define ROLL_MIN      128800
#define ROLL_RANGE    ROLL_MAX - ROLL_MIN

#define PITCH_MAX     179000
#define PITCH_MIN     132000
#define PITCH_RANGE   PITCH_MAX - PITCH_MIN
#define YAW_MAX       170150
#define YAW_MIN       129500
#define YAW_RANGE     YAW_MAX - YAW_MIN

#define min 100000
#define max 200000

void filter_PWMs(int* mixer);
void PWMS_to_Aero(int* PWMs, int* aero);
void Aero_to_PWMS(int* PWMs, int* aero);



#endif /* _CONTROLLERS_H */
