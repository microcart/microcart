/*
 * controllers.c
 *
 *  Created on: Oct 11, 2014
 *      Author: Tyler
 */

/**
 * Lots of useful information in controllers.h, look in there first
 */
#include "controllers.h"
#include "iic_mpu9150_utils.h"
#include "gam.h"
#include "quadposition.h"
#include "util.h"
#include "uart.h"
#include "sleep.h"
#include "stdio.h"
#include <math.h>

// 0 was -6600
int motor0_bias = -4500, motor1_bias = 100, motor2_bias = 5300, motor3_bias = 10300;




/**
 * Takes the raw signal inputs from the receiver and filters it so the
 * quadcopter doesn't flip or do something extreme
 */
void filter_PWMs(int* mixer) {

}

/**
 * Converts PWM signals back into 4 channel pitch, roll, yaw, throttle
 */
void PWMS_to_Aero(int* PWMs, int* aero) {
	/**
	 * Reference used to derive equations
	 */
	//	pwm0 = throttle_base - pitch_base + yaw_base;
	//	pwm1 = throttle_base + roll_base - yaw_base;
	//	pwm2 = throttle_base - roll_base - yaw_base;
	//	pwm3 = throttle_base + pitch_base + yaw_base;

	aero[THROTTLE] = (PWMs[0] + PWMs[1] + PWMs[2] + PWMs[3]) / 4;
	aero[ROLL]     = (PWMs[1] - PWMs[2]) / 2;
	aero[PITCH]    = (PWMs[3] - PWMs[0]) / 2;
	aero[YAW]      = (PWMs[3] + PWMs[0] - PWMs[1] - PWMs[2]) / 4;
}

/**
 * Converts Aero 4 channel signals to PWM signals
 * Aero channels are defined above
 */
void Aero_to_PWMS(int* PWMs, int* aero) {
	// Throttle, pitch, roll, yaw as a percentage of their max - Range 0.0 - 100.0
	float throttle_100 = (aero[THROTTLE] - THROTTLE_MIN) / (THROTTLE_RANGE*1.0);
	float pitch_100    = (aero[PITCH]    - PITCH_MIN)    / (PITCH_RANGE*1.0);
	float roll_100     = (aero[ROLL]     - ROLL_MIN)     / (ROLL_RANGE*1.0);
	float yaw_100      = (aero[YAW]      - YAW_MIN)      / (YAW_RANGE*1.0);

	// This adds a +/- 300 ms range bias for the throttle
	int throttle_base = BASE + (int) 60000 * (throttle_100 - .5);
	// This adds a +/- 200 ms range bias for the pitch
	int pitch_base    =        (int) 60000 * (pitch_100    - .5);
	// This adds a +/- 200 ms range bias for the roll
	int roll_base     =        (int) 60000 * (roll_100     - .5);
	// This adds a +/- 75 ms range bias for the yaw
	int yaw_base      =        (int) 15000 * (yaw_100      - .5);

	int pwm0, pwm1, pwm2, pwm3;
#ifdef X_CONFIG
	pwm1 = throttle_base + pitch_base/2 - roll_base/2 + yaw_base + motor1_bias;
	pwm3 = throttle_base + pitch_base/2 + roll_base/2 - yaw_base + motor3_bias;
	pwm0 = throttle_base - pitch_base/2 - roll_base/2 - yaw_base + motor0_bias;
	pwm2 = throttle_base - pitch_base/2 + roll_base/2 + yaw_base + motor2_bias;
#else
	pwm0 = throttle_base + pitch_base + yaw_base;
	pwm1 = throttle_base + roll_base - yaw_base;
	pwm2 = throttle_base - roll_base - yaw_base;
	pwm3 = throttle_base - pitch_base + yaw_base;
#endif
	/**
	 * Boundary checks
	 */
	if(pwm0 < min)
		pwm0 = min;
	else if(pwm0 > max)
		pwm0 = max;

	if(pwm1 < min)
		pwm1 = min;
	else if(pwm1 > max)
		pwm1 = max;

	if(pwm2 < min)
		pwm2 = min;
	else if(pwm2 > max)
		pwm2 = max;

	if(pwm3 < min)
		pwm3 = min;
	else if(pwm3 > max)
		pwm3 = max;



	PWMs[0] = pwm0;
	PWMs[1] = pwm1;
	PWMs[2] = pwm2;
	PWMs[3] = pwm3;

}







// Code graveyard - Viewer beware, you are now entering the S P O O K Y   Z O N E

//▒░░░░░░▄▄▄░░▄██▄░░░▒░░░░░░▄▄▄░░▄██▄░░░▒░░░░░░▄▄▄░░▄██▄░░░▒░░░░░░▄▄▄░░▄██▄░░░▒░░░░░░▄▄▄░░▄██▄░░░▒░░░░░░▄▄▄░░▄██▄░░░
//░░░░░░▐▀█▀▌░░░░▀█▄░░░░░░░▐▀█▀▌░░░░▀█▄░░░░░░░▐▀█▀▌░░░░▀█▄░░░░░░░▐▀█▀▌░░░░▀█▄░░░░░░░▐▀█▀▌░░░░▀█▄░░░░░░░▐▀█▀▌░░░░▀█▄░
//░░░░░░▐█▄█▌░░░░░░▀█▄░░░░░▐█▄█▌░░░░░░▀█▄░░░░░▐█▄█▌░░░░░░▀█▄░░░░░▐█▄█▌░░░░░░▀█▄░░░░░▐█▄█▌░░░░░░▀█▄░░░░░▐█▄█▌░░░░░░▀█▄
//░░░░░░░▀▄▀░░░▄▄▄▄▄▀▀░░░░░░▀▄▀░░░▄▄▄▄▄▀▀░░░░░░▀▄▀░░░▄▄▄▄▄▀▀░░░░░░▀▄▀░░░▄▄▄▄▄▀▀░░░░░░▀▄▀░░░▄▄▄▄▄▀▀░░░░░░▀▄▀░░░▄▄▄▄▄▀▀
//░░░░░▄▄▄██▀▀▀▀░░░░░░░░░░▄▄▄██▀▀▀▀░░░░░░░░░░▄▄▄██▀▀▀▀░░░░░░░░░░▄▄▄██▀▀▀▀░░░░░░░░░░▄▄▄██▀▀▀▀░░░░░░░░░░▄▄▄██▀▀▀▀░░░░░
//░░░░█▀▄▄▄█░▀▀░░░░░░░░░░█▀▄▄▄█░▀▀░░░░░░░░░░█▀▄▄▄█░▀▀░░░░░░░░░░█▀▄▄▄█░▀▀░░░░░░░░░░█▀▄▄▄█░▀▀░░░░░░░░░░█▀▄▄▄█░▀▀░░░░░░
//░░░░▌░▄▄▄▐▌▀▀▀░░░░░░░░░▌░▄▄▄▐▌▀▀▀░░░░░░░░░▌░▄▄▄▐▌▀▀▀░░░░░░░░░▌░▄▄▄▐▌▀▀▀░░░░░░░░░▌░▄▄▄▐▌▀▀▀░░░░░░░░░▌░▄▄▄▐▌▀▀▀░░░░░
//░▄░▐░░░▄▄░█░▀▀░░░░░░▄░▐░░░▄▄░█░▀▀░░░░░░▄░▐░░░▄▄░█░▀▀░░░░░░▄░▐░░░▄▄░█░▀▀░░░░░░▄░▐░░░▄▄░█░▀▀░░░░░░▄░▐░░░▄▄░█░▀▀░░░░░
//░▀█▌░░░▄░▀█▀░▀░░░░░░▀█▌░░░▄░▀█▀░▀░░░░░░▀█▌░░░▄░▀█▀░▀░░░░░░▀█▌░░░▄░▀█▀░▀░░░░░░▀█▌░░░▄░▀█▀░▀░░░░░░▀█▌░░░▄░▀█▀░▀░░░░░
//░░░░░░░░▄▄▐▌▄▄░░░░░░░░░░░░░▄▄▐▌▄▄░░░░░░░░░░░░░▄▄▐▌▄▄░░░░░░░░░░░░░▄▄▐▌▄▄░░░░░░░░░░░░░▄▄▐▌▄▄░░░░░░░░░░░░░▄▄▐▌▄▄░░░░░
//░░░░░░░░▀███▀█░▄░░░░░░░░░░░▀███▀█░▄░░░░░░░░░░░▀███▀█░▄░░░░░░░░░░░▀███▀█░▄░░░░░░░░░░░▀███▀█░▄░░░░░░░░░░░▀███▀█░▄░░░
//░░░░░░░▐▌▀▄▀▄▀▐▄░░░░░░░░░░▐▌▀▄▀▄▀▐▄░░░░░░░░░░▐▌▀▄▀▄▀▐▄░░░░░░░░░░▐▌▀▄▀▄▀▐▄░░░░░░░░░░▐▌▀▄▀▄▀▐▄░░░░░░░░░░▐▌▀▄▀▄▀▐▄░░░
//░░░░░░░▐▀░░░░░░▐▌░░░░░░░░░▐▀░░░░░░▐▌░░░░░░░░░▐▀░░░░░░▐▌░░░░░░░░░▐▀░░░░░░▐▌░░░░░░░░░▐▀░░░░░░▐▌░░░░░░░░░▐▀░░░░░░▐▌░░
//░░░░░░░█░░░░░░░░█░░░░░░░░░█░░░░░░░░█░░░░░░░░░█░░░░░░░░█░░░░░░░░░█░░░░░░░░█░░░░░░░░░█░░░░░░░░█░░░░░░░░░█░░░░░░░░█░░
//░░░░░░▐▌░░░░░░░░░█░░░░░░░▐▌░░░░░░░░░█░░░░░░░▐▌░░░░░░░░░█░░░░░░░▐▌░░░░░░░░░█░░░░░░░▐▌░░░░░░░░░█░░░░░░░▐▌░░░░░░░░░█░
// Old mixer stuff
//	double t_drive =  (aero[THROTTLE] - THROTTLE_MIN) * 1.0 / THROTTLE_MIN;
//
//	PWMs[0] = (int) (t_drive * aero[PITCH]) + THROTTLE_MIN;
//	PWMs[1] = (int) (t_drive * aero[ROLL]) + THROTTLE_MIN;
//	PWMs[2] = (int) (t_drive * (2 * ROLL_BIAS - aero[ROLL] )) + THROTTLE_MIN;
//	PWMs[3] = (int) (t_drive * (2 *PITCH_BIAS - aero[PITCH])) + THROTTLE_MIN;
