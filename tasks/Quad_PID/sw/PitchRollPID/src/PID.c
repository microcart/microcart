/*
 * PID.c
 *
 *  Created on: Nov 10, 2014
 *      Author: imciner2
 */

#include "PID.h"
#include <math.h>

float dt = .000429;

PID_values pid_computation(PID_t *pid) {

	float P=0, I=0, D=0;						// Each term's contribution
	//float T;									// Time between runs
	float error = pid->sensor - pid->setpoint;	// Error in the system

	// Accumulate the error
	if (fabs(pid->Ki) <= 1e-6) {
		pid->acc_error = 0;
	} else {
		pid->acc_error += error;
	}

	// Compute each term's contribution
	P = pid->Kp * error;
	I = pid->Ki * pid->acc_error * dt;
	D = pid->Kd * (error - pid->prev_error) / dt;

	PID_values ret = {P, I, D, error};

	pid->prev_error = error;			// Store the current error into the structure

	pid->pid_correction = P + I + D;	// Store the computed correction
	return ret;
}
