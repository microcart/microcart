/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

/*
 * Accelerometer and gyro angles on Quad
 *
 * In X Config,
 *
 * R W		2 3
 * R W		0 1
 *
 * (Pitch, Roll) signs are
 *
 * +,+  +,-					This means as roll turns to the right, it's angle is negative and when left, positive
 *
 * -,+  -,-
 *
 */
#include <stdio.h>
#include "platform.h"
#include <xparameters.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "util.h"
#include "xtime_l.h"
#include "uart.h"
#include "controllers.h"
#include "iic_mpu9150_utils.h"
#include "PID.h"
#include "stringBuilder.h"
#include "terminal.h"
#include "commandproc.h"
#include "gam.h"
#include "quadposition.h"
#include "sleep.h"

char *channel_types[4] = { "Throttle", "Roll    ", "Pitch   ", "Yaw     " };


#define PI_OVER_180 0.017453278

#define USE_GIMBAL

/**
 * Global variables
 */
logging logs = { };

/**
 * Function Prototypes
 */
int initializeAllComponents();  // Initializes all required components
void setPIDCoeff(PID_t* p, float pValue, float iValue, float dValue); // Sets the given PID's coefficients
void waitForArming();

int main() {

	int i, rc;
	int mixer[5];
	int pwms[4];
	double compX = 0, compY = 0; 	// Complementary Roll and Pitch angles
	float time_stamp = 0;
	gam_t gam; 		// GAM struct for gyro, accelerometer, and magnetometer data

	// Structures to hold the current and desired quad position & orientation
	quadPosition_t currentQuadPosition = { };
	quadPosition_t desiredQuadPosition = { };

	currentQuadPosition.yaw = -1.58;

	char buf[1000] = { };

	PID_t rollCFPID = { }, pitchCFPID = { }, yawCFPID = { }; // External Comp filter PIDs
	PID_t rollGPID = { }, pitchGPID = { }, yawGPID = { };  // Internal Gyro PIDs

	// Initialize all required components:
	// Uart, PWM receiver/generator, I2C, Sensor Board
	rc = initializeAllComponents();
	if (rc == -1) {
		printf(buf, "ERROR (main): Problem initializing...Goodbye\r\n");
		return rc;
	}

	// DO NOT CHANGE THESE NUMBERS UNLESS YOU KNOW WHAT YOU'RE DOING!!!!!!!!
	// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	// Outer PID coefficients (Angle)
	/*setPIDCoeff(&pitchCFPID, -7, 0, 0);
	setPIDCoeff(&rollCFPID, -6.0, 0, 0);
	setPIDCoeff(&yawCFPID, 0, 0, 0);

	// Inner PID coefficients (Angular Velocity)
	setPIDCoeff(&pitchGPID, -96, 0, 0);
	setPIDCoeff(&rollGPID, -100, 0, 0);
	setPIDCoeff(&yawGPID, 470, 0, 0);*/


	setPIDCoeff(&pitchCFPID, -7,0, -0.45);	  // Don't Touch
	setPIDCoeff(&rollCFPID, -6.0, 0, -0.4);   // Maybe back off roll gains current 4/23 -6, 0.4
	setPIDCoeff(&yawCFPID, 60, 0, 0); // 80

	// Inner PID coefficients (Angular Velocity)
	setPIDCoeff(&pitchGPID, -96,0,0);
	setPIDCoeff(&rollGPID, -96,0,0); // inner = -96 4/23
	setPIDCoeff(&yawGPID, 470, 0, 0);

	XTime before = 0.0, after = 0.0;
	int firstLoop = 1;

	stringBuilder_t* sb = stringBuilder_create();

	usleep(6e6);
	uart0_sendStr("\r\n\r\n#Welcome to SUPERQUAD 2000 (R)\r\n\r\n");

	int started = 0;
	int loop_counter = 0;

	// Protection Loop:
	while(mixer[THROTTLE] < 50000){
		read_rec_all(mixer);
	}
	while(!tryReceivePacket(sb, 0));

	// Main Flying Loop:
	// 1.Read in RC Receiver values
	// 2.Collect data from sensor board
	// 3.Calculate PID corrections (Outer and Inner)
	// 4.Output PWMs to motors
	// 5.Check if command from base station
	//    a.Increase parameter based off command
	// 6.Update log
	while (1) {
		float LOOP_TIME = (after - before) / (float) COUNTS_PER_SECOND;
		XTime_GetTime(&before);

		// Read in values from RC Receiver
		read_rec_all(mixer);

		if (!started && mixer[THROTTLE] > 150000) {
			started = 1;
		} else if ((started && mixer[THROTTLE] < 125000 && mixer[YAW] > 170000) || read_kill(mixer[AUX])) {
			break;
		}

		// Read gyro and accelerometer data
		// TODO: Check if mag needs to be read
		if (get_gam_reading(&gam) == -1) {
			//break;
		}
		gam.heading = 0;

		// read from cam system
		// readFromCamSystem(&camSystem);

		if (firstLoop) {
			XTime_GetTime(&after);
			firstLoop = 0;
			// Sets the first iteration to be at the accelerometer value since gyro initializes to {0,0,0} regardless of orientation
			compX = gam.accel_xAngle;
			compY = gam.accel_yAngle;
			continue;
		}

		/*
		 * 					Outer loop
		 * Calculates current orientation, and outputs
		 * a pitch, roll, or yaw velocity to an inner loop PID
		 */

		// HIGHLY EXPERIMENTAL (ADJUST GYROs using Gimbal Equations
		// DO NOT USE
		/////////////////////////////////////////////////////////////////////////
		// Phi_dot = p + q sin(Phi) tan(theta) + r cos(Phi) tan(theta)
		// theta_dot = q cos(Phi) - r sin(Phi)
		// Psi_dot = q sin(Phi) sec(theta) + r cos(Phi) sec(theta)

		// | Phi_d   |  |  1  sin(Phi)tan(theta)    cos(Phi)tan(theta) |  | p |
		// | theta_d |  |  0  cos(Phi)              -sin(Phi)		   |  | q |
		// | Psi_d   |  |  0  sin(Phi)sec(theta)    cos(Phi)sec(theat) |  | r |

/*
		double sin_phi = sin(compX);
		double cos_phi = cos(compX);
		double tan_theta = tan(compY);
		double sec_theta = 1/cos(compY);

		gam.gyro_xVel = gam.gyro_xVel + (gam.gyro_yVel*sin_phi*tan_theta) + (gam.gyro_zVel*sin_phi*tan_theta);
		gam.gyro_yVel = (gam.gyro_yVel*cos_phi) - (gam.gyro_zVel*sin_phi);
		gam.gyro_zVel = (gam.gyro_yVel*sin_phi*sec_theta)+ (gam.gyro_zVel*cos_phi*sec_theta);
*/
		//compX = 0.98 * phi_d + 0.02
		// LEAVING THE DANGER ZONE
		///////////////////////////////////////////////////////////////////////////

#ifdef USE_GIMBAL
		double sin_phi = sin(compX * PI_OVER_180);
		double cos_phi = cos(compX * PI_OVER_180);
		double tan_theta = tan(compY * PI_OVER_180);
		double sec_theta = 1/cos(compY * PI_OVER_180);

		double n_gx = gam.gyro_xVel + (gam.gyro_yVel*sin_phi*tan_theta) + (gam.gyro_zVel*cos_phi*tan_theta);
		double n_gy = (gam.gyro_yVel*cos_phi) - (gam.gyro_zVel*sin_phi);
		double n_gz = (gam.gyro_yVel*sin_phi*sec_theta)+ (gam.gyro_zVel*cos_phi*sec_theta);


		// LEAVING THE DANGER ZONE
		///////////////////////////////////////////////////////////////////////////


		compX = .98 * (compX + n_gx * LOOP_TIME)
				+ .02 * gam.accel_xAngle;

		compY = .98 * (compY + n_gy* LOOP_TIME)
				+ .02 * gam.accel_yAngle;
#else
		compX = .98 * (compX + gam.gyro_xVel * LOOP_TIME)
						+ .02 * gam.accel_xAngle;

		compY = .98 * (compY + gam.gyro_yVel* LOOP_TIME)
				+ .02 * gam.accel_yAngle;


#endif // USE_GIMBAL





		pitchCFPID.sensor = compX;
		pitchCFPID.setpoint = 0; // TODO : Add RC Controller set points

		rollCFPID.sensor = compY;
		rollCFPID.setpoint = 0; // TODO : Add RC Controller set points

		yawCFPID.sensor = currentQuadPosition.yaw;
		yawCFPID.setpoint = -1.58;

		static float prevYaw = -1;
		/**** YAW DEBUGGING ****/
		/*if (fabs(prevYaw - currentQuadPosition.yaw) > 0.001) {
			char buf[1024];
			sprintf(buf, "[yaw: %.3f]\r\n", currentQuadPosition.yaw);
			uart0_sendStr(buf);
			prevYaw = currentQuadPosition.yaw;
		}*/
		/***********************/

		logs.outer_pitch_PID = pid_computation(&pitchCFPID);
		logs.outer_roll_PID = pid_computation(&rollCFPID);
		logs.outer_yaw_PID = pid_computation(&yawCFPID);

		/*
		 * 				Inner Loop
		 * Takes the desired angular velocity from the outer loop,
		 * and uses a PID with the current angular velocity
		 */

#ifdef USE_GIMBAL
		pitchGPID.sensor = n_gx;//gam.gyro_xVel;
		//pitchGPID.setpoint = 0;
		pitchGPID.setpoint = pitchCFPID.pid_correction;

		rollGPID.sensor = n_gy;//gam.gyro_yVel;
		//rollGPID.setpoint = 0;
		rollGPID.setpoint = rollCFPID.pid_correction;

		yawGPID.sensor = n_gz;//gam.gyro_zVel;
		//yawGPID.setpoint = 0;
		yawGPID.setpoint = yawCFPID.pid_correction; // TODO : Implement with magnetometer

#else
		pitchGPID.sensor = gam.gyro_xVel;
		//pitchGPID.setpoint = 0;
		pitchGPID.setpoint = pitchCFPID.pid_correction;

		rollGPID.sensor = gam.gyro_yVel;
		//rollGPID.setpoint = 0;
		rollGPID.setpoint = rollCFPID.pid_correction;

		yawGPID.sensor = gam.gyro_zVel;
		//yawGPID.setpoint = 0;
		yawGPID.setpoint = yawCFPID.pid_correction; // TODO : Implement with magnetometer
#endif

		logs.inner_pitch_PID = pid_computation(&pitchGPID);
		logs.inner_roll_PID = pid_computation(&rollGPID);
		logs.inner_yaw_PID = pid_computation(&yawGPID);

		/*
		 * Motor corrections
		 */

		mixer[PITCH] += pitchGPID.pid_correction;
		mixer[ROLL] += rollGPID.pid_correction;
		mixer[YAW] += yawGPID.pid_correction;

		// Convert from the aero inputs to PWMS
		Aero_to_PWMS(pwms, mixer);

		//printf("Writing PWMs to register\r\n");
		for (i = 0; i < 4; i++)
			pwm_write_channel(pwms[i], i);


		// Listen on bluetooth and if there's a packet,
		// then receive the packet and process it.
		// Use the stringbuilder to keep track of data received so far
		int echo = 0;
		//uart0_sendStr("TRY PACKET\r\n");
		int hasPacket = tryReceivePacket(sb, echo);
		if (hasPacket) {
			processPacket(sb->buf, &currentQuadPosition, &desiredQuadPosition, &yawCFPID);
			sb->clear(sb);
		}

		//Finish logging
		logs.time_stamp = time_stamp;
		logs.time_slice = LOOP_TIME;

		logs.gam = gam; //Because C beats all

		logs.pitch_angle = compX;
		logs.roll_angle = compY;

		logs.motors[0] = pwms[0];
		logs.motors[1] = pwms[1];
		logs.motors[2] = pwms[2];
		logs.motors[3] = pwms[3];

		if (loop_counter == 10) {
			loop_counter = 0;
			updateLog();
		} else {
			++loop_counter;
		}

		XTime_GetTime(&after);
		time_stamp += LOOP_TIME;

	} //end while(1)
	stringBuilder_free(sb);

	pwm_kill();
	printLogging();
	return 0;
} //main

int initializeAllComponents() {

	int i2c_rc, mpu_rc;

	init_platform();

	// Initialize UART0 (Bluetooth)
	uart0_init(XPAR_PS7_UART_0_DEVICE_ID, 921600);
	uart0_clearFIFOs();

	// Initialize I2C controller and start the sensor board
	i2c_rc = initI2C0();
	mpu_rc = startMPU9150();

	if (i2c_rc == -1 || mpu_rc == -1) {
		return -1;
	}

	// Initialize PWM Recorders and Motor outputs
	pwm_init();

	printf("\nStarting\r\n");
	return 0;
}

void setPIDCoeff(PID_t* p, float pValue, float iValue, float dValue) {

	p->Kp = pValue;
	p->Ki = iValue;
	p->Kd = dValue;

}

int main2() {

	int i, rc;
	int mixer[5];
	int pwms[4];
	double compX = 0, compY = 0; 	// Complementary Roll and Pitch angles
	float time_stamp = 0;
	gam_t gam; 		// GAM struct for gyro, accelerometer, and magnetometer data

	double gx_int=0, gy_int=0, gz_int=0;


	// Structures to hold the current and desired quad position & orientation
	quadPosition_t currentQuadPosition = { };
	quadPosition_t desiredQuadPosition = { };

	char buf[1000] = { };

	PID_t rollCFPID = { }, pitchCFPID = { }, yawCFPID = { }; // External Comp filter PIDs
	PID_t rollGPID = { }, pitchGPID = { }, yawGPID = { };  // Internal Gyro PIDs

	// Initialize all required components:
	// Uart, PWM receiver/generator, I2C, Sensor Board

	initI2C0();
	startMPU9150();


	setPIDCoeff(&pitchCFPID, -7,0, -0.45);
		setPIDCoeff(&rollCFPID, -6.0, 0, -0.3);
		setPIDCoeff(&yawCFPID, 0, 0, 0);

		// Inner PID coefficients (Angular Velocity)
		setPIDCoeff(&pitchGPID, -96,0,0);
		setPIDCoeff(&rollGPID, -100,0,0);
		setPIDCoeff(&yawGPID, 470, 0, 0);

	XTime before = 0.0, after = 0.0;
	int firstLoop = 1;


	int started = 0;
	int loop_counter = 0;

	// Protection Loop:
	//while(mixer[THROTTLE]);

	// Main Flying Loop:
	// 1.Read in RC Receiver values
	// 2.Collect data from sensor board
	// 3.Calculate PID corrections (Outer and Inner)
	// 4.Output PWMs to motors
	// 5.Check if command from base station
	//    a.Increase parameter based off command
	// 6.Update log
	while (1) {
		float LOOP_TIME = (after - before) / (float) COUNTS_PER_SECOND;
		XTime_GetTime(&before);



		// Read gyro and accelerometer data
		// TODO: Check if mag needs to be read
		get_gam_reading(&gam);
		gam.heading = 0;


		if (firstLoop) {
			XTime_GetTime(&after);
			firstLoop = 0;
			// Sets the first iteration to be at the accelerometer value since gyro initializes to {0,0,0} regardless of orientation
			compX = gam.accel_xAngle;
			compY = gam.accel_yAngle;
			usleep(100000);
			continue;
		}



		/*
		 * 					Outer loop
		 * Calculates current orientation, and outputs
		 * a pitch, roll, or yaw velocity to an inner loop PID
		 */

		// HIGHLY EXPERIMENTAL (ADJUST GYROs using Gimbal Equations
		// DO NOT USE
		/////////////////////////////////////////////////////////////////////////
		// Phi_dot = p + q sin(Phi) tan(theta) + r cos(Phi) tan(theta)
		// theta_dot = q cos(Phi) - r sin(Phi)
		// Psi_dot = q sin(Phi) sec(theta) + r cos(Phi) sec(theta)

		// | Phi_d   |  |  1  sin(Phi)tan(theta)    cos(Phi)tan(theta) |  | p |
		// | theta_d |  |  0  cos(Phi)              -sin(Phi)		   |  | q |
		// | Psi_d   |  |  0  sin(Phi)sec(theta)    cos(Phi)sec(theat) |  | r |


		double sin_phi = sin(compX * PI_OVER_180);
		double cos_phi = cos(compX * PI_OVER_180);
		double tan_theta = tan(compY * PI_OVER_180);
		double sec_theta = 1/cos(compY * PI_OVER_180);

		double n_gx = gam.gyro_xVel + (gam.gyro_yVel*sin_phi*tan_theta) + (gam.gyro_zVel*cos_phi*tan_theta);
		double n_gy = (gam.gyro_yVel*cos_phi) - (gam.gyro_zVel*sin_phi);
		double n_gz = (gam.gyro_yVel*sin_phi*sec_theta)+ (gam.gyro_zVel*cos_phi*sec_theta);


		// LEAVING THE DANGER ZONE
		///////////////////////////////////////////////////////////////////////////


		compX = .98 * (compX + n_gx * LOOP_TIME)
				+ .02 * gam.accel_xAngle;

		compY = .98 * (compY + n_gy* LOOP_TIME)
				+ .02 * gam.accel_yAngle;



		printf("Gyro %8.3f %8.3f %8.3f\t NGyro %8.3f %8.3f %8.3f\tComp %8.3f %8.3f\r\n", gam.gyro_xVel, gam.gyro_yVel, gam.gyro_zVel, n_gx, n_gy, n_gz, compX, compY);

		XTime_GetTime(&after);
		time_stamp += LOOP_TIME;

	} //end while(1)

	return 0;
} //main
