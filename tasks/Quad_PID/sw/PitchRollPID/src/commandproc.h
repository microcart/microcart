/*
 * commandproc.h
 *
 *  Created on: Sep 24, 2014
 *      Author: Adam
 */


#ifndef COMMANDPROC_H_
#define COMMANDPROC_H_

#include "quadposition.h"
#include "PID.h"

int processPacket(char*, quadPosition_t* currentQuadPosition, quadPosition_t* desiredQuadPosition, PID_t* yawCFPID);
int processUpdate(char*, quadPosition_t* currentQuadPosition);
int processCommand(char*, quadPosition_t* desiredQuadPosition, PID_t* yawCFPID);

#endif /* COMMANDPROC_H_ */
