/*
 * commandproc.c
 *
 *  Created on: Sep 24, 2014
 *      Author: Adam
 */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "commandproc.h"
#include "platform.h"
#include "xparameters.h"
#include "sleep.h"
#include "quadposition.h"
#include "terminal.h"
#include "uart.h"
#include "math.h"
#include "PID.h"

#define UPDATE_SIZE 24
#define DEBUG 0

typedef struct {
	char** tokens;
	int numTokens;
} tokenList_t;

tokenList_t tokenize(char* cmd) {
	int maxTokens = 16;
	tokenList_t ret;
	ret.numTokens = 0;
	ret.tokens = malloc(sizeof(char*) * maxTokens);
	ret.tokens[0] = NULL;

	int i = 0;
	char* token = strtok(cmd, " ");
	while (token != NULL && i < maxTokens - 1) {
		ret.tokens[i++] = token;
		ret.tokens[i] = NULL;
		ret.numTokens++;
		token = strtok(NULL, " ");
	}

	return ret;
}

int doProcessing(char* cmd, tokenList_t tokens, quadPosition_t* desiredQuadPosition, PID_t* yawCFPID) {
#if DEBUG
	char buf2[512];
	snprintf(buf2, sizeof(buf2), "%d\r\n", tokens.numTokens);
	uart0_sendStr(buf2);
	usleep(1000);
	int i;
	for (i=0; i < tokens.numTokens; i++) {

		snprintf(buf2, sizeof(buf2), "--> %s\r\n", tokens.tokens[i]);
		uart0_sendStr(buf2);
		usleep(1000);
	}
#endif

	static int n=0;
	static char buf[16384];
	if (tokens.numTokens > 0) {
		if (strcmp(tokens.tokens[0], "hello") == 0) {
			uart0_sendStr("Hello there!\r\n");
		} else if (strcmp(tokens.tokens[0], "n") == 0) {
			int inc = tokens.tokens[1][0] - '0';
			n += inc;
		} else if (strcmp(tokens.tokens[0], "g") == 0) {
			sprintf(buf, "Num: %d\r\n", n);
			uart0_sendStr(buf);
		} else if (strcmp(tokens.tokens[0], "P") == 0){
			snprintf(buf, sizeof(buf), "You did the P command with %d args\r\n", tokens.numTokens - 1);
			//float val = atoff(tokens.tokens[1]);
			// pVal += val;
			uart0_sendStr(buf);
		} else if (strcmp(tokens.tokens[0], "setyaw") == 0) {
			if (tokens.numTokens == 2) {
				float desiredYaw;
				int numSet = sscanf(tokens.tokens[1], "%f", &desiredYaw);
				if (numSet != 1 || desiredYaw < -M_PI || desiredYaw > M_PI) {
					snprintf(buf, sizeof(buf), "Error in argument \"%s\" to setyaw - must be numeric and in the range [-pi, pi]\r\n", tokens.tokens[1]);
					uart0_sendStr(buf);
					return 1;
				}

				desiredQuadPosition->yaw = desiredYaw;
				snprintf(buf, sizeof(buf), "Successfully set desired yaw to %.2f\r\n", desiredYaw);
				uart0_sendStr(buf);
			} else {
				snprintf(buf, sizeof(buf), "Error in command \"%s\" - setyaw requires exactly one numeric argument\r\n", cmd);
				uart0_sendStr(buf);
				return 1;
			}
		}  else if (strcmp(tokens.tokens[0], "setyawp") == 0) {
			if (tokens.numTokens == 2) {
				float desiredYawP;
				int numSet = sscanf(tokens.tokens[1], "%f", &desiredYawP);
				if (numSet != 1 || desiredYawP < -100 || desiredYawP > 100) {
					snprintf(buf, sizeof(buf), "Error in argument \"%s\" to setyawp - must be numeric and in the range [-100, 100]\r\n", tokens.tokens[1]);
					uart0_sendStr(buf);
					return 1;
				}

				yawCFPID->Kp = desiredYawP;
				snprintf(buf, sizeof(buf), "Successfully set yaw P coefficient to %.2f\r\n", desiredYawP);
				uart0_sendStr(buf);
			} else {
				snprintf(buf, sizeof(buf), "Error in command \"%s\" - setyawp requires exactly one numeric argument\r\n", cmd);
				uart0_sendStr(buf);
				return 1;
			}
		}  else if (strcmp(tokens.tokens[0], "setyawd") == 0) {
			if (tokens.numTokens == 2) {
				float desiredYawD;
				int numSet = sscanf(tokens.tokens[1], "%f", &desiredYawD);
				if (numSet != 1 || desiredYawD < -100 || desiredYawD > 100) {
					snprintf(buf, sizeof(buf), "Error in argument \"%s\" to setyawd - must be numeric and in the range [-100, 100]\r\n", tokens.tokens[1]);
					uart0_sendStr(buf);
					return 1;
				}

				yawCFPID->Kd = desiredYawD;
				snprintf(buf, sizeof(buf), "Successfully set yaw D coefficient to %.2f\r\n", desiredYawD);
				uart0_sendStr(buf);
			} else {
				snprintf(buf, sizeof(buf), "Error in command \"%s\" - setyawd requires exactly one numeric argument\r\n", cmd);
				uart0_sendStr(buf);
				return 1;
			}
		} else if (strcmp(tokens.tokens[0], "dumprand") == 0) {
			// dump a bunch of bytes of random data
			// format: $> dumprand <numBytes>
			if (tokens.numTokens == 2) {
				int numBytes;
				int numSet = sscanf(tokens.tokens[1], "%d", &numBytes);
				if (numSet != 1 || numBytes < 0 || numBytes > 1000000) {
					snprintf(buf, sizeof(buf), "Error in argument \"%s\" to dumprand - must be integer and in the range [0, 1000000]\r\n", tokens.tokens[1]);
					uart0_sendStr(buf);
					return 1;
				}

				int i;
				srand(42);
				// split it into 10KB blocks
				char randBuf[10240];
				int counter = 0;
				for (i=0; i < numBytes; i++) {
					// get random letter
					randBuf[counter++] = (rand() % 26) + 'A';

					// send it if the block gets full
					if (counter == sizeof(randBuf)) {
						counter = 0;
						uart0_sendBytes(randBuf, sizeof(randBuf));
					}
				}
				if (counter != 0) {
					uart0_sendBytes(randBuf, counter);
				}
			} else {
				snprintf(buf, sizeof(buf), "Error in command \"%s\" - dumprand requires exactly one integer argument\r\n", cmd);
				uart0_sendStr(buf);
				return 1;
			}
		}

		else {
			snprintf(buf, sizeof(buf),
					"Unrecognized command: \"%s\" [length %d]\r\n", cmd, strlen(cmd));
			uart0_sendStr(buf);
			return 1;
		}
	}

	return 0;
}

int processPacket(char* packet, quadPosition_t* currentQuadPosition, quadPosition_t* desiredQuadPosition, PID_t* yawCFPID) {
	// cam system update
	if (packet[0] == 'U') {
		return processUpdate(&packet[1], currentQuadPosition);
	}

	// command
	else if (packet[0] == 'C') {
		int ret = processCommand(&packet[1], desiredQuadPosition, yawCFPID);
		return ret;
	}

	return 0;
}

float getFloat(char* str, int pos) {
	union {
		float f;
		int i;
	} x;
	x.i = ((str[pos+3] << 24) | (str[pos+2] << 16) | (str[pos+1] << 8) | (str[pos]));
	return x.f;
}

int processUpdate(char* update, quadPosition_t* currentQuadPosition) {
	//static char buf[16384];
	//sprintf(buf, "update..(%d)..[%s]\r\n", strlen(update), update);
	//uart0_sendStr(buf);


	// Packet must come as [NEARPY], 4 bytes each
	float northPos = getFloat(update, 0);
	float eastPos = getFloat(update, 4);
	float altPos = getFloat(update, 8);
	float roll = getFloat(update, 12);
	float pitch = getFloat(update, 16);
	float yaw = getFloat(update, 20);

	//sprintf(buf, "update [x:%.2f y:%.2f z:%.2f r:%.2f p:%.2f y:%.2f]\r\n", x, y, z, roll, pitch, yaw);
	//uart0_sendStr(buf);

	currentQuadPosition->northPos = northPos;
	currentQuadPosition->eastPos = eastPos;
	currentQuadPosition->altPos = altPos;
	currentQuadPosition->roll = roll;
	currentQuadPosition->pitch = pitch;
	currentQuadPosition->yaw = yaw;

	//sprintf(buf, "%.2f\r\n", y);
	//uart0_sendStr(buf);

	return 0;
}

int processCommand(char* cmd, quadPosition_t* desiredQuadPosition, PID_t* yawCFPID) {
#if DEBUG
	{
		char buf[512];
		snprintf(buf, sizeof(buf), "PROCESSING COMMAND \"%s\" (%d)", cmd, strlen(cmd));
		uart0_sendStr(buf);
	}
#endif

	int length = strlen(cmd);
	char* cmdOrig = malloc(length);
	strcpy(cmdOrig, cmd);
	int ret = 0;
	if (length > 0) {
		ret = doProcessing(cmd, tokenize(cmdOrig), desiredQuadPosition, yawCFPID);
	}
	free(cmdOrig);

	return ret;
}
