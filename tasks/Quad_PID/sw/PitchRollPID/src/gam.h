#ifndef _GAM_H
#define _GAM_H

#include "xbasic_types.h"

//Gyro, accelerometer, and magnetometer data structure
//Used for reading an instance of the sensor data
typedef struct {

	// GYRO
	//Xint16 raw_gyro_x, raw_gyro_y, raw_gyro_z;

	float gyro_xVel; // In degrees per second
	float gyro_yVel;
	float gyro_zVel;

	// ACCELEROMETER
	//Xint16 raw_accel_x, raw_accel_y, raw_accel_z;

	float accel_xAngle; //In degrees
	float accel_yAngle;


	// MAG
	//Xint16 raw_mag_x, raw_mag_y, raw_mag_z;

	float heading; // In degrees

	float mag_x; //Magnetic north: ~50 uT
	float mag_y;
	float mag_z;



}gam_t;
#endif /* _GAM_H */
