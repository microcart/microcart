import java.util.Random;
import java.util.Scanner;

import jssc.SerialPort;
import jssc.SerialPortException;
import jssc.SerialPortTimeoutException;

public class SendTest {
	public static void main(String[] args){
		SerialPort serialPort = new SerialPort("/dev/rfcomm0");
		Random rand = new Random(42);
		try{
			System.out.println("Opening port " + "/dev/rfcomm0" + "...");
			serialPort.openPort();
			serialPort.setParams(921600, 8, 1, 0);
			Scanner in = new Scanner(System.in);
			System.out.println("Success!");
			
			byte[] data = new byte[16384];
			
			//while(!in.nextLine().contains("s")) {
			while("".isEmpty()){
				byte[] b = null;
				try {
					b = serialPort.readBytes(1, 1000);
				} catch (SerialPortTimeoutException e) {
					b = new byte[1];
					b[0] = 101;
					System.out.println("Timeout");
				}
				
				long start = -1;
				int sum = 0;
				int hash = 0;
				int count = 0;
				
				if (b[0] == 101) {
					start = System.currentTimeMillis();
					
					while (b[0] == 101) {
						for (int i=0; i < data.length; i++) {
							data[i] = (byte) rand.nextInt(100);
							
							sum += data[i];
							hash = (hash * 31) + data[i];
						}
						System.out.println("START");
						serialPort.writeBytes(data);
						System.out.println("Sent 16K #" + count++);
						
						try {
							b = serialPort.readBytes(1, 1000);
						} catch (SerialPortTimeoutException e) {
							b = new byte[1];
							b[0] = 102;
							System.out.println("Timeout");
						}
					}
				}
				long end = System.currentTimeMillis();

				System.out.println("[sum:" + sum + "]  [hash:" + hash + "]  [time:" + (end - start) + "ms]");
			}
			
			serialPort.closePort();
		} catch (SerialPortException e) {
			e.printStackTrace();
		}
	}
}
