public class TicTacToe {
	public static void main(String[] args){
		TicTacToe game = new TicTacToe();
		game.makeMove(0, 0); // player 1
		game.makeMove(0, 2); // player 2
		game.makeMove(2, 0); // player 1
		game.makeMove(1, 2); // player 2
		game.makeMove(2, 2); // player 1
		game.makeMove(1, 0); // player 2
		game.makeMove(1, 1); // player 1
		System.out.println(game.checkForWinner());
	}

	public Integer[][] board = new Integer[3][3];
	public Integer currentPlayer = 0;

	public TicTacToe() {
		initialize();
	}

	// Initializes the board to zero, sets initial player to 1
	public void initialize(){
		board = new Integer[3][3];
		currentPlayer = 1;
		for(int i = 0; i < 3; i++){
			for(int j = 0; j < 3; j++){
				board[i][j] = 0;
			}
		}
	}

	// Returns current player, always 1 or 2
	public int getCurrentPlayer(){
		return currentPlayer;
	}

	// Returns true if the player can make the move at board[x][y],
	// Updates currentPlayer if move is valid
	public boolean makeMove(int x, int y){
		int player = currentPlayer;
		if(board[x][y] != 0){
			return false;
		} else{
			board[x][y] = player;
			updatePlayer();
			return true;
		}
	}

	// Updates the player. Always returns 1 or 2
	public void updatePlayer(){
		int player = currentPlayer;
		currentPlayer = (player % 2) + 1;
	}

	// Returns: the player who wins, 0 if no winner and game can continue,
	// -1 if board is full and no winner
	public int checkForWinner(){
		int winner;
		if((winner = checkRowsCols()) > 0){
			return winner;
		}
		if((winner = checkDiagonals()) > 0){
			return winner;
		}
		// check if board is full
		for(int i = 0; i < 3; i++){
			for(int j = 0; j < 3; j++){
				if(board[i][j] == 0){
					return 0;
				}
			}
		}
		return -1; // else no winner
	}

	public int checkRowsCols(){
		for(int i = 0; i < 3; i++){
			// rows
			if((board[i][0] == board[i][1]) && (board[i][0] == board[i][2])){
				if(board[i][0] != 0){
					return board[i][0];
				}
			}
			// columns
			if((board[0][i] == board[1][i]) && (board[1][i] == board[2][i])){
				if(board[0][i] != 0){
					return board[0][i];
				}
			}
		}
		return 0;
	}

	public int checkDiagonals(){
		if(((board[0][0] == board[1][1]) && (board[0][0] == board[2][2]))
				|| ((board[0][2] == board[2][0]) && (board[0][2] == board[1][1]))){
			if(board[1][1] != 0){
				return board[1][1];
			}
		}
		return 0;
	}
}