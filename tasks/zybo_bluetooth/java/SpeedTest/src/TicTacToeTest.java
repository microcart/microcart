import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class TicTacToeTest {
	TicTacToe game;

	/*
	 * Execution begins at main(). In this test class, we will execute a text
	 * test runner that will tell you if any of your tests fail.
	 */
	// public static void main(String[] args) {
	// junit.textui.TestRunner.run(suite());
	// }
	//
	// public static junit.framework.Test suite() {
	// return new JUnit4TestAdapter(TicTacToeTest.class);
	// }

	@Before
	public void setUp() throws Exception{
		game = new TicTacToe();
	}

	@Test
	public void testInitialize(){
		assertTrue(game.currentPlayer == 1);
	}

	@Test
	public void testInitialize2(){
		assertTrue(game.board[0][0] == 0);
		assertTrue(game.board[0][1] == 0);
		assertTrue(game.board[0][2] == 0);
		assertTrue(game.board[1][0] == 0);
		assertTrue(game.board[1][1] == 0);
		assertTrue(game.board[1][2] == 0);
		assertTrue(game.board[2][0] == 0);
		assertTrue(game.board[2][1] == 0);
		assertTrue(game.board[2][2] == 0);
	}

	@Test
	public void checkForWinner1(){
		game.board[0][0] = 2;
		game.board[0][1] = 1;
		game.board[0][2] = 2;
		game.board[1][0] = 1;
		game.board[1][1] = 2;
		game.board[1][2] = 1;
		game.board[2][0] = 1;
		game.board[2][1] = 2;
		game.board[2][2] = 1;
		assertTrue(game.checkForWinner() == -1);
	}

	@Test
	public void checkForWinner2(){
		game.board[0][0] = 2;
		game.board[0][1] = 1;
		game.board[0][2] = 1;
		game.board[1][0] = 1;
		game.board[1][1] = 2;
		game.board[1][2] = 1;
		game.board[2][0] = 1;
		game.board[2][1] = 1;
		game.board[2][2] = 2;
		assertTrue(game.checkForWinner() == 2);
	}

	@Test
	public void testGetCurrentPlayer(){
		assertTrue(game.getCurrentPlayer() == 1);
		game.makeMove(0, 0); // interaction with other unit!
		assertTrue(game.getCurrentPlayer() == 2);
		game.makeMove(1, 0);
		assertTrue(game.getCurrentPlayer() == 1);
		game.makeMove(1, 0); // illegal move
		assertTrue(game.getCurrentPlayer() == 1);
		boolean move = game.makeMove(0, 0);
		assertTrue(move == false);
	}

	@Test
	public void testMakeMove1(){
		int player = game.getCurrentPlayer();
		assertTrue(game.makeMove(0, 0)); // move is valid
		// player updates
		assertFalse(game.getCurrentPlayer() == player);
	}

	@Test
	public void testMakeMove2(){
		game.makeMove(0, 0);
		int player = game.getCurrentPlayer();
		assertFalse(game.makeMove(0, 0)); // illegal move
		// player is same
		assertTrue(game.getCurrentPlayer() == player);
	}

	@Test
	public void testUpdatePlayer(){
		int playerBefore = 1;
		game.currentPlayer = playerBefore;
		game.updatePlayer();
		int playerAfter = game.currentPlayer;
		assertFalse(playerBefore == playerAfter);
	}

	@Test
	public void testCheckForWinner(){
		game.makeMove(0, 0); // player 1
		game.makeMove(0, 2); // player 2
		game.makeMove(2, 0); // player 1
		assertTrue(game.checkForWinner() == 0);
		game.makeMove(0, 1); // player 2
		game.makeMove(2, 2); // player 1
		game.makeMove(1, 0); // player 2
		game.makeMove(1, 1); // player 1
		assertTrue(game.checkForWinner() == 1);
		// now check for full board
	}
}