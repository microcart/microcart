import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import jssc.SerialPort;
import jssc.SerialPortException;


public class LatencyTest {
	public static void main(String[] args){
		SerialPort serialPort = new SerialPort("/dev/rfcomm0");
		Random rand = new Random(42);
		System.out.println("Opening port " + "/dev/rfcomm0" + "...");
		try{
			serialPort.openPort();
			serialPort.setParams(921600, 8, 1, 0);
			Scanner in = new Scanner(System.in);
			System.out.println("Success!");
			
			List<Double> times = new ArrayList<Double>();
			do {
				System.out.println("Writing...");
				byte bytes = "A".getBytes()[0];
				serialPort.writeByte(bytes);
				long start = System.nanoTime();
				
				byte[] b = serialPort.readBytes(1);
				long end = System.nanoTime();
				double millis = (end - start)/1000000.0;
				times.add(millis);
	
				System.out.println("Time: " + String.format("%.2f", millis) + " ms");
				System.out.println("    AVG: " + String.format("%.2f", avg(times)) + " ms");
				System.out.println();
			} while(!in.nextLine().contains("s"));
		} catch(SerialPortException e){
			e.printStackTrace();
		}
	}
	
	private static double avg(List<Double> times) {
		double sum = 0;
		for (Double d : times) {
			sum += d;
		}
		
		return sum / times.size();
	}
}
