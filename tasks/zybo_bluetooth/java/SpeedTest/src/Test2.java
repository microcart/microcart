import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import jssc.SerialPort;
import jssc.SerialPortException;
import jssc.SerialPortTimeoutException;

public class Test2 {
	public static void main(String[] args){
		SerialPort serialPort = new SerialPort("COM41");
		Random rand = new Random(42);
		try{
			System.out.println("Opening port " + "COM41" + "...");
			serialPort.openPort();
			serialPort.setParams(921600, 8, 1, 0);
			Scanner in = new Scanner(System.in);
			System.out.println("Success!");
			
			int letterCount = 0;
			while(!in.nextLine().contains("S")) {
				serialPort.writeByte((byte)('A'+(letterCount++)%26));
				System.out.println("WROTE BYTE");
				
				int sum = 0;
				int count = 0;
				byte[] data = new byte[1<<20+1];
				int recvd = 0;
				while (true) {
					int packetCount = 0;
					byte[] ba = serialPort.readBytes(1);
					byte b = ba[0];
					if (b == 101) {
						long start = System.currentTimeMillis();
						while (b != 105) {
							ba = serialPort.readBytes(1);
							count += ba.length;
						//	System.out.println("RECEIVED: " + ba.length + " --- #" + packetCount++);
							for (int i=0; i < ba.length; i++) {
								b = ba[i];
								data[recvd++] = b;
								
								if (b != 105) {
									sum += b;
								} else {
									break;
								}
							}
						}
						long end = System.currentTimeMillis();
						System.out.println("#" + count + " [sum: " + sum + "] [time: " + (end - start) + "ms]");
						break;
					}
				}
			}
		} catch(SerialPortException ex){
			System.err.println("Could not open serial port!");
			ex.printStackTrace();
			System.exit(0);
		}
	}
}
