import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import jssc.SerialPort;
import jssc.SerialPortException;
import jssc.SerialPortTimeoutException;

public class Test {
	public static void main(String[] args){
		SerialPort serialPort = new SerialPort("/dev/rfcomm0");
		Random rand = new Random(42);
		try{
			System.out.println("Opening port " + "/dev/rfcomm0" + "...");
			serialPort.openPort();
			serialPort.setParams(921600, 8, 1, 0);
			Scanner in = new Scanner(System.in);

			System.out.println("Success!");
			int letterCount = 0;
			while(true){
				in.nextLine();
				serialPort.writeByte((byte) ('A' + (letterCount++) % 26));
				System.out.println("Wrote byte");

				// byte[] data = serialPort.readBytes(1);
				byte[] b = null;
				do{
					b = serialPort.readBytes(1);
				} while(b[0] != 101);
				long start = System.currentTimeMillis();
				System.out.println("STARTED");
				//
				// int count = 0;
				// int i=1;
				// while(count < 1048575)
				// {
				// //byte[] data2 = serialPort.readBytes(1024);
				// byte[] arr = serialPort.readBytes();
				// if (arr != null) {
				// count += arr.length;
				// System.out.println((i++)+ ": " + count);
				// } else {
				// System.out.println("null");
				// }
				// }
				// byte[] data2 = serialPort.readBytes((1<<15)-1);
				int hash = 1;
				boolean done = false;
				int sum = 0;
				int count = 0;
				int prevJ = -1;
				List<byte[]> list = new ArrayList<byte[]>();
				while(!done){
					// b = serialPort.readBytes(1);
					try{
						b = serialPort.readBytes(1<<13, 1000);
						list.add(b);
					//	serialPort.writeByte((byte) b[0]);
					} catch(SerialPortTimeoutException e){
						for(int i = 0; i < b.length; i++){
							if(b[i] == 105){
								done = true;
								break;

							}
						}
						if(!done){
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						break;
					}
					// System.out.println("GOT ARRAY");
					// b = serialPort.
					for(int i = 0; i < b.length; i++){
						if(b[i] == 105){
							done = true;
							break;

						}
						hash = (31 * hash) + b[i];
						sum += b[i];

					}
					if(done){
						break;
					}

					count += b.length;
					// if(b.length != 1){
					System.err.println("L: " + b.length + " -- C: " + count + " -- S: " + sum);
					// }
					if(b[0] != prevJ){
						System.out.println("J: " + b[0]);
						prevJ = b[0];
					}

					if(!false)break;
					// System.out.println(Arrays.toString(b));
					// b = serialPort.readBytes(1024);
					// if (b != null) {
					// for (int i=0; i < b.length; i++) {
					// if (b[i] == 80) {
					// System.out.println(Arrays.toString(b));
					// done = true;
					// break;
					// }
					// }
					// }
				}
				long end = System.currentTimeMillis();
				// System.out.println(data.length + " : " + data2.length);
				//
				System.out.println("#" + count + " [" + hash + "] [ " + sum + "]   " + (end - start) + "ms");

			}
			// char[] data3 = new char[data.length + data2.length];
			// data3[0] = (char) data[0];
			// for (int i=0; i < data2.length; i++) {
			// data3[i+1] = (char) data2[i];
			// }
			//
			// System.out.println(new String(data3));
			// System.out.println(Arrays.toString(data));
			// System.out.println();
			// System.out.println();
			// System.out.println(Arrays.toString(data2));

			// serialPort.closePort();
		} catch(SerialPortException ex){
			System.err.println("Could not open serial port!");
			ex.printStackTrace();
			System.exit(0);
		}
	}
}
