

#include <iostream> /* Standard input/output definitions */
#include <cstdio> /* Standard input/output definitions */
#include <string> /* String function definitions */
#include <unistd.h> /* UNIX standard function definitions */
#include <fcntl.h> /* File control definitions */
#include <errno.h> /* Error number definitions */
#include <termios.h> /* POSIX terminal control definitions */
#include <cstdlib>
#include <sys/time.h>
#include <vector>
#include <ctime>

int bt_fd;

void rfInit(const char* portName) {
	// portName is of the form "/dev/ttyS0"
	
  bt_fd = open(portName, O_RDWR | O_NDELAY);
  if (bt_fd  == -1)
  {
      //Could not open the port.
      perror("open_port: Unable to open /dev/ttyS0 - ");
  }
  else
  {
     fcntl(bt_fd , F_SETFL, 0);
  }
	

  struct termios options; // UART port options data structure

  // Get the current options for the port...
  tcgetattr(bt_fd, &options);

  // Set the baud rates to 9600...
  cfsetispeed(&options, B921600);
  cfsetospeed(&options, B921600);

  // Enable the receiver and set local mode...
  options.c_cflag |= (CLOCAL | CREAD);

  // Set charater size
  options.c_cflag &= ~CSIZE; // Mask the character size bits
  options.c_cflag |= CS8;    // Select 8 data bits

  // Set no parity 8N1
  options.c_cflag &= ~PARENB;
  options.c_cflag &= ~CSTOPB;
  options.c_cflag &= ~CSIZE;
  options.c_cflag |= CS8;

  // Disable Hardware flow control
  options.c_cflag &= ~CRTSCTS;

  // Use raw input
  options.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);

  // Disable SW flow control
  options.c_iflag &= ~(IXON | IXOFF | IXANY);

  // Use raw output
  options.c_oflag &= ~OPOST;

  // Set new options for the port...
  tcsetattr(bt_fd , TCSANOW, &options);
}


int main(int argc, char *argv[]) {
	std::string portName = "/dev/rfcomm0";
	std::cout << "Attempting connection to " << portName << "..." << std::endl;
	rfInit(portName.c_str());
	std::cout << "Success!" << std::endl;

	std::srand(time(NULL));	
	std::vector<double> times;
	while(1) {
		std::cout << std::endl << "Press Enter to continue." << std::endl;
		std::cin.get();
		timeval tim;
		
		char data[10];
		data[0] = 'A';
		char recvBuf[10];
		int num = 1;
		int nbyte = -1;
	 	gettimeofday(&tim, NULL);
		double t1=tim.tv_sec+(tim.tv_usec/1000000.0);
		{
			write(bt_fd, data, num);
			nbyte = read(bt_fd, recvBuf, num);
		}
		gettimeofday(&tim, NULL);
		double t2=tim.tv_sec+(tim.tv_usec/1000000.0);
	  	double elapsed_millis = (t2 - t1)*1000;
		printf("Time: %.3fms, Recv [#%d]: %d (%c)\n", elapsed_millis, nbyte, recvBuf[0], recvBuf[0]);  
		times.push_back(elapsed_millis);
		
		std::vector<double>::size_type size = times.size();
    	double sum = 0;
   	for(std::vector<double>::const_iterator i = times.begin(); i != times.end(); ++i)
      	sum += *i;
		double avg = sum/size;

		printf("\tAVG TIME: %.3f\n", avg);
	}
	
	return 0;
}
