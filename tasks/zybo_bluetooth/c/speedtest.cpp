

#include <iostream> /* Standard input/output definitions */
#include <cstdio> /* Standard input/output definitions */
#include <string> /* String function definitions */
#include <unistd.h> /* UNIX standard function definitions */
#include <fcntl.h> /* File control definitions */
#include <errno.h> /* Error number definitions */
#include <termios.h> /* POSIX terminal control definitions */
#include <cstdlib>
#include <time.h>
#include <ctime>

int bt_fd;

void rfInit(const char* portName) {
	// portName is of the form "/dev/ttyS0"
	
  bt_fd = open(portName, O_RDWR | O_NDELAY);
  if (bt_fd  == -1)
  {
      //Could not open the port.
      perror("open_port: Unable to open /dev/ttyS0 - ");
  }
  else
  {
     fcntl(bt_fd , F_SETFL, 0);
  }
	

  struct termios options; // UART port options data structure

  // Get the current options for the port...
  tcgetattr(bt_fd, &options);

  // Set the baud rates to 9600...
  cfsetispeed(&options, B921600);
  cfsetospeed(&options, B921600);

  // Enable the receiver and set local mode...
  options.c_cflag |= (CLOCAL | CREAD);

  // Set charater size
  options.c_cflag &= ~CSIZE; // Mask the character size bits
  options.c_cflag |= CS8;    // Select 8 data bits

  // Set no parity 8N1
  options.c_cflag &= ~PARENB;
  options.c_cflag &= ~CSTOPB;
  options.c_cflag &= ~CSIZE;
  options.c_cflag |= CS8;

  // Disable Hardware flow control
  options.c_cflag &= ~CRTSCTS;

  // Use raw input
  options.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);

  // Disable SW flow control
  options.c_iflag &= ~(IXON | IXOFF | IXANY);

  // Use raw output
  options.c_oflag &= ~OPOST;

  // Set new options for the port...
  tcsetattr(bt_fd , TCSANOW, &options);
}


int main(int argc, char *argv[]) {
	std::string portName = "/dev/rfcomm0";
	std::cout << "Attempting connection to " << portName << "..." << std::endl;
	rfInit(portName.c_str());
	std::cout << "Success!" << std::endl;

	std::srand(time(NULL));	
	while(1) {
		std::cout << std::endl << "Press Enter to continue." << std::endl;
		std::cin.get();
		
		int num = 1<<20;
		char* data = new char[num];
		int hash = 0;
		int sum = 0;
		
		data[0] = '_';
		for (int i=1; i < num - 33; i++) {
			data[i] = 65 + rand() % 26;
			
			sum = sum + data[i];
			hash = (hash * 31) + data[i];
		}
		data[num-33] = '_';
		sprintf(&data[num-32], "%15d %16d", hash, sum);
		printf("[hash:%d] [sum:%d]\n", hash, sum);
	
	 	time_t begin = time(NULL);
		{
			write(bt_fd, data, num);
		}
	 	time_t end = time(NULL);
	  	double elapsed_secs = difftime(end,begin);
		printf("Time: %.3fs\n", elapsed_secs);  
		
		delete[] data;
	}
	
	return 0;
}
