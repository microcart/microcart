/*
 * Copyright (c) 2009-2012 Xilinx, Inc.  All rights reserved.
 *
 * Xilinx, Inc.
 * XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS" AS A
 * COURTESY TO YOU.  BY PROVIDING THIS DESIGN, CODE, OR INFORMATION AS
 * ONE POSSIBLE   IMPLEMENTATION OF THIS FEATURE, APPLICATION OR
 * STANDARD, XILINX IS MAKING NO REPRESENTATION THAT THIS IMPLEMENTATION
 * IS FREE FROM ANY CLAIMS OF INFRINGEMENT, AND YOU ARE RESPONSIBLE
 * FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE FOR YOUR IMPLEMENTATION.
 * XILINX EXPRESSLY DISCLAIMS ANY WARRANTY WHATSOEVER WITH RESPECT TO
 * THE ADEQUACY OF THE IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO
 * ANY WARRANTIES OR REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE
 * FROM CLAIMS OF INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 *
 *
 *   Zybo Bluetooth example
 *   Authors: Paul Gerver and Adam Campbell
 *
 *   This file does demonstrates bluetooth capability on the Zybo board
 *   through the board's UART0 controller
 *
 *   TX pin = JB 2
 *   RX pint = JB 3
 *
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include "platform.h"

#include "sleep.h"
#include "uart.h"

#define size (1<<20)
//char data[size];

int main7() {

	printf("\r\nStart Bluetooth Example v3\r\n");
	printf("==========================\r\n\r\n");
	init_platform();

	uart0_init(XPAR_PS7_UART_0_DEVICE_ID, 921600);
	uart0_clearFIFOs();

	char* data = malloc(size);
	printf("DATA: %p, SIZE: %d\r\n", data, size);

	while (1) {
		printf("WAITING\r\n");

		int count = 0;

		int sum = 0;
		int hash = 0;
		for (count = 0; count < 4; count++) {
			// start
			uart0_sendByte(101);

			int num = 16384;
			num = uart0_recvBytes(data, num);

			int i;
			for (i = 0; i < num; i++) {
				sum += data[i];
				hash = (hash * 31) + data[i];
			}

			//	data[num] = 0;
			printf("RECEIVED (%d):\r\n", num);
			printf("[sum:%d]  [hash:%d]\r\n", sum, hash);

			int print = 0;
			if (print) {
				printf("[");

				for (i = 0; i < num - 1; i++) {
					printf("%d, ", data[i]);
				}
				printf("%d]\r\n", data[num - 1]);
			}
		}
		uart0_sendByte(105);
	}

	return 0;
}

