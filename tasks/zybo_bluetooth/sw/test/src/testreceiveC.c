/*
 * Copyright (c) 2009-2012 Xilinx, Inc.  All rights reserved.
 *
 * Xilinx, Inc.
 * XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS" AS A
 * COURTESY TO YOU.  BY PROVIDING THIS DESIGN, CODE, OR INFORMATION AS
 * ONE POSSIBLE   IMPLEMENTATION OF THIS FEATURE, APPLICATION OR
 * STANDARD, XILINX IS MAKING NO REPRESENTATION THAT THIS IMPLEMENTATION
 * IS FREE FROM ANY CLAIMS OF INFRINGEMENT, AND YOU ARE RESPONSIBLE
 * FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE FOR YOUR IMPLEMENTATION.
 * XILINX EXPRESSLY DISCLAIMS ANY WARRANTY WHATSOEVER WITH RESPECT TO
 * THE ADEQUACY OF THE IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO
 * ANY WARRANTIES OR REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE
 * FROM CLAIMS OF INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 *
 *
 *   Zybo Bluetooth example
 *   Authors: Paul Gerver and Adam Campbell
 *
 *   This file does demonstrates bluetooth capability on the Zybo board
 *   through the board's UART0 controller
 *
 *   TX pin = JB 2
 *   RX pint = JB 3
 *
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include "platform.h"

#include "sleep.h"
#include "uart.h"

#define size (1<<16)
//char data[size];

int main19() {

	printf("\r\nStart Bluetooth Example v4\r\n");
	init_platform();

	uart0_init(XPAR_PS7_UART_0_DEVICE_ID, 921600);
	uart0_clearFIFOs();

	//uart0_sendStr("\r\nGoodbye Hello Boo Testing, testing, 1 2 3...\r\n");

	char* data = malloc(size);
	printf("DATA: %p\r\n", data);

	while(1) {
		char b = uart0_recvByte();
		int hash = 0;
		int sum = 0;
		if (b == '_') {
			do {
				b = uart0_recvByte();
				if (b != '_') {
					sum = sum + b;
					hash = (31 * hash) + b;
				}
			} while (b != '_');

			printf("hash:[%15d sum:[%11d]\r\n", hash, sum);

			uart0_recvBytes(data, 32);
			data[32] = 0;
			printf("data:[%s]\r\n\r\n", data);
		}
	}

	return 0;
}

