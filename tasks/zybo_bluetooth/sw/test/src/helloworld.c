/*
 * Copyright (c) 2009-2012 Xilinx, Inc.  All rights reserved.
 *
 * Xilinx, Inc.
 * XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS" AS A
 * COURTESY TO YOU.  BY PROVIDING THIS DESIGN, CODE, OR INFORMATION AS
 * ONE POSSIBLE   IMPLEMENTATION OF THIS FEATURE, APPLICATION OR
 * STANDARD, XILINX IS MAKING NO REPRESENTATION THAT THIS IMPLEMENTATION
 * IS FREE FROM ANY CLAIMS OF INFRINGEMENT, AND YOU ARE RESPONSIBLE
 * FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE FOR YOUR IMPLEMENTATION.
 * XILINX EXPRESSLY DISCLAIMS ANY WARRANTY WHATSOEVER WITH RESPECT TO
 * THE ADEQUACY OF THE IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO
 * ANY WARRANTIES OR REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE
 * FROM CLAIMS OF INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    921600 (configured by bootrom/bsp)
 *
 *
 *   Zybo Bluetooth example
 *   Authors: Paul Gerver and Adam Campbell
 *
 *   This file does demonstrates bluetooth capability on the Zybo board
 *   through the board's UART0 controller
 *
 *   TX pin = JB 2
 *   RX pint = JB 3
 *
 *
 */

#include <stdio.h>
#include "platform.h"
#include "xparameters.h"
#include "xuartps.h"


XUartPs Uart_PS;


int main2()
{

	printf("\r\nStart Bluetooth Example\r\n");
    init_platform();


    //Get configure struct initialized
    XUartPs_Config* config;
    config = XUartPs_LookupConfig(XPAR_PS7_UART_0_DEVICE_ID);

    //Configure XUartPs instance
    int Status = XUartPs_CfgInitialize(&Uart_PS, config, config->BaseAddress);
    if (Status != XST_SUCCESS){
      return XST_FAILURE;
    }

    //Set Baudrate for BT
    XUartPs_SetBaudRate(&Uart_PS, 115200);

    //Get UART0 Control Register and clear the TX and RX Fifos
    int* uart_ctrl_reg = (int*) XPAR_PS7_UART_0_BASEADDR;
    *uart_ctrl_reg |= 0x00000003; // clear TX & RX


    char* SendFifo = "i am the very model of A MODERN major general!";
    char RecvFifo[1024] = {};


    //Send the message in SendFifo
    XUartPs_Send(&Uart_PS, SendFifo, strlen(SendFifo));

    printf("Sending...\r\n");
    //Wait until bytes are sent
    while(XUartPs_IsSending(&Uart_PS));

    printf("Send complete\r\n");



    //Wait until received data (if
    while(!XUartPs_IsReceiveData(config->BaseAddress));

    int ReceivedCount = 0;


    int prev = ReceivedCount;
    while(ReceivedCount < strlen(SendFifo) || 1) {
    	ReceivedCount += XUartPs_Recv(&Uart_PS, &RecvFifo[ReceivedCount], (strlen(SendFifo) - ReceivedCount));
    	int i;
    	if (ReceivedCount != prev) {
			for(i=prev; i < ReceivedCount; i++) {
				if (RecvFifo[i] == '\r') {
					RecvFifo[i] = 0;
					goto end;
				}
			}
    	}

    	prev = ReceivedCount;
    }

    end:

    printf("done: \"%s\"\r\n", RecvFifo);

    return 0;
}

