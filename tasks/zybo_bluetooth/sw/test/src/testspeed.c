/*
 * Copyright (c) 2009-2012 Xilinx, Inc.  All rights reserved.
 *
 * Xilinx, Inc.
 * XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS" AS A
 * COURTESY TO YOU.  BY PROVIDING THIS DESIGN, CODE, OR INFORMATION AS
 * ONE POSSIBLE   IMPLEMENTATION OF THIS FEATURE, APPLICATION OR
 * STANDARD, XILINX IS MAKING NO REPRESENTATION THAT THIS IMPLEMENTATION
 * IS FREE FROM ANY CLAIMS OF INFRINGEMENT, AND YOU ARE RESPONSIBLE
 * FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE FOR YOUR IMPLEMENTATION.
 * XILINX EXPRESSLY DISCLAIMS ANY WARRANTY WHATSOEVER WITH RESPECT TO
 * THE ADEQUACY OF THE IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO
 * ANY WARRANTIES OR REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE
 * FROM CLAIMS OF INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 *
 *
 *   Zybo Bluetooth example
 *   Authors: Paul Gerver and Adam Campbell
 *
 *   This file does demonstrates bluetooth capability on the Zybo board
 *   through the board's UART0 controller
 *
 *   TX pin = JB 2
 *   RX pint = JB 3
 *
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include "platform.h"

#include "sleep.h"
#include "uart.h"

#define size (1<<16)
//char data[size];

int main5() {

	printf("\r\nStart Bluetooth Example v3\r\n");
	init_platform();

	uart0_init(XPAR_PS7_UART_0_DEVICE_ID, 921600);
	uart0_clearFIFOs();

	//uart0_sendStr("\r\nGoodbye Hello Boo Testing, testing, 1 2 3...\r\n");

	signed char* data = malloc(size);
	printf("DATA: %p\r\n", data);
	int j = 0;

//    for (i=0; i < size; i++) {
//
//    }

	int dummy;
	while (1) {

		dummy = uart0_recvByte();
		printf("entered: %c\r\n\r\n", dummy);
		srand(42);
		int i = 0;
		int hash = 1;
		int sum = 0;

		int k = 0;
		//for (k=0;k<4;k++) {

		for (j = 0; j < 1; j++) {

			for (i = 0; i < size; i++) {
				//data[i] = rand() % 100;
				data[i] = i;
				if (data[i] == 105) {
					data[i] = 104;
				}

				if (j == 0 && i == 0) {
					data[i] = 101; // start
				} else if (i == size - 1 && j == 1 - 1) {
					data[i] = 105; //end
				} else {
					hash = (hash * 31) + data[i];
					sum += data[i];
				}

			}
			uart0_sendBytes((char*) data, size);

			while (uart0_isSending())
				;
			//int done = uart0_recvByte();
			//printf("RECEIVED: %d\r\n", done);

			//usleep(1e6);
		}
		//uart0_sendByte(105);
		printf("hash: %d, sum: %d, size: %d\r\n", hash, sum, size);

	}

	//	usleep(1e6);
	//  }

	return 0;
}

