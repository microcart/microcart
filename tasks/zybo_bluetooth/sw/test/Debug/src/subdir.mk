################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/helloworld.c \
../src/platform.c \
../src/testLatency.c \
../src/testreceive.c \
../src/testreceiveC.c \
../src/testspeed.c \
../src/uart.c 

LD_SRCS += \
../src/lscript.ld 

OBJS += \
./src/helloworld.o \
./src/platform.o \
./src/testLatency.o \
./src/testreceive.o \
./src/testreceiveC.o \
./src/testspeed.o \
./src/uart.o 

C_DEPS += \
./src/helloworld.d \
./src/platform.d \
./src/testLatency.d \
./src/testreceive.d \
./src/testreceiveC.d \
./src/testspeed.d \
./src/uart.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: ARM gcc compiler'
	arm-xilinx-eabi-gcc -Wall -O0 -g3 -c -fmessage-length=0 -I../../test_bsp/ps7_cortexa9_0/include -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


