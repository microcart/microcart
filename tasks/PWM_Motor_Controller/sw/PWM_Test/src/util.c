/*
 * util.c
 *
 *  Created on: Oct 11, 2014
 *      Author: Tyler
 */

#include "util.h"
#include "controllers.h"
#include "xparameters.h"
#include <stdio.h>
#include <math.h>
#include <sleep.h>

//Global variable representing the current pulseW
int pulseW = pulse_throttle_low;

/**
 * Initializes the PWM output components.
 * Default pulse length  = 1 ms
 * Default period legnth = 2.33 ms
 */
void pwm_init() {

	// Initializes all the PWM address to have the correct period width at 450 hz
	*(int*) (PWM_0_ADDR + PWM_PERIOD) = period_width;
	*(int*) (PWM_1_ADDR + PWM_PERIOD) = period_width;
	*(int*) (PWM_2_ADDR + PWM_PERIOD) = period_width;
	*(int*) (PWM_3_ADDR + PWM_PERIOD) = period_width;
	xil_printf("Period initialization successful %d\n", period_width);
	// Initializes the PWM pulse lengths to be 1 ms
	*(int*) (PWM_0_ADDR + PWM_PULSE) = pulse_throttle_low;
	*(int*) (PWM_1_ADDR + PWM_PULSE) = pulse_throttle_low;
	*(int*) (PWM_2_ADDR + PWM_PULSE) = pulse_throttle_low;
	*(int*) (PWM_3_ADDR + PWM_PULSE) = pulse_throttle_low;
	xil_printf("Pulse initialization successful %d\n", pulse_throttle_low);

#ifdef X_CONFIG
	xil_printf("In x config mode\n");
#else
	xil_printf("In + config mode\n");
#endif
	usleep(1000000);
}
/**
 * Writes all PWM components to be the same given pulsewidth
 */
void pwm_write_all(int pulseWidth) {
	// Check lower and upper bounds
	if (pulseWidth > pulse_throttle_high)
		pulseWidth = pulse_throttle_high;
	if (pulseWidth < pulse_throttle_low)
		pulseWidth = pulse_throttle_low;
	// Set all the pulse widths
	*(int*) (PWM_0_ADDR + PWM_PULSE) = pulseWidth;
	*(int*) (PWM_1_ADDR + PWM_PULSE) = pulseWidth;
	*(int*) (PWM_2_ADDR + PWM_PULSE) = pulseWidth;
	*(int*) (PWM_3_ADDR + PWM_PULSE) = pulseWidth;
}
/**
 * Write a given pulseWidth to a channel
 */
void pwm_write_channel(int pulseWidth, int channel){
	// Check lower and upper bounds
		if (pulseWidth > pulse_throttle_high)
			pulseWidth = pulse_throttle_high;
		if (pulseWidth < pulse_throttle_low)
			pulseWidth = pulse_throttle_low;

		switch(channel){
		case 0:
			*(int*) (PWM_0_ADDR + PWM_PULSE) = pulseWidth;
			break;
		case 1:
			*(int*) (PWM_1_ADDR + PWM_PULSE) = pulseWidth;
			break;
		case 2:
			*(int*) (PWM_2_ADDR + PWM_PULSE) = pulseWidth;
			break;
		case 3:
			*(int*) (PWM_3_ADDR + PWM_PULSE) = pulseWidth;
			break;
		default:
			break;
		}
}
/**
 * Reads the registers from the PWM_Recorders, and returns the pulse width
 * of the last PWM signal to come in
 */
int read_rec(int channel) {
	switch (channel) {
	case 0:
		return *((int*) PWM_REC_0_ADDR);
	case 1:
		return *((int*) PWM_REC_1_ADDR);
	case 2:
		return *((int*) PWM_REC_2_ADDR);
	case 3:
		return *((int*) PWM_REC_3_ADDR);
	case 4:
		return *((int*) PWM_REC_4_ADDR);
	default:
		return 0;
	}
}
/**
 * Reads all 4 receiver channels at once
 */
void read_rec_all(int* mixer){
	int i;
	for(i = 0; i < 5; i++){
		mixer[i] = read_rec(i);
	}
}

/**
 * Use the buttons to drive the pulse length of the channels
 */
void b_drive_pulse() {
	int* btns = XPAR_BTNS_BASEADDR;

	// Increment the pulse width by 5% throttle
	if (*btns & 0x1) {
		pulseW += 1000;
		if (pulseW > 200000)
			pulseW = 200000;
		pwm_write_all(pulseW);
		while (*btns & 0x1)
			;
	} //Decrease the pulse width by 5% throttle
	else if (*btns & 0x2) {
		pulseW -= 1000;
		if (pulseW < 100000) {
			pulseW = 100000;
		}
		pwm_write_all(pulseW);
		while (*btns & 0x2)
			;
	}
	// Set the pulses back to default
	else if (*btns & 0x4) {
		pulseW = MOTOR_0_PERCENT;
		pwm_write_all(pulseW);
	}
	// Read the pulse width of pwm_recorder 0
	else if (*btns & 0x8) {
		int i;
		for(i = 0; i < 4; i++){
			xil_printf("Channel %d:  %d\n", i, read_rec(i));
		}
		//xil_printf("%d\n",pulseW);
		while (*btns & 0x8)
			;
	}
}

/**
 * Creates a sine wave driving the motors from 0 to 100% throttle
 */
void sine_example(){

	int* btns = XPAR_BTNS_BASEADDR;
	/*        Sine Wave        */
	static double time = 0;

	time += .0001;
	pulseW = (int)fabs(sin(time)*(100000)) + 100000;
	if (*btns & 0x1){
			xil_printf("%d", pulseW);
			xil_printf("   %d\n", *(int*) (PWM_0_ADDR + PWM_PULSE));
	}
	pwm_write_all(pulseW);
	usleep(300);
}

void print_mixer(int * mixer){
	int i;
	for(i = 0; i < 4; i++){
		xil_printf("%d : %d			", i, mixer[i]);
	}
	xil_printf("\n");
}

/**
 * Argument is the reading from the pwm_recorder4 which is connected to the gear pwm
 * If the message from the receiver is 0 - gear, kill the system by sending a 1
 * Otherwise, do nothing
 */
int read_kill(int kill){
	if(kill > 118000 && kill < 118500)
		return 1;
	return 0;
}
/**
 * Turns off the motors
 */
void pwm_kill(){
	// Initializes the PWM pulse lengths to be 1 ms
	*(int*) (PWM_0_ADDR + PWM_PULSE) = pulse_throttle_low;
	*(int*) (PWM_1_ADDR + PWM_PULSE) = pulse_throttle_low;
	*(int*) (PWM_2_ADDR + PWM_PULSE) = pulse_throttle_low;
	*(int*) (PWM_3_ADDR + PWM_PULSE) = pulse_throttle_low;
	xil_printf("Kill switch was touched, shutting off the motors and ending the program\n");
}
