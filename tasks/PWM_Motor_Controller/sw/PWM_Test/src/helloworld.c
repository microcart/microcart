/*
 * Copyright (c) 2009-2012 Xilinx, Inc.  All rights reserved.
 *
 * Xilinx, Inc.
 * XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS" AS A
 * COURTESY TO YOU.  BY PROVIDING THIS DESIGN, CODE, OR INFORMATION AS
 * ONE POSSIBLE   IMPLEMENTATION OF THIS FEATURE, APPLICATION OR
 * STANDARD, XILINX IS MAKING NO REPRESENTATION THAT THIS IMPLEMENTATION
 * IS FREE FROM ANY CLAIMS OF INFRINGEMENT, AND YOU ARE RESPONSIBLE
 * FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE FOR YOUR IMPLEMENTATION.
 * XILINX EXPRESSLY DISCLAIMS ANY WARRANTY WHATSOEVER WITH RESPECT TO
 * THE ADEQUACY OF THE IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO
 * ANY WARRANTIES OR REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE
 * FROM CLAIMS OF INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include <stdio.h>
#include "platform.h"
#include <sleep.h>
#include <xparameters.h>
#include <math.h>
#include "util.h"
#include "controllers.h"

char *channel_types[4] = {"Throttle", "Roll    ", "Pitch   ", "Yaw     "};
/**
 * Testing definitions
 */
#define SINE_EX     0
#define BUTTON_TEST 0


int main() {

	init_platform();
	xil_printf("Hello\n");
	pwm_init();
	xil_printf("\nStarting\n");

	int mixer[5];
	int pwms[4];
	while (1) {


		// Either do the sine example or push buttons
		if (SINE_EX)
			sine_example();
		else if (BUTTON_TEST)
			b_drive_pulse();
		else {

			// Read in from the PWM recorders
			read_rec_all(mixer);

			// Convert from the aero inputs to PWMS
			Aero_to_PWMS(pwms, mixer);

			// If the kill switch was hit, end the system
			if(read_kill(mixer[4])){
				pwm_kill();
				return 0;
			}

			int i;
			for (i = 0; i < 4; i++)
				pwm_write_channel(pwms[i], i);


			for (i = 0; i < 4; i++) {
				xil_printf("Motor %d.  %6d", i,  pwms[i]);
				xil_printf("       Channel %s: %6d\r\n", channel_types[i], mixer[i]);

			}
			//xil_printf("\n\n");
			for(i=0;i<5;i++){
				xil_printf("####################");
			}
			xil_printf("\r\n");
			usleep(8e4);
			//xil_printf("\n\n\n\n\n\n\n\n\n\n");
			int* btns = XPAR_BTNS_BASEADDR;
			if (*btns & 0x8) {

				xil_printf("Kill channel %d\n", mixer[4]);
				//xil_printf("%d\n",pulseW);
				while (*btns & 0x8)
					;

			}
		}
	}
}
