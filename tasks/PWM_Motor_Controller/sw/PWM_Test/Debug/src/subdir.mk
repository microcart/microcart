################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/controllers.c \
../src/helloworld.c \
../src/platform.c \
../src/util.c 

LD_SRCS += \
../src/lscript.ld 

OBJS += \
./src/controllers.o \
./src/helloworld.o \
./src/platform.o \
./src/util.o 

C_DEPS += \
./src/controllers.d \
./src/helloworld.d \
./src/platform.d \
./src/util.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: ARM gcc compiler'
	arm-xilinx-eabi-gcc -Wall -O0 -g3 -c -fmessage-length=0 -I../../PWM_Test_bsp/ps7_cortexa9_0/include -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


