/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

#include "xsi.h"

struct XSI_INFO xsi_info;

char *IEEE_P_2592010699;
char *STD_STANDARD;
char *IEEE_P_3499444699;
char *IEEE_P_3620187407;
char *PROC_COMMON_V3_00_A_P_2444876401;
char *PROC_COMMON_V3_00_A_P_4147123038;
char *PROC_COMMON_V3_00_A_P_1541446978;
char *IEEE_P_1242562249;
char *IEEE_P_0017514958;
char *SYNOPSYS_P_3308480207;
char *UNISIM_P_0947159679;


int main(int argc, char **argv)
{
    xsi_init_design(argc, argv);
    xsi_register_info(&xsi_info);

    xsi_register_min_prec_unit(-12);
    ieee_p_2592010699_init();
    ieee_p_3499444699_init();
    ieee_p_3620187407_init();
    proc_common_v3_00_a_p_2444876401_init();
    proc_common_v3_00_a_p_1541446978_init();
    ieee_p_1242562249_init();
    synopsys_p_3308480207_init();
    ieee_p_0017514958_init();
    proc_common_v3_00_a_p_4147123038_init();
    unisim_p_0947159679_init();
    proc_common_v3_00_a_a_4169304074_3306564128_init();
    proc_common_v3_00_a_a_1935431134_3306564128_init();
    proc_common_v3_00_a_a_3604465104_3306564128_init();
    axi_lite_ipif_v1_01_a_a_2571985653_3306564128_init();
    axi_lite_ipif_v1_01_a_a_2455361901_3306564128_init();
    axi_lite_ipif_v1_01_a_a_0650785256_3306564128_init();
    unisim_a_2001057742_2388994713_init();
    proc_common_v3_00_a_a_0400612647_3640575771_init();
    pwm_signal_out_v1_00_a_a_2525893486_1744540347_init();
    pwm_signal_out_v1_00_a_a_2961576509_3306564128_init();
    pwm_signal_out_v1_00_a_a_0494838447_3306564128_init();


    xsi_register_tops("pwm_signal_out_v1_00_a_a_0494838447_3306564128");

    IEEE_P_2592010699 = xsi_get_engine_memory("ieee_p_2592010699");
    xsi_register_ieee_std_logic_1164(IEEE_P_2592010699);
    STD_STANDARD = xsi_get_engine_memory("std_standard");
    IEEE_P_3499444699 = xsi_get_engine_memory("ieee_p_3499444699");
    IEEE_P_3620187407 = xsi_get_engine_memory("ieee_p_3620187407");
    PROC_COMMON_V3_00_A_P_2444876401 = xsi_get_engine_memory("proc_common_v3_00_a_p_2444876401");
    PROC_COMMON_V3_00_A_P_4147123038 = xsi_get_engine_memory("proc_common_v3_00_a_p_4147123038");
    PROC_COMMON_V3_00_A_P_1541446978 = xsi_get_engine_memory("proc_common_v3_00_a_p_1541446978");
    IEEE_P_1242562249 = xsi_get_engine_memory("ieee_p_1242562249");
    IEEE_P_0017514958 = xsi_get_engine_memory("ieee_p_0017514958");
    SYNOPSYS_P_3308480207 = xsi_get_engine_memory("synopsys_p_3308480207");
    UNISIM_P_0947159679 = xsi_get_engine_memory("unisim_p_0947159679");

    return xsi_run_simulation(argc, argv);

}
