-------------------------------------------------------------------------------
-- system_stub.vhd
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

entity system_stub is
  port (
    CLK_N : in std_logic;
    CLK_P : in std_logic;
    processing_system7_0_MIO : inout std_logic_vector(53 downto 0);
    processing_system7_0_PS_SRSTB_pin : in std_logic;
    processing_system7_0_PS_CLK_pin : in std_logic;
    processing_system7_0_PS_PORB_pin : in std_logic;
    processing_system7_0_DDR_Clk : inout std_logic;
    processing_system7_0_DDR_Clk_n : inout std_logic;
    processing_system7_0_DDR_CKE : inout std_logic;
    processing_system7_0_DDR_CS_n : inout std_logic;
    processing_system7_0_DDR_RAS_n : inout std_logic;
    processing_system7_0_DDR_CAS_n : inout std_logic;
    processing_system7_0_DDR_WEB_pin : out std_logic;
    processing_system7_0_DDR_BankAddr : inout std_logic_vector(2 downto 0);
    processing_system7_0_DDR_Addr : inout std_logic_vector(14 downto 0);
    processing_system7_0_DDR_ODT : inout std_logic;
    processing_system7_0_DDR_DRSTB : inout std_logic;
    processing_system7_0_DDR_DQ : inout std_logic_vector(31 downto 0);
    processing_system7_0_DDR_DM : inout std_logic_vector(3 downto 0);
    processing_system7_0_DDR_DQS : inout std_logic_vector(3 downto 0);
    processing_system7_0_DDR_DQS_n : inout std_logic_vector(3 downto 0);
    processing_system7_0_DDR_VRN : inout std_logic;
    processing_system7_0_DDR_VRP : inout std_logic;
    pwm_recorder_0_pwm_in_master_pin : in std_logic;
    pwm_recorder_1_pwm_in_master_pin : in std_logic;
    pwm_recorder_2_pwm_in_master_pin : in std_logic;
    pwm_recorder_3_pwm_in_master_pin : in std_logic;
    pwm_recorder_4_pwm_in_master_pin : in std_logic;
    BTNs_4Bits_TRI_IO_GPIO_IO_I_pin : in std_logic_vector(3 downto 0);
    pwm_signal_out_0_pwm_out_sm_pin : out std_logic;
    pwm_signal_out_1_pwm_out_sm_pin : out std_logic;
    pwm_signal_out_2_pwm_out_sm_pin : out std_logic;
    pwm_signal_out_3_pwm_out_sm_pin : out std_logic
  );
end system_stub;

architecture STRUCTURE of system_stub is

  component system is
    port (
      CLK_N : in std_logic;
      CLK_P : in std_logic;
      processing_system7_0_MIO : inout std_logic_vector(53 downto 0);
      processing_system7_0_PS_SRSTB_pin : in std_logic;
      processing_system7_0_PS_CLK_pin : in std_logic;
      processing_system7_0_PS_PORB_pin : in std_logic;
      processing_system7_0_DDR_Clk : inout std_logic;
      processing_system7_0_DDR_Clk_n : inout std_logic;
      processing_system7_0_DDR_CKE : inout std_logic;
      processing_system7_0_DDR_CS_n : inout std_logic;
      processing_system7_0_DDR_RAS_n : inout std_logic;
      processing_system7_0_DDR_CAS_n : inout std_logic;
      processing_system7_0_DDR_WEB_pin : out std_logic;
      processing_system7_0_DDR_BankAddr : inout std_logic_vector(2 downto 0);
      processing_system7_0_DDR_Addr : inout std_logic_vector(14 downto 0);
      processing_system7_0_DDR_ODT : inout std_logic;
      processing_system7_0_DDR_DRSTB : inout std_logic;
      processing_system7_0_DDR_DQ : inout std_logic_vector(31 downto 0);
      processing_system7_0_DDR_DM : inout std_logic_vector(3 downto 0);
      processing_system7_0_DDR_DQS : inout std_logic_vector(3 downto 0);
      processing_system7_0_DDR_DQS_n : inout std_logic_vector(3 downto 0);
      processing_system7_0_DDR_VRN : inout std_logic;
      processing_system7_0_DDR_VRP : inout std_logic;
      pwm_recorder_0_pwm_in_master_pin : in std_logic;
      pwm_recorder_1_pwm_in_master_pin : in std_logic;
      pwm_recorder_2_pwm_in_master_pin : in std_logic;
      pwm_recorder_3_pwm_in_master_pin : in std_logic;
      pwm_recorder_4_pwm_in_master_pin : in std_logic;
      BTNs_4Bits_TRI_IO_GPIO_IO_I_pin : in std_logic_vector(3 downto 0);
      pwm_signal_out_0_pwm_out_sm_pin : out std_logic;
      pwm_signal_out_1_pwm_out_sm_pin : out std_logic;
      pwm_signal_out_2_pwm_out_sm_pin : out std_logic;
      pwm_signal_out_3_pwm_out_sm_pin : out std_logic
    );
  end component;

  attribute BOX_TYPE : STRING;
  attribute BOX_TYPE of system : component is "user_black_box";

begin

  system_i : system
    port map (
      CLK_N => CLK_N,
      CLK_P => CLK_P,
      processing_system7_0_MIO => processing_system7_0_MIO,
      processing_system7_0_PS_SRSTB_pin => processing_system7_0_PS_SRSTB_pin,
      processing_system7_0_PS_CLK_pin => processing_system7_0_PS_CLK_pin,
      processing_system7_0_PS_PORB_pin => processing_system7_0_PS_PORB_pin,
      processing_system7_0_DDR_Clk => processing_system7_0_DDR_Clk,
      processing_system7_0_DDR_Clk_n => processing_system7_0_DDR_Clk_n,
      processing_system7_0_DDR_CKE => processing_system7_0_DDR_CKE,
      processing_system7_0_DDR_CS_n => processing_system7_0_DDR_CS_n,
      processing_system7_0_DDR_RAS_n => processing_system7_0_DDR_RAS_n,
      processing_system7_0_DDR_CAS_n => processing_system7_0_DDR_CAS_n,
      processing_system7_0_DDR_WEB_pin => processing_system7_0_DDR_WEB_pin,
      processing_system7_0_DDR_BankAddr => processing_system7_0_DDR_BankAddr,
      processing_system7_0_DDR_Addr => processing_system7_0_DDR_Addr,
      processing_system7_0_DDR_ODT => processing_system7_0_DDR_ODT,
      processing_system7_0_DDR_DRSTB => processing_system7_0_DDR_DRSTB,
      processing_system7_0_DDR_DQ => processing_system7_0_DDR_DQ,
      processing_system7_0_DDR_DM => processing_system7_0_DDR_DM,
      processing_system7_0_DDR_DQS => processing_system7_0_DDR_DQS,
      processing_system7_0_DDR_DQS_n => processing_system7_0_DDR_DQS_n,
      processing_system7_0_DDR_VRN => processing_system7_0_DDR_VRN,
      processing_system7_0_DDR_VRP => processing_system7_0_DDR_VRP,
      pwm_recorder_0_pwm_in_master_pin => pwm_recorder_0_pwm_in_master_pin,
      pwm_recorder_1_pwm_in_master_pin => pwm_recorder_1_pwm_in_master_pin,
      pwm_recorder_2_pwm_in_master_pin => pwm_recorder_2_pwm_in_master_pin,
      pwm_recorder_3_pwm_in_master_pin => pwm_recorder_3_pwm_in_master_pin,
      pwm_recorder_4_pwm_in_master_pin => pwm_recorder_4_pwm_in_master_pin,
      BTNs_4Bits_TRI_IO_GPIO_IO_I_pin => BTNs_4Bits_TRI_IO_GPIO_IO_I_pin,
      pwm_signal_out_0_pwm_out_sm_pin => pwm_signal_out_0_pwm_out_sm_pin,
      pwm_signal_out_1_pwm_out_sm_pin => pwm_signal_out_1_pwm_out_sm_pin,
      pwm_signal_out_2_pwm_out_sm_pin => pwm_signal_out_2_pwm_out_sm_pin,
      pwm_signal_out_3_pwm_out_sm_pin => pwm_signal_out_3_pwm_out_sm_pin
    );

end architecture STRUCTURE;

