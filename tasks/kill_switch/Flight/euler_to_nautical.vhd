--------------------------------------------------------------
-- Microcart - Spring 2015
-- 
-- This file handles the conversion from euler to nautical.
--  It takes the Acceleration/velocity vectors from the Euler
--  coordinate system, (x, y, z) and converts it to Nautical
--  (roll, pitch, yaw)
--
-- Euler in and nautical out are both 16 bits.
--
-- TODO - Arctan Testing
-- TODO - SQRT Testing
-- TODO - Negative protection
-- TODO - Overall Component Test
-- Math implementation details - 
--  Arctan is calculated using Polynomial Approximation, shown
--  to be high speed, simple, and with a small error
--  The function used is y = -.30097 + .61955 * x - .001659 * x * x
-- Author Tyler Kurtz - tylerkurtz8@gmail.com
--------------------------------------------------------------


LIBRARY ieee;
USE ieee.std_logic_1164.all;
use work.vector_types.all;

entity euler_to_nautical is
	port(
      i_euler    : in Euler;
      o_nautical : out Nautical);
end entity;

architecture behavioral of euler_to_nautical is

begin
  


end behavioral;
