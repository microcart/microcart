--------------------------------------------------------------
-- Microcart - Spring 2015
-- 
-- The purpose of this file is to create types to handle 
--  clean and easy to read code for the hardware implementation
--  of the sensor board to PWM outputs component.
--
-- Author Tyler Kurtz - tylerkurtz8@gmail.com
--------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;

package vector_types is
  type Euler is 
    record
        velocity_x : std_logic_vector(15 downto 0);
        velocity_y : std_logic_vector(15 downto 0);
        velocity_z : std_logic_vector(15 downto 0);
        acceleration_x : std_logic_vector(15 downto 0);
        acceleration_y : std_logic_vector(15 downto 0);
        acceleration_z : std_logic_vector(15 downto 0);
    end record;
  type Nautical is
    record
      Roll_a  : std_logic_vector(15 downto 0);
      Pitch_a : std_logic_vector(15 downto 0);
      Yaw_a   : std_logic_vector(15 downto 0);
      Roll_v  : std_logic_vector(15 downto 0);
      Pitch_v : std_logic_vector(15 downto 0);
      Yaw_v   : std_logic_vector(15 downto 0);
     end record;

  -- Square Root Function
  -- Function used from http://vhdlguru.blogspot.com/2010/03/vhdl-function-for-finding-square-root.html
  function  sqrt  ( d : UNSIGNED ) return UNSIGNED is
  variable a : unsigned(31 downto 0):=d;  --original input.
  variable q : unsigned(15 downto 0):=(others => '0');  --result.
  variable left,right,r : unsigned(17 downto 0):=(others => '0');  --input to adder/sub.r-remainder.
  variable i : integer:=0;

  begin
  for i in 0 to 15 loop
    right(0):='1';
    right(1):=r(17);
    right(17 downto 2):=q;
    left(1 downto 0):=a(31 downto 30);
    left(17 downto 2):=r(15 downto 0);
    a(31 downto 2):=a(29 downto 0);  --shifting by 2 bit.
    if ( r(17) = '1') then
      r := left + right;
    else
      r := left - right;
    end if;
    q(15 downto 1) := q(14 downto 0);
    q(0) := not r(17);
  end loop;
  return q;

  end sqrt;
  
  -- ARCTAN Function
  -- Implements y = -.30097 + .61955 * x - .001659 * x * x
  function arctan (              
end vector_types;
