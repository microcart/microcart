--------------------------------------------------------------
-- Microcart - Spring 2015
-- 
-- The purpose of this project is to handle the guts of the 
--  microcart quadcopter. This is intended to be ran independently
--  of software on the Xilinx board, but also to have enough
--  flexibility to include input from software.
--  
-- The basic steps of this project is interfacing with a sensor
--  board using an iic protocol. Next, clocking in those values
--  from the sensor board, and attaching a 'valid data' bit
--  after 6 are clocked in. Next is the conversion from Euler
--  to nautical. 
--
-- From there, an orientation PID is fed into
--  with registers holding KP, KD, Desired Pitch, Desired Roll
--  (Note, in the future a third outer loop can be implemented to 
--  make desired pitch/roll on the FPGA as well).
--
-- Next is a component that takes in the desired velocity,
--  and multiplies by another constant to achieve an angular 
--  desired rate with respects to the axis's roll/pitch/yaw.
--
-- After this, is a scaling factor added, and a mixing matrix
--  to convert the desired velocities roll/pitch/yaw into 
--  different motor thrusts. Lastly, these motor thrusts are
--  converted to PWM signals to send to the ESCs on the quadcopter.
--
-- This was done in 2-3 weeks, so EXPECT that there are bugs
--  in the system. 
--
-- TODO: THE WHOLE THING
-- Author Tyler Kurtz - tylerkurtz8@gmail.com
--------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity Flight_System is
    port(
        );
end Flight_System;

architecture behavioral of Flight_System is
begin



end behavioral;
