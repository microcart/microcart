##############################################################################
## Filename:          /local/ucart/microcart/tasks/zybo_spi/system/drivers/axi_pwm_v1_00_a/data/axi_pwm_v2_1_0.tcl
## Description:       Microprocess Driver Command (tcl)
## Date:              Sat Jan 31 17:37:37 2015 (by Create and Import Peripheral Wizard)
##############################################################################

#uses "xillib.tcl"

proc generate {drv_handle} {
  xdefine_include_file $drv_handle "xparameters.h" "axi_pwm" "NUM_INSTANCES" "DEVICE_ID" "C_BASEADDR" "C_HIGHADDR" 
}
