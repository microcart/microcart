/*
 * Copyright (c) 2009-2012 Xilinx, Inc.  All rights reserved.
 *
 * Xilinx, Inc.
 * XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS" AS A
 * COURTESY TO YOU.  BY PROVIDING THIS DESIGN, CODE, OR INFORMATION AS
 * ONE POSSIBLE   IMPLEMENTATION OF THIS FEATURE, APPLICATION OR
 * STANDARD, XILINX IS MAKING NO REPRESENTATION THAT THIS IMPLEMENTATION
 * IS FREE FROM ANY CLAIMS OF INFRINGEMENT, AND YOU ARE RESPONSIBLE
 * FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE FOR YOUR IMPLEMENTATION.
 * XILINX EXPRESSLY DISCLAIMS ANY WARRANTY WHATSOEVER WITH RESPECT TO
 * THE ADEQUACY OF THE IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO
 * ANY WARRANTIES OR REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE
 * FROM CLAIMS OF INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include <stdio.h>
#include "platform.h"
#include "xspips.h"
#include "xparameters.h"

XSpiPs xspi;
XSpiPs_Config* xspi_conf;

int main()
{
    init_platform();

    print("Hello World\n\r");

    int* PWM_Period = (int*) XPAR_AXI_PWM_0_BASEADDR;
    int* PWM_Pulse = (int*) (XPAR_AXI_PWM_0_BASEADDR) + 1;

    *PWM_Period = 200000; // Set period to 20ms
    *PWM_Pulse = 15000; //Set period to 1.5ms


    int Status;

    xspi_conf = XSpiPs_LookupConfig(XPAR_PS7_SPI_0_DEVICE_ID);
    Status = XSpiPs_CfgInitialize(&xspi, xspi_conf, xspi_conf->BaseAddress);
    Status = XSpiPs_SelfTest(&xspi);
    if(Status != XST_SUCCESS){
    	printf("ERROR: self test\r\n");
    }

    //set options
    //set clk prescalar

    return 0;
}
