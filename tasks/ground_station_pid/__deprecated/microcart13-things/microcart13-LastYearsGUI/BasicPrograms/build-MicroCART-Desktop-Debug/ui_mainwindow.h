/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.4.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QTabWidget *tabWidget;
    QWidget *Controls_tab;
    QPushButton *startButton;
    QTextBrowser *DebugConsole;
    QLabel *ConsoleLabel;
    QPushButton *Execute_Button;
    QLineEdit *ManualCommanLineEdit;
    QLabel *ManualCommandLabel;
    QFrame *Visuals_frame;
    QFrame *Heading_Frame;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_FS_Dir_N;
    QFrame *Inner_heading_frame;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_FS_Dir_W;
    QGraphicsView *gView_FS_Dir;
    QLabel *label_FS_E;
    QLabel *label_FS_Dir_S;
    QFrame *Pitch_Frame;
    QGraphicsView *gView_FS_Pit;
    QLabel *label_FS_Pit;
    QLineEdit *lEdit_FS_PitText;
    QGroupBox *Y_groupBox;
    QHBoxLayout *horizontalLayout_19;
    QLineEdit *Sensor_Current_Y;
    QLabel *label_16;
    QLineEdit *Sensor_Target_Y;
    QGroupBox *Z_groupBox;
    QHBoxLayout *horizontalLayout_17;
    QLineEdit *Sensor_Current_Z;
    QLabel *label_14;
    QLineEdit *Sensor_Target_Z;
    QFrame *Roll_Frame;
    QGraphicsView *gView_FS_Roll;
    QLabel *label_FS_Roll;
    QLineEdit *lEdit_FS_RollText;
    QGroupBox *X_groupBox;
    QHBoxLayout *horizontalLayout_18;
    QLineEdit *Sensor_Current_X;
    QLabel *label_15;
    QLineEdit *Sensor_Target_X;
    QFrame *Controls_frame;
    QLabel *LongitudeLabel;
    QSpinBox *LatitudeSpinbox;
    QSpinBox *LongitudeSpinbox;
    QLabel *Altitudelabel;
    QSlider *AltitudeSlider;
    QSlider *LatitudeSlider;
    QLabel *LatitudeLabel;
    QSpinBox *ManualThrottlespinBox;
    QPushButton *Apply_pushButton;
    QCheckBox *ManualThrottle_checkBox;
    QSlider *horizontalSlider;
    QSpinBox *AltitudeSpinbox;
    QSlider *LongitudeSlider;
    QWidget *Settings_tab;
    QCheckBox *Camera_checkbox;
    QLabel *Options_label;
    QGroupBox *Control_groupBox;
    QRadioButton *Manual_button;
    QRadioButton *Auto_button;
    QGroupBox *Devices_groupBox;
    QLineEdit *Bluetooth_addr;
    QLineEdit *Camera_ip_addr;
    QLabel *Bluetooth_label;
    QLabel *Camera_ip_label;
    QLabel *Log_Output_label;
    QLineEdit *Log_output_text;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(800, 600);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        MainWindow->setMinimumSize(QSize(800, 600));
        MainWindow->setMaximumSize(QSize(800, 600));
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        tabWidget = new QTabWidget(centralwidget);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        tabWidget->setGeometry(QRect(0, 0, 800, 600));
        QSizePolicy sizePolicy1(QSizePolicy::Maximum, QSizePolicy::Maximum);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(tabWidget->sizePolicy().hasHeightForWidth());
        tabWidget->setSizePolicy(sizePolicy1);
        Controls_tab = new QWidget();
        Controls_tab->setObjectName(QStringLiteral("Controls_tab"));
        startButton = new QPushButton(Controls_tab);
        startButton->setObjectName(QStringLiteral("startButton"));
        startButton->setGeometry(QRect(10, 0, 114, 32));
        DebugConsole = new QTextBrowser(Controls_tab);
        DebugConsole->setObjectName(QStringLiteral("DebugConsole"));
        DebugConsole->setGeometry(QRect(20, 290, 571, 251));
        ConsoleLabel = new QLabel(Controls_tab);
        ConsoleLabel->setObjectName(QStringLiteral("ConsoleLabel"));
        ConsoleLabel->setGeometry(QRect(20, 270, 101, 16));
        Execute_Button = new QPushButton(Controls_tab);
        Execute_Button->setObjectName(QStringLiteral("Execute_Button"));
        Execute_Button->setGeometry(QRect(650, 50, 114, 32));
        ManualCommanLineEdit = new QLineEdit(Controls_tab);
        ManualCommanLineEdit->setObjectName(QStringLiteral("ManualCommanLineEdit"));
        ManualCommanLineEdit->setGeometry(QRect(540, 30, 221, 21));
        ManualCommandLabel = new QLabel(Controls_tab);
        ManualCommandLabel->setObjectName(QStringLiteral("ManualCommandLabel"));
        ManualCommandLabel->setGeometry(QRect(540, 10, 121, 16));
        Visuals_frame = new QFrame(Controls_tab);
        Visuals_frame->setObjectName(QStringLiteral("Visuals_frame"));
        Visuals_frame->setGeometry(QRect(600, 100, 191, 421));
        Visuals_frame->setFrameShape(QFrame::NoFrame);
        Visuals_frame->setFrameShadow(QFrame::Sunken);
        Heading_Frame = new QFrame(Visuals_frame);
        Heading_Frame->setObjectName(QStringLiteral("Heading_Frame"));
        Heading_Frame->setGeometry(QRect(20, 310, 150, 102));
        Heading_Frame->setMinimumSize(QSize(150, 102));
        Heading_Frame->setMaximumSize(QSize(150, 102));
        Heading_Frame->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255);"));
        Heading_Frame->setFrameShape(QFrame::StyledPanel);
        Heading_Frame->setFrameShadow(QFrame::Plain);
        verticalLayout_2 = new QVBoxLayout(Heading_Frame);
        verticalLayout_2->setSpacing(0);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        label_FS_Dir_N = new QLabel(Heading_Frame);
        label_FS_Dir_N->setObjectName(QStringLiteral("label_FS_Dir_N"));
        label_FS_Dir_N->setMinimumSize(QSize(0, 20));
        label_FS_Dir_N->setMaximumSize(QSize(16777215, 20));
        QFont font;
        font.setBold(true);
        font.setWeight(75);
        label_FS_Dir_N->setFont(font);
        label_FS_Dir_N->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(label_FS_Dir_N);

        Inner_heading_frame = new QFrame(Heading_Frame);
        Inner_heading_frame->setObjectName(QStringLiteral("Inner_heading_frame"));
        Inner_heading_frame->setMaximumSize(QSize(16777215, 60));
        Inner_heading_frame->setFrameShape(QFrame::NoFrame);
        Inner_heading_frame->setFrameShadow(QFrame::Raised);
        horizontalLayout_3 = new QHBoxLayout(Inner_heading_frame);
        horizontalLayout_3->setSpacing(0);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        horizontalLayout_3->setContentsMargins(0, 0, 0, 0);
        label_FS_Dir_W = new QLabel(Inner_heading_frame);
        label_FS_Dir_W->setObjectName(QStringLiteral("label_FS_Dir_W"));
        label_FS_Dir_W->setMinimumSize(QSize(20, 20));
        label_FS_Dir_W->setMaximumSize(QSize(20, 20));
        label_FS_Dir_W->setFont(font);
        label_FS_Dir_W->setAlignment(Qt::AlignCenter);

        horizontalLayout_3->addWidget(label_FS_Dir_W);

        gView_FS_Dir = new QGraphicsView(Inner_heading_frame);
        gView_FS_Dir->setObjectName(QStringLiteral("gView_FS_Dir"));
        gView_FS_Dir->setMinimumSize(QSize(70, 60));
        gView_FS_Dir->setMaximumSize(QSize(70, 60));
        gView_FS_Dir->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        gView_FS_Dir->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        gView_FS_Dir->setInteractive(false);

        horizontalLayout_3->addWidget(gView_FS_Dir);

        label_FS_E = new QLabel(Inner_heading_frame);
        label_FS_E->setObjectName(QStringLiteral("label_FS_E"));
        label_FS_E->setMinimumSize(QSize(20, 20));
        label_FS_E->setMaximumSize(QSize(20, 20));
        label_FS_E->setFont(font);
        label_FS_E->setAlignment(Qt::AlignCenter);

        horizontalLayout_3->addWidget(label_FS_E);


        verticalLayout_2->addWidget(Inner_heading_frame);

        label_FS_Dir_S = new QLabel(Heading_Frame);
        label_FS_Dir_S->setObjectName(QStringLiteral("label_FS_Dir_S"));
        label_FS_Dir_S->setMinimumSize(QSize(0, 20));
        label_FS_Dir_S->setMaximumSize(QSize(16777215, 20));
        label_FS_Dir_S->setFont(font);
        label_FS_Dir_S->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(label_FS_Dir_S);

        Pitch_Frame = new QFrame(Visuals_frame);
        Pitch_Frame->setObjectName(QStringLiteral("Pitch_Frame"));
        Pitch_Frame->setGeometry(QRect(20, 150, 150, 70));
        Pitch_Frame->setMinimumSize(QSize(150, 70));
        Pitch_Frame->setMaximumSize(QSize(150, 70));
        Pitch_Frame->setFrameShape(QFrame::StyledPanel);
        Pitch_Frame->setFrameShadow(QFrame::Raised);
        gView_FS_Pit = new QGraphicsView(Pitch_Frame);
        gView_FS_Pit->setObjectName(QStringLiteral("gView_FS_Pit"));
        gView_FS_Pit->setGeometry(QRect(0, 0, 150, 70));
        gView_FS_Pit->setMinimumSize(QSize(150, 70));
        gView_FS_Pit->setMaximumSize(QSize(150, 70));
        gView_FS_Pit->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        gView_FS_Pit->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        label_FS_Pit = new QLabel(Pitch_Frame);
        label_FS_Pit->setObjectName(QStringLiteral("label_FS_Pit"));
        label_FS_Pit->setGeometry(QRect(6, 3, 40, 20));
        label_FS_Pit->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        lEdit_FS_PitText = new QLineEdit(Pitch_Frame);
        lEdit_FS_PitText->setObjectName(QStringLiteral("lEdit_FS_PitText"));
        lEdit_FS_PitText->setEnabled(true);
        lEdit_FS_PitText->setGeometry(QRect(110, 0, 40, 20));
        lEdit_FS_PitText->setFocusPolicy(Qt::NoFocus);
        lEdit_FS_PitText->setAcceptDrops(false);
        lEdit_FS_PitText->setFrame(false);
        lEdit_FS_PitText->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        lEdit_FS_PitText->setReadOnly(true);
        Y_groupBox = new QGroupBox(Visuals_frame);
        Y_groupBox->setObjectName(QStringLiteral("Y_groupBox"));
        Y_groupBox->setGeometry(QRect(20, 57, 150, 43));
        QFont font1;
        font1.setPointSize(8);
        Y_groupBox->setFont(font1);
        horizontalLayout_19 = new QHBoxLayout(Y_groupBox);
        horizontalLayout_19->setSpacing(0);
        horizontalLayout_19->setObjectName(QStringLiteral("horizontalLayout_19"));
        horizontalLayout_19->setContentsMargins(0, 0, 0, 0);
        Sensor_Current_Y = new QLineEdit(Y_groupBox);
        Sensor_Current_Y->setObjectName(QStringLiteral("Sensor_Current_Y"));

        horizontalLayout_19->addWidget(Sensor_Current_Y);

        label_16 = new QLabel(Y_groupBox);
        label_16->setObjectName(QStringLiteral("label_16"));

        horizontalLayout_19->addWidget(label_16);

        Sensor_Target_Y = new QLineEdit(Y_groupBox);
        Sensor_Target_Y->setObjectName(QStringLiteral("Sensor_Target_Y"));

        horizontalLayout_19->addWidget(Sensor_Target_Y);

        Z_groupBox = new QGroupBox(Visuals_frame);
        Z_groupBox->setObjectName(QStringLiteral("Z_groupBox"));
        Z_groupBox->setGeometry(QRect(20, 100, 150, 43));
        Z_groupBox->setFont(font1);
        horizontalLayout_17 = new QHBoxLayout(Z_groupBox);
        horizontalLayout_17->setSpacing(0);
        horizontalLayout_17->setObjectName(QStringLiteral("horizontalLayout_17"));
        horizontalLayout_17->setContentsMargins(0, 0, 0, 0);
        Sensor_Current_Z = new QLineEdit(Z_groupBox);
        Sensor_Current_Z->setObjectName(QStringLiteral("Sensor_Current_Z"));

        horizontalLayout_17->addWidget(Sensor_Current_Z);

        label_14 = new QLabel(Z_groupBox);
        label_14->setObjectName(QStringLiteral("label_14"));

        horizontalLayout_17->addWidget(label_14);

        Sensor_Target_Z = new QLineEdit(Z_groupBox);
        Sensor_Target_Z->setObjectName(QStringLiteral("Sensor_Target_Z"));

        horizontalLayout_17->addWidget(Sensor_Target_Z);

        Roll_Frame = new QFrame(Visuals_frame);
        Roll_Frame->setObjectName(QStringLiteral("Roll_Frame"));
        Roll_Frame->setGeometry(QRect(20, 230, 150, 70));
        Roll_Frame->setMinimumSize(QSize(150, 70));
        Roll_Frame->setMaximumSize(QSize(150, 70));
        Roll_Frame->setFrameShape(QFrame::StyledPanel);
        Roll_Frame->setFrameShadow(QFrame::Raised);
        gView_FS_Roll = new QGraphicsView(Roll_Frame);
        gView_FS_Roll->setObjectName(QStringLiteral("gView_FS_Roll"));
        gView_FS_Roll->setGeometry(QRect(0, 0, 150, 70));
        gView_FS_Roll->setMinimumSize(QSize(150, 70));
        gView_FS_Roll->setMaximumSize(QSize(150, 70));
        gView_FS_Roll->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        gView_FS_Roll->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        label_FS_Roll = new QLabel(Roll_Frame);
        label_FS_Roll->setObjectName(QStringLiteral("label_FS_Roll"));
        label_FS_Roll->setGeometry(QRect(6, 3, 40, 20));
        label_FS_Roll->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        lEdit_FS_RollText = new QLineEdit(Roll_Frame);
        lEdit_FS_RollText->setObjectName(QStringLiteral("lEdit_FS_RollText"));
        lEdit_FS_RollText->setEnabled(true);
        lEdit_FS_RollText->setGeometry(QRect(110, 0, 40, 20));
        lEdit_FS_RollText->setFocusPolicy(Qt::NoFocus);
        lEdit_FS_RollText->setAcceptDrops(false);
        lEdit_FS_RollText->setFrame(false);
        lEdit_FS_RollText->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        lEdit_FS_RollText->setReadOnly(true);
        X_groupBox = new QGroupBox(Visuals_frame);
        X_groupBox->setObjectName(QStringLiteral("X_groupBox"));
        X_groupBox->setGeometry(QRect(20, 14, 150, 43));
        X_groupBox->setFont(font1);
        horizontalLayout_18 = new QHBoxLayout(X_groupBox);
        horizontalLayout_18->setSpacing(0);
        horizontalLayout_18->setObjectName(QStringLiteral("horizontalLayout_18"));
        horizontalLayout_18->setContentsMargins(0, 0, 0, 0);
        Sensor_Current_X = new QLineEdit(X_groupBox);
        Sensor_Current_X->setObjectName(QStringLiteral("Sensor_Current_X"));

        horizontalLayout_18->addWidget(Sensor_Current_X);

        label_15 = new QLabel(X_groupBox);
        label_15->setObjectName(QStringLiteral("label_15"));

        horizontalLayout_18->addWidget(label_15);

        Sensor_Target_X = new QLineEdit(X_groupBox);
        Sensor_Target_X->setObjectName(QStringLiteral("Sensor_Target_X"));

        horizontalLayout_18->addWidget(Sensor_Target_X);

        Controls_frame = new QFrame(Controls_tab);
        Controls_frame->setObjectName(QStringLiteral("Controls_frame"));
        Controls_frame->setGeometry(QRect(10, 30, 391, 181));
        Controls_frame->setFrameShape(QFrame::NoFrame);
        Controls_frame->setFrameShadow(QFrame::Sunken);
        LongitudeLabel = new QLabel(Controls_frame);
        LongitudeLabel->setObjectName(QStringLiteral("LongitudeLabel"));
        LongitudeLabel->setGeometry(QRect(40, 110, 101, 20));
        LatitudeSpinbox = new QSpinBox(Controls_frame);
        LatitudeSpinbox->setObjectName(QStringLiteral("LatitudeSpinbox"));
        LatitudeSpinbox->setGeometry(QRect(320, 80, 49, 24));
        LatitudeSpinbox->setMinimum(-300);
        LatitudeSpinbox->setMaximum(300);
        LongitudeSpinbox = new QSpinBox(Controls_frame);
        LongitudeSpinbox->setObjectName(QStringLiteral("LongitudeSpinbox"));
        LongitudeSpinbox->setGeometry(QRect(320, 110, 49, 24));
        LongitudeSpinbox->setMinimum(-300);
        LongitudeSpinbox->setMaximum(300);
        Altitudelabel = new QLabel(Controls_frame);
        Altitudelabel->setObjectName(QStringLiteral("Altitudelabel"));
        Altitudelabel->setGeometry(QRect(50, 50, 91, 20));
        AltitudeSlider = new QSlider(Controls_frame);
        AltitudeSlider->setObjectName(QStringLiteral("AltitudeSlider"));
        AltitudeSlider->setGeometry(QRect(150, 50, 160, 22));
        AltitudeSlider->setMinimum(-300);
        AltitudeSlider->setMaximum(300);
        AltitudeSlider->setOrientation(Qt::Horizontal);
        LatitudeSlider = new QSlider(Controls_frame);
        LatitudeSlider->setObjectName(QStringLiteral("LatitudeSlider"));
        LatitudeSlider->setGeometry(QRect(150, 80, 160, 22));
        LatitudeSlider->setMinimum(-300);
        LatitudeSlider->setMaximum(300);
        LatitudeSlider->setOrientation(Qt::Horizontal);
        LatitudeLabel = new QLabel(Controls_frame);
        LatitudeLabel->setObjectName(QStringLiteral("LatitudeLabel"));
        LatitudeLabel->setGeometry(QRect(50, 80, 91, 20));
        ManualThrottlespinBox = new QSpinBox(Controls_frame);
        ManualThrottlespinBox->setObjectName(QStringLiteral("ManualThrottlespinBox"));
        ManualThrottlespinBox->setGeometry(QRect(320, 20, 49, 24));
        ManualThrottlespinBox->setMaximum(100);
        ManualThrottlespinBox->setSingleStep(5);
        Apply_pushButton = new QPushButton(Controls_frame);
        Apply_pushButton->setObjectName(QStringLiteral("Apply_pushButton"));
        Apply_pushButton->setGeometry(QRect(243, 140, 121, 32));
        ManualThrottle_checkBox = new QCheckBox(Controls_frame);
        ManualThrottle_checkBox->setObjectName(QStringLiteral("ManualThrottle_checkBox"));
        ManualThrottle_checkBox->setGeometry(QRect(10, 20, 131, 20));
        horizontalSlider = new QSlider(Controls_frame);
        horizontalSlider->setObjectName(QStringLiteral("horizontalSlider"));
        horizontalSlider->setGeometry(QRect(150, 20, 160, 22));
        horizontalSlider->setMaximum(100);
        horizontalSlider->setOrientation(Qt::Horizontal);
        AltitudeSpinbox = new QSpinBox(Controls_frame);
        AltitudeSpinbox->setObjectName(QStringLiteral("AltitudeSpinbox"));
        AltitudeSpinbox->setGeometry(QRect(320, 50, 49, 24));
        AltitudeSpinbox->setMinimum(-300);
        AltitudeSpinbox->setMaximum(300);
        LongitudeSlider = new QSlider(Controls_frame);
        LongitudeSlider->setObjectName(QStringLiteral("LongitudeSlider"));
        LongitudeSlider->setGeometry(QRect(150, 110, 160, 22));
        LongitudeSlider->setMinimum(-300);
        LongitudeSlider->setMaximum(300);
        LongitudeSlider->setOrientation(Qt::Horizontal);
        tabWidget->addTab(Controls_tab, QString());
        Settings_tab = new QWidget();
        Settings_tab->setObjectName(QStringLiteral("Settings_tab"));
        Camera_checkbox = new QCheckBox(Settings_tab);
        Camera_checkbox->setObjectName(QStringLiteral("Camera_checkbox"));
        Camera_checkbox->setGeometry(QRect(20, 30, 150, 20));
        Camera_checkbox->setChecked(true);
        Options_label = new QLabel(Settings_tab);
        Options_label->setObjectName(QStringLiteral("Options_label"));
        Options_label->setGeometry(QRect(20, 10, 62, 16));
        Control_groupBox = new QGroupBox(Settings_tab);
        Control_groupBox->setObjectName(QStringLiteral("Control_groupBox"));
        Control_groupBox->setGeometry(QRect(20, 70, 111, 81));
        Manual_button = new QRadioButton(Control_groupBox);
        Manual_button->setObjectName(QStringLiteral("Manual_button"));
        Manual_button->setGeometry(QRect(10, 50, 102, 20));
        Auto_button = new QRadioButton(Control_groupBox);
        Auto_button->setObjectName(QStringLiteral("Auto_button"));
        Auto_button->setGeometry(QRect(10, 20, 102, 20));
        Auto_button->setChecked(true);
        Auto_button->setAutoRepeat(false);
        Devices_groupBox = new QGroupBox(Settings_tab);
        Devices_groupBox->setObjectName(QStringLiteral("Devices_groupBox"));
        Devices_groupBox->setGeometry(QRect(200, 20, 331, 131));
        Bluetooth_addr = new QLineEdit(Devices_groupBox);
        Bluetooth_addr->setObjectName(QStringLiteral("Bluetooth_addr"));
        Bluetooth_addr->setGeometry(QRect(160, 30, 151, 21));
        Camera_ip_addr = new QLineEdit(Devices_groupBox);
        Camera_ip_addr->setObjectName(QStringLiteral("Camera_ip_addr"));
        Camera_ip_addr->setGeometry(QRect(160, 60, 151, 21));
        Bluetooth_label = new QLabel(Devices_groupBox);
        Bluetooth_label->setObjectName(QStringLiteral("Bluetooth_label"));
        Bluetooth_label->setGeometry(QRect(10, 30, 71, 16));
        Camera_ip_label = new QLabel(Devices_groupBox);
        Camera_ip_label->setObjectName(QStringLiteral("Camera_ip_label"));
        Camera_ip_label->setGeometry(QRect(10, 60, 91, 16));
        Log_Output_label = new QLabel(Devices_groupBox);
        Log_Output_label->setObjectName(QStringLiteral("Log_Output_label"));
        Log_Output_label->setGeometry(QRect(10, 90, 151, 16));
        Log_output_text = new QLineEdit(Devices_groupBox);
        Log_output_text->setObjectName(QStringLiteral("Log_output_text"));
        Log_output_text->setGeometry(QRect(160, 90, 151, 21));
        tabWidget->addTab(Settings_tab, QString());
        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QStringLiteral("menubar"));
        menubar->setGeometry(QRect(0, 0, 800, 25));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QStringLiteral("statusbar"));
        MainWindow->setStatusBar(statusbar);

        retranslateUi(MainWindow);
        QObject::connect(horizontalSlider, SIGNAL(valueChanged(int)), ManualThrottlespinBox, SLOT(setValue(int)));
        QObject::connect(ManualThrottlespinBox, SIGNAL(valueChanged(int)), horizontalSlider, SLOT(setValue(int)));
        QObject::connect(AltitudeSlider, SIGNAL(valueChanged(int)), AltitudeSpinbox, SLOT(setValue(int)));
        QObject::connect(LatitudeSlider, SIGNAL(valueChanged(int)), LatitudeSpinbox, SLOT(setValue(int)));
        QObject::connect(LongitudeSlider, SIGNAL(valueChanged(int)), LongitudeSpinbox, SLOT(setValue(int)));
        QObject::connect(AltitudeSpinbox, SIGNAL(valueChanged(int)), AltitudeSlider, SLOT(setValue(int)));
        QObject::connect(LatitudeSpinbox, SIGNAL(valueChanged(int)), LatitudeSlider, SLOT(setValue(int)));
        QObject::connect(LatitudeSpinbox, SIGNAL(valueChanged(int)), LatitudeSlider, SLOT(setValue(int)));
        QObject::connect(LongitudeSpinbox, SIGNAL(valueChanged(int)), LongitudeSlider, SLOT(setValue(int)));

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MicroCART", 0));
        startButton->setText(QApplication::translate("MainWindow", "START", 0));
        ConsoleLabel->setText(QApplication::translate("MainWindow", "Debug Console:", 0));
        Execute_Button->setText(QApplication::translate("MainWindow", "Execute", 0));
        ManualCommandLabel->setText(QApplication::translate("MainWindow", "Manual Command:", 0));
        label_FS_Dir_N->setText(QApplication::translate("MainWindow", "N", 0));
        label_FS_Dir_W->setText(QApplication::translate("MainWindow", "W", 0));
        label_FS_E->setText(QApplication::translate("MainWindow", "E", 0));
        label_FS_Dir_S->setText(QApplication::translate("MainWindow", "S", 0));
        label_FS_Pit->setText(QApplication::translate("MainWindow", "Pitch", 0));
        Y_groupBox->setTitle(QApplication::translate("MainWindow", "Y (Latitude)", 0));
        Sensor_Current_Y->setText(QString());
        label_16->setText(QApplication::translate("MainWindow", "->", 0));
        Sensor_Target_Y->setText(QString());
        Z_groupBox->setTitle(QApplication::translate("MainWindow", "Z (Altitude)", 0));
        label_14->setText(QApplication::translate("MainWindow", "->", 0));
        Sensor_Target_Z->setText(QString());
        label_FS_Roll->setText(QApplication::translate("MainWindow", "Roll", 0));
        X_groupBox->setTitle(QApplication::translate("MainWindow", "X (longitude)", 0));
        Sensor_Current_X->setText(QString());
        label_15->setText(QApplication::translate("MainWindow", "->", 0));
        Sensor_Target_X->setText(QString());
        LongitudeLabel->setText(QApplication::translate("MainWindow", "Longitude (cm):", 0));
        Altitudelabel->setText(QApplication::translate("MainWindow", "Altitude (cm):", 0));
        LatitudeLabel->setText(QApplication::translate("MainWindow", "Latitude (cm):", 0));
        Apply_pushButton->setText(QApplication::translate("MainWindow", "Apply Changes", 0));
        ManualThrottle_checkBox->setText(QApplication::translate("MainWindow", "Manual Throttle:", 0));
        tabWidget->setTabText(tabWidget->indexOf(Controls_tab), QApplication::translate("MainWindow", "Controls", 0));
        Camera_checkbox->setText(QApplication::translate("MainWindow", "Use Camera System", 0));
        Options_label->setText(QApplication::translate("MainWindow", "Options:", 0));
        Control_groupBox->setTitle(QApplication::translate("MainWindow", "Control Mode:", 0));
        Manual_button->setText(QApplication::translate("MainWindow", "Manual", 0));
        Auto_button->setText(QApplication::translate("MainWindow", "Auto", 0));
        Devices_groupBox->setTitle(QApplication::translate("MainWindow", "Devices and Locations:", 0));
        Bluetooth_addr->setText(QApplication::translate("MainWindow", "/dev/ttyUSB0", 0));
        Camera_ip_addr->setText(QApplication::translate("MainWindow", "192.168.0.120:3883", 0));
        Bluetooth_label->setText(QApplication::translate("MainWindow", "Bluetooth:", 0));
        Camera_ip_label->setText(QApplication::translate("MainWindow", "VRPN Server:", 0));
        Log_Output_label->setText(QApplication::translate("MainWindow", "Log Output File Name:", 0));
        Log_output_text->setText(QApplication::translate("MainWindow", "output.txt", 0));
        tabWidget->setTabText(tabWidget->indexOf(Settings_tab), QApplication::translate("MainWindow", "Settings", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
