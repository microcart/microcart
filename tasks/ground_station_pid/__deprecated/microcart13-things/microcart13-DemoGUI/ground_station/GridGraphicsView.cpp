#include "GridGraphicsView.h"

GridGraphicsView::GridGraphicsView(QFrame*&)
{
    init();
}

GridGraphicsView::GridGraphicsView(QWidget*&)
{
    init();
}

void GridGraphicsView::init()
{
    this->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    this->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    grid_graphics_scene = new QGraphicsScene;
    grid_graphics_scene->setSceneRect(100,100,100,100);
    setScene(grid_graphics_scene);
    clearPath();

    current_location.x = 0;
    current_location.y = 0;
    current_location.z = 0;
    current_location.heading = 0;

    target_location.x = 0;
    target_location.y = 0;
    target_location.z = 0;
    target_location.heading = 0;

    x_min   = -1;
    x_max   =  1;
    y_min   = -1;
    y_max   =  1;
    z       =  0;
    heading =  0;
    minimum_segment_distance = 0.01;//In meters
}

GridGraphicsView::~GridGraphicsView()
{
}

std::vector<Position> GridGraphicsView::getPathAM()
{
    return path;
}

std::vector<Position> GridGraphicsView::getPathRM()
{
    int i,i_max;
    std::vector<Position> path_rm;
    Position vertex;

    if (path.size() == 0)
        return path_rm;

    vertex.x = path[0].x - current_location.x;
    vertex.y = path[0].y - current_location.y;
    vertex.z = path[0].z - current_location.z;
    vertex.heading = path[0].heading - current_location.heading;
    path_rm.push_back(vertex);

    i_max = path.size();
    for (i=1; i<i_max; i=i+1)
    {
        vertex.x = path[i].x - path[i-1].x;
        vertex.y = path[i].y - path[i-1].y;
        vertex.z = path[i].z - path[i-1].z;
        vertex.heading = path[i].heading - path[i-1].heading;
        path_rm.push_back(vertex);
    }
    return path_rm;
}

bool GridGraphicsView::setPathAM(std::vector<Position> new_path)
{
    path = new_path;
    redrawPath();
    return true;
}

bool GridGraphicsView::setPathRM(std::vector<Position> new_path)
{
    int i,i_max;
    Position vertex;

    clearPath();

    i_max = new_path.size();

    for (i=0; i<i_max; i=i+1)
    {
        vertex.x += new_path[i].x;
        vertex.y += new_path[i].y;
        vertex.z += new_path[i].z;
        addVertex(vertex);
    }

    redrawPath();
    return true;
}

bool GridGraphicsView::clearPath()
{
    Position origin;
    //The first position in the path is always the (0,0,0,0) origin.
    origin.x = 0;
    origin.y = 0;
    origin.z = 0;
    origin.heading = 0;
    path.resize(0);
    path.push_back(origin);
    grid_graphics_scene->clear();
    return true;
}

bool GridGraphicsView::smoothPath()
{
    return true;
}

bool GridGraphicsView::redrawPath()
{
    int i, i_max;
    Position vertex1;
    Position vertex2;

    if (path.size() < 2)
        return true;

    grid_graphics_scene->clear();

    vertex2 = path[0];

    i_max = path.size();
    for (i=1; i<i_max; i=i+1)
    {
        vertex1.x = vertex2.x;
        vertex1.y = vertex2.y;
        vertex1.z = vertex2.z;
        vertex1.heading = vertex2.heading;

        vertex2 = path[i];

        drawSegment(vertex1, vertex2);
    }

    return true;
}

bool GridGraphicsView::setCurrentLocation(Position position)
{
    current_location = position;
    return true;
}

bool GridGraphicsView::setTargetLocation(Position position)
{
    target_location = position;
    return true;
}

bool GridGraphicsView::addVertex(Position vertex)
{
    Position half_vertex;
    Position back = path.back();
    //A slightly different implementation may not
    //initially push a position into the path
    //for right now, we suppose there is always at least one position in the path
    //if (path.size() > 0)
    //    drawSegment(vertex, path.back());

    //If the segment is too long
    //We recursively add segments
    if (isAboveDist(path.back(), vertex, 2*minimum_segment_distance))
    {
        //Half the distance between then back and the vertex
        half_vertex.x = vertex.x-(vertex.x-path.back().x)/2;
        half_vertex.y = vertex.y-(vertex.y-path.back().y)/2;
        half_vertex.z = vertex.z; //We dont account for z-axis movement
        half_vertex.heading = vertex.heading; //We don't account for heading
        addVertex(half_vertex);
        addVertex(vertex); //By the time this executes, the path.back() position should have moved closer.
    }
    else
    {
        drawSegment(path.back(),vertex);
        path.push_back(vertex);
    }

    return true;
}

bool GridGraphicsView::drawSegment(Position vertex1, Position vertex2)
{
    Position new_vertex1;
    Position new_vertex2;

    new_vertex1 = alignMeterToPixel(vertex1);
    new_vertex2 = alignMeterToPixel(vertex2);

    grid_graphics_scene->addLine(new_vertex1.x, new_vertex1.y, new_vertex2.x, new_vertex2.y);
    return true;
}

bool GridGraphicsView::isAboveDist(Position v1, Position v2, double dist)
{
    double calculated_dist;

    calculated_dist = sqrt((v2.x-v1.x)*(v2.x-v1.x) + (v2.y-v1.y)*(v2.y-v1.y));

    if (calculated_dist < dist)
        return false;
    return true;
}

Position GridGraphicsView::alignMeterToPixel(Position vertex)
{
    vertex.x = width*(vertex.x-x_min)/(x_max-x_min);
    vertex.y = height*((vertex.y-y_min)/(y_max-y_min));
    return vertex;
}

Position GridGraphicsView::alignPixelToMeter(Position vertex)
{
    vertex.x = ((vertex.x/width)  * (x_max-x_min)) + x_min;
    vertex.y = ((vertex.y/height) * (y_max-y_min)) + y_min;
    return vertex;
}

void GridGraphicsView::mousePressEvent ( QMouseEvent *event )
{
    Position vertex;
    int x,y;

    //Get the location of the mouse click.
    x = event->x();
    y = event->y();

    //Add the vertex into the path.
    vertex.x = x;
    vertex.y = y;
    vertex.z = z;
    vertex.heading = heading;

    //Convert the vertex into meters
    vertex = alignPixelToMeter(vertex);

    addVertex(vertex);
}

void GridGraphicsView::mouseMoveEvent ( QMouseEvent *event )
{
    Position vertex;
    int x,y;

    x = event->x();
    y = event->y();

    vertex.x = x;
    vertex.y = y;
    vertex.z = z;
    vertex.heading = heading;

    //Convert the vertex into meters
    vertex = alignPixelToMeter(vertex);

    if (!isAboveDist(path.back(), vertex, minimum_segment_distance))
        return;

    addVertex(vertex);
}

void GridGraphicsView::resizeEvent( QResizeEvent *event )
{
    width    = this->geometry().width();
    height   = this->geometry().height();

    grid_graphics_scene->setSceneRect(0,0,width,height);

    redrawPath();
}
