#ifndef FILEPARSER_H
#define FILEPARSER_H

#include "MCTStructures.h"
#include <vector>
#include <string>
#include <sstream>
#include <ostream>
#include <iostream>

/** @brief Determines the type of command.
  * @details For commands that require input arguments, the command must be followed
  *          by a space to be correctly recognized.  'sh' and 'sethome' do not need
  *          the space.  This is because 'x' is a prefix for 'xyz'
  * @param[in] command String representing a command as it is typed into the console.
  * @return number corresponding to the command.
  *         - 0=unknown
  *         - 1=alt
  *         - 2=lat
  *         - 3=long
  *         - 4=dir
  *         - 5=xyz
  *         - 6=sh
  * @todo Make this return a enumerator of command types.
  */
int parseCommand(std::string command);

/** @brief Parses the arguments of a command
  * @details Arguments can be delimeted with the following: ' ' '\\t' '\\n' '\\r' '\\0'.
  *          Multiple delimeters will NOT be ignored.
  * @param[in] command String representing a command as it is typed into the console.
   *@return Vector of arguments.
  *         The first argument should be the name of the command.
  *         The size of the vector is always at least 5.
  *         Unused indices will have values of 0.
  */
std::vector<std::string> parseArguments(std::string command);

/** @brief Turns a vector of string commands into a vector of positions.
  * @details Invalid commands and the "set home" command will be ignored.
  * @param[in] commands Vector of strings representing commands exactly how they are typed into the console.
  * @return A vector of positions.
  *         The vector will be size 0 if no commands were parsed.
  */
std::vector<Position> parseCommandsToPositions(std::vector<std::string> commands);

/** @brief Turns a vector of positions into a vector of string commands.
  * @param[in] path Vector of coordinates.
  * @return Vector of strings representing commands as they are typed into the console.
  *         The vector will be size 0 if no positions were parsed.
  */
std::vector<std::string> parsePositionsToCommands(std::vector<Position> path);

/** @brief Parses a string command into a position.
  * @param[in] command String representing exactly how it is typed into the console.
  * @return Position structure containing position changes to the x,y,z,heading.
  *         The position structure will be all 0 if the command could not be parsed.
  */
Position parseCommandToPosition(std::string command);

/** @brief Parses a position into a string command.
  * @param[in] position Position structure containing position changes to the x,y,z,heading.
  * @return String command representing exactly how it is typed into the console.
  *         NULL if the position could not be parsed.
  */
std::string parsePositionToCommand(Position position);

#endif // FILEPARSER_H
