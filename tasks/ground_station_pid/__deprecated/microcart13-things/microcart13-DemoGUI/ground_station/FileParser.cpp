#include "FileParser.h"
#include <cstdlib>


int parseCommand(std::string command)
{
    //Make sure there is a space after the string of each command.
    //Some commands are prefixes of others.

    if (command.find("alt ") == 0)
        return 1;
    if (command.find("z ") == 0)
        return 1;
    if (command.find("altitude ") == 0)
        return 1;

    if (command.find("lat ") == 0)
        return 2;
    if (command.find("y ") == 0)
        return 2;
    if (command.find("latitude ") == 0)
        return 2;

    if (command.find("long ") == 0)
        return 3;
    if (command.find("lon ") == 0)
        return 3;
    if (command.find("x ") == 0)
        return 3;
    if (command.find("longitude ") == 0)
        return 3;

    if (command.find("dir ") == 0)
        return 4;
    if (command.find("direction ") == 0)
        return 4;

    if (command.find("xyz ") == 0)
        return 5;
    if (command.find("p ") == 0)
        return 5;
    if (command.find("pos ") == 0)
        return 5;
    if (command.find("position ") == 0)
        return 5;
    if (command.find("target ") == 0)
        return 5;

    if (command.find("sh") == 0)
        return 5;
    if (command.find("sethome") == 0)
        return 5;

    return 0;
}

std::vector<std::string> parseArguments(std::string command)
{
    //each command is terminated by a null character
    //each command may or may not contain \n or \r\n characters
    //\n \r\n characters should be omitted

    int i, i_max,start;
    std::vector<std::string> arguments;

    i_max = command.size();
    start = 0;
    for (i=0; i<i_max; i=i+1)
    {
        if (command[i] == ' ' || command[i] == '\t' || command[i] == '\n' || command[i] == '\r' || command[i] == '\0')
        {
            arguments.push_back(command.substr(start,i-start));
            start = i+1;
        }
    }

    //if (start<i)
        arguments.push_back(command.substr(start,i-start));

    //Pad the vector to a length of 5 to prevent segmentation faults.
    for (i=arguments.size();i<5; i=i+1)
    {
        arguments.push_back("test");
    }

    return arguments;
}

std::vector<Position> parseCommandsToPositions(std::vector<std::string> commands)
{
    std::vector<Position> path;
    Position position;
    int i, i_max, command_type;
    std::vector<std::string> arguments;

    i_max = commands.size();
    for (i=0; i<i_max; i=i+1)
    {
        command_type = parseCommand(commands[i]);
        if (command_type == 0)
            continue;

        position = parseCommandToPosition(commands[i]);
        if (position.x != 0 || position.y != 0 and position.z != 0 || position.heading != 0)
        {
            path.push_back(position);
        }
    }
    return path;
}

std::vector<std::string> parsePositionsToCommands(std::vector<Position> positions)
{
    int i, i_max;
    std::vector<std::string> commands;
    std::string command;

    i_max = positions.size();
    for (i=0; i<i_max; i=i+1)
    {
        command = parsePositionToCommand(positions[i]);
        commands.push_back(command);
    }

    return commands;
}

Position parseCommandToPosition(std::string command)
{
    int command_type;
    std::vector<std::string> arguments;
    Position position;

    command_type = parseCommand(command);
    arguments    = parseArguments(command);

    position.x = 0;
    position.y = 0;
    position.z = 0;
    position.heading = 0;

    //Ignore Invalid Commands
    if (command_type == 0)
        return position;

    //Change Altitude
    else if (command_type == 1)
    {
        position.z = atof(arguments[1].c_str());
    }

    //Change Latitude
    else if (command_type == 2)
    {
        position.x = atof(arguments[1].c_str());
    }

    //Change Longitude
    else if (command_type == 3)
    {
        position.y = atof(arguments[1].c_str());
    }

    //Change Direction
    else if (command_type == 4)
    {
        position.heading = atof(arguments[1].c_str());
    }

    //Change Altitude Latitude and Longitude
    else if (command_type == 5)
    {
        position.x = atof(arguments[1].c_str());
        position.y = atof(arguments[2].c_str());
        position.z = atof(arguments[3].c_str());
    }

    return position;
}

std::string parsePositionToCommand(Position position)
{
    std::string command;
    std::ostringstream buffer;

    buffer.str(std::string());//Clear the buffer for reuse
    buffer << "xyz " << position.x << " " << position.y << " " << position.z;
    command = buffer.str();

    return command;
}
