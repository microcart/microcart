#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "FileParser.h"
#include "MCTStructures.h"

#include <QPixmap>
#include <QFileDialog>
#include <QFile>
#include <QTextStream>
#include <iostream>

MainWindow::MainWindow(MCTController *cont, MCTController *secondary, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    control = cont;
    second  = secondary;
    ui->setupUi(this);
    this->syncController();
    Command_Queue = new CommandQueue();
    Command_Queue->control = control;
    groundLevel = 0;
    groundFlag = 0;
    gridSetup();
    updateGrid();
}

MainWindow::~MainWindow()
{
    delete ui;
    delete control;
    delete second;
    delete Command_Queue;
}

void MainWindow::syncController()
{
    unsigned int j;
    QStringList items = QStringList();
    std::vector<std::string> *templist = new std::vector<std::string>();
    MODULE_TYPE modtype;
    std::vector<MODULE_TYPE> looptype = std::vector<MODULE_TYPE>();
    looptype.push_back(ORIENTATION);
    looptype.push_back(POSITION);
    looptype.push_back(DIRECTION);
    looptype.push_back(COMMUNICATION);
    looptype.push_back(CONTROL);

    // loop through all module types to put the selections into the proper combo box
    for(unsigned int i=0;i<looptype.size();i++) {
        modtype = looptype.at(i);
        *templist = control->getAllModName(modtype);
        for(j = 0;j<templist->size();j++) {
            items.push_back(QString::fromStdString(templist->at(j).c_str()));
        }
        switch(modtype) {
        case(ORIENTATION):
            ui->cBox_Mod_Ori->addItems(items);
            break;
        case(POSITION):
            ui->cBox_Mod_Pos->addItems(items);
            break;
        case(DIRECTION):
            ui->cBox_Mod_Dir->addItems(items);
            break;
        case(COMMUNICATION):
            ui->cBox_Mod_Comm->addItems(items);
            break;
        case(CONTROL):
            ui->cBox_Mod_Cont->addItems(items);
            break;
        }
        items.clear();
    }
    *templist = control->getBehaviors();
    for(j = 0; j < templist->size();j++) {
        items.push_back(QString::fromStdString(templist->at(j).c_str()));
    }
    ui->cBox_Bhvr->addItems(items);
    //load images for roll/pitch/direction

    QPixmap dirImage = QPixmap::fromImage(QImage("./Images/direction.jpg",0));
    QPixmap rollImage = QPixmap::fromImage(QImage("./Images/roll.jpg",0));
    QPixmap pitchImage = QPixmap::fromImage(QImage("./Images/pitch.jpg",0));
    QGraphicsScene *dirscene = new QGraphicsScene ();
    QGraphicsScene *rollscene = new QGraphicsScene();
    QGraphicsScene *pitchscene = new QGraphicsScene();
    dirscene->addPixmap(dirImage);
    rollscene->addPixmap(rollImage);
    pitchscene->addPixmap(pitchImage);
    // show pitch, roll and yaw images for the primary control (UAV)
    ui->gView_FS_Dir->setScene(dirscene);
    ui->gView_FS_Roll->setScene(rollscene);
    ui->gView_FS_Pit->setScene(pitchscene);
    // show pitch, roll and yaw images for the secondary control (omnibot)
    ui->gView_FS_Dir_2->setScene(dirscene);
    ui->gView_FS_Roll_2->setScene(rollscene);
    ui->gView_FS_Pit_2->setScene(pitchscene);

    updateManSelect();
}

void MainWindow::updateController()
{
    double time = control->update();
    if(time == -1) {
        on_pButton_Main_Stop_clicked();
    } else {
        updateFS();
        avgTime = (avgTime*.90) + (time*.10);
        ui->lEdit_Main_AvrExec->setText(QString::number(avgTime, 'g', 3));
    }
}

void MainWindow::printMessage(QString line)
{
    ui->tEdit_IO_Out->append(line);
}

void MainWindow::updateFS()
{
    Position current_position;
    Position target_position;

    current_position = control->getCurrentPosition();
    target_position  = control->getTargetPosition();

    //update the position texts
    ui->Sensor_Current_X->setText(QString::number(current_position.x, 'g', 2));
    ui->Sensor_Current_Y->setText(QString::number(current_position.y, 'g', 2));
    ui->Sensor_Current_Z->setText(QString::number(current_position.z, 'g', 2));

    ui->Sensor_Target_X->setText(QString::number(target_position.x, 'g', 2));
    ui->Sensor_Target_Y->setText(QString::number(target_position.y, 'g', 2));
    ui->Sensor_Target_Z->setText(QString::number(target_position.z, 'g', 2));


    {   // @deprecated
        ui->lEdit_FS_Alt->setText(QString::fromStdString(control->getPosition("altitude",(bool) ui->check_FS_Abso->checkState() )));
        ui->lEdit_FS_Lat->setText(QString::fromStdString(control->getPosition("latitude", ui->check_FS_Abso->checkState() )));
        ui->lEdit_FS_Long->setText(QString::fromStdString(control->getPosition("longitude", ui->check_FS_Abso->checkState() )));

        ui->lEdit_FS_TarAlt->setText(QString::fromStdString(control->getTarget("altitude", ui->check_FS_Abso->checkState() )));
        ui->lEdit_FS_TarLat->setText(QString::fromStdString(control->getTarget("latitude", ui->check_FS_Abso->checkState() )));
        ui->lEdit_FS_TarLong->setText(QString::fromStdString(control->getTarget("longitude", ui->check_FS_Abso->checkState() )));

        // Outputs the information for long, lat and alt for the secondary control (omnibot)
        // Not deprecated
        if(ui->cBox_Mod_TrackOmni->isChecked()){
        ui->lEdit_FS_Alt_2->setText(QString::fromStdString(second->getPosition("altitude",(bool) ui->check_FS_Abso->checkState() )));
        ui->lEdit_FS_Lat_2->setText(QString::fromStdString(second->getPosition("latitude", ui->check_FS_Abso->checkState() )));
        ui->lEdit_FS_Long_2->setText(QString::fromStdString(second->getPosition("longitude", ui->check_FS_Abso->checkState() )));
        ui->lEdit_FS_TarAlt_2->setText(QString::fromStdString(second->getTarget("altitude", ui->check_FS_Abso->checkState() )));
        ui->lEdit_FS_TarLat_2->setText(QString::fromStdString(second->getTarget("latitude", ui->check_FS_Abso->checkState() )));
        ui->lEdit_FS_TarLong_2->setText(QString::fromStdString(second->getTarget("longitude", ui->check_FS_Abso->checkState() )));
    }

    }

    //update the orientation texts and graphics
    ui->gView_FS_Roll->resetTransform();
    ui->gView_FS_Roll->rotate(current_position.roll);
    ui->lEdit_FS_RollText->setText(QString::number(current_position.roll, 'g', 2));

    ui->gView_FS_Pit->resetTransform();
    ui->gView_FS_Pit->rotate(-1*current_position.pitch);
    ui->lEdit_FS_PitText->setText(QString::number(current_position.pitch, 'g', 2));

    ui->gView_FS_Dir->resetTransform();
    ui->gView_FS_Dir->rotate(current_position.heading);
    ui->lEdit_FS_DirText->setText(QString::number(current_position.heading,'g',2));

    //update the Grid View
    updateGrid();

    if(ui->cBox_Mod_TrackOmni->isChecked()){
        double roll_2 = second->getOrientation("roll");
        ui->gView_FS_Roll_2->resetTransform();
        ui->gView_FS_Roll_2->rotate(roll_2);
        ui->lEdit_FS_RollText_2->setText(QString::number(roll_2, 'g', 2));

        double pitch_2 = second->getOrientation("pitch");
        ui->gView_FS_Pit_2->resetTransform();
        ui->gView_FS_Pit_2->rotate(-pitch_2);
        ui->lEdit_FS_PitText_2->setText(QString::number(pitch_2, 'g', 2));

        double head_2 = second->getDirection();
        ui->gView_FS_Dir_2->resetTransform();
        ui->gView_FS_Dir_2->rotate(head_2);
    }

    // Sets groundLevel to the altitude of the quad on the ground
    if(groundFlag <= 1){
        QString temp = QString::fromStdString(control->getPosition("altitude",(bool) ui->check_FS_Abso->checkState() ));
        groundLevel = temp.left(temp.length()-1).toDouble();
        std::cout << temp.toStdString() << temp.toDouble() << std::endl;
        groundFlag ++;
    }
}

void MainWindow::on_cBox_Bhvr_currentIndexChanged(const QString &arg1)
{
    unsigned int j;
    if(arg1 == "")
        return;
    if(control->setBehavior(arg1.toStdString())) {
        printMessage("Behavior set to : " + arg1);
        std::vector<std::string> *templist = new std::vector<std::string>();
        *templist = control->getBehaviorModels();
        ui->cBox_BhvrModel->clear();
        for(j = 0; j < templist->size();j++) {
            ui->cBox_BhvrModel->addItem(QString::fromStdString(templist->at(j).c_str()));
        }
    } else {
        printMessage("Failed to set Behavior");
    }
}

void MainWindow::on_cBox_Mod_Ori_currentIndexChanged(const QString &arg1)
{
    if(arg1 == "")
        return;
    if(control->setModule(ORIENTATION,arg1.toStdString())) {
        printMessage("Orientation Module set to : " + arg1);
    } else {
        printMessage("Failed to set Orientation Module");
    }
}

void MainWindow::on_cBox_Mod_Pos_currentIndexChanged(const QString &arg1)
{
    if(arg1 == "")
        return;
    if( control->setModule(POSITION,arg1.toStdString()) ) {
        printMessage("Position Module set to : " + arg1);
    } else {
        printMessage("Failed to set Position Module");
    }
}

void MainWindow::on_cBox_Mod_Dir_currentIndexChanged(const QString &arg1)
{
    if(arg1 == "")
        return;
    if(control->setModule(DIRECTION,arg1.toStdString())) {
        printMessage("Direction Module set to : " + arg1);
    } else {
        printMessage("Failed to set Direction Module");
    }
}

void MainWindow::on_cBox_Mod_Comm_currentIndexChanged(const QString &arg1)
{
    if(arg1 == "")
        return;
   if( control->setModule(COMMUNICATION,arg1.toStdString())) {
        printMessage("Communication Module set to : " + arg1);
    } else {
        printMessage("Failed to set Communication Module");
    }
}

void MainWindow::on_cBox_Mod_Cont_currentIndexChanged(const QString &arg1)
{
    if(control->setModule(CONTROL,arg1.toStdString())) {
        printMessage("Manual Control Module set to : " + arg1);
    } else {
        printMessage("Failed to set Manual Control Module");
    }
    updateManSelect();
}

/** Imports a list of commands from a file put them into a queue
  *
  * Called when a button is clicked.
  *
  * @return void
  */
void MainWindow::on_pButton_IO_ImportFile_clicked()
{
    // Possible improvement: Currently, the path to the file has to be input in the console line and is consumed by Import File.
    // QFileDialog doesn't work with the current set up of the base station, but something prettier could be developed.
    QString fileName = ui->lEdit_IO_In->text();
    ui->lEdit_IO_In->clear();

    QFile file(fileName);
    cmdQueue = QStringList();

    if(file.open(QIODevice::ReadOnly)){
        printMessage("Inputting Commands from: " + file.fileName());
        currentCmd = 0;
        QTextStream in(&file);
        do{
            cmdQueue.append(in.readLine());
        } while(!cmdQueue.last().isNull());
        file.close();

        ui->pButton_IO_Send->setEnabled(false);
        ui->pButton_IO_ImportFile->setEnabled(false);
        QTimer::singleShot(5000, this, SLOT(sendCommand()));
    }
}

/** Sends commands from a queue of commands
  *
  * Gets a command from a queue and send it.
  * Waits 5 seconds between commands.
  *
  * @return void
  */
void MainWindow::sendCommand()
{
    if(currentCmd >= 0){
        printMessage("Sending Command: " + cmdQueue.at(currentCmd));
        if(control->parseCommand(cmdQueue.at(currentCmd).toStdString()) == true) {
            printMessage("Command Executed");
        } else {
            printMessage("Error in Command");
        }

        currentCmd++;
        if(!cmdQueue.at(currentCmd).isNull()){
            QTimer::singleShot(5000, this, SLOT(sendCommand()));
        }
        else{
            printMessage("File Input Completed");
            ui->pButton_IO_Send->setEnabled(true);
            ui->pButton_IO_ImportFile->setEnabled(true);
        }
    }
    else{
        printMessage("File Input Cancelled");
        ui->pButton_IO_Send->setEnabled(true);
        ui->pButton_IO_ImportFile->setEnabled(true);
    }
}

/** Updates currentCmd to an invalid number, so the automated command sending will stop
  *
  * @return void
  */
void MainWindow::on_pButton_IO_CancelCmds_clicked()
{
    printMessage("Cancelling File Input");
    currentCmd = -1;
}

/**
  * Handles the commands being input on the console tab.
  **/
void MainWindow::on_pButton_IO_Send_clicked()
{
    QString tmpStr = ui->lEdit_IO_In->text();
    if(tmpStr != "") {
        printMessage("Sending Command: " + tmpStr);
        if(control->parseCommand(tmpStr.toStdString()) == true) {
            printMessage("Command Executed");
        } else {
            printMessage("Error in Command");
        }
        ui->lEdit_IO_In->clear();
    }
}

void MainWindow::on_pButton_Main_Start_clicked()
{
    ui->pButton_Main_Start->setEnabled(false);
    ui->pButton_Take_Off->setEnabled(true);
    ui->pButton_Land->setEnabled(false);
    // There has been an issue that after a bad stop, the quad maintains the throttle value it had when stop was pressed
    // So if this will set the throttle to 0 right when it tries to start to try to avoid this.
    control->setThrottleVal(0);
    if(ui->cBox_Mod_Cont->currentText() == "Automatic"){
        ui->pButton_Take_Off->setEnabled(true);
    }
    printMessage("Starting Modules...");
    if( control->start() ) {
        printMessage("All Modules Are Now Active");
        avgTime = control->update();
        ui->lEdit_Main_AvrExec->setText(QString::number(avgTime, 'g', 4));
        timer = new QTimer(this);
        connect(timer, SIGNAL(timeout()), this, SLOT(updateController()) );
        timer->start(5);
    } else {
        printMessage("Error on Module Startup");
    }
    updateFS();
    updateStatus();
}

void MainWindow::on_pButton_Main_Stop_clicked()
{
    printMessage("Stopping Modules");
    control->stop();
    second->stop();
    timer->stop();
    // There has been an issue that after a bad stop, the quad maintains the throttle value it had when stop was pressed
    // So if this will set the throttle to 0 right when it stops to try to avoid this.
    control->setThrottleVal(0);
    ui->cBox_Mod_FollowOmni->setChecked(false);
    ui->pButton_Take_Off->setEnabled(false);
    ui->pButton_Land->setEnabled(false);
    updateFS();
    updateStatus();
}

/** Automatically launches the quadcopter
  *
  * Called when a button is clicked.
  *
  * @return void
  */
void MainWindow::on_pButton_Take_Off_clicked(){
    control->setThrottleVal(0);

    QString altAbs = QString::fromStdString(control->getPosition("altitude", ui->check_FS_Abso->isChecked()));
    double currAlt = altAbs.left(altAbs.length()-1).toDouble();

    on_rButton_Sel_Thr_Man_clicked();
    if(!control->getThrottle() && currAlt < groundLevel + .25){
        printMessage("Taking off");
        ui->pButton_Take_Off->setEnabled(false);
        QTimer::singleShot(30, this, SLOT(throttleUp()));
    }
}

/**
  * Action function for increasing the throttle at a safe rate to take off.
  * Called every 80 ms until the UAV is considered safely flying.
  **/
void MainWindow::throttleUp(){
    QString altAbs = QString::fromStdString(control->getPosition("altitude", ui->check_FS_Abso->isChecked()));
    double currAlt = altAbs.left(altAbs.length()-1).toDouble();

    if(control->getStatus() == RUNNING){
        // currAlt has a slight delay inherent to the code, so it claims to stop at .5, but it actually stops at a safe point about 1m up
        if(currAlt > .5 && control->getThrottleVal() > 50){
            printMessage("Flying");
            // At a stable height and throttle
            control->setThrottleVal(5);
            on_rButton_Sel_Thr_Auto_clicked();
            ui->pButton_Land->setEnabled(true);
        }
        else {
            // Make sure that the throttle doesn't just increase faster than the quad can flight up and throw the quad into the ceiling
            if(control->getThrottleVal() < 75){
                control->setThrottleVal(.8);
            }
            //printMessage(QString::number(currAlt) + " " + QString::number(control->getThrottleVal()));
            QTimer::singleShot(80, this, SLOT(throttleUp()));
        }
    }
}

/** Automatically Lands the quadcopter
  *
  * Called when a button is clicked.
  *
  * @return void
  */
void MainWindow::on_pButton_Land_clicked()
{
    ui->pButton_Land->setEnabled(false);
    on_rButton_Sel_Thr_Man_clicked();
    throttleDown();
}

/**
  * Action function for decreasing the throttle at a safe rate to land.
  * Called regularly until the UAV has been safely landed.
  **/
void MainWindow::throttleDown(){
    QString altAbs = QString::fromStdString(control->getPosition("altitude", ui->check_FS_Abso->isChecked()));
    double altDiff = altAbs.left(altAbs.length()-1).toDouble() - groundLevel;

    if(altDiff <= .1){
        control->setThrottleVal(0);
        on_pButton_Main_Stop_clicked();

        ui->pButton_Take_Off->setEnabled(true);
    }
    else if(altDiff <= .5 && altDiff > .1){
        if(control->getThrottleVal() >= 20){
            control->setThrottleVal(-.8);
        }
        QTimer::singleShot(120, this, SLOT(throttleDown()));
    }
    else if(altDiff <= 1 && altDiff > .5){
        if(control->getThrottleVal() >= 45){
            control->setThrottleVal(-.8);
        }
        QTimer::singleShot(170, this, SLOT(throttleDown()));
    }
    else {
        if(control->getThrottleVal() >= 60){
            control->setThrottleVal(-.8);
        }
        QTimer::singleShot(130, this, SLOT(throttleDown()));
    }
}

void MainWindow::updateStatus()
{
    bool status = false;
    if(control->getStatus() == RUNNING) {
        status = true;
    }
    //ui->gBox_Modules->setEnabled(!status);
    ui->pButton_Main_Start->setEnabled(!status);
    ui->pButton_Main_Stop->setEnabled(status);
}

void MainWindow::updateManSelect()
{
    if(ui->cBox_Mod_Cont->currentText() == "None") {
        //ui->gBox_Cont_Sel->setEnabled(false);
    } else {
        //ui->gBox_Cont_Sel->setEnabled(true);
    }
    ui->rButton_Sel_Roll_Auto->setChecked(control->getRoll());
    ui->rButton_Sel_Roll_Man->setChecked(!control->getRoll());
    ui->rButton_Sel_Thr_Auto->setChecked(control->getThrottle());
    ui->rButton_Sel_Thr_Man->setChecked(!control->getThrottle());
    ui->rButton_Sel_Pit_Auto->setChecked(control->getPitch());
    ui->rButton_Sel_Pit_Man->setChecked(!control->getPitch());
    ui->rButton_Sel_Yaw_Auto->setChecked(control->getYaw());
    ui->rButton_Sel_Yaw_Man->setChecked(!control->getYaw());
}

void MainWindow::on_rButton_Sel_Thr_Auto_clicked()
{
    control->setThrottle(true);
    updateManSelect();
}

void MainWindow::on_rButton_Sel_Thr_Man_clicked()
{
    control->setThrottle(false);
    updateManSelect();
}

void MainWindow::on_rButton_Sel_Pit_Auto_clicked()
{
    control->setPitch(true);
    updateManSelect();
}

void MainWindow::on_rButton_Sel_Pit_Man_clicked()
{
    control->setPitch(false);
    updateManSelect();
}

void MainWindow::on_rButton_Sel_Roll_Auto_clicked()
{
    control->setRoll(true);
    updateManSelect();
}

void MainWindow::on_rButton_Sel_Roll_Man_clicked()
{
    control->setRoll(false);
    updateManSelect();
}

void MainWindow::on_rButton_Sel_Yaw_Auto_clicked()
{
    control->setYaw(true);
    updateManSelect();
}

void MainWindow::on_rButton_Sel_Yaw_Man_clicked()
{
    control->setYaw(false);
    updateManSelect();
}

void MainWindow::on_cBox_BhvrModel_currentIndexChanged(const QString &arg1)
{
    if(arg1 == "")
        return;
    if(control->setBehaviorModel(arg1.toStdString())) {
        printMessage("Behavior Model set to : " + arg1);

    } else {
        printMessage("Failed to set Behavior Model");
    }
}

void MainWindow::on_Settings_Grid_X_editingFinished()
{
    double x_min;
    double x_max;

    x_min = -1 * ui->Settings_Grid_X->value()/2;
    x_max = ui->Settings_Grid_X->value()/2;
    ui->Grid_Graphics_View->x_min=x_min;
    ui->Grid_Graphics_View->x_max=x_max;

    ui->Grid_Graphics_View->redrawPath();
}

void MainWindow::on_Settings_Grid_Y_editingFinished()
{
    double y_min;
    double y_max;

    y_min = -1 * ui->Settings_Grid_Y->value()/2;
    y_max = ui->Settings_Grid_Y->value()/2;
    ui->Grid_Graphics_View->y_min=y_min;
    ui->Grid_Graphics_View->y_max=y_max;

    ui->Grid_Graphics_View->redrawPath();
}

void MainWindow::on_Settings_Grid_Z_editingFinished()
{
    double z_max;

    //z_max = -1 * ui->Settings_Grid_Z->value();
    //ui->Grid_Graphics_View->z_max=z_max;
    //ui->Grid_Heading->setMaximum(z_max);
}

void MainWindow::on_Settings_Grid_Queue_Interval_editingFinished()
{
    double queue_interval;

    queue_interval = ui->Settings_Grid_Queue_Interval->value();
    Command_Queue->command_interval_ms = queue_interval;
}

void MainWindow::on_Grid_Toolbar_Clear_clicked()
{
    ui->Grid_Graphics_View->clearPath();
}

void MainWindow::on_Grid_Toolbar_Save_clicked()
{
    int i, i_max;
    std::vector<Position> path;
    std::vector<std::string> commands;
    QString file_name;
    QFile output_file;

    file_name = QFileDialog::getSaveFileName();
    output_file.setFileName(file_name);

    if (output_file.open(QIODevice::ReadWrite))
    {
        QTextStream output_stream(&output_file);
        path = ui->Grid_Graphics_View->getPathRM();
        commands = parsePositionsToCommands(path);

        i_max = commands.size();
        for (i=0; i<i_max; i=i+1)
        {
            output_stream << commands[i].c_str() << endl;
        }
        output_file.close();
    }
}

void MainWindow::on_Grid_Toolbar_Load_clicked()
{
    std::vector<Position> path;
    std::vector<std::string> commands;
    QString file_name;
    QString command;
    QFile input_file;

    file_name = QFileDialog::getOpenFileName();
    input_file.setFileName(file_name);

    if (input_file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        while (input_file.atEnd() == false)
        {
            command = input_file.readLine();
            commands.push_back(command.toStdString());
        }
        input_file.close();
        path = parseCommandsToPositions(commands);
        ui->Grid_Graphics_View->setPathRM(path);
        //ui->Grid_Graphics_View->setPathAM(path);
    }
}

void MainWindow::on_Grid_Toolbar_Send_clicked()
{
    std::vector<Position> positions;

    positions = ui->Grid_Graphics_View->getPathRM();

    Command_Queue->addPositions(positions);

    ui->Grid_Toolbar_Send->setVisible(false);
    ui->Grid_Toolbar_Reset->setVisible(true);
}

void MainWindow::on_Grid_Toolbar_Reset_clicked()
{
    Command_Queue->clear();
}

void MainWindow::gridSetup()
{
    double x_min, x_max, y_min, y_max, z_max, z, heading, queue_interval;

    ui->Grid_Toolbar_Reset->setVisible(false);

    x_min = -1 * ui->Settings_Grid_X->value()/2;
    x_max = ui->Settings_Grid_X->value()/2;
    y_min = -1 * ui->Settings_Grid_Y->value()/2;
    y_max = ui->Settings_Grid_Y->value()/2;
    queue_interval = ui->Settings_Grid_Queue_Interval->value();

    Command_Queue->command_interval_ms = queue_interval;
    ui->Grid_Graphics_View->x_min=x_min;
    ui->Grid_Graphics_View->x_max=x_max;
    ui->Grid_Graphics_View->y_min=y_min;
    ui->Grid_Graphics_View->y_max=y_max;

    updateGrid();
}

void MainWindow::updateGrid()
{
    if (Command_Queue->getQueueState() == STATE_IDLE)
    {
        ui->Grid_Toolbar_Send->setVisible(true);
        ui->Grid_Toolbar_Reset->setVisible(false);
    }
}

/**
  * Sets up the MCTController for the omnibot when tracking is turned on
  **/
void MainWindow::on_cBox_Mod_TrackOmni_clicked(){
    second->setModule(ORIENTATION, "OmniTrack-VRPN");
    second->setModule(POSITION, "OmniTrack-VRPN");
    second->setModule(DIRECTION, "OmniTrack-VRPN");
    second->setModule(COMMUNICATION, "6-Channel Trainer");
    second->setModule(CONTROL, "None");
    second->setBehavior("PID 2012");
    second->setBehaviorModel("GAUI");

    second->setThrottle(false);
    second->setPitch(false);
    second->setRoll(false);
    second->setYaw(false);

    if(ui->cBox_Mod_TrackOmni->isChecked()){
        printMessage("Start tracking");
        second->start();
        ui->cBox_Mod_FollowOmni->setEnabled(true);
    }
    else {
        printMessage("Stop tracking");
        // Doesn't call second->stop() because most of MCTController's stop() stops important modules to the UAV's flight
        ui->cBox_Mod_FollowOmni->setChecked(false);
        ui->cBox_Mod_FollowOmni->setEnabled(false);
    }
}

/**
  * Starting point for following the omnibot when the follow checkmark is pressed
  **/
void MainWindow::on_cBox_Mod_FollowOmni_clicked(){
    if(ui->cBox_Mod_FollowOmni->isChecked() && ui->cBox_Mod_TrackOmni->isChecked()){
        printMessage("Start following");
        setOmniToTarget();
    }
    else if (ui->cBox_Mod_FollowOmni->isChecked() && !ui->cBox_Mod_TrackOmni->isChecked()){
        // Only follow if the omnibot is being tracked
        ui->cBox_Mod_FollowOmni->setChecked(false);
    }
    else{
        // Stop following
    }
}

/**
  * Action function for updating the target location of the UAV based on the location of the omnibot
  * Calls itself every 100ms while the UAV should be following
  **/
void MainWindow::setOmniToTarget(){
    if(control->getStatus() == RUNNING && ui->cBox_Mod_FollowOmni->isChecked()){
        Position current_position = second->getCurrentPosition();

        // The additions to the lat and lon difference are to take care of an offset with the UAV's tracking
        // Adjustments may have to be made to these offsets to get the UAV to fly directly over the omnibot
        double latDiff = current_position.y; //+.5/*- .5*/; //lat -.75  -1.0m
        double lonDiff = current_position.x; //-.5/*+.25*/; //lon -.75  +1.5m

        if(latDiff > .2 || latDiff < -.2 || lonDiff > .2 || lonDiff < -.2){
            control->parseCommand("omni " + QString::number(latDiff).toStdString() + " " + QString::number(lonDiff).toStdString() );
        }

        QTimer::singleShot(100, this, SLOT(setOmniToTarget()));
    }
    else{
        printMessage("Stop following");
    }
}

/**
  * Allows the enter button to be used to send commands on the console tab
  **/
void MainWindow::keyPressEvent(QKeyEvent* event){
    if(event->key() == Qt::Key_Return || event->key() == Qt::Key_Enter){
        on_pButton_IO_Send_clicked();
    }
}
