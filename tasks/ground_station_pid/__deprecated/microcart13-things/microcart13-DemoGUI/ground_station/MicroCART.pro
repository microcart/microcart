#-------------------------------------------------
#
# Project created by QtCreator 2011-12-30T08:27:57
#
#-------------------------------------------------

QT       += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MicroCART
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    Trainer.cpp \
    SixchanTrainMod.cpp \
    SingleCameraMod.cpp \
    Module.cpp \
    MCTController.cpp \
    ImuMod.cpp \
    FourchanTrainMod.cpp \
    Datalogger.cpp \
    MCTUtilities.cpp \
    USBRemoteMod.cpp \
    ballMath.cpp \
    OptitrackMod.cpp \
    BehaviorModule.cpp \
    PIDBehavior.cpp \
    SSBehavior.cpp \
    GridGraphicsView.cpp \
    FileParser.cpp \
    CommandQueue.cpp \
    OmnibotTracker.cpp

HEADERS  += mainwindow.h \
    MCTStructures.h \
    MCTController.h \
    ImuMod.h \
    FourchanTrainMod.h \
    Datalogger.h \
    Trainer.h \
    SixchanTrainMod.h \
    SingleCameraMod.h \
    Module.h \
    MCTUtilities.h \
    USBRemoteMod.h \
    ballMath.h \
    OptitrackMod.h \
    usb2train.h \
    BehaviorModule.h \
    PIDBehavior.h \
    SSBehavior.h \
    GridGraphicsView.h \
    FileParser.h \
    CommandQueue.h \
    OmnibotTracker.h

FORMS    += mainwindow.ui






unix|win32: LIBS += -lvrpn
unix|win32: LIBS += -lquat
