#include "OmnibotTracker.h"
#include "Module.h"
#include "MCTUtilities.h"
#include "vrpn_Connection.h"
#include "vrpn_Tracker.h"
#include "quat.h"

#include <iostream>

static void VRPN_CALLBACK handle_pos1 (void *ori, const vrpn_TRACKERCB t)
{
    MCTorientation *temp = (MCTorientation *) ori;
    char stats[100];

    q_vec_type yawPitchRoll;
    q_to_euler(yawPitchRoll, t.quat);
    temp->pitch = yawPitchRoll[1];
    temp->roll = yawPitchRoll[2];
}
static void VRPN_CALLBACK handle_pos2 (void *pos, const vrpn_TRACKERCB t)
{
    MCTposition *temp = (MCTposition *) pos;
    temp->altitude = (double)  -t.pos[2];
    temp->longitude = (double) t.pos[1];
    temp->latitude = (double) t.pos[0];
}
static void VRPN_CALLBACK handle_pos3 (void *dir, const vrpn_TRACKERCB t)
{
    double *temp = (double *) dir;

    q_vec_type yawPitchRoll;
    q_to_euler(yawPitchRoll, t.quat);
    *temp = yawPitchRoll[0];
}

OmnibotTracker::OmnibotTracker() : Module("OmniTrack-VRPN")
{
    modPosition.GPSalt = -1000.0;
    modPosition.GPSlat = -1000.0;
    modPosition.GPSlong = -1000.0;
}

OmnibotTracker::~OmnibotTracker()
{

}

bool OmnibotTracker::start()
{
    //open vrpn connection
    char connectionName[128];

    sprintf(connectionName,"192.168.0.120:3883");

    connection = vrpn_get_connection_by_name(connectionName);

    tracker = new vrpn_Tracker_Remote("OMNIBOT",connection);

    tracker->register_change_handler((void*)&modOrientation, handle_pos1);
    tracker->register_change_handler((void*)&modPosition, handle_pos2);
    tracker->register_change_handler((void*)&modDirection, handle_pos3);

    //small wait for the preliminary values to be read and written locally
    usleep(10000);

    //get initial values
    connection->mainloop();
    tracker->mainloop();
    return true;
}

bool OmnibotTracker::update()
{
    connection->mainloop();
    tracker->mainloop();
    normalize_Orientation(&modOrientation);
}

bool OmnibotTracker::stop()
{

}
