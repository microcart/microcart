\section{USB2Train Class Reference}
\label{classUSB2Train}\index{USB2Train@{USB2Train}}
{\tt \#include $<$usb2train.h$>$}

Inheritance diagram for USB2Train::\begin{figure}[H]
\begin{center}
\leavevmode
\includegraphics[height=2cm]{classUSB2Train}
\end{center}
\end{figure}
\subsection*{Public Member Functions}
\begin{CompactItemize}
\item 
{\bf USB2Train} ()
\item 
{\bf $\sim$USB2Train} ()
\item 
bool {\bf isContMod} ()
\begin{CompactList}\small\item\em Checks if this module is responsible for manually controlling the quadcopter. \item\end{CompactList}\item 
bool {\bf getModValues} (double $\ast$data, int size)
\begin{CompactList}\small\item\em Gets specific values from a module. \item\end{CompactList}\item 
bool {\bf start} ()
\begin{CompactList}\small\item\em Initializes the module and starts its funcitons. \item\end{CompactList}\item 
bool {\bf update} ()
\begin{CompactList}\small\item\em Updates the internal state of the module. \item\end{CompactList}\item 
bool {\bf stop} ()
\begin{CompactList}\small\item\em Stops all functions of the module. \item\end{CompactList}\end{CompactItemize}
\subsection*{Protected Attributes}
\begin{CompactItemize}
\item 
std::string {\bf usb\_\-id}
\item 
int {\bf rc\_\-fd}
\item 
int {\bf axis} [5]
\item 
char {\bf button} [2]
\item 
unsigned char {\bf axes}
\item 
unsigned char {\bf buttons}
\item 
double {\bf thrtVal}
\item 
double {\bf rollVal}
\item 
double {\bf yawVal}
\item 
double {\bf pitchVal}
\item 
double {\bf IL\_\-ROLL\_\-NEUTRAL}
\item 
double {\bf IL\_\-ROLL\_\-RANGE}
\item 
double {\bf IL\_\-PITCH\_\-NEUTRAL}
\item 
double {\bf IL\_\-PITCH\_\-RANGE}
\item 
double {\bf IL\_\-THROTTLE\_\-NEUTRAL}
\item 
double {\bf IL\_\-THROTTLE\_\-RANGE}
\item 
double {\bf IL\_\-YAW\_\-NEUTRAL}
\item 
double {\bf IL\_\-YAW\_\-RANGE}
\end{CompactItemize}


\subsection{Constructor \& Destructor Documentation}
\index{USB2Train@{USB2Train}!USB2Train@{USB2Train}}
\index{USB2Train@{USB2Train}!USB2Train@{USB2Train}}
\subsubsection{\setlength{\rightskip}{0pt plus 5cm}USB2Train::USB2Train ()}\label{classUSB2Train_cff4616d23cc54c691db7e8452b05cc7}


\index{USB2Train@{USB2Train}!~USB2Train@{$\sim$USB2Train}}
\index{~USB2Train@{$\sim$USB2Train}!USB2Train@{USB2Train}}
\subsubsection{\setlength{\rightskip}{0pt plus 5cm}USB2Train::$\sim$USB2Train ()}\label{classUSB2Train_f23489aa608349c086d36b59a80e98da}




\subsection{Member Function Documentation}
\index{USB2Train@{USB2Train}!isContMod@{isContMod}}
\index{isContMod@{isContMod}!USB2Train@{USB2Train}}
\subsubsection{\setlength{\rightskip}{0pt plus 5cm}bool USB2Train::isContMod ()\hspace{0.3cm}{\tt  [inline, virtual]}}\label{classUSB2Train_ce9efe9b313c27daec69ea5c7d9344b9}


Checks if this module is responsible for manually controlling the quadcopter. 

This needs to be reimplemented on a per module basis. \begin{Desc}
\item[Returns:]True if this module is responsible for manually controlling the quadcopter. \end{Desc}


Reimplemented from {\bf Module} \doxyref{}{p.}{classModule_6e946d53c4a60e382e48d5818b78b46e}.\index{USB2Train@{USB2Train}!getModValues@{getModValues}}
\index{getModValues@{getModValues}!USB2Train@{USB2Train}}
\subsubsection{\setlength{\rightskip}{0pt plus 5cm}bool USB2Train::getModValues (double $\ast$ {\em data}, int {\em size})\hspace{0.3cm}{\tt  [virtual]}}\label{classUSB2Train_b4861afafe7e927a35240ab9243c6a2a}


Gets specific values from a module. 

What the values are depends on the type of module. \begin{Desc}
\item[Parameters:]
\begin{description}
\item[\mbox{$\rightarrow$} {\em $\ast$data}]Pointer to a data array. \item[\mbox{$\leftarrow$} {\em size}]Number of elements in the data array. \end{description}
\end{Desc}
\begin{Desc}
\item[Returns:]True if successful. \end{Desc}


Reimplemented from {\bf Module} \doxyref{}{p.}{classModule_032d2df9e6545c0515adae97879c9f51}.\index{USB2Train@{USB2Train}!start@{start}}
\index{start@{start}!USB2Train@{USB2Train}}
\subsubsection{\setlength{\rightskip}{0pt plus 5cm}bool USB2Train::start ()\hspace{0.3cm}{\tt  [virtual]}}\label{classUSB2Train_44cf791accacd76d61817b5e900b31e6}


Initializes the module and starts its funcitons. 

This needs to be reimplemented on a per module basis. \begin{Desc}
\item[Returns:]True if successful. \end{Desc}


Reimplemented from {\bf Module} \doxyref{}{p.}{classModule_9e8dbb01960ce6d63c47442f8c699af8}.\index{USB2Train@{USB2Train}!update@{update}}
\index{update@{update}!USB2Train@{USB2Train}}
\subsubsection{\setlength{\rightskip}{0pt plus 5cm}bool USB2Train::update ()\hspace{0.3cm}{\tt  [virtual]}}\label{classUSB2Train_bdede7a14530094428e19463b2c86126}


Updates the internal state of the module. 

This needs to be reimplemented on a per module basis. \begin{Desc}
\item[Returns:]True if successful. \end{Desc}


Reimplemented from {\bf Module} \doxyref{}{p.}{classModule_788f329a62cb8244b2a0c99f183f3528}.\index{USB2Train@{USB2Train}!stop@{stop}}
\index{stop@{stop}!USB2Train@{USB2Train}}
\subsubsection{\setlength{\rightskip}{0pt plus 5cm}bool USB2Train::stop ()\hspace{0.3cm}{\tt  [virtual]}}\label{classUSB2Train_b8921d81c0998a7b388149e7aaa6c5cb}


Stops all functions of the module. 

This needs to be reimplemented on a per module basis. \begin{Desc}
\item[Returns:]True if successful. \end{Desc}


Reimplemented from {\bf Module} \doxyref{}{p.}{classModule_4c29d30d7e8e0d3904f5cab625932c94}.

\subsection{Member Data Documentation}
\index{USB2Train@{USB2Train}!usb_id@{usb\_\-id}}
\index{usb_id@{usb\_\-id}!USB2Train@{USB2Train}}
\subsubsection{\setlength{\rightskip}{0pt plus 5cm}std::string {\bf USB2Train::usb\_\-id}\hspace{0.3cm}{\tt  [protected]}}\label{classUSB2Train_78bf3f861ddf38f77b2a53e8feb2d5e3}


\index{USB2Train@{USB2Train}!rc_fd@{rc\_\-fd}}
\index{rc_fd@{rc\_\-fd}!USB2Train@{USB2Train}}
\subsubsection{\setlength{\rightskip}{0pt plus 5cm}int {\bf USB2Train::rc\_\-fd}\hspace{0.3cm}{\tt  [protected]}}\label{classUSB2Train_8f82f755135a92bf8e4506f18a1cbb69}


\index{USB2Train@{USB2Train}!axis@{axis}}
\index{axis@{axis}!USB2Train@{USB2Train}}
\subsubsection{\setlength{\rightskip}{0pt plus 5cm}int {\bf USB2Train::axis}[5]\hspace{0.3cm}{\tt  [protected]}}\label{classUSB2Train_637fd04feed3cf7b784864d9ffefdb2c}


\index{USB2Train@{USB2Train}!button@{button}}
\index{button@{button}!USB2Train@{USB2Train}}
\subsubsection{\setlength{\rightskip}{0pt plus 5cm}char {\bf USB2Train::button}[2]\hspace{0.3cm}{\tt  [protected]}}\label{classUSB2Train_c4056bdafbeaee1a3398fcbdde4026ab}


\index{USB2Train@{USB2Train}!axes@{axes}}
\index{axes@{axes}!USB2Train@{USB2Train}}
\subsubsection{\setlength{\rightskip}{0pt plus 5cm}unsigned char {\bf USB2Train::axes}\hspace{0.3cm}{\tt  [protected]}}\label{classUSB2Train_7f84c31e92c4851612179f99d908025f}


\index{USB2Train@{USB2Train}!buttons@{buttons}}
\index{buttons@{buttons}!USB2Train@{USB2Train}}
\subsubsection{\setlength{\rightskip}{0pt plus 5cm}unsigned char {\bf USB2Train::buttons}\hspace{0.3cm}{\tt  [protected]}}\label{classUSB2Train_cfe7c86a40521930dffc9152fab17526}


\index{USB2Train@{USB2Train}!thrtVal@{thrtVal}}
\index{thrtVal@{thrtVal}!USB2Train@{USB2Train}}
\subsubsection{\setlength{\rightskip}{0pt plus 5cm}double {\bf USB2Train::thrtVal}\hspace{0.3cm}{\tt  [protected]}}\label{classUSB2Train_d6d8be306f493576281785236de0fedb}


\index{USB2Train@{USB2Train}!rollVal@{rollVal}}
\index{rollVal@{rollVal}!USB2Train@{USB2Train}}
\subsubsection{\setlength{\rightskip}{0pt plus 5cm}double {\bf USB2Train::rollVal}\hspace{0.3cm}{\tt  [protected]}}\label{classUSB2Train_d9969fa88cc5a05c6292bf8a064eb8da}


\index{USB2Train@{USB2Train}!yawVal@{yawVal}}
\index{yawVal@{yawVal}!USB2Train@{USB2Train}}
\subsubsection{\setlength{\rightskip}{0pt plus 5cm}double {\bf USB2Train::yawVal}\hspace{0.3cm}{\tt  [protected]}}\label{classUSB2Train_8a5b563b95169a8904390175c3a6b786}


\index{USB2Train@{USB2Train}!pitchVal@{pitchVal}}
\index{pitchVal@{pitchVal}!USB2Train@{USB2Train}}
\subsubsection{\setlength{\rightskip}{0pt plus 5cm}double {\bf USB2Train::pitchVal}\hspace{0.3cm}{\tt  [protected]}}\label{classUSB2Train_ce044ebd04376bc3657ff1305592ac1f}


\index{USB2Train@{USB2Train}!IL_ROLL_NEUTRAL@{IL\_\-ROLL\_\-NEUTRAL}}
\index{IL_ROLL_NEUTRAL@{IL\_\-ROLL\_\-NEUTRAL}!USB2Train@{USB2Train}}
\subsubsection{\setlength{\rightskip}{0pt plus 5cm}double {\bf USB2Train::IL\_\-ROLL\_\-NEUTRAL}\hspace{0.3cm}{\tt  [protected]}}\label{classUSB2Train_fbab9f0fe9997dd43b36b113e8f6aab6}


\index{USB2Train@{USB2Train}!IL_ROLL_RANGE@{IL\_\-ROLL\_\-RANGE}}
\index{IL_ROLL_RANGE@{IL\_\-ROLL\_\-RANGE}!USB2Train@{USB2Train}}
\subsubsection{\setlength{\rightskip}{0pt plus 5cm}double {\bf USB2Train::IL\_\-ROLL\_\-RANGE}\hspace{0.3cm}{\tt  [protected]}}\label{classUSB2Train_4aea6904421eed8146f5380ebc5858d3}


\index{USB2Train@{USB2Train}!IL_PITCH_NEUTRAL@{IL\_\-PITCH\_\-NEUTRAL}}
\index{IL_PITCH_NEUTRAL@{IL\_\-PITCH\_\-NEUTRAL}!USB2Train@{USB2Train}}
\subsubsection{\setlength{\rightskip}{0pt plus 5cm}double {\bf USB2Train::IL\_\-PITCH\_\-NEUTRAL}\hspace{0.3cm}{\tt  [protected]}}\label{classUSB2Train_454a850babbc15ceea3529cfdc4493d5}


\index{USB2Train@{USB2Train}!IL_PITCH_RANGE@{IL\_\-PITCH\_\-RANGE}}
\index{IL_PITCH_RANGE@{IL\_\-PITCH\_\-RANGE}!USB2Train@{USB2Train}}
\subsubsection{\setlength{\rightskip}{0pt plus 5cm}double {\bf USB2Train::IL\_\-PITCH\_\-RANGE}\hspace{0.3cm}{\tt  [protected]}}\label{classUSB2Train_aced6cc9fe639e45fe694f31eb6b055d}


\index{USB2Train@{USB2Train}!IL_THROTTLE_NEUTRAL@{IL\_\-THROTTLE\_\-NEUTRAL}}
\index{IL_THROTTLE_NEUTRAL@{IL\_\-THROTTLE\_\-NEUTRAL}!USB2Train@{USB2Train}}
\subsubsection{\setlength{\rightskip}{0pt plus 5cm}double {\bf USB2Train::IL\_\-THROTTLE\_\-NEUTRAL}\hspace{0.3cm}{\tt  [protected]}}\label{classUSB2Train_fd12ef14d998f6ba4ee3c25b0cab6ccb}


\index{USB2Train@{USB2Train}!IL_THROTTLE_RANGE@{IL\_\-THROTTLE\_\-RANGE}}
\index{IL_THROTTLE_RANGE@{IL\_\-THROTTLE\_\-RANGE}!USB2Train@{USB2Train}}
\subsubsection{\setlength{\rightskip}{0pt plus 5cm}double {\bf USB2Train::IL\_\-THROTTLE\_\-RANGE}\hspace{0.3cm}{\tt  [protected]}}\label{classUSB2Train_da1d11a7ccf6c07d19af81ea0c0d7ed3}


\index{USB2Train@{USB2Train}!IL_YAW_NEUTRAL@{IL\_\-YAW\_\-NEUTRAL}}
\index{IL_YAW_NEUTRAL@{IL\_\-YAW\_\-NEUTRAL}!USB2Train@{USB2Train}}
\subsubsection{\setlength{\rightskip}{0pt plus 5cm}double {\bf USB2Train::IL\_\-YAW\_\-NEUTRAL}\hspace{0.3cm}{\tt  [protected]}}\label{classUSB2Train_5e92eed2bdea447e693820d982fc47be}


\index{USB2Train@{USB2Train}!IL_YAW_RANGE@{IL\_\-YAW\_\-RANGE}}
\index{IL_YAW_RANGE@{IL\_\-YAW\_\-RANGE}!USB2Train@{USB2Train}}
\subsubsection{\setlength{\rightskip}{0pt plus 5cm}double {\bf USB2Train::IL\_\-YAW\_\-RANGE}\hspace{0.3cm}{\tt  [protected]}}\label{classUSB2Train_314a5196bf8ce43131c2e51ada19566d}




The documentation for this class was generated from the following file:\begin{CompactItemize}
\item 
{\bf usb2train.h}\end{CompactItemize}
