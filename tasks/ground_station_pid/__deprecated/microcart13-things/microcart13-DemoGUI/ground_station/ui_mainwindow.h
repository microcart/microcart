/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created: Mon Oct 7 14:28:45 2013
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QAction>
#include <QApplication>
#include <QButtonGroup>
#include <QCheckBox>
#include <QComboBox>
#include <QDoubleSpinBox>
#include <QFormLayout>
#include <QFrame>
#include <QGraphicsView>
#include <QGroupBox>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QLabel>
#include <QLineEdit>
#include <QMainWindow>
#include <QMenuBar>
#include <QPushButton>
#include <QRadioButton>
#include <QScrollArea>
#include <QSpacerItem>
#include <QSpinBox>
#include <QTabWidget>
#include <QTextEdit>
#include <QVBoxLayout>
#include <QWidget>
#include "GridGraphicsView.h"

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *menuDefault;
    QAction *menuGAUIQuad;
    QAction *menuBig_Heli;
    QWidget *centralWidget;
    QHBoxLayout *horizontalLayout;
    QFrame *frame;
    QVBoxLayout *verticalLayout;
    QFrame *frame_7;
    QVBoxLayout *verticalLayout_6;
    QPushButton *pButton_Main_Start;
    QPushButton *pButton_Main_Stop;
    QFrame *line;
    QFrame *frame_6;
    QVBoxLayout *verticalLayout_5;
    QPushButton *pButton_Take_Off;
    QPushButton *pButton_Land;
    QFrame *line_3;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    QVBoxLayout *verticalLayout_4;
    QFrame *frame_11;
    QGraphicsView *gView_FS_Pit;
    QLabel *label_FS_Pit;
    QLineEdit *lEdit_FS_PitText;
    QFrame *frame_12;
    QGraphicsView *gView_FS_Roll;
    QLabel *label_FS_Roll;
    QLineEdit *lEdit_FS_RollText;
    QFrame *frame_2;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_FS_Dir_N;
    QFrame *frame_4;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_FS_Dir_W;
    QGraphicsView *gView_FS_Dir;
    QLabel *label_FS_E;
    QLabel *label_FS_Dir_S;
    QGroupBox *groupBox_10;
    QHBoxLayout *horizontalLayout_18;
    QLineEdit *Sensor_Current_X;
    QLabel *label_15;
    QLineEdit *Sensor_Target_X;
    QGroupBox *groupBox_11;
    QHBoxLayout *horizontalLayout_19;
    QLineEdit *Sensor_Current_Y;
    QLabel *label_16;
    QLineEdit *Sensor_Target_Y;
    QGroupBox *groupBox_9;
    QHBoxLayout *horizontalLayout_17;
    QLineEdit *Sensor_Current_Z;
    QLabel *label_14;
    QLineEdit *Sensor_Target_Z;
    QSpacerItem *verticalSpacer;
    QFrame *line_2;
    QTabWidget *tabWidget;
    QWidget *tab_Settings;
    QVBoxLayout *verticalLayout_8;
    QFrame *frame_3;
    QVBoxLayout *verticalLayout_13;
    QScrollArea *scrollArea_2;
    QWidget *scrollAreaWidgetContents_2;
    QVBoxLayout *verticalLayout_11;
    QFrame *frame_10;
    QVBoxLayout *verticalLayout_12;
    QGroupBox *groupBox_6;
    QVBoxLayout *verticalLayout_10;
    QComboBox *cBox_Bhvr;
    QComboBox *cBox_BhvrModel;
    QGroupBox *groupBox_7;
    QFormLayout *formLayout_3;
    QLabel *label_Mod_Ori;
    QComboBox *cBox_Mod_Ori;
    QLabel *label_Mod_Dir;
    QComboBox *cBox_Mod_Dir;
    QLabel *label_Mod_Pos;
    QComboBox *cBox_Mod_Pos;
    QLabel *label_Mod_Comm;
    QComboBox *cBox_Mod_Comm;
    QLabel *label_Mod_Control;
    QComboBox *cBox_Mod_Cont;
    QGroupBox *groupBox_8;
    QFormLayout *formLayout;
    QLabel *label_8;
    QFrame *frame_14;
    QHBoxLayout *horizontalLayout_7;
    QRadioButton *rButton_Sel_Thr_Man;
    QRadioButton *rButton_Sel_Thr_Auto;
    QLabel *label_9;
    QLabel *label_11;
    QLabel *label_12;
    QFrame *frame_16;
    QHBoxLayout *horizontalLayout_8;
    QRadioButton *rButton_Sel_Pit_Man;
    QRadioButton *rButton_Sel_Pit_Auto;
    QFrame *frame_19;
    QHBoxLayout *horizontalLayout_9;
    QRadioButton *rButton_Sel_Roll_Man;
    QRadioButton *rButton_Sel_Roll_Auto;
    QFrame *frame_20;
    QHBoxLayout *horizontalLayout_15;
    QRadioButton *rButton_Sel_Yaw_Man;
    QRadioButton *rButton_Sel_Yaw_Auto;
    QGroupBox *groupBox_5;
    QFormLayout *formLayout_2;
    QLabel *label_3;
    QDoubleSpinBox *Settings_Grid_X;
    QLabel *label_4;
    QDoubleSpinBox *Settings_Grid_Y;
    QDoubleSpinBox *Settings_Grid_Z;
    QLabel *label_10;
    QLabel *label_13;
    QSpinBox *Settings_Grid_Queue_Interval;
    QSpacerItem *verticalSpacer_2;
    QWidget *tab_Grid;
    QVBoxLayout *verticalLayout_9;
    QFrame *frame_15;
    QHBoxLayout *horizontalLayout_10;
    QPushButton *Grid_Toolbar_Save;
    QPushButton *Grid_Toolbar_Load;
    QPushButton *Grid_Toolbar_Send;
    QPushButton *Grid_Toolbar_Reset;
    QPushButton *Grid_Toolbar_Clear;
    QSpacerItem *horizontalSpacer_3;
    GridGraphicsView *Grid_Graphics_View;
    QWidget *tab_Console;
    QVBoxLayout *verticalLayout_3;
    QFrame *frame_13;
    QHBoxLayout *horizontalLayout_16;
    QPushButton *pButton_IO_ImportFile;
    QPushButton *pButton_IO_CancelCmds;
    QSpacerItem *horizontalSpacer_4;
    QTextEdit *tEdit_IO_Out;
    QFrame *frame_5;
    QHBoxLayout *horizontalLayout_4;
    QLineEdit *lEdit_IO_In;
    QPushButton *pButton_IO_Send;
    QWidget *tab_Omnibot;
    QScrollArea *scrollArea_3;
    QWidget *scrollAreaWidgetContents_3;
    QVBoxLayout *verticalLayout_7;
    QCheckBox *cBox_Mod_TrackOmni;
    QCheckBox *cBox_Mod_FollowOmni;
    QFrame *frame_17;
    QGraphicsView *gView_FS_Pit_2;
    QLabel *label_FS_Pit_2;
    QLineEdit *lEdit_FS_PitText_2;
    QFrame *frame_18;
    QGraphicsView *gView_FS_Roll_2;
    QLabel *label_FS_Roll_2;
    QLineEdit *lEdit_FS_RollText_2;
    QFrame *frame_8;
    QVBoxLayout *verticalLayout_14;
    QLabel *label_FS_Dir_N_2;
    QFrame *frame_9;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label_FS_Dir_W_2;
    QGraphicsView *gView_FS_Dir_2;
    QLabel *label_FS_E_2;
    QLabel *label_FS_Dir_S_2;
    QGroupBox *groupBox_12;
    QHBoxLayout *horizontalLayout_20;
    QLineEdit *lEdit_FS_Long_2;
    QLabel *label_17;
    QLineEdit *lEdit_FS_TarLong_2;
    QGroupBox *groupBox_13;
    QHBoxLayout *horizontalLayout_21;
    QLineEdit *lEdit_FS_Lat_2;
    QLabel *label_18;
    QLineEdit *lEdit_FS_TarLat_2;
    QGroupBox *groupBox_14;
    QHBoxLayout *horizontalLayout_22;
    QLineEdit *lEdit_FS_Alt_2;
    QLabel *label_19;
    QLineEdit *lEdit_FS_TarAlt_2;
    QCheckBox *check_FS_Abso;
    QSpacerItem *verticalSpacer_3;
    QWidget *tab_Sensors;
    QGroupBox *groupBox;
    QHBoxLayout *horizontalLayout_11;
    QLineEdit *lEdit_FS_Alt;
    QLabel *label_5;
    QLineEdit *lEdit_FS_TarAlt;
    QGroupBox *groupBox_2;
    QHBoxLayout *horizontalLayout_12;
    QLineEdit *lEdit_FS_Long;
    QLabel *label_6;
    QLineEdit *lEdit_FS_TarLong;
    QGroupBox *groupBox_3;
    QHBoxLayout *horizontalLayout_13;
    QLineEdit *lEdit_FS_Lat;
    QLabel *label_7;
    QLineEdit *lEdit_FS_TarLat;
    QGroupBox *groupBox_4;
    QHBoxLayout *horizontalLayout_14;
    QLineEdit *lEdit_FS_Spd;
    QLineEdit *lEdit_FS_DirText;
    QLineEdit *lEdit_Main_AvrExec;
    QMenuBar *menuBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(1003, 613);
        menuDefault = new QAction(MainWindow);
        menuDefault->setObjectName(QString::fromUtf8("menuDefault"));
        menuDefault->setCheckable(true);
        menuDefault->setChecked(true);
        menuDefault->setAutoRepeat(false);
        menuGAUIQuad = new QAction(MainWindow);
        menuGAUIQuad->setObjectName(QString::fromUtf8("menuGAUIQuad"));
        menuGAUIQuad->setCheckable(true);
        menuGAUIQuad->setAutoRepeat(false);
        menuBig_Heli = new QAction(MainWindow);
        menuBig_Heli->setObjectName(QString::fromUtf8("menuBig_Heli"));
        menuBig_Heli->setCheckable(true);
        menuBig_Heli->setAutoRepeat(false);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        horizontalLayout = new QHBoxLayout(centralWidget);
        horizontalLayout->setSpacing(0);
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        frame = new QFrame(centralWidget);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setMinimumSize(QSize(170, 0));
        frame->setMaximumSize(QSize(170, 16777215));
        frame->setStyleSheet(QString::fromUtf8(""));
        frame->setFrameShape(QFrame::NoFrame);
        frame->setFrameShadow(QFrame::Plain);
        verticalLayout = new QVBoxLayout(frame);
        verticalLayout->setSpacing(0);
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        frame_7 = new QFrame(frame);
        frame_7->setObjectName(QString::fromUtf8("frame_7"));
        frame_7->setFrameShape(QFrame::NoFrame);
        frame_7->setFrameShadow(QFrame::Raised);
        verticalLayout_6 = new QVBoxLayout(frame_7);
        verticalLayout_6->setSpacing(6);
        verticalLayout_6->setContentsMargins(11, 11, 11, 11);
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        pButton_Main_Start = new QPushButton(frame_7);
        pButton_Main_Start->setObjectName(QString::fromUtf8("pButton_Main_Start"));
        pButton_Main_Start->setMinimumSize(QSize(150, 30));
        pButton_Main_Start->setMaximumSize(QSize(150, 30));

        verticalLayout_6->addWidget(pButton_Main_Start);

        pButton_Main_Stop = new QPushButton(frame_7);
        pButton_Main_Stop->setObjectName(QString::fromUtf8("pButton_Main_Stop"));
        pButton_Main_Stop->setEnabled(false);
        pButton_Main_Stop->setMinimumSize(QSize(150, 30));
        pButton_Main_Stop->setMaximumSize(QSize(150, 30));

        verticalLayout_6->addWidget(pButton_Main_Stop);


        verticalLayout->addWidget(frame_7);

        line = new QFrame(frame);
        line->setObjectName(QString::fromUtf8("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        verticalLayout->addWidget(line);

        frame_6 = new QFrame(frame);
        frame_6->setObjectName(QString::fromUtf8("frame_6"));
        frame_6->setFrameShape(QFrame::NoFrame);
        frame_6->setFrameShadow(QFrame::Raised);
        verticalLayout_5 = new QVBoxLayout(frame_6);
        verticalLayout_5->setSpacing(6);
        verticalLayout_5->setContentsMargins(11, 11, 11, 11);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        pButton_Take_Off = new QPushButton(frame_6);
        pButton_Take_Off->setObjectName(QString::fromUtf8("pButton_Take_Off"));

        verticalLayout_5->addWidget(pButton_Take_Off);

        pButton_Land = new QPushButton(frame_6);
        pButton_Land->setObjectName(QString::fromUtf8("pButton_Land"));

        verticalLayout_5->addWidget(pButton_Land);


        verticalLayout->addWidget(frame_6);

        line_3 = new QFrame(frame);
        line_3->setObjectName(QString::fromUtf8("line_3"));
        line_3->setFrameShape(QFrame::HLine);
        line_3->setFrameShadow(QFrame::Sunken);

        verticalLayout->addWidget(line_3);

        scrollArea = new QScrollArea(frame);
        scrollArea->setObjectName(QString::fromUtf8("scrollArea"));
        scrollArea->setFrameShape(QFrame::NoFrame);
        scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        scrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QString::fromUtf8("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 170, 422));
        verticalLayout_4 = new QVBoxLayout(scrollAreaWidgetContents);
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setContentsMargins(9, 9, 9, 9);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        frame_11 = new QFrame(scrollAreaWidgetContents);
        frame_11->setObjectName(QString::fromUtf8("frame_11"));
        frame_11->setMinimumSize(QSize(150, 70));
        frame_11->setMaximumSize(QSize(150, 70));
        frame_11->setFrameShape(QFrame::StyledPanel);
        frame_11->setFrameShadow(QFrame::Raised);
        gView_FS_Pit = new QGraphicsView(frame_11);
        gView_FS_Pit->setObjectName(QString::fromUtf8("gView_FS_Pit"));
        gView_FS_Pit->setGeometry(QRect(0, 0, 150, 70));
        gView_FS_Pit->setMinimumSize(QSize(150, 70));
        gView_FS_Pit->setMaximumSize(QSize(150, 70));
        gView_FS_Pit->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        gView_FS_Pit->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        label_FS_Pit = new QLabel(frame_11);
        label_FS_Pit->setObjectName(QString::fromUtf8("label_FS_Pit"));
        label_FS_Pit->setGeometry(QRect(6, 3, 40, 20));
        label_FS_Pit->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        lEdit_FS_PitText = new QLineEdit(frame_11);
        lEdit_FS_PitText->setObjectName(QString::fromUtf8("lEdit_FS_PitText"));
        lEdit_FS_PitText->setEnabled(true);
        lEdit_FS_PitText->setGeometry(QRect(110, 0, 40, 20));
        lEdit_FS_PitText->setFocusPolicy(Qt::NoFocus);
        lEdit_FS_PitText->setAcceptDrops(false);
        lEdit_FS_PitText->setFrame(false);
        lEdit_FS_PitText->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        lEdit_FS_PitText->setReadOnly(true);

        verticalLayout_4->addWidget(frame_11);

        frame_12 = new QFrame(scrollAreaWidgetContents);
        frame_12->setObjectName(QString::fromUtf8("frame_12"));
        frame_12->setMinimumSize(QSize(150, 70));
        frame_12->setMaximumSize(QSize(150, 70));
        frame_12->setFrameShape(QFrame::StyledPanel);
        frame_12->setFrameShadow(QFrame::Raised);
        gView_FS_Roll = new QGraphicsView(frame_12);
        gView_FS_Roll->setObjectName(QString::fromUtf8("gView_FS_Roll"));
        gView_FS_Roll->setGeometry(QRect(0, 0, 150, 70));
        gView_FS_Roll->setMinimumSize(QSize(150, 70));
        gView_FS_Roll->setMaximumSize(QSize(150, 70));
        gView_FS_Roll->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        gView_FS_Roll->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        label_FS_Roll = new QLabel(frame_12);
        label_FS_Roll->setObjectName(QString::fromUtf8("label_FS_Roll"));
        label_FS_Roll->setGeometry(QRect(6, 3, 40, 20));
        label_FS_Roll->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        lEdit_FS_RollText = new QLineEdit(frame_12);
        lEdit_FS_RollText->setObjectName(QString::fromUtf8("lEdit_FS_RollText"));
        lEdit_FS_RollText->setEnabled(true);
        lEdit_FS_RollText->setGeometry(QRect(110, 0, 40, 20));
        lEdit_FS_RollText->setFocusPolicy(Qt::NoFocus);
        lEdit_FS_RollText->setAcceptDrops(false);
        lEdit_FS_RollText->setFrame(false);
        lEdit_FS_RollText->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        lEdit_FS_RollText->setReadOnly(true);

        verticalLayout_4->addWidget(frame_12);

        frame_2 = new QFrame(scrollAreaWidgetContents);
        frame_2->setObjectName(QString::fromUtf8("frame_2"));
        frame_2->setMinimumSize(QSize(150, 102));
        frame_2->setMaximumSize(QSize(150, 102));
        frame_2->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);"));
        frame_2->setFrameShape(QFrame::StyledPanel);
        frame_2->setFrameShadow(QFrame::Plain);
        verticalLayout_2 = new QVBoxLayout(frame_2);
        verticalLayout_2->setSpacing(0);
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        label_FS_Dir_N = new QLabel(frame_2);
        label_FS_Dir_N->setObjectName(QString::fromUtf8("label_FS_Dir_N"));
        label_FS_Dir_N->setMinimumSize(QSize(0, 20));
        label_FS_Dir_N->setMaximumSize(QSize(16777215, 20));
        QFont font;
        font.setBold(true);
        font.setWeight(75);
        label_FS_Dir_N->setFont(font);
        label_FS_Dir_N->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(label_FS_Dir_N);

        frame_4 = new QFrame(frame_2);
        frame_4->setObjectName(QString::fromUtf8("frame_4"));
        frame_4->setMaximumSize(QSize(16777215, 60));
        frame_4->setFrameShape(QFrame::NoFrame);
        frame_4->setFrameShadow(QFrame::Raised);
        horizontalLayout_3 = new QHBoxLayout(frame_4);
        horizontalLayout_3->setSpacing(0);
        horizontalLayout_3->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        label_FS_Dir_W = new QLabel(frame_4);
        label_FS_Dir_W->setObjectName(QString::fromUtf8("label_FS_Dir_W"));
        label_FS_Dir_W->setMinimumSize(QSize(20, 20));
        label_FS_Dir_W->setMaximumSize(QSize(20, 20));
        label_FS_Dir_W->setFont(font);
        label_FS_Dir_W->setAlignment(Qt::AlignCenter);

        horizontalLayout_3->addWidget(label_FS_Dir_W);

        gView_FS_Dir = new QGraphicsView(frame_4);
        gView_FS_Dir->setObjectName(QString::fromUtf8("gView_FS_Dir"));
        gView_FS_Dir->setMinimumSize(QSize(70, 60));
        gView_FS_Dir->setMaximumSize(QSize(70, 60));
        gView_FS_Dir->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        gView_FS_Dir->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        gView_FS_Dir->setInteractive(false);

        horizontalLayout_3->addWidget(gView_FS_Dir);

        label_FS_E = new QLabel(frame_4);
        label_FS_E->setObjectName(QString::fromUtf8("label_FS_E"));
        label_FS_E->setMinimumSize(QSize(20, 20));
        label_FS_E->setMaximumSize(QSize(20, 20));
        label_FS_E->setFont(font);
        label_FS_E->setAlignment(Qt::AlignCenter);

        horizontalLayout_3->addWidget(label_FS_E);


        verticalLayout_2->addWidget(frame_4);

        label_FS_Dir_S = new QLabel(frame_2);
        label_FS_Dir_S->setObjectName(QString::fromUtf8("label_FS_Dir_S"));
        label_FS_Dir_S->setMinimumSize(QSize(0, 20));
        label_FS_Dir_S->setMaximumSize(QSize(16777215, 20));
        label_FS_Dir_S->setFont(font);
        label_FS_Dir_S->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(label_FS_Dir_S);


        verticalLayout_4->addWidget(frame_2);

        groupBox_10 = new QGroupBox(scrollAreaWidgetContents);
        groupBox_10->setObjectName(QString::fromUtf8("groupBox_10"));
        QFont font1;
        font1.setPointSize(8);
        groupBox_10->setFont(font1);
        horizontalLayout_18 = new QHBoxLayout(groupBox_10);
        horizontalLayout_18->setSpacing(0);
        horizontalLayout_18->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_18->setObjectName(QString::fromUtf8("horizontalLayout_18"));
        Sensor_Current_X = new QLineEdit(groupBox_10);
        Sensor_Current_X->setObjectName(QString::fromUtf8("Sensor_Current_X"));

        horizontalLayout_18->addWidget(Sensor_Current_X);

        label_15 = new QLabel(groupBox_10);
        label_15->setObjectName(QString::fromUtf8("label_15"));

        horizontalLayout_18->addWidget(label_15);

        Sensor_Target_X = new QLineEdit(groupBox_10);
        Sensor_Target_X->setObjectName(QString::fromUtf8("Sensor_Target_X"));

        horizontalLayout_18->addWidget(Sensor_Target_X);


        verticalLayout_4->addWidget(groupBox_10);

        groupBox_11 = new QGroupBox(scrollAreaWidgetContents);
        groupBox_11->setObjectName(QString::fromUtf8("groupBox_11"));
        groupBox_11->setFont(font1);
        horizontalLayout_19 = new QHBoxLayout(groupBox_11);
        horizontalLayout_19->setSpacing(0);
        horizontalLayout_19->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_19->setObjectName(QString::fromUtf8("horizontalLayout_19"));
        Sensor_Current_Y = new QLineEdit(groupBox_11);
        Sensor_Current_Y->setObjectName(QString::fromUtf8("Sensor_Current_Y"));

        horizontalLayout_19->addWidget(Sensor_Current_Y);

        label_16 = new QLabel(groupBox_11);
        label_16->setObjectName(QString::fromUtf8("label_16"));

        horizontalLayout_19->addWidget(label_16);

        Sensor_Target_Y = new QLineEdit(groupBox_11);
        Sensor_Target_Y->setObjectName(QString::fromUtf8("Sensor_Target_Y"));

        horizontalLayout_19->addWidget(Sensor_Target_Y);


        verticalLayout_4->addWidget(groupBox_11);

        groupBox_9 = new QGroupBox(scrollAreaWidgetContents);
        groupBox_9->setObjectName(QString::fromUtf8("groupBox_9"));
        groupBox_9->setFont(font1);
        horizontalLayout_17 = new QHBoxLayout(groupBox_9);
        horizontalLayout_17->setSpacing(0);
        horizontalLayout_17->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_17->setObjectName(QString::fromUtf8("horizontalLayout_17"));
        Sensor_Current_Z = new QLineEdit(groupBox_9);
        Sensor_Current_Z->setObjectName(QString::fromUtf8("Sensor_Current_Z"));

        horizontalLayout_17->addWidget(Sensor_Current_Z);

        label_14 = new QLabel(groupBox_9);
        label_14->setObjectName(QString::fromUtf8("label_14"));

        horizontalLayout_17->addWidget(label_14);

        Sensor_Target_Z = new QLineEdit(groupBox_9);
        Sensor_Target_Z->setObjectName(QString::fromUtf8("Sensor_Target_Z"));

        horizontalLayout_17->addWidget(Sensor_Target_Z);


        verticalLayout_4->addWidget(groupBox_9);

        verticalSpacer = new QSpacerItem(20, 165, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_4->addItem(verticalSpacer);

        scrollArea->setWidget(scrollAreaWidgetContents);
        frame_11->raise();
        frame_12->raise();
        frame_2->raise();
        groupBox_10->raise();
        groupBox_11->raise();
        groupBox_9->raise();

        verticalLayout->addWidget(scrollArea);


        horizontalLayout->addWidget(frame);

        line_2 = new QFrame(centralWidget);
        line_2->setObjectName(QString::fromUtf8("line_2"));
        line_2->setFrameShape(QFrame::VLine);
        line_2->setFrameShadow(QFrame::Sunken);

        horizontalLayout->addWidget(line_2);

        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        QFont font2;
        font2.setFamily(QString::fromUtf8("Arial"));
        font2.setPointSize(12);
        font2.setBold(true);
        font2.setWeight(75);
        font2.setKerning(true);
        tabWidget->setFont(font2);
        tabWidget->setStyleSheet(QString::fromUtf8("\n"
" QTabWidget::tab-bar {\n"
"\n"
" }\n"
"\n"
" QTabWidget::pane {\n"
"     border-top: 4px solid #3399FF;\n"
" }\n"
"\n"
"QTabBar::tab {\n"
"	background: #EEEEEE;\n"
"	color: #000000;\n"
"}\n"
"\n"
"QTabBar::tab:hover {\n"
"	background: #CCCCCC;\n"
"}\n"
"\n"
"QTabBar::tab:selected {\n"
"	background: #3399FF;\n"
"}\n"
""));
        tabWidget->setTabPosition(QTabWidget::North);
        tabWidget->setTabShape(QTabWidget::Rounded);
        tab_Settings = new QWidget();
        tab_Settings->setObjectName(QString::fromUtf8("tab_Settings"));
        verticalLayout_8 = new QVBoxLayout(tab_Settings);
        verticalLayout_8->setSpacing(0);
        verticalLayout_8->setContentsMargins(0, 0, 0, 0);
        verticalLayout_8->setObjectName(QString::fromUtf8("verticalLayout_8"));
        frame_3 = new QFrame(tab_Settings);
        frame_3->setObjectName(QString::fromUtf8("frame_3"));
        frame_3->setFrameShape(QFrame::NoFrame);
        frame_3->setFrameShadow(QFrame::Raised);
        verticalLayout_13 = new QVBoxLayout(frame_3);
        verticalLayout_13->setSpacing(0);
        verticalLayout_13->setContentsMargins(0, 0, 0, 0);
        verticalLayout_13->setObjectName(QString::fromUtf8("verticalLayout_13"));
        scrollArea_2 = new QScrollArea(frame_3);
        scrollArea_2->setObjectName(QString::fromUtf8("scrollArea_2"));
        scrollArea_2->setFrameShape(QFrame::NoFrame);
        scrollArea_2->setWidgetResizable(true);
        scrollArea_2->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        scrollAreaWidgetContents_2 = new QWidget();
        scrollAreaWidgetContents_2->setObjectName(QString::fromUtf8("scrollAreaWidgetContents_2"));
        scrollAreaWidgetContents_2->setGeometry(QRect(0, 0, 812, 631));
        verticalLayout_11 = new QVBoxLayout(scrollAreaWidgetContents_2);
        verticalLayout_11->setSpacing(0);
        verticalLayout_11->setContentsMargins(0, 0, 0, 0);
        verticalLayout_11->setObjectName(QString::fromUtf8("verticalLayout_11"));
        frame_10 = new QFrame(scrollAreaWidgetContents_2);
        frame_10->setObjectName(QString::fromUtf8("frame_10"));
        frame_10->setMaximumSize(QSize(300, 800));
        frame_10->setFrameShape(QFrame::NoFrame);
        frame_10->setFrameShadow(QFrame::Raised);
        verticalLayout_12 = new QVBoxLayout(frame_10);
        verticalLayout_12->setSpacing(6);
        verticalLayout_12->setContentsMargins(11, 11, 11, 11);
        verticalLayout_12->setObjectName(QString::fromUtf8("verticalLayout_12"));
        groupBox_6 = new QGroupBox(frame_10);
        groupBox_6->setObjectName(QString::fromUtf8("groupBox_6"));
        verticalLayout_10 = new QVBoxLayout(groupBox_6);
        verticalLayout_10->setSpacing(6);
        verticalLayout_10->setContentsMargins(11, 11, 11, 11);
        verticalLayout_10->setObjectName(QString::fromUtf8("verticalLayout_10"));
        cBox_Bhvr = new QComboBox(groupBox_6);
        cBox_Bhvr->setObjectName(QString::fromUtf8("cBox_Bhvr"));

        verticalLayout_10->addWidget(cBox_Bhvr);

        cBox_BhvrModel = new QComboBox(groupBox_6);
        cBox_BhvrModel->setObjectName(QString::fromUtf8("cBox_BhvrModel"));

        verticalLayout_10->addWidget(cBox_BhvrModel);


        verticalLayout_12->addWidget(groupBox_6);

        groupBox_7 = new QGroupBox(frame_10);
        groupBox_7->setObjectName(QString::fromUtf8("groupBox_7"));
        groupBox_7->setMinimumSize(QSize(0, 0));
        formLayout_3 = new QFormLayout(groupBox_7);
        formLayout_3->setSpacing(6);
        formLayout_3->setContentsMargins(11, 11, 11, 11);
        formLayout_3->setObjectName(QString::fromUtf8("formLayout_3"));
        label_Mod_Ori = new QLabel(groupBox_7);
        label_Mod_Ori->setObjectName(QString::fromUtf8("label_Mod_Ori"));

        formLayout_3->setWidget(0, QFormLayout::LabelRole, label_Mod_Ori);

        cBox_Mod_Ori = new QComboBox(groupBox_7);
        cBox_Mod_Ori->setObjectName(QString::fromUtf8("cBox_Mod_Ori"));

        formLayout_3->setWidget(0, QFormLayout::FieldRole, cBox_Mod_Ori);

        label_Mod_Dir = new QLabel(groupBox_7);
        label_Mod_Dir->setObjectName(QString::fromUtf8("label_Mod_Dir"));

        formLayout_3->setWidget(1, QFormLayout::LabelRole, label_Mod_Dir);

        cBox_Mod_Dir = new QComboBox(groupBox_7);
        cBox_Mod_Dir->setObjectName(QString::fromUtf8("cBox_Mod_Dir"));

        formLayout_3->setWidget(1, QFormLayout::FieldRole, cBox_Mod_Dir);

        label_Mod_Pos = new QLabel(groupBox_7);
        label_Mod_Pos->setObjectName(QString::fromUtf8("label_Mod_Pos"));

        formLayout_3->setWidget(2, QFormLayout::LabelRole, label_Mod_Pos);

        cBox_Mod_Pos = new QComboBox(groupBox_7);
        cBox_Mod_Pos->setObjectName(QString::fromUtf8("cBox_Mod_Pos"));

        formLayout_3->setWidget(2, QFormLayout::FieldRole, cBox_Mod_Pos);

        label_Mod_Comm = new QLabel(groupBox_7);
        label_Mod_Comm->setObjectName(QString::fromUtf8("label_Mod_Comm"));

        formLayout_3->setWidget(3, QFormLayout::LabelRole, label_Mod_Comm);

        cBox_Mod_Comm = new QComboBox(groupBox_7);
        cBox_Mod_Comm->setObjectName(QString::fromUtf8("cBox_Mod_Comm"));

        formLayout_3->setWidget(3, QFormLayout::FieldRole, cBox_Mod_Comm);

        label_Mod_Control = new QLabel(groupBox_7);
        label_Mod_Control->setObjectName(QString::fromUtf8("label_Mod_Control"));

        formLayout_3->setWidget(4, QFormLayout::LabelRole, label_Mod_Control);

        cBox_Mod_Cont = new QComboBox(groupBox_7);
        cBox_Mod_Cont->setObjectName(QString::fromUtf8("cBox_Mod_Cont"));

        formLayout_3->setWidget(4, QFormLayout::FieldRole, cBox_Mod_Cont);


        verticalLayout_12->addWidget(groupBox_7);

        groupBox_8 = new QGroupBox(frame_10);
        groupBox_8->setObjectName(QString::fromUtf8("groupBox_8"));
        formLayout = new QFormLayout(groupBox_8);
        formLayout->setSpacing(6);
        formLayout->setContentsMargins(11, 11, 11, 11);
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        label_8 = new QLabel(groupBox_8);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        formLayout->setWidget(0, QFormLayout::LabelRole, label_8);

        frame_14 = new QFrame(groupBox_8);
        frame_14->setObjectName(QString::fromUtf8("frame_14"));
        frame_14->setFrameShape(QFrame::NoFrame);
        frame_14->setFrameShadow(QFrame::Raised);
        horizontalLayout_7 = new QHBoxLayout(frame_14);
        horizontalLayout_7->setSpacing(0);
        horizontalLayout_7->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        rButton_Sel_Thr_Man = new QRadioButton(frame_14);
        rButton_Sel_Thr_Man->setObjectName(QString::fromUtf8("rButton_Sel_Thr_Man"));
        rButton_Sel_Thr_Man->setAutoExclusive(false);

        horizontalLayout_7->addWidget(rButton_Sel_Thr_Man);

        rButton_Sel_Thr_Auto = new QRadioButton(frame_14);
        rButton_Sel_Thr_Auto->setObjectName(QString::fromUtf8("rButton_Sel_Thr_Auto"));
        rButton_Sel_Thr_Auto->setChecked(true);
        rButton_Sel_Thr_Auto->setAutoExclusive(false);

        horizontalLayout_7->addWidget(rButton_Sel_Thr_Auto);


        formLayout->setWidget(0, QFormLayout::FieldRole, frame_14);

        label_9 = new QLabel(groupBox_8);
        label_9->setObjectName(QString::fromUtf8("label_9"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label_9);

        label_11 = new QLabel(groupBox_8);
        label_11->setObjectName(QString::fromUtf8("label_11"));

        formLayout->setWidget(2, QFormLayout::LabelRole, label_11);

        label_12 = new QLabel(groupBox_8);
        label_12->setObjectName(QString::fromUtf8("label_12"));

        formLayout->setWidget(3, QFormLayout::LabelRole, label_12);

        frame_16 = new QFrame(groupBox_8);
        frame_16->setObjectName(QString::fromUtf8("frame_16"));
        frame_16->setFrameShape(QFrame::NoFrame);
        frame_16->setFrameShadow(QFrame::Raised);
        horizontalLayout_8 = new QHBoxLayout(frame_16);
        horizontalLayout_8->setSpacing(0);
        horizontalLayout_8->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        rButton_Sel_Pit_Man = new QRadioButton(frame_16);
        rButton_Sel_Pit_Man->setObjectName(QString::fromUtf8("rButton_Sel_Pit_Man"));
        rButton_Sel_Pit_Man->setAutoExclusive(false);

        horizontalLayout_8->addWidget(rButton_Sel_Pit_Man);

        rButton_Sel_Pit_Auto = new QRadioButton(frame_16);
        rButton_Sel_Pit_Auto->setObjectName(QString::fromUtf8("rButton_Sel_Pit_Auto"));
        rButton_Sel_Pit_Auto->setChecked(true);
        rButton_Sel_Pit_Auto->setAutoExclusive(false);

        horizontalLayout_8->addWidget(rButton_Sel_Pit_Auto);


        formLayout->setWidget(1, QFormLayout::FieldRole, frame_16);

        frame_19 = new QFrame(groupBox_8);
        frame_19->setObjectName(QString::fromUtf8("frame_19"));
        frame_19->setFrameShape(QFrame::NoFrame);
        frame_19->setFrameShadow(QFrame::Raised);
        horizontalLayout_9 = new QHBoxLayout(frame_19);
        horizontalLayout_9->setSpacing(0);
        horizontalLayout_9->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        rButton_Sel_Roll_Man = new QRadioButton(frame_19);
        rButton_Sel_Roll_Man->setObjectName(QString::fromUtf8("rButton_Sel_Roll_Man"));
        rButton_Sel_Roll_Man->setAutoExclusive(false);

        horizontalLayout_9->addWidget(rButton_Sel_Roll_Man);

        rButton_Sel_Roll_Auto = new QRadioButton(frame_19);
        rButton_Sel_Roll_Auto->setObjectName(QString::fromUtf8("rButton_Sel_Roll_Auto"));
        rButton_Sel_Roll_Auto->setChecked(true);
        rButton_Sel_Roll_Auto->setAutoExclusive(false);

        horizontalLayout_9->addWidget(rButton_Sel_Roll_Auto);


        formLayout->setWidget(2, QFormLayout::FieldRole, frame_19);

        frame_20 = new QFrame(groupBox_8);
        frame_20->setObjectName(QString::fromUtf8("frame_20"));
        frame_20->setFrameShape(QFrame::NoFrame);
        frame_20->setFrameShadow(QFrame::Raised);
        horizontalLayout_15 = new QHBoxLayout(frame_20);
        horizontalLayout_15->setSpacing(0);
        horizontalLayout_15->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_15->setObjectName(QString::fromUtf8("horizontalLayout_15"));
        rButton_Sel_Yaw_Man = new QRadioButton(frame_20);
        rButton_Sel_Yaw_Man->setObjectName(QString::fromUtf8("rButton_Sel_Yaw_Man"));
        rButton_Sel_Yaw_Man->setAutoExclusive(false);

        horizontalLayout_15->addWidget(rButton_Sel_Yaw_Man);

        rButton_Sel_Yaw_Auto = new QRadioButton(frame_20);
        rButton_Sel_Yaw_Auto->setObjectName(QString::fromUtf8("rButton_Sel_Yaw_Auto"));
        rButton_Sel_Yaw_Auto->setChecked(true);
        rButton_Sel_Yaw_Auto->setAutoExclusive(false);

        horizontalLayout_15->addWidget(rButton_Sel_Yaw_Auto);


        formLayout->setWidget(3, QFormLayout::FieldRole, frame_20);


        verticalLayout_12->addWidget(groupBox_8);

        groupBox_5 = new QGroupBox(frame_10);
        groupBox_5->setObjectName(QString::fromUtf8("groupBox_5"));
        formLayout_2 = new QFormLayout(groupBox_5);
        formLayout_2->setSpacing(6);
        formLayout_2->setContentsMargins(11, 11, 11, 11);
        formLayout_2->setObjectName(QString::fromUtf8("formLayout_2"));
        label_3 = new QLabel(groupBox_5);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        formLayout_2->setWidget(0, QFormLayout::LabelRole, label_3);

        Settings_Grid_X = new QDoubleSpinBox(groupBox_5);
        Settings_Grid_X->setObjectName(QString::fromUtf8("Settings_Grid_X"));
        Settings_Grid_X->setButtonSymbols(QAbstractSpinBox::UpDownArrows);
        Settings_Grid_X->setDecimals(1);
        Settings_Grid_X->setSingleStep(0.1);
        Settings_Grid_X->setValue(2);

        formLayout_2->setWidget(0, QFormLayout::FieldRole, Settings_Grid_X);

        label_4 = new QLabel(groupBox_5);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        formLayout_2->setWidget(1, QFormLayout::LabelRole, label_4);

        Settings_Grid_Y = new QDoubleSpinBox(groupBox_5);
        Settings_Grid_Y->setObjectName(QString::fromUtf8("Settings_Grid_Y"));
        Settings_Grid_Y->setDecimals(1);
        Settings_Grid_Y->setSingleStep(0.1);
        Settings_Grid_Y->setValue(2);

        formLayout_2->setWidget(1, QFormLayout::FieldRole, Settings_Grid_Y);

        Settings_Grid_Z = new QDoubleSpinBox(groupBox_5);
        Settings_Grid_Z->setObjectName(QString::fromUtf8("Settings_Grid_Z"));
        Settings_Grid_Z->setEnabled(false);
        Settings_Grid_Z->setDecimals(1);
        Settings_Grid_Z->setSingleStep(0.1);
        Settings_Grid_Z->setValue(2);

        formLayout_2->setWidget(2, QFormLayout::FieldRole, Settings_Grid_Z);

        label_10 = new QLabel(groupBox_5);
        label_10->setObjectName(QString::fromUtf8("label_10"));

        formLayout_2->setWidget(2, QFormLayout::LabelRole, label_10);

        label_13 = new QLabel(groupBox_5);
        label_13->setObjectName(QString::fromUtf8("label_13"));

        formLayout_2->setWidget(3, QFormLayout::LabelRole, label_13);

        Settings_Grid_Queue_Interval = new QSpinBox(groupBox_5);
        Settings_Grid_Queue_Interval->setObjectName(QString::fromUtf8("Settings_Grid_Queue_Interval"));
        Settings_Grid_Queue_Interval->setMinimum(20);
        Settings_Grid_Queue_Interval->setMaximum(1000);
        Settings_Grid_Queue_Interval->setSingleStep(10);
        Settings_Grid_Queue_Interval->setValue(100);

        formLayout_2->setWidget(3, QFormLayout::FieldRole, Settings_Grid_Queue_Interval);


        verticalLayout_12->addWidget(groupBox_5);


        verticalLayout_11->addWidget(frame_10);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_11->addItem(verticalSpacer_2);

        scrollArea_2->setWidget(scrollAreaWidgetContents_2);

        verticalLayout_13->addWidget(scrollArea_2);


        verticalLayout_8->addWidget(frame_3);

        tabWidget->addTab(tab_Settings, QString());
        tab_Grid = new QWidget();
        tab_Grid->setObjectName(QString::fromUtf8("tab_Grid"));
        verticalLayout_9 = new QVBoxLayout(tab_Grid);
        verticalLayout_9->setSpacing(0);
        verticalLayout_9->setContentsMargins(0, 0, 0, 0);
        verticalLayout_9->setObjectName(QString::fromUtf8("verticalLayout_9"));
        frame_15 = new QFrame(tab_Grid);
        frame_15->setObjectName(QString::fromUtf8("frame_15"));
        frame_15->setMinimumSize(QSize(0, 27));
        frame_15->setMaximumSize(QSize(16777215, 27));
        frame_15->setFrameShape(QFrame::NoFrame);
        frame_15->setFrameShadow(QFrame::Plain);
        horizontalLayout_10 = new QHBoxLayout(frame_15);
        horizontalLayout_10->setSpacing(0);
        horizontalLayout_10->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
        Grid_Toolbar_Save = new QPushButton(frame_15);
        Grid_Toolbar_Save->setObjectName(QString::fromUtf8("Grid_Toolbar_Save"));

        horizontalLayout_10->addWidget(Grid_Toolbar_Save);

        Grid_Toolbar_Load = new QPushButton(frame_15);
        Grid_Toolbar_Load->setObjectName(QString::fromUtf8("Grid_Toolbar_Load"));

        horizontalLayout_10->addWidget(Grid_Toolbar_Load);

        Grid_Toolbar_Send = new QPushButton(frame_15);
        Grid_Toolbar_Send->setObjectName(QString::fromUtf8("Grid_Toolbar_Send"));
        Grid_Toolbar_Send->setEnabled(true);
        Grid_Toolbar_Send->setFlat(false);

        horizontalLayout_10->addWidget(Grid_Toolbar_Send);

        Grid_Toolbar_Reset = new QPushButton(frame_15);
        Grid_Toolbar_Reset->setObjectName(QString::fromUtf8("Grid_Toolbar_Reset"));

        horizontalLayout_10->addWidget(Grid_Toolbar_Reset);

        Grid_Toolbar_Clear = new QPushButton(frame_15);
        Grid_Toolbar_Clear->setObjectName(QString::fromUtf8("Grid_Toolbar_Clear"));

        horizontalLayout_10->addWidget(Grid_Toolbar_Clear);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_10->addItem(horizontalSpacer_3);


        verticalLayout_9->addWidget(frame_15);

        Grid_Graphics_View = new GridGraphicsView(tab_Grid);
        Grid_Graphics_View->setObjectName(QString::fromUtf8("Grid_Graphics_View"));

        verticalLayout_9->addWidget(Grid_Graphics_View);

        tabWidget->addTab(tab_Grid, QString());
        tab_Console = new QWidget();
        tab_Console->setObjectName(QString::fromUtf8("tab_Console"));
        verticalLayout_3 = new QVBoxLayout(tab_Console);
        verticalLayout_3->setSpacing(0);
        verticalLayout_3->setContentsMargins(0, 0, 0, 0);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        frame_13 = new QFrame(tab_Console);
        frame_13->setObjectName(QString::fromUtf8("frame_13"));
        frame_13->setMinimumSize(QSize(0, 27));
        frame_13->setMaximumSize(QSize(16777215, 27));
        frame_13->setFrameShape(QFrame::NoFrame);
        frame_13->setFrameShadow(QFrame::Raised);
        horizontalLayout_16 = new QHBoxLayout(frame_13);
        horizontalLayout_16->setSpacing(0);
        horizontalLayout_16->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_16->setObjectName(QString::fromUtf8("horizontalLayout_16"));
        pButton_IO_ImportFile = new QPushButton(frame_13);
        pButton_IO_ImportFile->setObjectName(QString::fromUtf8("pButton_IO_ImportFile"));
        pButton_IO_ImportFile->setEnabled(true);

        horizontalLayout_16->addWidget(pButton_IO_ImportFile);

        pButton_IO_CancelCmds = new QPushButton(frame_13);
        pButton_IO_CancelCmds->setObjectName(QString::fromUtf8("pButton_IO_CancelCmds"));
        pButton_IO_CancelCmds->setEnabled(true);

        horizontalLayout_16->addWidget(pButton_IO_CancelCmds);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_16->addItem(horizontalSpacer_4);


        verticalLayout_3->addWidget(frame_13);

        tEdit_IO_Out = new QTextEdit(tab_Console);
        tEdit_IO_Out->setObjectName(QString::fromUtf8("tEdit_IO_Out"));
        tEdit_IO_Out->setUndoRedoEnabled(false);
        tEdit_IO_Out->setReadOnly(true);

        verticalLayout_3->addWidget(tEdit_IO_Out);

        frame_5 = new QFrame(tab_Console);
        frame_5->setObjectName(QString::fromUtf8("frame_5"));
        frame_5->setFrameShape(QFrame::NoFrame);
        frame_5->setFrameShadow(QFrame::Raised);
        horizontalLayout_4 = new QHBoxLayout(frame_5);
        horizontalLayout_4->setSpacing(0);
        horizontalLayout_4->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        lEdit_IO_In = new QLineEdit(frame_5);
        lEdit_IO_In->setObjectName(QString::fromUtf8("lEdit_IO_In"));

        horizontalLayout_4->addWidget(lEdit_IO_In);

        pButton_IO_Send = new QPushButton(frame_5);
        pButton_IO_Send->setObjectName(QString::fromUtf8("pButton_IO_Send"));

        horizontalLayout_4->addWidget(pButton_IO_Send);


        verticalLayout_3->addWidget(frame_5);

        tabWidget->addTab(tab_Console, QString());
        tab_Omnibot = new QWidget();
        tab_Omnibot->setObjectName(QString::fromUtf8("tab_Omnibot"));
        scrollArea_3 = new QScrollArea(tab_Omnibot);
        scrollArea_3->setObjectName(QString::fromUtf8("scrollArea_3"));
        scrollArea_3->setGeometry(QRect(10, 0, 170, 511));
        scrollArea_3->setFrameShape(QFrame::NoFrame);
        scrollArea_3->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        scrollArea_3->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        scrollArea_3->setWidgetResizable(true);
        scrollAreaWidgetContents_3 = new QWidget();
        scrollAreaWidgetContents_3->setObjectName(QString::fromUtf8("scrollAreaWidgetContents_3"));
        scrollAreaWidgetContents_3->setGeometry(QRect(0, 0, 170, 515));
        verticalLayout_7 = new QVBoxLayout(scrollAreaWidgetContents_3);
        verticalLayout_7->setSpacing(6);
        verticalLayout_7->setContentsMargins(9, 9, 9, 9);
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        cBox_Mod_TrackOmni = new QCheckBox(scrollAreaWidgetContents_3);
        cBox_Mod_TrackOmni->setObjectName(QString::fromUtf8("cBox_Mod_TrackOmni"));
        cBox_Mod_TrackOmni->setLayoutDirection(Qt::RightToLeft);

        verticalLayout_7->addWidget(cBox_Mod_TrackOmni);

        cBox_Mod_FollowOmni = new QCheckBox(scrollAreaWidgetContents_3);
        cBox_Mod_FollowOmni->setObjectName(QString::fromUtf8("cBox_Mod_FollowOmni"));
        cBox_Mod_FollowOmni->setEnabled(false);
        cBox_Mod_FollowOmni->setLayoutDirection(Qt::RightToLeft);

        verticalLayout_7->addWidget(cBox_Mod_FollowOmni);

        frame_17 = new QFrame(scrollAreaWidgetContents_3);
        frame_17->setObjectName(QString::fromUtf8("frame_17"));
        frame_17->setMinimumSize(QSize(150, 70));
        frame_17->setMaximumSize(QSize(150, 70));
        frame_17->setFrameShape(QFrame::StyledPanel);
        frame_17->setFrameShadow(QFrame::Raised);
        gView_FS_Pit_2 = new QGraphicsView(frame_17);
        gView_FS_Pit_2->setObjectName(QString::fromUtf8("gView_FS_Pit_2"));
        gView_FS_Pit_2->setGeometry(QRect(0, 0, 150, 70));
        gView_FS_Pit_2->setMinimumSize(QSize(150, 70));
        gView_FS_Pit_2->setMaximumSize(QSize(150, 70));
        gView_FS_Pit_2->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        gView_FS_Pit_2->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        label_FS_Pit_2 = new QLabel(frame_17);
        label_FS_Pit_2->setObjectName(QString::fromUtf8("label_FS_Pit_2"));
        label_FS_Pit_2->setGeometry(QRect(6, 3, 40, 20));
        label_FS_Pit_2->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        lEdit_FS_PitText_2 = new QLineEdit(frame_17);
        lEdit_FS_PitText_2->setObjectName(QString::fromUtf8("lEdit_FS_PitText_2"));
        lEdit_FS_PitText_2->setEnabled(true);
        lEdit_FS_PitText_2->setGeometry(QRect(110, 0, 40, 20));
        lEdit_FS_PitText_2->setFocusPolicy(Qt::NoFocus);
        lEdit_FS_PitText_2->setAcceptDrops(false);
        lEdit_FS_PitText_2->setFrame(false);
        lEdit_FS_PitText_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        lEdit_FS_PitText_2->setReadOnly(true);

        verticalLayout_7->addWidget(frame_17);

        frame_18 = new QFrame(scrollAreaWidgetContents_3);
        frame_18->setObjectName(QString::fromUtf8("frame_18"));
        frame_18->setMinimumSize(QSize(150, 70));
        frame_18->setMaximumSize(QSize(150, 70));
        frame_18->setFrameShape(QFrame::StyledPanel);
        frame_18->setFrameShadow(QFrame::Raised);
        gView_FS_Roll_2 = new QGraphicsView(frame_18);
        gView_FS_Roll_2->setObjectName(QString::fromUtf8("gView_FS_Roll_2"));
        gView_FS_Roll_2->setGeometry(QRect(0, 0, 150, 70));
        gView_FS_Roll_2->setMinimumSize(QSize(150, 70));
        gView_FS_Roll_2->setMaximumSize(QSize(150, 70));
        gView_FS_Roll_2->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        gView_FS_Roll_2->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        label_FS_Roll_2 = new QLabel(frame_18);
        label_FS_Roll_2->setObjectName(QString::fromUtf8("label_FS_Roll_2"));
        label_FS_Roll_2->setGeometry(QRect(6, 3, 40, 20));
        label_FS_Roll_2->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        lEdit_FS_RollText_2 = new QLineEdit(frame_18);
        lEdit_FS_RollText_2->setObjectName(QString::fromUtf8("lEdit_FS_RollText_2"));
        lEdit_FS_RollText_2->setEnabled(true);
        lEdit_FS_RollText_2->setGeometry(QRect(110, 0, 40, 20));
        lEdit_FS_RollText_2->setFocusPolicy(Qt::NoFocus);
        lEdit_FS_RollText_2->setAcceptDrops(false);
        lEdit_FS_RollText_2->setFrame(false);
        lEdit_FS_RollText_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        lEdit_FS_RollText_2->setReadOnly(true);

        verticalLayout_7->addWidget(frame_18);

        frame_8 = new QFrame(scrollAreaWidgetContents_3);
        frame_8->setObjectName(QString::fromUtf8("frame_8"));
        frame_8->setMinimumSize(QSize(150, 102));
        frame_8->setMaximumSize(QSize(150, 102));
        frame_8->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);"));
        frame_8->setFrameShape(QFrame::StyledPanel);
        frame_8->setFrameShadow(QFrame::Plain);
        verticalLayout_14 = new QVBoxLayout(frame_8);
        verticalLayout_14->setSpacing(0);
        verticalLayout_14->setContentsMargins(0, 0, 0, 0);
        verticalLayout_14->setObjectName(QString::fromUtf8("verticalLayout_14"));
        label_FS_Dir_N_2 = new QLabel(frame_8);
        label_FS_Dir_N_2->setObjectName(QString::fromUtf8("label_FS_Dir_N_2"));
        label_FS_Dir_N_2->setMinimumSize(QSize(0, 20));
        label_FS_Dir_N_2->setMaximumSize(QSize(16777215, 20));
        label_FS_Dir_N_2->setFont(font);
        label_FS_Dir_N_2->setAlignment(Qt::AlignCenter);

        verticalLayout_14->addWidget(label_FS_Dir_N_2);

        frame_9 = new QFrame(frame_8);
        frame_9->setObjectName(QString::fromUtf8("frame_9"));
        frame_9->setMaximumSize(QSize(16777215, 60));
        frame_9->setFrameShape(QFrame::NoFrame);
        frame_9->setFrameShadow(QFrame::Raised);
        horizontalLayout_5 = new QHBoxLayout(frame_9);
        horizontalLayout_5->setSpacing(0);
        horizontalLayout_5->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        label_FS_Dir_W_2 = new QLabel(frame_9);
        label_FS_Dir_W_2->setObjectName(QString::fromUtf8("label_FS_Dir_W_2"));
        label_FS_Dir_W_2->setMinimumSize(QSize(20, 20));
        label_FS_Dir_W_2->setMaximumSize(QSize(20, 20));
        label_FS_Dir_W_2->setFont(font);
        label_FS_Dir_W_2->setAlignment(Qt::AlignCenter);

        horizontalLayout_5->addWidget(label_FS_Dir_W_2);

        gView_FS_Dir_2 = new QGraphicsView(frame_9);
        gView_FS_Dir_2->setObjectName(QString::fromUtf8("gView_FS_Dir_2"));
        gView_FS_Dir_2->setMinimumSize(QSize(70, 60));
        gView_FS_Dir_2->setMaximumSize(QSize(70, 60));
        gView_FS_Dir_2->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        gView_FS_Dir_2->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        gView_FS_Dir_2->setInteractive(false);

        horizontalLayout_5->addWidget(gView_FS_Dir_2);

        label_FS_E_2 = new QLabel(frame_9);
        label_FS_E_2->setObjectName(QString::fromUtf8("label_FS_E_2"));
        label_FS_E_2->setMinimumSize(QSize(20, 20));
        label_FS_E_2->setMaximumSize(QSize(20, 20));
        label_FS_E_2->setFont(font);
        label_FS_E_2->setAlignment(Qt::AlignCenter);

        horizontalLayout_5->addWidget(label_FS_E_2);


        verticalLayout_14->addWidget(frame_9);

        label_FS_Dir_S_2 = new QLabel(frame_8);
        label_FS_Dir_S_2->setObjectName(QString::fromUtf8("label_FS_Dir_S_2"));
        label_FS_Dir_S_2->setMinimumSize(QSize(0, 20));
        label_FS_Dir_S_2->setMaximumSize(QSize(16777215, 20));
        label_FS_Dir_S_2->setFont(font);
        label_FS_Dir_S_2->setAlignment(Qt::AlignCenter);

        verticalLayout_14->addWidget(label_FS_Dir_S_2);


        verticalLayout_7->addWidget(frame_8);

        groupBox_12 = new QGroupBox(scrollAreaWidgetContents_3);
        groupBox_12->setObjectName(QString::fromUtf8("groupBox_12"));
        groupBox_12->setFont(font1);
        horizontalLayout_20 = new QHBoxLayout(groupBox_12);
        horizontalLayout_20->setSpacing(0);
        horizontalLayout_20->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_20->setObjectName(QString::fromUtf8("horizontalLayout_20"));
        lEdit_FS_Long_2 = new QLineEdit(groupBox_12);
        lEdit_FS_Long_2->setObjectName(QString::fromUtf8("lEdit_FS_Long_2"));

        horizontalLayout_20->addWidget(lEdit_FS_Long_2);

        label_17 = new QLabel(groupBox_12);
        label_17->setObjectName(QString::fromUtf8("label_17"));

        horizontalLayout_20->addWidget(label_17);

        lEdit_FS_TarLong_2 = new QLineEdit(groupBox_12);
        lEdit_FS_TarLong_2->setObjectName(QString::fromUtf8("lEdit_FS_TarLong_2"));

        horizontalLayout_20->addWidget(lEdit_FS_TarLong_2);


        verticalLayout_7->addWidget(groupBox_12);

        groupBox_13 = new QGroupBox(scrollAreaWidgetContents_3);
        groupBox_13->setObjectName(QString::fromUtf8("groupBox_13"));
        groupBox_13->setFont(font1);
        horizontalLayout_21 = new QHBoxLayout(groupBox_13);
        horizontalLayout_21->setSpacing(0);
        horizontalLayout_21->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_21->setObjectName(QString::fromUtf8("horizontalLayout_21"));
        lEdit_FS_Lat_2 = new QLineEdit(groupBox_13);
        lEdit_FS_Lat_2->setObjectName(QString::fromUtf8("lEdit_FS_Lat_2"));

        horizontalLayout_21->addWidget(lEdit_FS_Lat_2);

        label_18 = new QLabel(groupBox_13);
        label_18->setObjectName(QString::fromUtf8("label_18"));

        horizontalLayout_21->addWidget(label_18);

        lEdit_FS_TarLat_2 = new QLineEdit(groupBox_13);
        lEdit_FS_TarLat_2->setObjectName(QString::fromUtf8("lEdit_FS_TarLat_2"));

        horizontalLayout_21->addWidget(lEdit_FS_TarLat_2);


        verticalLayout_7->addWidget(groupBox_13);

        groupBox_14 = new QGroupBox(scrollAreaWidgetContents_3);
        groupBox_14->setObjectName(QString::fromUtf8("groupBox_14"));
        groupBox_14->setFont(font1);
        horizontalLayout_22 = new QHBoxLayout(groupBox_14);
        horizontalLayout_22->setSpacing(0);
        horizontalLayout_22->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_22->setObjectName(QString::fromUtf8("horizontalLayout_22"));
        lEdit_FS_Alt_2 = new QLineEdit(groupBox_14);
        lEdit_FS_Alt_2->setObjectName(QString::fromUtf8("lEdit_FS_Alt_2"));

        horizontalLayout_22->addWidget(lEdit_FS_Alt_2);

        label_19 = new QLabel(groupBox_14);
        label_19->setObjectName(QString::fromUtf8("label_19"));

        horizontalLayout_22->addWidget(label_19);

        lEdit_FS_TarAlt_2 = new QLineEdit(groupBox_14);
        lEdit_FS_TarAlt_2->setObjectName(QString::fromUtf8("lEdit_FS_TarAlt_2"));

        horizontalLayout_22->addWidget(lEdit_FS_TarAlt_2);


        verticalLayout_7->addWidget(groupBox_14);

        check_FS_Abso = new QCheckBox(scrollAreaWidgetContents_3);
        check_FS_Abso->setObjectName(QString::fromUtf8("check_FS_Abso"));
        check_FS_Abso->setLayoutDirection(Qt::RightToLeft);
        check_FS_Abso->setChecked(true);

        verticalLayout_7->addWidget(check_FS_Abso);

        verticalSpacer_3 = new QSpacerItem(20, 165, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_7->addItem(verticalSpacer_3);

        scrollArea_3->setWidget(scrollAreaWidgetContents_3);
        tabWidget->addTab(tab_Omnibot, QString());
        tab_Sensors = new QWidget();
        tab_Sensors->setObjectName(QString::fromUtf8("tab_Sensors"));
        groupBox = new QGroupBox(tab_Sensors);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setGeometry(QRect(30, 130, 152, 42));
        groupBox->setFont(font1);
        horizontalLayout_11 = new QHBoxLayout(groupBox);
        horizontalLayout_11->setSpacing(0);
        horizontalLayout_11->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_11->setObjectName(QString::fromUtf8("horizontalLayout_11"));
        lEdit_FS_Alt = new QLineEdit(groupBox);
        lEdit_FS_Alt->setObjectName(QString::fromUtf8("lEdit_FS_Alt"));
        lEdit_FS_Alt->setFocusPolicy(Qt::NoFocus);
        lEdit_FS_Alt->setAcceptDrops(false);
        lEdit_FS_Alt->setReadOnly(true);

        horizontalLayout_11->addWidget(lEdit_FS_Alt);

        label_5 = new QLabel(groupBox);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        horizontalLayout_11->addWidget(label_5);

        lEdit_FS_TarAlt = new QLineEdit(groupBox);
        lEdit_FS_TarAlt->setObjectName(QString::fromUtf8("lEdit_FS_TarAlt"));
        lEdit_FS_TarAlt->setFocusPolicy(Qt::NoFocus);
        lEdit_FS_TarAlt->setAcceptDrops(false);
        lEdit_FS_TarAlt->setReadOnly(true);

        horizontalLayout_11->addWidget(lEdit_FS_TarAlt);

        groupBox_2 = new QGroupBox(tab_Sensors);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setGeometry(QRect(40, 30, 152, 42));
        groupBox_2->setFont(font1);
        horizontalLayout_12 = new QHBoxLayout(groupBox_2);
        horizontalLayout_12->setSpacing(0);
        horizontalLayout_12->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_12->setObjectName(QString::fromUtf8("horizontalLayout_12"));
        lEdit_FS_Long = new QLineEdit(groupBox_2);
        lEdit_FS_Long->setObjectName(QString::fromUtf8("lEdit_FS_Long"));
        lEdit_FS_Long->setFocusPolicy(Qt::NoFocus);
        lEdit_FS_Long->setAcceptDrops(false);
        lEdit_FS_Long->setReadOnly(true);

        horizontalLayout_12->addWidget(lEdit_FS_Long);

        label_6 = new QLabel(groupBox_2);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        horizontalLayout_12->addWidget(label_6);

        lEdit_FS_TarLong = new QLineEdit(groupBox_2);
        lEdit_FS_TarLong->setObjectName(QString::fromUtf8("lEdit_FS_TarLong"));
        lEdit_FS_TarLong->setFocusPolicy(Qt::NoFocus);
        lEdit_FS_TarLong->setAcceptDrops(false);
        lEdit_FS_TarLong->setReadOnly(true);

        horizontalLayout_12->addWidget(lEdit_FS_TarLong);

        groupBox_3 = new QGroupBox(tab_Sensors);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        groupBox_3->setGeometry(QRect(40, 80, 152, 42));
        groupBox_3->setFont(font1);
        horizontalLayout_13 = new QHBoxLayout(groupBox_3);
        horizontalLayout_13->setSpacing(0);
        horizontalLayout_13->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_13->setObjectName(QString::fromUtf8("horizontalLayout_13"));
        lEdit_FS_Lat = new QLineEdit(groupBox_3);
        lEdit_FS_Lat->setObjectName(QString::fromUtf8("lEdit_FS_Lat"));
        lEdit_FS_Lat->setFocusPolicy(Qt::NoFocus);
        lEdit_FS_Lat->setAcceptDrops(false);
        lEdit_FS_Lat->setReadOnly(true);

        horizontalLayout_13->addWidget(lEdit_FS_Lat);

        label_7 = new QLabel(groupBox_3);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        horizontalLayout_13->addWidget(label_7);

        lEdit_FS_TarLat = new QLineEdit(groupBox_3);
        lEdit_FS_TarLat->setObjectName(QString::fromUtf8("lEdit_FS_TarLat"));
        lEdit_FS_TarLat->setFocusPolicy(Qt::NoFocus);
        lEdit_FS_TarLat->setAcceptDrops(false);
        lEdit_FS_TarLat->setReadOnly(true);

        horizontalLayout_13->addWidget(lEdit_FS_TarLat);

        groupBox_4 = new QGroupBox(tab_Sensors);
        groupBox_4->setObjectName(QString::fromUtf8("groupBox_4"));
        groupBox_4->setGeometry(QRect(40, 190, 152, 42));
        groupBox_4->setFont(font1);
        horizontalLayout_14 = new QHBoxLayout(groupBox_4);
        horizontalLayout_14->setSpacing(0);
        horizontalLayout_14->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_14->setObjectName(QString::fromUtf8("horizontalLayout_14"));
        lEdit_FS_Spd = new QLineEdit(groupBox_4);
        lEdit_FS_Spd->setObjectName(QString::fromUtf8("lEdit_FS_Spd"));
        lEdit_FS_Spd->setFocusPolicy(Qt::NoFocus);
        lEdit_FS_Spd->setAcceptDrops(false);
        lEdit_FS_Spd->setReadOnly(true);

        horizontalLayout_14->addWidget(lEdit_FS_Spd);

        lEdit_FS_DirText = new QLineEdit(tab_Sensors);
        lEdit_FS_DirText->setObjectName(QString::fromUtf8("lEdit_FS_DirText"));
        lEdit_FS_DirText->setGeometry(QRect(60, 250, 40, 27));
        lEdit_FS_DirText->setMinimumSize(QSize(40, 0));
        lEdit_FS_DirText->setMaximumSize(QSize(40, 16777215));
        lEdit_FS_DirText->setFocusPolicy(Qt::NoFocus);
        lEdit_FS_DirText->setAcceptDrops(false);
        lEdit_FS_DirText->setReadOnly(true);
        lEdit_Main_AvrExec = new QLineEdit(tab_Sensors);
        lEdit_Main_AvrExec->setObjectName(QString::fromUtf8("lEdit_Main_AvrExec"));
        lEdit_Main_AvrExec->setEnabled(false);
        lEdit_Main_AvrExec->setGeometry(QRect(30, 300, 150, 24));
        lEdit_Main_AvrExec->setMinimumSize(QSize(150, 24));
        lEdit_Main_AvrExec->setMaximumSize(QSize(150, 24));
        lEdit_Main_AvrExec->setMouseTracking(false);
        lEdit_Main_AvrExec->setFocusPolicy(Qt::NoFocus);
        lEdit_Main_AvrExec->setAcceptDrops(false);
        lEdit_Main_AvrExec->setReadOnly(true);
        tabWidget->addTab(tab_Sensors, QString());

        horizontalLayout->addWidget(tabWidget);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1003, 25));
        menuBar->setAcceptDrops(false);
        menuBar->setDefaultUp(true);
        MainWindow->setMenuBar(menuBar);

        retranslateUi(MainWindow);

        tabWidget->setCurrentIndex(4);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MicroCART Ground", 0));
        menuDefault->setText(QApplication::translate("MainWindow", "Default", 0));
        menuGAUIQuad->setText(QApplication::translate("MainWindow", "GAUI Quad", 0));
        menuBig_Heli->setText(QApplication::translate("MainWindow", "Big Heli", 0));
        pButton_Main_Start->setText(QApplication::translate("MainWindow", "START", 0));
        pButton_Main_Stop->setText(QApplication::translate("MainWindow", "STOP", 0));
        pButton_Main_Stop->setShortcut(QString());
        pButton_Take_Off->setText(QApplication::translate("MainWindow", "Take Off", 0));
        pButton_Land->setText(QApplication::translate("MainWindow", "Land", 0));
        label_FS_Pit->setText(QApplication::translate("MainWindow", "Pitch", 0));
        label_FS_Roll->setText(QApplication::translate("MainWindow", "Roll", 0));
        label_FS_Dir_N->setText(QApplication::translate("MainWindow", "N", 0));
        label_FS_Dir_W->setText(QApplication::translate("MainWindow", "W", 0));
        label_FS_E->setText(QApplication::translate("MainWindow", "E", 0));
        label_FS_Dir_S->setText(QApplication::translate("MainWindow", "S", 0));
        groupBox_10->setTitle(QApplication::translate("MainWindow", "X (longitude)", 0));
        Sensor_Current_X->setText(QString());
        label_15->setText(QApplication::translate("MainWindow", "->", 0));
        Sensor_Target_X->setText(QString());
        groupBox_11->setTitle(QApplication::translate("MainWindow", "Y (Latitude)", 0));
        Sensor_Current_Y->setText(QString());
        label_16->setText(QApplication::translate("MainWindow", "->", 0));
        Sensor_Target_Y->setText(QString());
        groupBox_9->setTitle(QApplication::translate("MainWindow", "Z (Altitude)", 0));
        label_14->setText(QApplication::translate("MainWindow", "->", 0));
        Sensor_Target_Z->setText(QString());
        groupBox_6->setTitle(QApplication::translate("MainWindow", "Behavior", 0));
        groupBox_7->setTitle(QApplication::translate("MainWindow", "Modules", 0));
        label_Mod_Ori->setText(QApplication::translate("MainWindow", "Orientation", 0));
        label_Mod_Dir->setText(QApplication::translate("MainWindow", "Direction", 0));
        label_Mod_Pos->setText(QApplication::translate("MainWindow", "Position", 0));
        label_Mod_Comm->setText(QApplication::translate("MainWindow", "Communication", 0));
        label_Mod_Control->setText(QApplication::translate("MainWindow", "Manual", 0));
        groupBox_8->setTitle(QApplication::translate("MainWindow", "Controller Selection", 0));
        label_8->setText(QApplication::translate("MainWindow", "Throttle", 0));
        rButton_Sel_Thr_Man->setText(QApplication::translate("MainWindow", "Manual", 0));
        rButton_Sel_Thr_Auto->setText(QApplication::translate("MainWindow", "Auto", 0));
        label_9->setText(QApplication::translate("MainWindow", "Pitch", 0));
        label_11->setText(QApplication::translate("MainWindow", "Roll", 0));
        label_12->setText(QApplication::translate("MainWindow", "Yaw", 0));
        rButton_Sel_Pit_Man->setText(QApplication::translate("MainWindow", "Manual", 0));
        rButton_Sel_Pit_Auto->setText(QApplication::translate("MainWindow", "Auto", 0));
        rButton_Sel_Roll_Man->setText(QApplication::translate("MainWindow", "Manual", 0));
        rButton_Sel_Roll_Auto->setText(QApplication::translate("MainWindow", "Auto", 0));
        rButton_Sel_Yaw_Man->setText(QApplication::translate("MainWindow", "Manual", 0));
        rButton_Sel_Yaw_Auto->setText(QApplication::translate("MainWindow", "Auto", 0));
        groupBox_5->setTitle(QApplication::translate("MainWindow", "Grid View", 0));
        label_3->setText(QApplication::translate("MainWindow", "Max Width", 0));
        Settings_Grid_X->setSuffix(QApplication::translate("MainWindow", " m", 0));
        label_4->setText(QApplication::translate("MainWindow", "Max Height", 0));
        Settings_Grid_Y->setSuffix(QApplication::translate("MainWindow", " m", 0));
        Settings_Grid_Z->setSuffix(QApplication::translate("MainWindow", " m", 0));
        label_10->setText(QApplication::translate("MainWindow", "Max Altitude", 0));
        label_13->setText(QApplication::translate("MainWindow", "Queue Interval", 0));
        Settings_Grid_Queue_Interval->setSuffix(QApplication::translate("MainWindow", " ms", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_Settings), QApplication::translate("MainWindow", "Settings", 0));
        Grid_Toolbar_Save->setText(QApplication::translate("MainWindow", "Save", 0));
        Grid_Toolbar_Load->setText(QApplication::translate("MainWindow", "Load", 0));
        Grid_Toolbar_Send->setText(QApplication::translate("MainWindow", "Send", 0));
        Grid_Toolbar_Reset->setText(QApplication::translate("MainWindow", "Reset", 0));
        Grid_Toolbar_Clear->setText(QApplication::translate("MainWindow", "Clear", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_Grid), QApplication::translate("MainWindow", "Grid", 0));
        pButton_IO_ImportFile->setText(QApplication::translate("MainWindow", "Import File", 0));
        pButton_IO_CancelCmds->setText(QApplication::translate("MainWindow", "Cancel", 0));
        pButton_IO_Send->setText(QApplication::translate("MainWindow", "Send", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_Console), QApplication::translate("MainWindow", "Console", 0));
        cBox_Mod_TrackOmni->setText(QApplication::translate("MainWindow", "Track Omni-bot", 0));
        cBox_Mod_FollowOmni->setText(QApplication::translate("MainWindow", "Follow Omni-bot", 0));
        label_FS_Pit_2->setText(QApplication::translate("MainWindow", "Pitch", 0));
        label_FS_Roll_2->setText(QApplication::translate("MainWindow", "Roll", 0));
        label_FS_Dir_N_2->setText(QApplication::translate("MainWindow", "N", 0));
        label_FS_Dir_W_2->setText(QApplication::translate("MainWindow", "W", 0));
        label_FS_E_2->setText(QApplication::translate("MainWindow", "E", 0));
        label_FS_Dir_S_2->setText(QApplication::translate("MainWindow", "S", 0));
        groupBox_12->setTitle(QApplication::translate("MainWindow", "X (longitude)", 0));
        lEdit_FS_Long_2->setText(QString());
        label_17->setText(QApplication::translate("MainWindow", "->", 0));
        lEdit_FS_TarLong_2->setText(QString());
        groupBox_13->setTitle(QApplication::translate("MainWindow", "Y (Latitude)", 0));
        lEdit_FS_Lat_2->setText(QString());
        label_18->setText(QApplication::translate("MainWindow", "->", 0));
        lEdit_FS_TarLat_2->setText(QString());
        groupBox_14->setTitle(QApplication::translate("MainWindow", "Z (Altitude)", 0));
        label_19->setText(QApplication::translate("MainWindow", "->", 0));
        lEdit_FS_TarAlt_2->setText(QString());
        check_FS_Abso->setText(QApplication::translate("MainWindow", "Absolute", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_Omnibot), QApplication::translate("MainWindow", "Omni-bot", 0));
        groupBox->setTitle(QApplication::translate("MainWindow", "Altitude", 0));
        label_5->setText(QApplication::translate("MainWindow", "->", 0));
        groupBox_2->setTitle(QApplication::translate("MainWindow", "Longitude", 0));
        label_6->setText(QApplication::translate("MainWindow", "->", 0));
        groupBox_3->setTitle(QApplication::translate("MainWindow", "Latitude", 0));
        label_7->setText(QApplication::translate("MainWindow", "->", 0));
        groupBox_4->setTitle(QApplication::translate("MainWindow", "Speed", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_Sensors), QApplication::translate("MainWindow", "Sensors", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
