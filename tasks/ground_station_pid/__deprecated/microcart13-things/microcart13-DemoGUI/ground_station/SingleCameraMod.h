#ifndef SINGLECAMERA_H
#define SINGLECAMERA_H

#include <signal.h>
#include <sys/types.h>

#include "Module.h"
#include "MCTStructures.h"

/** @deprecated This module has been replaced with the optitrack module.
  * @brief Module tracking the quadcopter using a single USB camera.
  */
class SingleCamera : public Module
{
private:
    int cam_pID;

public:
    SingleCamera();
    ~SingleCamera();
    bool isDirMod()  {return true;}
    bool isPosMod()  {return true;}
    bool isOriMod()  {return true;}
    bool start();
    bool update();
    bool stop();
};

#endif // SINGLECAMERA_H
