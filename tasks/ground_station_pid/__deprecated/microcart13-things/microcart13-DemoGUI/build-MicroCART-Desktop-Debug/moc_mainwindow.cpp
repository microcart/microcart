/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.4.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../ground_station/mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.4.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[46];
    char stringdata[1210];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 35), // "on_cBox_Mod_Ori_currentIndexC..."
QT_MOC_LITERAL(2, 47, 0), // ""
QT_MOC_LITERAL(3, 48, 4), // "arg1"
QT_MOC_LITERAL(4, 53, 35), // "on_cBox_Mod_Pos_currentIndexC..."
QT_MOC_LITERAL(5, 89, 35), // "on_cBox_Mod_Dir_currentIndexC..."
QT_MOC_LITERAL(6, 125, 36), // "on_cBox_Mod_Comm_currentIndex..."
QT_MOC_LITERAL(7, 162, 36), // "on_cBox_Mod_Cont_currentIndex..."
QT_MOC_LITERAL(8, 199, 32), // "on_cBox_Bhvr_currentIndexChanged"
QT_MOC_LITERAL(9, 232, 37), // "on_cBox_BhvrModel_currentInde..."
QT_MOC_LITERAL(10, 270, 32), // "on_pButton_IO_CancelCmds_clicked"
QT_MOC_LITERAL(11, 303, 32), // "on_pButton_IO_ImportFile_clicked"
QT_MOC_LITERAL(12, 336, 26), // "on_pButton_IO_Send_clicked"
QT_MOC_LITERAL(13, 363, 29), // "on_pButton_Main_Start_clicked"
QT_MOC_LITERAL(14, 393, 28), // "on_pButton_Main_Stop_clicked"
QT_MOC_LITERAL(15, 422, 27), // "on_pButton_Take_Off_clicked"
QT_MOC_LITERAL(16, 450, 23), // "on_pButton_Land_clicked"
QT_MOC_LITERAL(17, 474, 10), // "throttleUp"
QT_MOC_LITERAL(18, 485, 12), // "throttleDown"
QT_MOC_LITERAL(19, 498, 31), // "on_rButton_Sel_Thr_Auto_clicked"
QT_MOC_LITERAL(20, 530, 30), // "on_rButton_Sel_Thr_Man_clicked"
QT_MOC_LITERAL(21, 561, 31), // "on_rButton_Sel_Pit_Auto_clicked"
QT_MOC_LITERAL(22, 593, 30), // "on_rButton_Sel_Pit_Man_clicked"
QT_MOC_LITERAL(23, 624, 32), // "on_rButton_Sel_Roll_Auto_clicked"
QT_MOC_LITERAL(24, 657, 31), // "on_rButton_Sel_Roll_Man_clicked"
QT_MOC_LITERAL(25, 689, 31), // "on_rButton_Sel_Yaw_Auto_clicked"
QT_MOC_LITERAL(26, 721, 30), // "on_rButton_Sel_Yaw_Man_clicked"
QT_MOC_LITERAL(27, 752, 9), // "gridSetup"
QT_MOC_LITERAL(28, 762, 34), // "on_Settings_Grid_X_editingFin..."
QT_MOC_LITERAL(29, 797, 34), // "on_Settings_Grid_Y_editingFin..."
QT_MOC_LITERAL(30, 832, 34), // "on_Settings_Grid_Z_editingFin..."
QT_MOC_LITERAL(31, 867, 47), // "on_Settings_Grid_Queue_Interv..."
QT_MOC_LITERAL(32, 915, 11), // "sendCommand"
QT_MOC_LITERAL(33, 927, 29), // "on_Grid_Toolbar_Clear_clicked"
QT_MOC_LITERAL(34, 957, 28), // "on_Grid_Toolbar_Save_clicked"
QT_MOC_LITERAL(35, 986, 29), // "on_cBox_Mod_TrackOmni_clicked"
QT_MOC_LITERAL(36, 1016, 30), // "on_cBox_Mod_FollowOmni_clicked"
QT_MOC_LITERAL(37, 1047, 15), // "setOmniToTarget"
QT_MOC_LITERAL(38, 1063, 28), // "on_Grid_Toolbar_Load_clicked"
QT_MOC_LITERAL(39, 1092, 28), // "on_Grid_Toolbar_Send_clicked"
QT_MOC_LITERAL(40, 1121, 29), // "on_Grid_Toolbar_Reset_clicked"
QT_MOC_LITERAL(41, 1151, 10), // "updateGrid"
QT_MOC_LITERAL(42, 1162, 16), // "updateController"
QT_MOC_LITERAL(43, 1179, 13), // "keyPressEvent"
QT_MOC_LITERAL(44, 1193, 10), // "QKeyEvent*"
QT_MOC_LITERAL(45, 1204, 5) // "event"

    },
    "MainWindow\0on_cBox_Mod_Ori_currentIndexChanged\0"
    "\0arg1\0on_cBox_Mod_Pos_currentIndexChanged\0"
    "on_cBox_Mod_Dir_currentIndexChanged\0"
    "on_cBox_Mod_Comm_currentIndexChanged\0"
    "on_cBox_Mod_Cont_currentIndexChanged\0"
    "on_cBox_Bhvr_currentIndexChanged\0"
    "on_cBox_BhvrModel_currentIndexChanged\0"
    "on_pButton_IO_CancelCmds_clicked\0"
    "on_pButton_IO_ImportFile_clicked\0"
    "on_pButton_IO_Send_clicked\0"
    "on_pButton_Main_Start_clicked\0"
    "on_pButton_Main_Stop_clicked\0"
    "on_pButton_Take_Off_clicked\0"
    "on_pButton_Land_clicked\0throttleUp\0"
    "throttleDown\0on_rButton_Sel_Thr_Auto_clicked\0"
    "on_rButton_Sel_Thr_Man_clicked\0"
    "on_rButton_Sel_Pit_Auto_clicked\0"
    "on_rButton_Sel_Pit_Man_clicked\0"
    "on_rButton_Sel_Roll_Auto_clicked\0"
    "on_rButton_Sel_Roll_Man_clicked\0"
    "on_rButton_Sel_Yaw_Auto_clicked\0"
    "on_rButton_Sel_Yaw_Man_clicked\0gridSetup\0"
    "on_Settings_Grid_X_editingFinished\0"
    "on_Settings_Grid_Y_editingFinished\0"
    "on_Settings_Grid_Z_editingFinished\0"
    "on_Settings_Grid_Queue_Interval_editingFinished\0"
    "sendCommand\0on_Grid_Toolbar_Clear_clicked\0"
    "on_Grid_Toolbar_Save_clicked\0"
    "on_cBox_Mod_TrackOmni_clicked\0"
    "on_cBox_Mod_FollowOmni_clicked\0"
    "setOmniToTarget\0on_Grid_Toolbar_Load_clicked\0"
    "on_Grid_Toolbar_Send_clicked\0"
    "on_Grid_Toolbar_Reset_clicked\0updateGrid\0"
    "updateController\0keyPressEvent\0"
    "QKeyEvent*\0event"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      41,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,  219,    2, 0x08 /* Private */,
       4,    1,  222,    2, 0x08 /* Private */,
       5,    1,  225,    2, 0x08 /* Private */,
       6,    1,  228,    2, 0x08 /* Private */,
       7,    1,  231,    2, 0x08 /* Private */,
       8,    1,  234,    2, 0x08 /* Private */,
       9,    1,  237,    2, 0x08 /* Private */,
      10,    0,  240,    2, 0x08 /* Private */,
      11,    0,  241,    2, 0x08 /* Private */,
      12,    0,  242,    2, 0x08 /* Private */,
      13,    0,  243,    2, 0x08 /* Private */,
      14,    0,  244,    2, 0x08 /* Private */,
      15,    0,  245,    2, 0x08 /* Private */,
      16,    0,  246,    2, 0x08 /* Private */,
      17,    0,  247,    2, 0x08 /* Private */,
      18,    0,  248,    2, 0x08 /* Private */,
      19,    0,  249,    2, 0x08 /* Private */,
      20,    0,  250,    2, 0x08 /* Private */,
      21,    0,  251,    2, 0x08 /* Private */,
      22,    0,  252,    2, 0x08 /* Private */,
      23,    0,  253,    2, 0x08 /* Private */,
      24,    0,  254,    2, 0x08 /* Private */,
      25,    0,  255,    2, 0x08 /* Private */,
      26,    0,  256,    2, 0x08 /* Private */,
      27,    0,  257,    2, 0x08 /* Private */,
      28,    0,  258,    2, 0x08 /* Private */,
      29,    0,  259,    2, 0x08 /* Private */,
      30,    0,  260,    2, 0x08 /* Private */,
      31,    0,  261,    2, 0x08 /* Private */,
      32,    0,  262,    2, 0x08 /* Private */,
      33,    0,  263,    2, 0x08 /* Private */,
      34,    0,  264,    2, 0x08 /* Private */,
      35,    0,  265,    2, 0x08 /* Private */,
      36,    0,  266,    2, 0x08 /* Private */,
      37,    0,  267,    2, 0x08 /* Private */,
      38,    0,  268,    2, 0x08 /* Private */,
      39,    0,  269,    2, 0x08 /* Private */,
      40,    0,  270,    2, 0x08 /* Private */,
      41,    0,  271,    2, 0x08 /* Private */,
      42,    0,  272,    2, 0x08 /* Private */,
      43,    1,  273,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 44,   45,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainWindow *_t = static_cast<MainWindow *>(_o);
        switch (_id) {
        case 0: _t->on_cBox_Mod_Ori_currentIndexChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 1: _t->on_cBox_Mod_Pos_currentIndexChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 2: _t->on_cBox_Mod_Dir_currentIndexChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 3: _t->on_cBox_Mod_Comm_currentIndexChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 4: _t->on_cBox_Mod_Cont_currentIndexChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 5: _t->on_cBox_Bhvr_currentIndexChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 6: _t->on_cBox_BhvrModel_currentIndexChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 7: _t->on_pButton_IO_CancelCmds_clicked(); break;
        case 8: _t->on_pButton_IO_ImportFile_clicked(); break;
        case 9: _t->on_pButton_IO_Send_clicked(); break;
        case 10: _t->on_pButton_Main_Start_clicked(); break;
        case 11: _t->on_pButton_Main_Stop_clicked(); break;
        case 12: _t->on_pButton_Take_Off_clicked(); break;
        case 13: _t->on_pButton_Land_clicked(); break;
        case 14: _t->throttleUp(); break;
        case 15: _t->throttleDown(); break;
        case 16: _t->on_rButton_Sel_Thr_Auto_clicked(); break;
        case 17: _t->on_rButton_Sel_Thr_Man_clicked(); break;
        case 18: _t->on_rButton_Sel_Pit_Auto_clicked(); break;
        case 19: _t->on_rButton_Sel_Pit_Man_clicked(); break;
        case 20: _t->on_rButton_Sel_Roll_Auto_clicked(); break;
        case 21: _t->on_rButton_Sel_Roll_Man_clicked(); break;
        case 22: _t->on_rButton_Sel_Yaw_Auto_clicked(); break;
        case 23: _t->on_rButton_Sel_Yaw_Man_clicked(); break;
        case 24: _t->gridSetup(); break;
        case 25: _t->on_Settings_Grid_X_editingFinished(); break;
        case 26: _t->on_Settings_Grid_Y_editingFinished(); break;
        case 27: _t->on_Settings_Grid_Z_editingFinished(); break;
        case 28: _t->on_Settings_Grid_Queue_Interval_editingFinished(); break;
        case 29: _t->sendCommand(); break;
        case 30: _t->on_Grid_Toolbar_Clear_clicked(); break;
        case 31: _t->on_Grid_Toolbar_Save_clicked(); break;
        case 32: _t->on_cBox_Mod_TrackOmni_clicked(); break;
        case 33: _t->on_cBox_Mod_FollowOmni_clicked(); break;
        case 34: _t->setOmniToTarget(); break;
        case 35: _t->on_Grid_Toolbar_Load_clicked(); break;
        case 36: _t->on_Grid_Toolbar_Send_clicked(); break;
        case 37: _t->on_Grid_Toolbar_Reset_clicked(); break;
        case 38: _t->updateGrid(); break;
        case 39: _t->updateController(); break;
        case 40: _t->keyPressEvent((*reinterpret_cast< QKeyEvent*(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow.data,
      qt_meta_data_MainWindow,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata))
        return static_cast<void*>(const_cast< MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 41)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 41;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 41)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 41;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
