#ifndef RFUTILS_H
#define RFUTILS_H

#define NUM_CHANNELS 4

void rfInit(const char*);

void sendRFData(int* data);

#endif
