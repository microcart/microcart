
#include "rfutils.h"

#include <stdio.h> /* Standard input/output definitions */
#include <string.h> /* String function definitions */
#include <unistd.h> /* UNIX standard function definitions */
#include <fcntl.h> /* File control definitions */
#include <errno.h> /* Error number definitions */
#include <termios.h> /* POSIX terminal control definitions */


int uart_fd;  /* File descriptor for the port */


void sendRFData(int* data) {
	char command[NUM_CHANNELS * 3] = {};
   for (int i=0; i < NUM_CHANNELS; i++) {
   	command[i*3] = 0xC0 | (i + 1); // channel num (0xCN)
  		command[i*3 + 1] = (data[i] >> 8) & 0xFF; // hi byte
  		command[i*3 + 2] = data[i] & 0xFF; // lo byte
   }
   
   write(uart_fd, command, NUM_CHANNELS * 3);
}


void rfInit(const char* portName) {
	// portName is of the form "/dev/ttyS0"
	
  uart_fd = open(portName, O_RDWR | O_NDELAY);
  if (uart_fd  == -1)
  {
      //Could not open the port.
      perror("open_port: Unable to open /dev/ttyS0 - ");
  }
  else
  {
     fcntl(uart_fd , F_SETFL, 0);
  }
	

  struct termios options; // UART port options data structure

  // Get the current options for the port...
  tcgetattr(uart_fd, &options);

  // Set the baud rates to 9600...
  cfsetispeed(&options, B38400);
  cfsetospeed(&options, B38400);

  // Enable the receiver and set local mode...
  options.c_cflag |= (CLOCAL | CREAD);

  // Set charater size
  options.c_cflag &= ~CSIZE; // Mask the character size bits
  options.c_cflag |= CS8;    // Select 8 data bits

  // Set no parity 8N1
  options.c_cflag &= ~PARENB;
  options.c_cflag &= ~CSTOPB;
  options.c_cflag &= ~CSIZE;
  options.c_cflag |= CS8;

  // Disable Hardware flow control
  options.c_cflag &= ~CRTSCTS;

  // Use raw input
  options.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);

  // Disable SW flow control
  options.c_iflag &= ~(IXON | IXOFF | IXANY);

  // Use raw output
  options.c_oflag &= ~OPOST;

  // Set new options for the port...
  tcsetattr(uart_fd , TCSANOW, &options);
}

