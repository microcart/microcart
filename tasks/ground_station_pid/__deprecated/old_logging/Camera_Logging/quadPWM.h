#ifndef QUAD_PWM
#define QUAD_PWM

#include <cmath>


#include <unistd.h>
#include <quat.h>
#include <time.h>

#include <sys/types.h>
#include <sys/stat.h>

#include "structs.h"
#include "PID.h"
#include "quadcontrol.h"
#include "vrpn.h"
#include "rfutils.h"

void init();

void VRPN_CALLBACK handle_pos(void*, const vrpn_TRACKERCB t);

bool logStart();

bool logStop();

void logStandard();

void printTerminal();


#endif
