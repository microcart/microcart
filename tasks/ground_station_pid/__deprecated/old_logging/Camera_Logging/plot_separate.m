function plot_separate(expData, varargin)
%PLOT_SEPARATE This function plots separate plots for the data-headers
%passed to the function
%   expData     - structure that contains all the data
%   varargin    - the headers of the data to be plotted
%
%   Example of varargin: 'Pitch','r-','Roll','Yaw','go' 
%   This means that Pitch will be plotted in red solids, Roll with default 
%   formatting and Yaw in green circles.

time = expData.Time; % extracting time structure

% bulding the plot statement that will be executed
for i = 1:length(varargin)
    
    dataHeader = varargin{i};
    
    % continue to next iteration if current argument is a plotting character string
    if(isPlotCharString(dataHeader))
        continue;
    end
    
    % adding the header to the plot statment to be executed
    plotString = strcat('plot(time.data,expData.',dataHeader,'.data');
    
    % adding plot formatting string, if it exists
    if(i ~= length(varargin))
        if(isPlotCharString(varargin{i+1}))
            plotString = strcat(plotString,',''',varargin{i+1},''');');
        else
            % use the plotting parameters set for this header
            plotCharString = buildPlotCharString(eval(['expData.' dataHeader '.params']));
            plotString = strcat(plotString,',''', plotCharString, ''');');
        end
    else
        % use the plotting parameters set for this header
        plotCharString = buildPlotCharString(eval(['expData.' dataHeader '.params']));
        plotString = strcat(plotString,',''', plotCharString, ''');');
    end
    
    % plotting data and making it look good
    figure;
    eval(plotString);
    title(dataHeader);
    xlabel(['Time (' time.unit ')']);
    if(eval(['isempty(expData.' dataHeader '.unit)']))
        yAxisLabel = dataHeader;
    else
        yAxisLabel = [dataHeader ' (' eval(['expData.' dataHeader '.unit']) ')'];
    end
    ylabel(yAxisLabel);
    xlim([0,time.data(length(time.data))]);
    grid ON;
    
end

end

