clc
clear


%fileName = input('Enter the file path of the screen log\n', 's');
fileID = fopen('screenlog.0');


%Begin function attempt

%function log_open(log)

%fileID = fopen(log)

%End function attempt

if fileID == -1
    'Error opening the file'
    return;
end
A = fscanf(fileID, '%f');


%A is now a vector of all the information
% The order is Timestamp, Roll, Pitch, Yaw

% This gets rid of entries that aren't complete
A = A(1:floor(length(A)/4)*4);

start_time  = A(1);
start_roll  = A(2);
start_pitch = A(3);
start_yaw   = A(4);

time  = A(1:4:end) - start_time;
roll  = A(2:4:end) - start_roll;
pitch = A(3:4:end) - start_pitch;
yaw   = A(4:4:end) - start_yaw;


d_roll  = diff(roll);
d_pitch = diff(pitch);
d_yaw   = diff(yaw);

figure(2)
%pretty graphs and things
subplot(3,1,1);
plot(time, smooth(roll));
hold on
plot(time(1:end-1), smooth(d_roll * 5), 'r', 'LineSmoothing','on');
title('Roll', 'FontWeight', 'bold');
legend('y = roll', 'y = derivative');
ylim([-90 90]);
xlabel('Seconds');


subplot(3,1,2);
plot(time, smooth(pitch));
hold on
plot(time(1:end-1), smooth(d_pitch * 5), 'r', 'LineSmoothing','on');
title('Pitch', 'FontWeight', 'bold');
legend('y = pitch', 'y = derivative');
ylim([-90 90]);
xlabel('Seconds');

subplot(3,1,3);
plot(time, smooth(yaw));
hold on
plot(time(1:end-1), smooth(d_yaw * 5), 'r', 'LineSmoothing','on');
title('Yaw', 'FontWeight', 'bold');
legend('y = yaw', 'y = derivative');
ylim([-90 90]);
xlabel('Seconds');

%Some magic I found on stackexchange, it maximizes the window
pause(0.00001);
frame_h = get(handle(gcf),'JavaFrame');
set(frame_h,'Maximized',1);
