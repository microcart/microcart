figure;
minval = 100000;
maxval = 200000;
l = length(Rolls(Rolls>minval&Rolls<maxval));
h = plot(1:l,Rolls(Rolls>minval&Rolls<maxval), 'r-');
movegui(h, 'northwest');
title('Rolls');
axis([0 l minval maxval]);

figure;
minval = 100000;
maxval = 200000;
l = length(Yaws(Yaws>minval&Yaws<maxval));
h = plot(1:l,Yaws(Yaws>minval&Yaws<maxval), 'b-');
movegui(h, 'northeast');
title('Yaws');
axis([0 l minval maxval]);


figure;
minval = 100000;
maxval = 200000;
l=length(Pitches(Pitches>minval&Pitches<maxval));
h = plot(1:l,Pitches(Pitches>minval&Pitches<maxval), 'g-');
movegui(h, 'southeast');
title('Pitches');
axis([0 l minval maxval]);

figure;
minval = 100000;
maxval = 200000;
l=length(Throttles(Throttles>minval&Throttles<maxval));
h = plot(1:l,Throttles(Throttles>minval&Throttles<maxval), 'k-');
movegui(h, 'southwest');
title('Throttles');
axis([0 l minval maxval]);
