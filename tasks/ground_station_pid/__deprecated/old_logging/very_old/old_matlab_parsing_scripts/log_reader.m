%Read in a file and do cool stuff
%Author Tyler K

%Matt V and Ravi N will attempt to turn this into a function, if things break, check that first
%Matt V will add more comments as he goes along.  He finds the lack of comments...disturbing.

clc
clear
close all
frequency = 100;
dt = 1/frequency;


fileName = input('Enter the file path of the log\n', 's');
fileID = fopen(fileName);


%Begin function attempt

%function log_open(log)

%fileID = fopen(log)

%End function attempt

if fileID == -1
    'Error opening the file'
    return;
end
A = fscanf(fileID, '%f');


%A is now a vector of all the information
% The order is Roll Pitch Yaw X Y Z Time

% This gets rid of entries that aren't complete
% AKA, if there is a row of information that contains Roll Pitch Yaw 
% and nothing else, it filters it out
A = A(1:floor(length(A)/7)*7);

roll_start  = A(1);
pitch_start = A(2);
yaw_start   = A(3);
x_start     = A(4);
y_start     = A(5);
z_start     = A(6);
time_start  = A(7);

roll  = A(1:7:end) - roll_start;
pitch = A(2:7:end) - pitch_start;
yaw   = A(3:7:end) - yaw_start;
x     = A(4:7:end) - x_start;
y     = A(5:7:end) - y_start;
z     = A(6:7:end) - z_start;
time  = A(7:7:end) - time_start;

d_roll  = diff(roll);
d_pitch = diff(pitch);
d_yaw   = diff(yaw);
d_x     = diff(x);
d_y     = diff(y);
d_z     = diff(z);

d2_roll  = diff(d_roll);
d2_pitch = diff(d_pitch);
d2_yaw   = diff(d_yaw);
d2_x     = diff(d_x);
d2_y     = diff(d_y);
d2_z     = diff(d_z);



%ALRIGHT time for cool pretty graphs
t = dt:dt:length(x)*dt;
d_t = t(1:end-1);
subplot(3,2,1);
plot(t, smooth(roll));
hold on
plot(d_t, smooth(d_roll * 5), 'r', 'LineSmoothing','on');
title('Roll', 'FontWeight', 'bold');
legend('y = roll', 'y = derivative');
ylim([-3.14 4]);
xlabel('Seconds');

subplot(3,2,2);
plot(t, smooth(pitch));
hold on
plot(d_t, smooth(d_pitch * 5), 'r', 'LineSmoothing','on');
title('Pitch', 'FontWeight', 'bold');
legend('y = pitch', 'y = derivative');
ylim([-3.14 4]);
xlabel('Seconds');

subplot(3,2,3);
plot(t, smooth(yaw));
hold on
plot(d_t, smooth(d_yaw * 5), 'r', 'LineSmoothing','on');
title('Yaw', 'FontWeight', 'bold');
legend('y = yaw', 'y = derivative');
ylim([-3.14 4]);
xlabel('Seconds');


subplot(3,2,4);
plot(t, smooth(x));
hold on
plot(d_t, smooth(d_x * 50, 5), 'r', 'LineSmoothing','on');
title('X', 'FontWeight', 'bold');
legend('y = x', 'y = derivative');
ylim([-2.5 2.5]);
xlabel('Seconds');

subplot(3,2,5);
plot(t, smooth(y));
hold on
plot(d_t, smooth(d_y * 50, 5),  'r', 'LineSmoothing','on');
title('Y', 'FontWeight', 'bold');
legend('y = y', 'y = derivative');
ylim([-2.5 2.5]);
xlabel('Seconds');

subplot(3,2,6);
plot(t, smooth(z));
hold on
plot(d_t, smooth(d_z * 50, 5),  'r', 'LineSmoothing','on');
title('Z', 'FontWeight', 'bold');
legend('y = z', 'y = derivative');
ylim([-2.5 2.5]);
xlabel('Seconds');

%Some magic I found on stackexchange, it maximizes the window
pause(0.00001);
frame_h = get(handle(gcf),'JavaFrame');
set(frame_h,'Maximized',1);

run('screen_reader.m')