import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
import java.lang.Exception;
import java.lang.StringBuilder;


/** Read the screenlog.0 file and output values */
public class ProcessScreenLog {
	public static void main(String[] args) throws FileNotFoundException {
		List<String> throttles = new ArrayList<String>();
		List<String> rolls = new ArrayList<String>();
		List<String> pitches = new ArrayList<String>();
		List<String> yaws = new ArrayList<String>();

		Scanner in = new Scanner(new File("screenlog.0"));
		while (in.hasNextLine()) {
			String line = in.nextLine();
			if (line.matches("Motor [0-9]\\.\\s+[0-9]+\\s+Channel (Throttle|Roll|Pitch|Yaw)\\s*:\\s*[0-9]+")) {
				String[] words = line.split(" ");
				if (line.contains("Throttle")) {
					throttles.add(words[words.length - 1]);
				} else if (line.contains("Roll")) {
					rolls.add(words[words.length - 1]);
				} else if (line.contains("Pitch")) {
					pitches.add(words[words.length - 1]);
				} else if (line.contains("Yaw")) {
					yaws.add(words[words.length - 1]);
				}
			}
		}


		String output = formatOutput("Throttles", throttles);
		System.out.println(output);
		System.out.println();
		
		output = formatOutput("Rolls", rolls);
		System.out.println(output);
		System.out.println();
		
		output = formatOutput("Pitches", pitches);
		System.out.println(output);
		System.out.println();
		
		output = formatOutput("Yaws", yaws);
		System.out.println(output);
		System.out.println();
	}
	
	private static String formatOutput(String title, List<String> list) {
		StringBuilder sb = new StringBuilder();
		sb.append(title + "=[");
		int count = 0;
		for (String x : list) {
			try {
				int i = Integer.parseInt(x);
				sb.append(i + ",");
				
				count++;
			} catch(Exception e) {
				// only add int values
			}
		}		
		if (count > 0) {
			sb.setLength(sb.length() - 1);
		}
		sb.append("];");
		
		
		return sb.toString();
	}

}
