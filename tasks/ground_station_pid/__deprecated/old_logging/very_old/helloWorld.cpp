
#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <unistd.h>
#include "quat.h"
#include "vrpn_Connection.h"
#include "vrpn_Tracker.h"

#include <stdio.h> /* Standard input/output definitions */
#include <string.h> /* String function definitions */
#include <unistd.h> /* UNIX standard function definitions */
#include <fcntl.h> /* File control definitions */
#include <errno.h> /* Error number definitions */
#include <termios.h> /* POSIX terminal control definitions */

int i, n, nChannels = 6;
	int data[6];

// Prototypes
int open_port(void);           		// Open a serial port
void set_uart_options(int fd); 		// Configure serial port
void read_uart(int fd, char *payload, int length); // Read from UART
void VRPN_CALLBACK handle_pos(void*, const vrpn_TRACKERCB t);


// Prototypes
int open_port(void);           		// Open a serial port
void set_uart_options(int fd); 		// Configure serial port
void read_uart(int fd, char *payload, int length); // Read from UART

int uart_fd;
int main()
{
	vrpn_Connection *connection;
	
	char connectionName[50];

	strcpy(connectionName, "192.168.0.120:3883");


	connection = vrpn_get_connection_by_name(connectionName);

	vrpn_Tracker_Remote *tracker = new vrpn_Tracker_Remote("UAV", connection);

	tracker->register_change_handler(0, handle_pos);
	usleep(2000);
	
	
            	// UART file descriptor
int n = 0, i;              	// number of bytes written out of the UART port
//char payload[100];      	// store/send packet payload
int nChannels = 6;
unsigned char command[nChannels * 3];	//command data 


  // Open the UART port
  if((uart_fd = open_port()) == -1)
  {
    fprintf(stderr,"ERROR opening UART port\n");
  }

  // Set UART port options 8N1 (e.g. BAUD rate, parity, flow control)
  set_uart_options(uart_fd);
  
  // 16-bit data for each channel
//  int data[] = {1312, 1500, 3000, 24000, 30000, 15000};
while(1){
		connection->mainloop();
		tracker->mainloop();
		usleep(3000);
	}

}

void VRPN_CALLBACK handle_pos(void*, const vrpn_TRACKERCB t){

	
	q_vec_type euler;
	q_to_euler(euler, t.quat);

//	printf("Tracker Position: (%.4f, %.4f, %.4f) Orientation: (%.2f, %.2f, %.2f, %.2f)\n", t.pos[0], t.pos[1], t.pos[2], t.quat[0], t.quat[1], t.quat[2], t.quat[3]);  
	
	
	/* Prints out x, y, z and YAW PITCH ROLL
		Where yaw 0 is the two closer dots facing south, going up CCW when viewed from above,
		pitch is 0 horizontal, increases when rotating CCW (about the N-S axis) viewed while looking north
		roll is 0 horizontal, increases when rotating CCW (about the E-W axis) viewed while looking east
	*/
	printf("Tracker Position: (%.4f, %.4f, %.4f) Orientation: (%.2f, %.2f, %.2f\n", t.pos[0], t.pos[1], t.pos[2], euler[0], euler[1], euler[2]);  
	
	
//	-.25, +.25
	double pitch = euler[1];
	int value = (int)((pitch + .25) / .5 * 1500);
	value = 1500 - value;
	
	printf("%d\n", value);
	// send this to channel 3
	
	
int start = 00;
for(i=0; i < nChannels;i++) {
	data[i] = start;
}
data[2] = value;
int counter = start;
int channelnum = 0;
char command[18];
 for(i=0; i < nChannels; i++) {
  	command[i*3] = 0xC0 | (i + 1); // channel num (0xCN)
  	command[i*3 + 1] = (data[i] >> 8) & 0xFF; // hi byte
  	command[i*3 + 2] = data[i] & 0xFF; // lo byte
  }
  
   
  // fprintf(stderr, "%d\n", counter);
	//for(i = 0; i < nChannels*3; i++)
	//{
	//	fprintf(stderr,"%u\n",command[i]);
	//}
  
  
  // Write data out of the UART port
    n = write(uart_fd, command, nChannels*3);
//    fprintf(stderr,"Wrote %d of %d bytes\n",n, nCha
	
}





///////////////////////////////////
// Define helper functions start //
///////////////////////////////////


//////////////////////////////////////////////////////////////
// 'open_port()' - Open serial port 1.                      //
//                                                          //
// Returns the file descriptor on success or -1 on error.   //
//                                                          //
//////////////////////////////////////////////////////////////
int open_port(void)
{
int fd; /* File descriptor for the port */
  fd = open("/dev/ttyS0", O_RDWR | O_NDELAY);
  //fd = open("/dev/ttyS0", O_RDWR | O_NOCTTY | O_NDELAY);
  if (fd == -1)
  {
    //Could not open the port.
    perror("open_port: Unable to open /dev/ttyS0 - ");
  }
  else
  {
  	fcntl(fd, F_SETFL, 0);
  }
return(fd);
}


///////////////////////////////////////////////////////////
// Function Name: set_uart_options                       //
//                                                       //
//Description: Sets up various UART port attributes      //
//                                                       //
///////////////////////////////////////////////////////////
void set_uart_options(int fd)
{
struct termios options; // UART port options data structure

  // Get the current options for the port...
  tcgetattr(fd, &options);

  // Set the baud rates to 9600...
  cfsetispeed(&options, B38400);
  cfsetospeed(&options, B38400);

  // Enable the receiver and set local mode...
  options.c_cflag |= (CLOCAL | CREAD);

  // Set charater size
  options.c_cflag &= ~CSIZE; // Mask the character size bits
  options.c_cflag |= CS8;    // Select 8 data bits

  // Set no parity 8N1
  options.c_cflag &= ~PARENB;
  options.c_cflag &= ~CSTOPB;
  options.c_cflag &= ~CSIZE;
  options.c_cflag |= CS8;

  // Disable Hardware flow control
  options.c_cflag &= ~CRTSCTS;

  // Use raw input
  options.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);

  // Disable SW flow control
  options.c_iflag &= ~(IXON | IXOFF | IXANY);

  // Use raw output
  options.c_oflag &= ~OPOST;

  // Set new options for the port...
  tcsetattr(fd, TCSANOW, &options);

}


///////////////////////////////////////////////////////////
// Function Name: read_uart                              //
//                                                       //
//Description: Read "length" bytes from the UART and     //
//             store in the "payload" buffer             //
//                                                       //
///////////////////////////////////////////////////////////

void read_uart(int fd, char *payload, int length)
{
char *buf;  // Pointer into payload buffer
int i;

  buf = payload;

  // Read from UART one byte at a time
  for(i=0; i<length; i++)
  {
    if(read(fd, (void *)buf, 1) == -1)
    {
      fprintf(stderr,"READ ERROR!!!");
    }
    buf = buf++;
  }
}

///////////////////////////////////
// Define helper functions end   //
///////////////////////////////////
