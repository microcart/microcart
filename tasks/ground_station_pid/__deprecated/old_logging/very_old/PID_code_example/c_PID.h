#ifndef C_PID_H
#define C_PID_H

// Euler PIDS
#define P_CONT 650
#define I_CONT 0
#define D_CONT 120


#define testp_CONT .025
#define testi_CONT 0
#define testd_CONT 0
#define THROTTLE_CONT 3


#define SAMPLE_RATE 90.0 // Hertz
#define SAMPLE_PERIOD  ((1.0) / (SAMPLE_RATE))
#define c_PI 3.1415926535897932384626433832795028841971693993751058209749445923078164062

double euler_PID(double, double);
double euler_PD(double, double);
double throttle_PID(double, double);

double proportional(double, double);
double integral(double, double);
double derivative(double, double);


#endif
