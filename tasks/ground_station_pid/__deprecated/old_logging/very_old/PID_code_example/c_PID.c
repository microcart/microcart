/**
  PID implementation for the quadcopter roll, pitch, yaw, and elevation
*/

#include "c_PID.h"
#include <stdio.h>

/**
	Returns a PID coefficient based on the current position and the desired position.
	This is for roll/pitch/yaw as they are all within the same range of -pi : +pi
*/
double euler_PID(double current, double desired){
	double p_comp, d_comp;
	static double i_comp     = 0;
	static double error_last = 0;
	
	double error = desired - current;
	// Ensure the error is between -pi : +pi
	if(error < -c_PI) error += 2 * c_PI;
	if(error >  c_PI) error -= 2 * c_PI;
	
	p_comp  = P_CONT * error;
	i_comp += I_CONT * error;
	d_comp  = D_CONT * (error - error_last) / SAMPLE_PERIOD;
	
	error_last = error;
//	printf("\t%8.2f %8.2f %8.2f\n", p_comp, i_comp, d_comp);
	return p_comp + i_comp + d_comp;
}

double euler_PD(double current, double desired){
	double p_comp, d_comp;
	static double error_last = 0;
	
	double error = desired - current;
	// Ensure the error is between -pi : +pi
	if(error < -c_PI) error += 2 * c_PI;
	if(error >  c_PI) error -= 2 * c_PI;
	
	p_comp  = P_CONT * error;
	d_comp  = D_CONT * (error - error_last) / SAMPLE_PERIOD;
	
	error_last = error;
	return p_comp + d_comp;
}

double throttle_PID(double current, double desired){
	double p_comp, d_comp;
	static double i_comp     = 0;
	static double error_last = 0;
	
	double error = desired - current;
	
	p_comp  = THROTTLE_CONT * P_CONT * error;
	i_comp += THROTTLE_CONT * I_CONT * error;
	d_comp  = THROTTLE_CONT * D_CONT * (error - error_last) / SAMPLE_PERIOD;
	
	error_last = error;
	return p_comp + i_comp + d_comp;
}
/*
  These three functions will be used to help demonstrate how PIDs work
*/
// max return value = pi * .025
double proportional(double current, double desired){
	double error = desired - current;
	// Ensure the error is between -pi : +pi
	while(error < -c_PI) error += 2 * c_PI;
	while(error >  c_PI) error -= 2 * c_PI;
	
	return testp_CONT * error;
}

double integral(double current, double desired){
	static float i_comp = 0;
	i_comp += testi_CONT * (desired - current);
	return i_comp;
}

double derivative(double current, double desired){
	static double error_last = 0.0;
	double error = desired - current;
	double ret = testd_CONT * (error - error_last) / SAMPLE_PERIOD;
	error_last = error;
	return ret;
}

/* PID Test main
#include <stdio.h>
#include <unistd.h>

int main(){
 float initial = .503;
 float desired = c_PI;
 float current = initial;

 while(1){
 	printf("Current is %.02f Desired is %.02f\n", current, desired);
   current += euler_PID(current, desired);
 	usleep(100000);
 }
} 
*/
