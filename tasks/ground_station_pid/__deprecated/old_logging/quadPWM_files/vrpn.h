#ifndef VRPN_H
#define VRPN_H

#include <string>
#include <unistd.h>
#include "vrpn_Connection.h"
#include "vrpn_Tracker.h"
#include "structs.h"

void vrpnInit(std::string connectionName, void (*)(void*, const vrpn_TRACKERCB));

void vrpnGo();

#endif
