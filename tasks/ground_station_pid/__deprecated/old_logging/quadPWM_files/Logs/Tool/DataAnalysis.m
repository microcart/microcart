%% Initial options

fname = 'sampleLogFileWunits.txt';      % set file name here if it is in the current directory

fpath = '';                             % leave this blank (initialization)
if(isempty(fname))
    [fname, fpath] = uigetfile('.txt','Select log file'); % otherwise, choose through explorer window
end
params.file.name = fname;
params.file.path = [fpath fname];

params.plotting.plot = 1;               % switch to choose plotting
params.plotting.separatePlot = 1;           % to generate separate plots
params.plotting.multiPlot = 1;              % to super impose multiple quantities on the same plot
params.plotting.subPlot = 1;           % to generate sub plots for multiple quantities
params.plotting.separateData = {'Pitch','Roll', 'Motor_2'};     % column numbers to plot on separate figures
                                                           % if empty & plot = 1, all the columns will be plotted
                                                
params.plotting.multiData = {'Pitch', 'Roll', 'Motor_1'};         % if empty but switch is on, use separateData values

params.plotting.subData = {};      % if empty but switch is on, use multiData values

params.plotting.color = 'r'; % one character for color of the plotting line
params.plotting.marker = ''; % one character for the maker of the plotting line
params.plotting.style = ':'; % one character for the style of the plotting line
params.plotting.backgnd = [1 1 1]; % rgb array for background color of the plot

save params params;

%% parsing the log file specified

% parse the log and instantiate the data structure, expData
expData = parse_log(params.file.path, params);

save expData expData;

%% plotting routines

% plot the data accoriding to the plotting parameters set
plot_data(expData, params.plotting);

%% storing the main structure