#!/bin/bash
#
#Author: Matt Vitale
#
#This script is made to ease the pid testing process
#also makes the testing avalable to those who don't know the commands
#
#If things seem wonky, make sure the zybo board is on, and the bluetooth pmod is connected
#


#Established connection between the base station's "dongle" and the bluetooth PMOD on the quad
sudo ./connect_zybo_bt.sh &

#Stores the pid of "sudo ./connect_zybo_bt.sh" into "connection_pid" variable
#MUST be the line right after the connection command starts.  Else "$!" will change
connection_pid=$!


#Give some time for the bluetooth to connect, should see some debug messages
sleep 7


#DATE LOGGING
#Check if a directory of the current date exists under the 'logs' directory.
#If not, create the directory. Then write log file under that directory

#stores the current date, month day  year
#current_day=`date +'%m:%d:%Y'`
#stores the current time, minutes then hours
#current_time=`date +'%M:%H'`

#if [ ! -d "logs/$current_day" ]; then
	#Directory does not exist, create it.
#	mkdir logs/$current_day
#fi


#Create the file that will be the current log file
#echo '' > logs/$current_day/$current_time.log

#The following command creates logs.  This should run in the background
#until logging is no longer needed, likely after screen is terminated.
#./quadPWM > logs/$current_day/$current_time.log &
#./quadPWM &

#Stores the pid of "./quadPWM > logs/testLog.log &" into "logging_pid"
#MUST be the line right after the logging command starts.  Else "$!" will change.
logging_pid=$!


#This is how commands are sent to the quad.  Typing into the terminal window when screen is up
#should send the commands to the quad.
#To exit from screen:   'ctrl-a'  followed by 'ctrl-d'
sudo screen -L /dev/rfcomm0 921600
#sudo screen -L /dev/rfcomm0 115200
#cu -l /dev/rfcomm0 -s 115200



#this section will only be reached if the user exits screen
#Releases the connection of the zybo and the logging
#Note: Unix: SIGINT = "ctrl-c"
sudo kill -SIGINT $connection_pid
sleep 2

kill $logging_pid
echo
sleep 2

#A horrible way to nicely disconnect bluetooth.  Open to better suggestions.
2>/dev/null 1>&2 sudo ./connect_zybo_bt.sh &
sleep 2

mv screenlog.0 QuadLogs/log-`date +"%F_%T"`.txt

#Run Matlab to graph the logfile
#Currently, everything will automate until Tyler's Matlab script requests a logfile
#Can be made into a function by adding a function header declaration and then arguments accordingly
#matlab -r "cd /local/ucart/Desktop/Microcart14/ground_station_pid/logging; GUI"




