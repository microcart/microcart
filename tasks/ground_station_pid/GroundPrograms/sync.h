#ifndef SYNC_H
#define SYNC_H

// the name of the semaphore
#define SEM_NAME "/uartWriteSem3"


/*
 * On the whole semaphore business:
 *		It's suspected there may be issues calling the write() system call
 *    from multiple threads to write to the same uart file descriptor for 
 *    the bluetooth. In the interest of correct concurrency and avoiding 
 *    race conditions, the capability is provided to use a semaphor as a mutex
 *    to keep only one thread calling write() at a time. However, testing has 
 *    shown this may not be necessary so it is disabled at this time. If you
 *    wish to enable it, change the UART_USE_SEM value to 1 in "sync.h".
 */
// whether to use a semaphore to synchronize calls to uartWrite
#define  UART_USE_SEM  0

#endif
