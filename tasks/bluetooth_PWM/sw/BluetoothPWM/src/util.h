/*
 * util.h
 *
 *  Created on: Oct 11, 2014
 *      Author: Tyler
 */

#define clock_rate          100000000
#define frequency           450
#define period_width        clock_rate/frequency
#define pulse_throttle_low  clock_rate / 1000
#define pulse_throttle_high clock_rate / 500
#define MOTOR_0_PERCENT     115000


#define XPAR_BTNS_BASEADDR 0x41200000

/**
 * Various Addresses of custom IP components
 */
#define PWM_0_ADDR     0x64e00000
#define PWM_1_ADDR     0x64e20000
#define PWM_2_ADDR     0x64e40000
#define PWM_3_ADDR     0x64e60000
#define PWM_REC_0_ADDR 0x76e00000
#define PWM_REC_1_ADDR 0x76e20000
#define PWM_REC_2_ADDR 0x76e40000
#define PWM_REC_3_ADDR 0x76e60000
#define PWM_REC_4_ADDR 0x76e80000

/**
 * Register offsets within the custom IP
 */
#define PWM_PERIOD     0
#define PWM_PULSE      4

void pwm_init();
void pwm_write_all(int pulseWidth);
void pwm_write_channel(int pulseWidth, int channel);

int read_rec(int channel);
void read_rec_all(int* mixer);

void read_bluetooth_all(int* mixer);

void b_drive_pulse();

void sine_example();

void print_mixer(int* mixer);
int read_kill(int kill);
void pwm_kill();
