/*
 * Copyright (c) 2009-2012 Xilinx, Inc.  All rights reserved.
 *
 * Xilinx, Inc.
 * XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS" AS A
 * COURTESY TO YOU.  BY PROVIDING THIS DESIGN, CODE, OR INFORMATION AS
 * ONE POSSIBLE   IMPLEMENTATION OF THIS FEATURE, APPLICATION OR
 * STANDARD, XILINX IS MAKING NO REPRESENTATION THAT THIS IMPLEMENTATION
 * IS FREE FROM ANY CLAIMS OF INFRINGEMENT, AND YOU ARE RESPONSIBLE
 * FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE FOR YOUR IMPLEMENTATION.
 * XILINX EXPRESSLY DISCLAIMS ANY WARRANTY WHATSOEVER WITH RESPECT TO
 * THE ADEQUACY OF THE IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO
 * ANY WARRANTIES OR REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE
 * FROM CLAIMS OF INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include <stdio.h>
#include "platform.h"
#include <sleep.h>
#include <xparameters.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "util.h"
#include "uart.h"
#include "controllers.h"

char *channel_types[4] = {"Throttle", "Roll    ", "Pitch   ", "Yaw     "};
/**
 * Testing definitions
 */
#define SINE_EX     0
#define BUTTON_TEST 0
#define BLUETOOTH_TEST 1


int main2() {

	init_platform();
	xil_printf("Hello\n");
	pwm_init();
	xil_printf("\nStarting\n");



	int mixer[5];
	int pwms[4];
	char buffer[1024];
	uart0_init(XPAR_PS7_UART_0_DEVICE_ID, 115200);
	uart0_clearFIFOs();

//	uart0_sendStr("$$$");
////	uart0_sendStr("R,1\r\n");
//	uart0_sendStr("C,001583027903\r\n");
//	char buf[4];
//	printf("HI\n");
//	while(1) {
//
//		uart0_recvBytes(buf, 1);
//		buf[1] = 0;
//		printf("text: %s\n", buf);
//	}
//		printf("\n\n");


	while (1) {


		// Either do the sine example or push buttons
		if (SINE_EX)
			sine_example();
		else if (BUTTON_TEST)
			b_drive_pulse();
		else if (BLUETOOTH_TEST){
			uart0_recvBytes(buffer, 11);
			buffer[11] = 0;
			printf("Data: %s\r\n", buffer);
			int values[4];
			char *pEnd;
			values[0] = strtol(buffer, &pEnd, 16);
			values[1] = strtol(pEnd, &pEnd, 16);
			values[2] = strtol(pEnd, &pEnd, 16);
			values[3] = strtol(pEnd, NULL, 16);

			printf("\tDec: %d %d %d %d\r\n", values[0], values[1], values[2], values[3]);

			int i;
			int pwmvalues[4];
			for(i=0; i < 4; i++) {
				// 100000 - 1ms, 200000 - 2ms
				int start = 100000;
				int end = 200000;
				pwmvalues[i] = (end - start) * values[i] / 255 + start;
			}

			printf("\tPWM: %7d %7d %7d %7d\r\n", pwmvalues[0], pwmvalues[1], pwmvalues[2], pwmvalues[3]);

			for (i = 0; i < 4; i++)
				pwm_write_channel(pwmvalues[i], i);
		} else {

			// Read in from the PWM recorders
			read_rec_all(mixer);
			//read_bluetooth_all(mixer);


			// Convert from the aero inputs to PWMS
			Aero_to_PWMS(pwms, mixer);

			// If the kill switch was hit, end the system
			if(read_kill(mixer[4])){
				pwm_kill();
				return 0;
			}

			int i;
			for (i = 0; i < 4; i++) {
				//if (i == 1 || i == 2) {
					pwm_write_channel(pwms[i], i);
				//} else {
				//	pwm_write_channel(0*pwms[i], i);
			//	}
			}


			for (i = 0; i < 4; i++) {
				xil_printf("Motor %d.  %6d", i,  pwms[i]);
				xil_printf("       Channel %s: %6d\r\n", channel_types[i], mixer[i]);

			}
			//xil_printf("\n\n");
			for(i=0;i<5;i++){
				xil_printf("####################");
			}
			xil_printf("\r\n");

			usleep(8e4);

			//xil_printf("\n\n\n\n\n\n\n\n\n\n");
			int* btns = XPAR_BTNS_BASEADDR;
			if (*btns & 0x8) {

				xil_printf("Kill channel %d\n", mixer[4]);
				//xil_printf("%d\n",pulseW);
				while (*btns & 0x8)
					;

			}
		}
	}
}
