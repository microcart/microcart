/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "D:/Revision1/pcores/pwm_signal_out_v1_00_a/hdl/vhdl/pwm_signal_out.vhd";
extern char *IEEE_P_2592010699;

unsigned char ieee_p_2592010699_sub_1690584930_503743352(char *, unsigned char );
unsigned char ieee_p_2592010699_sub_2545490612_503743352(char *, unsigned char , unsigned char );


static void pwm_signal_out_v1_00_a_a_0494838447_3306564128_p_0(char *t0)
{
    char *t1;
    char *t2;
    unsigned int t3;
    unsigned int t4;
    unsigned int t5;
    char *t6;
    char *t7;
    int t8;
    char *t9;
    char *t10;
    int t11;
    char *t12;
    char *t13;
    char *t14;
    char *t15;
    char *t16;
    char *t17;

LAB0:    xsi_set_current_line(370, ng0);
    t1 = (t0 + 5032U);
    t2 = *((char **)t1);
    t3 = (1 - 1);
    t4 = (t3 * 1U);
    t5 = (0 + t4);
    t1 = (t2 + t5);
    t6 = (t0 + 26590);
    t8 = xsi_mem_cmp(t6, t1, 2U);
    if (t8 == 1)
        goto LAB3;

LAB6:    t9 = (t0 + 26592);
    t11 = xsi_mem_cmp(t9, t1, 2U);
    if (t11 == 1)
        goto LAB4;

LAB7:
LAB5:    xsi_set_current_line(373, ng0);
    t1 = xsi_get_transient_memory(32U);
    memset(t1, 0, 32U);
    t2 = t1;
    memset(t2, (unsigned char)2, 32U);
    t6 = (t0 + 15064);
    t7 = (t6 + 56U);
    t9 = *((char **)t7);
    t10 = (t9 + 56U);
    t12 = *((char **)t10);
    memcpy(t12, t1, 32U);
    xsi_driver_first_trans_fast(t6);

LAB2:    t1 = (t0 + 14872);
    *((int *)t1) = 1;

LAB1:    return;
LAB3:    xsi_set_current_line(371, ng0);
    t12 = (t0 + 7432U);
    t13 = *((char **)t12);
    t12 = (t0 + 15064);
    t14 = (t12 + 56U);
    t15 = *((char **)t14);
    t16 = (t15 + 56U);
    t17 = *((char **)t16);
    memcpy(t17, t13, 32U);
    xsi_driver_first_trans_fast(t12);
    goto LAB2;

LAB4:    xsi_set_current_line(372, ng0);
    t1 = xsi_get_transient_memory(32U);
    memset(t1, 0, 32U);
    t2 = t1;
    memset(t2, (unsigned char)2, 32U);
    t6 = (t0 + 15064);
    t7 = (t6 + 56U);
    t9 = *((char **)t7);
    t10 = (t9 + 56U);
    t12 = *((char **)t10);
    memcpy(t12, t1, 32U);
    xsi_driver_first_trans_fast(t6);
    goto LAB2;

LAB8:;
}

static void pwm_signal_out_v1_00_a_a_0494838447_3306564128_p_1(char *t0)
{
    char *t1;
    char *t2;
    unsigned char t3;
    char *t4;
    unsigned char t5;
    unsigned char t6;
    char *t7;
    char *t8;
    char *t9;
    char *t10;
    char *t11;

LAB0:    xsi_set_current_line(378, ng0);

LAB3:    t1 = (t0 + 7752U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t1 = (t0 + 6632U);
    t4 = *((char **)t1);
    t5 = *((unsigned char *)t4);
    t6 = ieee_p_2592010699_sub_2545490612_503743352(IEEE_P_2592010699, t3, t5);
    t1 = (t0 + 15128);
    t7 = (t1 + 56U);
    t8 = *((char **)t7);
    t9 = (t8 + 56U);
    t10 = *((char **)t9);
    *((unsigned char *)t10) = t6;
    xsi_driver_first_trans_fast(t1);

LAB2:    t11 = (t0 + 14888);
    *((int *)t11) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void pwm_signal_out_v1_00_a_a_0494838447_3306564128_p_2(char *t0)
{
    char *t1;
    char *t2;
    unsigned char t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;

LAB0:    xsi_set_current_line(379, ng0);

LAB3:    t1 = (t0 + 7592U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t1 = (t0 + 15192);
    t4 = (t1 + 56U);
    t5 = *((char **)t4);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = t3;
    xsi_driver_first_trans_fast(t1);

LAB2:    t8 = (t0 + 14904);
    *((int *)t8) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void pwm_signal_out_v1_00_a_a_0494838447_3306564128_p_3(char *t0)
{
    char *t1;
    char *t2;
    unsigned char t3;
    char *t4;
    unsigned char t5;
    unsigned char t6;
    char *t7;
    char *t8;
    char *t9;
    char *t10;
    char *t11;

LAB0:    xsi_set_current_line(380, ng0);

LAB3:    t1 = (t0 + 7912U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t1 = (t0 + 6792U);
    t4 = *((char **)t1);
    t5 = *((unsigned char *)t4);
    t6 = ieee_p_2592010699_sub_2545490612_503743352(IEEE_P_2592010699, t3, t5);
    t1 = (t0 + 15256);
    t7 = (t1 + 56U);
    t8 = *((char **)t7);
    t9 = (t8 + 56U);
    t10 = *((char **)t9);
    *((unsigned char *)t10) = t6;
    xsi_driver_first_trans_fast(t1);

LAB2:    t11 = (t0 + 14920);
    *((int *)t11) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void pwm_signal_out_v1_00_a_a_0494838447_3306564128_p_4(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    int t4;
    int t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    char *t9;
    char *t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;

LAB0:    xsi_set_current_line(382, ng0);

LAB3:    t1 = (t0 + 5192U);
    t2 = *((char **)t1);
    t1 = (t0 + 10728U);
    t3 = *((char **)t1);
    t4 = *((int *)t3);
    t5 = (t4 - 1);
    t6 = (2 - t5);
    t7 = (t6 * 1U);
    t8 = (0 + t7);
    t1 = (t2 + t8);
    t9 = (t0 + 15320);
    t10 = (t9 + 56U);
    t11 = *((char **)t10);
    t12 = (t11 + 56U);
    t13 = *((char **)t12);
    memcpy(t13, t1, 2U);
    xsi_driver_first_trans_fast(t9);

LAB2:    t14 = (t0 + 14936);
    *((int *)t14) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void pwm_signal_out_v1_00_a_a_0494838447_3306564128_p_5(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    int t4;
    int t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    char *t9;
    char *t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;

LAB0:    xsi_set_current_line(383, ng0);

LAB3:    t1 = (t0 + 5352U);
    t2 = *((char **)t1);
    t1 = (t0 + 10728U);
    t3 = *((char **)t1);
    t4 = *((int *)t3);
    t5 = (t4 - 1);
    t6 = (2 - t5);
    t7 = (t6 * 1U);
    t8 = (0 + t7);
    t1 = (t2 + t8);
    t9 = (t0 + 15384);
    t10 = (t9 + 56U);
    t11 = *((char **)t10);
    t12 = (t11 + 56U);
    t13 = *((char **)t12);
    memcpy(t13, t1, 2U);
    xsi_driver_first_trans_fast(t9);

LAB2:    t14 = (t0 + 14952);
    *((int *)t14) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void pwm_signal_out_v1_00_a_a_0494838447_3306564128_p_6(char *t0)
{
    char *t1;
    char *t2;
    unsigned char t3;
    unsigned char t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    char *t9;

LAB0:    xsi_set_current_line(385, ng0);

LAB3:    t1 = (t0 + 4392U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t4 = ieee_p_2592010699_sub_1690584930_503743352(IEEE_P_2592010699, t3);
    t1 = (t0 + 15448);
    t5 = (t1 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = t4;
    xsi_driver_first_trans_fast(t1);

LAB2:    t9 = (t0 + 14968);
    *((int *)t9) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void pwm_signal_out_v1_00_a_a_0494838447_3306564128_p_7(char *t0)
{
    char *t1;
    char *t2;
    unsigned char t3;
    unsigned char t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    char *t9;

LAB0:    xsi_set_current_line(386, ng0);

LAB3:    t1 = (t0 + 6472U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t4 = ieee_p_2592010699_sub_1690584930_503743352(IEEE_P_2592010699, t3);
    t1 = (t0 + 15512);
    t5 = (t1 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = t4;
    xsi_driver_first_trans_fast(t1);

LAB2:    t9 = (t0 + 14984);
    *((int *)t9) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}


extern void pwm_signal_out_v1_00_a_a_0494838447_3306564128_init()
{
	static char *pe[] = {(void *)pwm_signal_out_v1_00_a_a_0494838447_3306564128_p_0,(void *)pwm_signal_out_v1_00_a_a_0494838447_3306564128_p_1,(void *)pwm_signal_out_v1_00_a_a_0494838447_3306564128_p_2,(void *)pwm_signal_out_v1_00_a_a_0494838447_3306564128_p_3,(void *)pwm_signal_out_v1_00_a_a_0494838447_3306564128_p_4,(void *)pwm_signal_out_v1_00_a_a_0494838447_3306564128_p_5,(void *)pwm_signal_out_v1_00_a_a_0494838447_3306564128_p_6,(void *)pwm_signal_out_v1_00_a_a_0494838447_3306564128_p_7};
	xsi_register_didat("pwm_signal_out_v1_00_a_a_0494838447_3306564128", "isim/pwm_signal_out_isim_beh.exe.sim/pwm_signal_out_v1_00_a/a_0494838447_3306564128.didat");
	xsi_register_executes(pe);
}
